# --- AMR default configuration file ---
#
# description and type fields are used by the GUI
# #sec  - section description
# #f    - fields description
# #type - fields data type (int, float, boolean, list, string) 
#
# --------------------------------------

#sec input data for AMR simualtion
[input]
#f device type (tokamak, stellarator)
#type list tokamak stellarator
device = 
#f name of the experiment
# (e.g., MAST, NSTX, WEGA, COMPASS etc.)
#type string
experiment = 
#f shot number
#type int
shot_number = 
#f start time of the simulation
#type float
start_time = 
#f end time of the simulation
#type float
end_time = 
#f use TS times between start_time and end_time
# 'ebe_times_XXXXXX.dat' is used when run_IDL=no or run_code=yes)
# and must be produced manually (from 'ts_times_XXXXXX.out')
# 'ts_times_XXXXXX.out' is used in the testing phase and is produced automatically when run_IDL is 'yes'
#type boolean
ts_times = 
#f equidistant time points between start_time and end_time when NOT USING ts_times
# if time_samples <= 1, only start_time is considered
#type int
time_samples = 1
#f run IDL programs to get input data (TS, EFIT ...)
# if set to no, all files must be provided prior to execution of the program
#type boolean
run_IDL = no


#sec selecting which output data are needed
[output]
#f use only the central ray in simulations
#type bool
central_ray_only = no
#f Output each ray's path
#type bool
ray_details = no
#f power deposition profiles
#type bool
power_deposition = yes
#f current density calculation
# none, simple - obsolete
# hansen - PPCF 27 (1985) 1077
# curba - not finished
# mcgregor - PPCF 50 (2008) 015003 - experimental
# travis - N. Maruschenko's momentum-conservative CD model
#type list none simple hansen curba mcgregor travis
current_density = none
#f Output conversion eff. for each freq/angle/time/ray
# If conv_eff = no and temperature = yes, temperature with conv_eff=100% is considered
#type bool
conv_eff = yes
#f Output radiative temperature for each freq/angle/time
#type bool
temperature = yes
#f Ouput plasma profiles
#type bool
test_profiles = yes
#f Run the prepared code
#type bool
run_code = yes
#f postprocess the data (rays->beam integration, process and create some output files, e.g., results_ray.dat, results_beam.dat)
#type bool
postprocess = yes
#f sum power deposition of individual antennas
#type bool
sum_antennas = yes
#f run LUKE from AMR
#type bool
run_luke = no

#sec priority and cluster setting
[cluster]
#f use cluster (local computer otherwise)
#type bool
use_cluster = no
#f use Parallel Python (local or cluster parallel run)
#type bool
use_pp = yes
#f list of pp servers (white-space separated)
#type string
pp_servers = 
#f beam ditribution between cluster jobs
# job = beam distributes jobs by inidividual beam = 41 rays, i.e. one job = 41 rays
# job = optimum distributes optimally, i.e. there will be ncpu jobs submitted to cluster
# optimum should be faster, but would be harder to get partial results if the code
# is interrupted (for whatever reason)
#type list optimum beam
job = optimum
#f cluster que to use
#type string
que = sque # sunfire, sque, kestrel
#f number of cluster cpu's to use
#type int
ncpu = 0
#f interval for checking for job completition [min]
# in minutes, can be floating point, recommended values > 1
#type float
check_int = 1
#f priority on the cluster
#type int
priority = 0
#f priority class of the python and all child processes (even if not run on a cluster)
# 0 = lowest, 1 = low, 2 = default
#type list 0 1 2
master_priority = 0

#sec general settings
[general]
#f how to determite the beam (ray) spots: LCFS or PCO (plasma cut-off)
# for PCO, be sure to have LSOL >~ 0.2. check WKB curves if O/X modes are reasonably present
#type list LCFS PCO
beam_spot_type = LCFS
#f Matlab .m files interpreter
#type list matlab octave
m_interpreter = matlab

#sec conversion efficiency ('adaptive') setting
[adaptive]
#f type ofconversion efficiency calculation
#type list adaptive preinh
method = adaptive
#f collision frequency for the full-wave adaptive solver
#type float
coll_freq = 0.0005
#f initial node density
#type int
n_init = 350
#f accuracy for the full-wave adaptive solver
#type float
accuracy = 0.001
#f full-wave calculation domain extension, S_RZmax = getxPsiMin()*Extension_xend_search
#type float
extension_xend_search = 1.0


#sec ray-tracing parameters
[ray_tracing]
#f Zeff - constant throughout the plasma
# influences only the e-i collision frequency
#type float
Zeff = 1.0
#f root of the electrostatic dispersion to use to start the EBW ray-tracing
# 1 - small root, makes a U-turn on EBW path (i.e., this is already electrostatic X-mode)
# 2 - large root, no U-turn
#type list 1 2
Nperp = 1
#f accuracy of the Runge-Kutta integration
#type float
accuracy = 1.e-5
#f power limit to stop the ray-tracing
#type float
power_limit = 1.e-3
#f initial ray-tracing time step
#type float
time_step = 1e-9
#f maximum ray-tracing time (used in case the ray is not damped)
#type float
maximum_time= 1e-5
#f check harmonic number and frequency in EBW init (particularly for WEGA)
# if freq > n*f_ce do not search for EBW roots
#type bool
rt_check_harmonic = no
#f if rt_check_harmonic and freq > n*f_ce do not search for EBW roots
#type int
rt_check_harmonic_n = 2
#f resonant damping model
#type list non-relativistic weakly-relativistic fully-relativistic saveliev weakly-relativistic-nocurv
resonant_damping_model = weakly-relativistic
#f collisional damping model
#type list Ginzburg-Ruchadze cold none
collisional_damping_model = Ginzburg-Ruchadze
#f Accuracy of Saveliev's formula (series convergence criterion)
#type float
saveliev_accuracy = 1e-5
#f Maximum elements (Lambda_m) in Saveliev's dispersion
#type int
saveliev_mmax = 50000
#f Lower limit of weakly-relativistic Im(D) to be recalculated by Saveliev (if used)
#type float
saveliev_limit = 1e-5
#f Upper limit on the weakly-relativistic p_n to calculate the fully-relativistic damping (when chosen)
#type float
pn_FR_limit = 5
#f Ray-tracing debug outputs
#type bool
debug = no
#f Limit for asymptotic formula in non-relativistic D
#type float
W_asympt = 0.02

#sec LUKE parameters
[luke]
#f create LUKE outputs (turned on automatically if run_luke = yes)
#type bool
outputs = no
#f minimum ray fragment length [m]
#type float
dsmin = 0
#f Wave damping model
# 0: cold
# 1: kinetic
# 2: R2D2 (requires IMSL)
# 4: electrostatic (fast)
#type list 0 1 2 4
kmode = 4
#f LUKE np parameter (dimension of psi grid)
#type int
np = 101
#f LUKE nt parameter (dimension of theta grid)
#type int
nt = 65 
#f equilibrium export type (R-Z or psi-theta)
# (psi-theta overrides settings in [profiles] section)
# (R-Z turns 'matlab' export on with nR,nZ specified in [profiles] section)
#type list R-Z psi-theta
equilibrium = R-Z
#f LUKE code parameters (case sensitive)
#type list EBW_RT_PNNEG EC_RT LH_RT NONUNIFORM10010020 UNIFORM10010020
id_dkeparam = EBW_RT_PNNEG


#sec test_profiles parameters
[test_profiles]
# R,Z bounding box on which to output plasma profiles
# 0 should adjust maximum values automatically
#type float
R_min = 0
#type float
R_max = 0
#type float
Z_min = 0
#type float
Z_max = 0
#f phi-coordinate of the test profiles plane (in degrees)
#type float
phi_test = 0
# phi interval if 3D output is created
#type float
phi_min = 0
#type float
phi_max = 0
# output grid dimensions
#type int
R_dim = 51
#type int
Z_dim = 51
#type int
phi_dim = 1
#f wkb_curve_ray - for which ray(s) produce wkb curve file
# 1 - central ray only, 0 - no wkb curve, 50 - all rays with wkbcurve but only for non zero shot_time_test
#type int
wkb_curve_ray = 1
# shot_time_test - 0 for all the shot times
#type float
shot_time_test = 0
# Output cyclotron harmonic Doppler broadennig with N_parallel (above/below the harmonic) during test_profiles
# 0 to disable
#type float
Nparal_above = 0
#type float
Nparal_below = 0


#sec plasma profiles parameters
[profiles]
#f equilibrium version (device dependent)
# NSTX - efit01, efit02
# stellarators - bc filename
# default will choose automatically if possible
#type string
equil_ver = default
#f equilibrium input format
#type list default idl mclib matlab eqdsk
input_type = default
#f equilibrium files input path (relative or absolute)
#type string
eq_input_path = input
#f dimension of psi grid for flux functions (cross section area, volume, B averages etc.)
#type int
psiSurfDim = 20
#f coefficient for dl in fluxTrace, dl = fluxTraceDLcoeff * 2Pi(R-R_axis)
#type float
fluxTraceDLcoeff = 0.05
# equilibrium splines order
#type int
equilSplineOrder = 5
# flux surface functions spline order
#type int
FluxFnSplineOrder = 3
#f export profiles to other formats
#type list none matlab
export = none
# exported profiles dimensions
#type int
export_nR = 201
#type int
export_nZ = 201
#type int
export_npsi = 101

# -- stellarators fields --
#f B_axis [T] - on axis magnetic field (can be math expression) at phi=phi0
#type float
B_axis = 
#f phi0 for B_axis in degrees
#type float
phi0 = 0
#f maximum normalized toroidal flux to resize mesh to (for stellarators) -old psi
#type float
s_max = 1
#f s (psi) of the LCFS (to shift the LCFS), original LCFS = 1
#type float
s_LCFS = 1
#f mclib spectrum truncation 
#type float
epsTrunc = 1.0e-8
#f mclib mesh accuracy
#type float
epsA = 1.0e-6
#f compute mesh, must be done once for each equil_ver, eps and s_max
#type bool
compute_mesh = no
# -- end stellaratos fields --

# experimental ne, Te data (TS for Thomson scattering) selector
# effective only when reading from experimental database (run_idl = yes)
# ts_data = raw or spline for NSTX
# ruby or ndyag for MAST
#type string
ts_data = default
# number of components (e.g. 2 for bulk + tail)
#type int
n_components = 1
#f spline type for ne, Te splines
# 1 - B-spline, 2 - Akima cubic spline (IMSL only), 3 - CSCON (IMSL only)
# 4 - cubic splines, 5 - CMLIB monotonicity preserving
#type list B-spline Akima CSCON cubic DPCHIM
spline_type = cubic
#f monotonize the density profile up to the critical density for the monotonize_freq frequency
#type bool
monotonize = no
#f monotonize_freq [GHz]
# 0 - use maximum simulated frequency
#type float
monotonize_freq = 0
#f error tolerance for experimental data (if error bars available)
#type float
errorToleranceDen = 1
#type float
errorToleranceTe = 1
# profiles averaging
# from the last datapoint along the data line backwards the specified distance
#type list none mean median constant
averageProfilesDen = none
#type list none mean median constant
averageProfilesTe = none
#type float
averageDistanceDen = 0.2
#type float
averageDistanceTe = 0.1
# profiles extension
# possibilities: none, exp, constant
#type list none exp constant
extendProfilesDen = none
#type list none exp constant
extendProfilesTe = none
#f fraction of last datapoint - LCFS datapoint distance to be replaced by the extension profile
#type float
extendDistanceDen = 0
#type float
extendDistanceTe = 0
#f relative density of data points to add with respect to the input data density
#type float
extDataPoints = 1.0
# scale length for exp profiles extension
#type float
LSOL_den = 0.1
#type float
LSOL_Te = 0.1
#f SOL width - distance from LCFS where the full-wave EBW-X-O module and the EBW root search module start
#type float
LSOL = 0.0
#f enable/disable use of analytical profiles (i.e., no for experimental profiles)
#type bool
analytical_profiles = 

#f LCFS profiles scaling (reduce/increase LCFS temperature and/or density)
# normalized psi (rho^2) scaling starting point
#type float
LCFS_scale_break = 0.7225
#f scale factors (1 will cause no effect)
#type float
LCFS_scale_den_factor = 1
#type float
LCFS_scale_Te_factor = 1

#f Poloidal B scaling (aka Ip scaling)
# effectively psi = Bpol_scale * psi_original
#type float
Bpol_scale = 1
#f Toroidal B scaling
#type float
Btor_scale = 1
#f psi R shift
#type float
psi_R_shift = 0


#sec analytical profile parameters
[analytical_profiles]
#f analytical profile type (specified in the analytical_profiles module)
#type string
type = 
#f phi (in degrees) coordinate of the profiles
#type float
phi = 
#f number of grid points in psi grid for splines
#type int
nGrid = 50
#f coordinate on which the analytical profiles are specified
#possibilities: R, rho
#type list R rho 
variable = R
#f el. density parameters
# format [[p(1,1), p(1,2),...],[p(2,1),p(2,2),...],...]
# where p(c,n) in the n-th parameter for c-th component
# 1 component, 3 parameters example: [[1,0.2,3.3]]
# 2 components,2 parameters example: [[1,0.2],[2.2,4]]
#type string
den_parameters = 
#f el. density parameters
#type string
Te_parameters = 


#sec collisions with neutrals
[collisions]
#f number of non-elastic electron-ion collision processes considered (0 to disable)
#type int
n_sigma_ei_nonelastic = 0
#f number of all electron-neutral collision processes considered (0 to disable)
#type int
n_sigma_en = 0
#f neutral density [10^19 m-3]
#type float
neutral_concentration = 0
#f filename of sigma vs. Te profiles
#type string
datafile = 


#sec antenna
[antenna]
#f antenna configuration filename
#type string
cfg_file = 
#f which antennas to use - white space separated list of antenna id's (in square brackets in the antenna cfg file)
#type string
ids = 


#sec rays -> beam integration
[integration]
#f choose integration method
# full = perform full integration of the results over the waists (i.e. int(ray_func * ray_intensity * dS)
# power = use the already integrated ray power (i.e. sum(ray_func * ray_power)
#type list full power
type = power
#f enable/disable integration of functions of R
#type bool
integrate_R = no
#f R-limits for the integration
#type float
R_min = 
#type float
R_max = 
#f enable/disable integration of functions of a flux surface coordinate (r_eff, rho or psi)
#type bool
integrate_flux = yes
#f flux surface coordinate (r_eff, rho or psi)
#type list r_eff rho psi
flux_coordinate = rho
#f flux-coordinate limits for the integration
#type float
flux_min = 0
#type float
flux_max = 1
#f dimension of the R/r_eff/rho/psi arrays (number of bins for the integrated profiles)
#type int
tabdim = 20


# figures configuration (requires Matplotlib)
[figures]
#f enable/disable figure creation
# figures require NumPy + Matplotlib
#type bool
create = no
#f plot LUKE results
#type bool
luke_results = no
#f output figure format (png, jpg, eps, pdf ...)
#type string
ext = png
#f use latex for typesetting
#type bool
use_latex = no
#f plot B&W
#type bool
plot_bw = no
#f plot experimental (input) data in profile plots
#type bool
experimental_data = yes
#f plot error bars for experimental profiles
#type bool
error_bars = yes
#f generate test profiles plots 
#type bool
test_profiles = yes
#f rho value on LCFS
#type float
rho_LCFS = 1
#f conversion efficiency angular dependence for individual rays
#type bool
conv_eff_angular_ray = no
#f ray tracing trajectories for all rays (one plot per beam)
#type bool
ray_tracing_details = yes
#f ray tracing plots for individual rays
#type bool
ray_tracing_details_individual = no
#f power deposition markers in rt plots (can be empty)
#type string
rt_power_dep_markers = 0.1 0.3 0.5 0.7 0.9  
#f plot waists in ray-tracing figures
#type bool
plot_waists = no
#f (integrated) power deposition plots
#type bool
power_deposition = yes
#f power deposition frequency spectra (will not work for multiple angles)
#type bool
power_deposition_spectra = no
#f power deposition for individual rays
#type bool
power_deposition_individual = no
#f power deposition for individual components
#type bool
power_deposition_comp = no
#f WKB Nx solution in the 1D slab
# can be none, central, all
#type list none central all
wkb_curves = central
#f EBE Trad frequency spectrum for each time (doesn't work with multiple angles)
#type bool
ebe_Trad_time = no
