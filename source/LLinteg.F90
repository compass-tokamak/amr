!***************************************************************        
!                        MODULE curdrv                         *        
!***************************************************************        
!                                                              *        
!     'curdrv' contains the SUBROUTINES 'jdrive' and 'JDE',    *        
!     and the FUNCTIONS 'dJ','Pd','dH','dF','dfc'.             *        
!                                                              *        
!     The module provides rapid estimates of current drive     *        
!     efficiency. The calculation is valid for arbitrary       *        
!     larmor radius, and includes particle trapping. Both      *        
!     dimensionless current drive efficiency and current       *        
!     drive efficiency in A/W are returned.                    *        
!                                                              *        
!     'jdrive' collects data from the main program needed for  *        
!     the calculation, performs some elementary calculations,  *        
!     calls the proceures for setting up a spline of           *        
!     <sin(theta)>, and calculates the trapped particle        *        
!     fraction fc, before it calls 'jde'.                      *        
!                                                              *        
!     'jde' partitions the interval of values of gamma, then   *        
!     calls the integration routines required to calculate     *        
!     the dimensionless current drive efficiency.              *        
!                                                              *        
!     'dJ', 'Pd', are integrands, integrated over ga.          *        
!     to give current driven and power absorbed.               *        
!                                                              *        
!     'dH', and 'dF' are integrands, integrated over lambda    *        
!     as part of calculating 'dJ'.                             *        
!                                                              *        
!     'dfc' is an integrand, integrated over lambda to give    *        
!     the trapped particle fraction.                           *        
!                                                              *        
!***************************************************************        

module LLinteg

use equilibrium

implicit none

real(8),private :: dFx_u

contains
                                                                        
      FUNCTION dJ(ga) 
!     ===============                                                   
!     dJ is the integrand in current drive calculation.                 
!     Parameter block: par1 = (/n,w,npar,Re(nperp),Im(nperp),fc/)       
                                                                        
      ! use LLinterp 
      use LLquadrat 
!                                                                       
      use LLglob 
!                                                                       
      double precision    upar,uperp,u,lin1,lin2,                       &
     &                     lin,fm,Diff,dJ,rho,Finc,                     &
     &                     H,F,dR,dFdu,fluxav,Hinc,                     &
     &                     dHdlam,lambda,dgad,                          &
     &                     uperp2,ga                                    
      double complex      z,coszz 
      double complex      jj(3) 
      integer             nz,ifail,in 
      integer             Hstsiz,Fstsiz 
      parameter           (HstSiz=10,FstSiz=10) 
                                                                        
      double precision    Hst(HstSiz), Fst(FstSiz) 
                                                                        
!                                                                       
!     Calculate components of resonant momentum at given ga             
!     =====================================================             
                                                                        
      upar   = c*(ga - r)/npar 
      uperp2 = ga**2-1.0d0-((ga-r)/npar)**2 
      uperp  = c*SqRt(ga**2-1.0d0-((ga-r)/npar)**2) 
      u      = SqRt(upar**2 + uperp**2) 
                                                                        
      IF (uperp2 .lt. 0.0d0)  PRINT*, "uperp2=", uperp2 
                                                                        
      lambda = Bmax/Blocal*(uperp/u)**2 
                                                                        
!     Calculate Maxwellian                                              
!     ====================                                              
                                                                        
      IF ((2.0d0*(c/ue)**2*(1.0d0-ga)).gt.-100.0d0 ) THEN 
         fm   = EXP(2.0d0*(c/ue)**2*(1.0d0-ga)) 
      ELSE 
         fm =0.0d0 
      ENDIF 
                                                                        
!  ********************************************************             
!  If fm is sufficiently small we have no need to continue.             
!  ********************************************************             
                                                                        
      IF (fm.gt.1.0d-40) THEN 
                                                                        
!        Evaluate functions fc(u), H(lambda), F(u) for given 'ga'       
!        ========================================================       
                                                                        
         H = LLHFun(psi,lambda)
                                                                        
         F      = 0.0d0 
         Fst(1) = 0.0d0 
         Finc   = 1.0d0/(FstSiz-1.0d0) 
                                                                        
         DO in=1,(FstSiz-1) 
            Fst(in+1) = Fst(in) + Finc 
         ENDDO

         DO in=1,(FstSiz-1) 
            Fst(in+1) = Fst(in) + Finc 
            CALL qromb(dF,Fst(in),Fst(in+1),F,tol,u,1) 
            ! alternative quadrature
            !dFx_u = u
            !F = F + integ2(dFx,Fst(in),Fst(in+1),tol)
         ENDDO
         F = F * (1+SqRt(1.0d0 + (u/c)*(u/c)))**((Zeff+1)/fc) / fc*(u/ue)**4
                                                                        
!        Evaluate derivatives dH/dlambda, dF/du for given 'ga'          
!        =====================================================          
                                                                        
         rho  = (Zeff+1.0d0)/fc 
                                                                        
         dgad = u / (ga*c**2) 
         dR   = - rho * dgad / (ga**2-1.0d0) 
         dFdu = dR*F + (u/ga)**3/(fc*ue**4) 
                                                                        
         IF (lambda .lt. 1.0d0) THEN 
            dHdlam = LLdHdlmbdaFun(psi,lambda)
         ELSE 
            dHdlam = 0.0d0 
         ENDIF 
                                                                        
!        Solution to Response Function                                  
!        =============================                                  
                                                                        
         lin1 = ga*ue**2/u * dFdu * H 
         lin2 = 2.0d0*Bmax/Blocal*upar*ue**2/                           &
     &          u**3*(ga*upar/u-npar*u/c)*F*dHdlam                      
         lin  = lin1 + lin2 
         IF (upar .lt. 0.0d0) lin = -lin 
!                                                                       
!        Quasilinear diffusion term                                     
!        ==========================                                     
                                                                        
         IF (iec .gt. 1) THEN 
           Ez = sqrt(1.0d0-Eplus**2-Eminus**2) 
            z = nperp/omcoverom*uperp*ga/c 
                                                                        
            IF (ABS(z) .gt. 1.0d6) THEN 
                Diff = 0.0d0 
            ELSEIF (ABS(z) .gt. 1.01d4) THEN 
                coszz = COS(z-(2*har-1)*Pi/4.0d0) 
                jj(1) = SqRt(Pi/(2.0d0*z))*coszz 
                jj(2) = SqRt(Pi/(2.0d0*z))*SqRt(1.0d0-coszz**2) 
                jj(3) = -jj(1) 
                Diff  = (uperp/c)**2*                                   &
     &                  (ABS( Eplus* jj(1) + Eminus* jj(3)              &
     &                  + upar/uperp*Ez*jj(2) ))**2 * ga                
            ELSE 
#ifdef _IMSL
               CALL DCBJS ((har-1)*1.0d0, Z, 3, jj)
#else
               CALL S17DEF((har-1)*1.0d0,z,3,"U",jj,nz,ifail) 
#endif
               Diff = (uperp/c)**2*                                     &
     &                (ABS( Eplus* jj(1) + Eminus* jj(3)                &
     &                + upar/uperp*Ez*jj(2) ))**2 * ga                  
            ENDIF 
                                                                        
         ELSE 
            Diff = uperp**(har*2) 
         ENDIF 
!                                                                       
!        Final current increment                                        
!        =======================                                        
!                                                                       
         dJ = Diff*fm*lin 
                                                                        
      ELSE 
                                                                        
         dJ = 0.0d0 
                                                                        
      ENDIF 
                                                                        
      END FUNCTION                                          
!                                                                       
!     *********************************************************         
!                                                                       
      FUNCTION Pd(ga) 
!     ===============                                                   
!     The integrand from which total absorbed power is calculated.      
!                                                                       
      use LLglob 
!                                                                       
      double precision  u,upar,uperp,Pd,fm,Diff,ga,imz 
      double complex    z, coszz 
      double complex    Besj(3) 
      integer           nz, ifail 
                                                                        
!                                                                       
!     Create machine variables                                          
!     ========================                                          
                                                                        
      upar  = c*(ga - r)/npar 
      uperp = c*SqRt(ga**2-1.0d0-((ga-r)/npar)**2) 
      u     = upar**2 + uperp**2 
                                                                        
!      IF (npar.gt.1) PRINT*, "u=", u                                   
                                                                        
!     Quasilinear diffusion term                                        
!     ==========================                                        
                                                                        
      IF ((2.0d0*(c/ue)**2*(1.0d0-ga)).gt.-100.0d0 ) THEN 
         fm   = EXP(2.0d0*(c/ue)**2*(1.0d0-ga)) 
      ELSE 
         fm =0.0d0 
      ENDIF 
                                                                        
!      IF ((npar.gt.1) .and. (fm .gt. 0.0d0)) PRINT*, "fm=", fm         
                                                                        
      IF (fm.gt.1.0d-40) THEN 
                                                                        
         IF (iec .gt. 1) THEN 
            Ez = sqrt(1.0d0-Eplus**2-Eminus**2) 
            z = nperp/omcoverom*uperp*ga/c 
                                                                        
            IF (ABS(z) .gt. 1.0d6) THEN 
                Diff = 0.0d0 
            ELSEIF (ABS(z) .gt. 1.01d4) THEN 
               PRINT*, "COS(ZZ) used" 
               coszz   = COS(z-(2*har-1)*Pi/4.0d0) 
               Besj(1) = SqRt(Pi/(2.0d0*z))*coszz 
               Besj(2) = SqRt(Pi/(2.0d0*z))*SqRt(1.0d0-coszz**2) 
               Besj(3) = -Besj(1) 
               Diff = (uperp/c)**2*                                     &
     &                (ABS( Eplus* Besj(1) + Eminus* Besj(3)            &
     &                + upar/uperp*Ez*Besj(2) ))**2 * ga                
            ELSE 
#ifdef _IMSL
               CALL DCBJS ((har-1)*1.0d0, Z, 3, Besj)
#else
               CALL S17DEF((har-1)*1.0d0, z, 3, "U",Besj,nz,ifail) 
#endif
               Diff = (uperp/c)**2*                                     &
     &                (ABS( Eplus* Besj(1) + Eminus* Besj(3)            &
     &                + upar/uperp*Ez*Besj(2) ))**2 * ga                
            ENDIF 
                                                                        
         ELSE 
            Diff = uperp**(har*2) 
         ENDIF 
                                                                        
         Pd = Diff*fm 
                                                                        
      ELSE 
                                                                        
         Pd = 0.0d0 
                                                                        
      ENDIF 
!                                                                       
      END FUNCTION                                         
!                                                                       
!     *********************************************************         
!                                                                       
      FUNCTION dF(x,u) 
!     ================                                                  
!     Integrand in Eqn (33) of Lin-Liu's paper.                         
                                                                        
!      use LLinterp 
!                                                                       
      use LLglob 
!                                                                       
      double precision   x,u,rho,rel,relx,fac,dif,dF 
!                                                                       
!      EXTERNAL          splint                                         
!                                                                       
      rho  = (Zeff+1.0d0)/fc 
      relx = SqRt(1.0d0 + (u*x/c)*(u*x/c)) 
      fac  = relx*relx*relx
      dif  = (1.0d0+relx)**rho 
!                                                                       
      dF = x**(rho+3.0d0)/fac/dif 
!                                                                       
      END FUNCTION                                          
!                                                                       
!     *********************************************************         
!                                                                       
      FUNCTION dFx(x) 
!     ================                                                  
!     Integrand in Eqn (33) of Lin-Liu's paper.                         
                                                                        
!      use LLinterp 
!                                                                       
      use LLglob 
!                                                                       
      double precision   x,rho,rel,relx,fac,dif,dFx 
!                                                                       
!      EXTERNAL          splint                                         
!                                                                       
      rho  = (Zeff+1.0d0)/fc 
      relx = SqRt(1.0d0 + (dFx_u*x/c)*(dFx_u*x/c)) 
      fac  = relx*relx*relx
      dif  = ((1.0d0+relx))**rho 
!                                                                       
      dFx = x**(rho+3.0d0)/fac/dif 
!                                                                       
      END FUNCTION                                          
!                                                                       
!     *********************************************************         
!                                                                       
end module
