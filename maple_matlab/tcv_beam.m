function [xW,yW,zW,rW,Nx,Ny,Nz] = tcv_beam(theta_l,phi_l,launcher_no)
% Calculates beam waist parameters from TCV launcher angles

% get TCV launcher parameters
%
% addpath('/home/goodman/psitbx_develop')
[xL,yL,zL,x_h,y_h,z_h,s34] = launch_point(theta_l,phi_l,launcher_no);
% xL, yL, zL, are the cartesian coordinate of the launch point
% i.e. the intersection of the beam center and the last mirror (M4) surface
% x_h, y_h, z_h are the components of the unit vector in the direction of the
% wave vector at the launch point
% s34 is the distance along the beam between the last focusing mirror (M3)
% and the last mirror (M4)
%
% Calculate antenna Gaussian beam waist position, radius, central ray wave vector
%
s3W = 0.2756;	% Distance from last focusing mirror (M3) to the beam waist
s4W = s3W - s34; % Distance from the last mirror (M4) to the beam waist
xW = xL+s4W.*x_h;	% x coordinate of the waist position
yW = yL+s4W.*y_h;	% y coordinate of the waist position
zW = zL+s4W.*z_h;	% z coordinate of the waist position

rW = 0.0188; % beam waist radius [m]
Nx = x_h; % central ray Nx
Ny = y_h; % central ray Ny
Nz = z_h; % central ray Nz

%
% Print to stdout - important, this is read by python!
for i=1:length(theta_l)
    fprintf('TCV_Beam: %.12g %.12g %.12g %.12g %.12g %.12g %.12g\n',xW(i),yW(i),zW(i),rW, ...
            Nx(i),Ny(i),Nz(i));
end
% and exit
exit
