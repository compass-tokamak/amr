pro ts_times_nstx

openr,2,'shot_no.in'
readf,2,shotn
shot=long(shotn)
close,2

if (shot ge 100000) then fname=string(shot,format='(''ts_times_'',I6,''.out'')') $
    else if (shot ge 10000) then fname=string(shot,format='(''ts_times_0'',I5,''.out'')') $
    else if (shot ge 1000) then fname=string(shot,format='(''ts_times_00'',I4,''.out'')') $
    else if (shot ge 100) then fname=string(shot,format='(''ts_times_000'',I3,''.out'')') $
    else if (shot ge 10) then fname=string(shot,format='(''ts_times_0000'',I2,''.out'')') $
    else fname=string(shot,format='(''ts_times_00000'',I1,''.out'')')
  openw,1,fname
  tsdata=ts(shot)
  for i=0,n_elements(tsdata.time)-1 do begin
      printf,1,tsdata.time[i]
  endfor

  close,1

end
