!> Function needed in F and H functions from McGregor current drive code.
!!
!! LLintegFun(R,Z) = \f$ \left( {1 - \lambda B/B_{\max } } \right)^{{1}/{2}} \f$.
!! lambda and Bmax must be set before by setLLintegFun
real(8) function LLintegFun(R,Z)
  real(8),intent(in) :: R,Z

  LLintegFun = sqrt(1-LLintegLambda*getBtot_RZ(R,Z)/LLintegBmax)
end function

real(8) function LLdHFun(l)
  real(8),intent(in) :: l

  ! set Bmax before
  ! set traces to current surface
  LLintegLambda = l
  LLdHFun = 1.d0/fluxAverage(LLdHRtrace,LLdHZtrace,LLdHLtrace,LLdHNTrace,LLintegFun)

end function

subroutine setLLintegLambda(lambda)
  real(8),intent(in) :: lambda

  LLintegLambda = lambda
end subroutine

subroutine setLLintegBmax(Bmax)
  real(8),intent(in) :: Bmax

  LLintegBmax = Bmax
end subroutine

!> H-function, (7) in McGregor's paper
real(8) function LLHFun(psi,l)
  real(8),intent(in) :: psi,l
#ifdef _IMSL
  real(8) :: DBS2VL
#else
#endif

  if (l>=1 .or. l<0 .or. psi>psi_LCFS) then
    LLHFun = 0
  else
    LLHFun = DBS2VL(psi,l,FluxSplineOrder,FluxSplineOrder, &
                    flux_spline_knots,LLlambda_spline_knots, &
                    Nflux_spline_knots,NLLlambda_spline_knots, &
                    LLHfun_spline_coef)
  endif
end function

!> dH/dlambda-function, (7) in McGregor's paper
real(8) function LLdHdlmbdaFun(psi,l)
  real(8),intent(in) :: psi,l
#ifdef _IMSL
  real(8) :: DBS2DR
#else
#endif

  if (l>1 .or. l<0 .or. psi>psi_LCFS) then
    LLdHdlmbdaFun = 0
  else
    LLdHdlmbdaFun = DBS2DR(0,1,psi,l,FluxSplineOrder,FluxSplineOrder, &
                    flux_spline_knots,LLlambda_spline_knots, &
                    Nflux_spline_knots,NLLlambda_spline_knots, &
                    LLHfun_spline_coef)
  endif
end function


!> H-function with fixed psi=LLHFun_lambda_psi, (7) in McGregor's paper
real(8) function LLHFun_lambda(l)
  real(8),intent(in) :: l

  LLHFun_lambda = LLHFun(LLHFun_lambda_psi,l)
end function

!> f_c function, (10) in McGregor's paper
real(8) function LLfcFun(psi)
  real(8),intent(in) :: psi
#ifdef _IMSL
    real(8) :: dbsval
#endif

  if (psi<=psi_LCFS) then
    LLfcFun = DBSVAL(psi,FluxSplineOrder,flux_spline_knots,Nflux_spline_knots,LLfcFun_spline_coef)
  else
    LLfcFun = 1
  endif
end function
