!> \file wkb.f90
!! WKB solver.

!> Solve the cold dispersion relation in the WKB
!! approximation and finds the refractive indices Nx for the fast and
!! slow waves at a given x, as well as the electric field polarization
!! for both wave types.
!!
!! Uses plasmaSlab::getSlabNy and plasmaSlab::getSlabNz to obtain the wave vector.
!! i=1 fast forward wave,
!! i=2 slow forward wave,
!! i=3 fast backward wave,
!! i=4 slow backward wave.
!! \param x x coordinate (plasmaSlab) where WKB solution is sought
!! \param nx indices of refraction
!! \param e_pol E(k,i) wave polarization. k = 1 and 2 correspond to Ey and Ez amplitudes, respectively; i is the wave type (see below).
  subroutine wkb(x,nx,e_pol)

  use PlasmaSlab

  implicit none
  integer :: i
  real(8) :: nyq,nzq,x,eps_nz,TWO,nyz,Re_C &
  ,Re_ntq1,Re_ntq2,Re_det
  parameter (eps_nz=1.d-5, TWO=2.d0)
  complex(8) nxi,nx(2),ntq(2),nxq(2),nxy,nxzq,ntqp,nxz
! complex(8) l,r,s,p,g,a,b,c,det,root
  complex(8) exx,exy,exz,eyy,eyz,ezz,a,b,c,det,root
  complex(8) e_pol(2,4),eex,eey,eez,ee,ONEC,IU,xx,yy,zz,yz,exyq,exzq
  parameter (ONEC=(1.d0,0.d0), IU=(0.d0,1.d0))
  common/Re_c/Re_c,Re_ntq1,Re_ntq2,Re_det
  real(8) :: Ny,Nz
! ----------------------------------------------------------------------
!  if(abs(nz0) < eps_nz) then
!    nz=eps_nz
!  else
!    nz=nz0
!  end if
! call loccom(xwkb,s,p,g)
  call loccom(x,exx,exy,exz,eyy,eyz,ezz)
! l=s+g
! r=s-g

  Ny = getSlabNy()
  Nz = getSlabNz()


  nzq=nz**2
  nyq=ny**2
  nyz=ny*nz
  exyq=exy*exy
  exzq=exz*exz
  xx=nyq+nzq-exx
  yy=nzq-eyy
  zz=nyq-ezz
  yz=nyz+eyz
! a=s
! b=(nzq-s)*(nzq+p)-(nzq-l)*(nzq-r)
! c=(nzq-l)*(nzq-r)*p
  a=exx
  b=-xx*(yy+zz)+2.0d0*nyz*yz-exzq-exyq+nzq*yy+nyq*zz
  c=-xx*yy*zz-2.0d0*yz*exy*exz-exyq*zz-exzq*yy+yz*yz*xx
  Re_C=c
  det=b**2-4.d0*a*c
  Re_det=det
  root=sqrt(det)
  ntq(1)=-(b+root)/(TWO*a)
  ntq(2)=-(b-root)/(TWO*a)
  Re_ntq1=ntq(1)
  Re_ntq2=ntq(2)
! nxq(1)=ntq(1)-nyq
! nxq(2)=ntq(2)-nyq
  nxq(1)=ntq(1)
  nxq(2)=ntq(2)
  nx(1)=sqrt(nxq(1))
  nx(2)=sqrt(nxq(2))
  if(dimag(nx(1)) < 0) nx(1)=-nx(1)
  if(dimag(nx(2)) < 0) nx(2)=-nx(2)
!  d111    format(7(1pe14.6))
!  d       write(89,*)'a,b,c'
!  d       write(89,111)a,b,c
!  d       write(89,*)'det,ntqf,ntqs'
!  d       write(89,111)det,ntq(1),ntq(2)
  do i=1,4
    if(i <= 2) then   ! i=1 - fast forward wave
      nxi=nx(i)    ! i=2 - slow forward wave
    ! nxzq=nxq(i)*nzq      ! i=3 - fast backward wave
    ! ntqp=ntq(i)-p        ! i=4 - slow backward wave
      nxzq=nxq(i)*nzq-exzq       ! i=3 - fast backward wave
      ntqp=ntq(i)+nyq-ezz        ! i=4 - slow backward wave
    else
      nxi=-nx(i-2)
    ! nxzq=nxq(i-2)*nzq
    ! ntqp=ntq(i-2)-p
      nxzq=nxq(i-2)*nzq-exzq
      ntqp=ntq(i-2)+nyq-ezz
    end if
  ! nxy=nxi*ny
    nxy=nxi*ny +exy
    nxz=nxi*nz
  ! eex=((iu*g+nxy)*ntqp+nxy*nzq)/((nyq+nzq-s)*ntqp-nxzq)
    eex=(nxy*ntqp+nxz*nyz+exz*eyz+exz*nyz+eyz*nxz)/(xx*ntqp-nxzq)
    eey=ONEC
  ! eez=nz/ntqp*(ny+nxi*eex)
    eez=(nyz+eyz+(nxz-exz)*eex)/ntqp
 !   d       write(89,'(a,I2,2(1PE14.6))')' i=',i,nxi
 !   d       write(89,*)'eex,eey,eez'
 !   d       write(89,111)eex,eey,eez
 !   d       write(89,*)'nyz,nxz,ntqp'
 !   d       write(89,111)nyz,nxz,ntqp
    ee=sqrt(eex**2+eey**2+eez**2)
    eex=eex/ee
    eey=eey/ee
    eez=eez/ee
  ! e(1,i)=eey
  ! e(2,i)=eez
    e_pol(1,i)=eey
    e_pol(2,i)=eez
!    d       write(89,'(a,2(1PE14.6))')' ee=',ee
!    d       write(89,*)'eex,eey,eez'
!    d       write(89,111)eex,eey,eez
  enddo
!  d       print *,' exx=',exx
!  d       print *,' ezz=',ezz
!  d       print *,' exy=',exy
!  d     print *,' a=',a
!  d     print *,' b=',b
!  d     print *,' c=',c
!  d     print *,' det=',det
!  d     print *,' ntqf=',ntq(1)
!  d     print *,' ntqs=',ntq(2)
!  d     print *,' nxqf=',nxq(1)
!  d     print *,' nxqs=',nxq(2)
!  d     print *,' nxf=',nx(1)
!  d     print *,' nxs=',nx(2)
  end subroutine wkb

!> Solve the cold dispersion relation in the WKB
!! approximation and find the electric field polarization
!! for both wave types.
!!
!! Uses plasmaSlab::getSlabNy and plasmaSlab::getSlabNz to obtain the wave vector.
!! The output is stored in common /inbound/P1,P2,Nx1,Nx2.
!! \param xwkb x coordinate where WKB solution is sought
  subroutine wkb_polarization(xwkb)
  use PlasmaSlab
  implicit none
  integer :: i
  real(8) :: nyq,nzq,xwkb,eps_nz,TWO,nyz,Re_C &
  ,Re_ntq1,Re_ntq2
  parameter (eps_nz=1.d-5, TWO=2.d0)
  complex(8) nxi,nx(2),ntq(2),nxq(2),nxy,nxzq,ntqp,nxz
  complex(8) exx,exy,exz,eyy,eyz,ezz,a,b,c,det,root
  complex(8) e_pol(2,4),eex,eey,eez,ee,ONEC,IU,Q,yy,zz,yz,exyq,exzq
  complex(8) P1,P2,Nx1,Nx2
  parameter (ONEC=(1.d0,0.d0), IU=(0.d0,1.d0))
  common /inbound/ P1,P2,Nx1,Nx2
  real(8) :: Ny,Nz
! ----------------------------------------------------------------------
!  if(abs(nz0) < eps_nz) then
!    nz=eps_nz
!  else
!    nz=nz0
!  end if
  call loccom(xwkb,exx,exy,exz,eyy,eyz,ezz)
  Ny = getSlabNy()
  Nz = getSlabNz()

  nzq=nz**2
  nyq=ny**2
  nyz=ny*nz
  exyq=exy*exy
  exzq=exz*exz
  Q=nyq+nzq-exx
  yy=nzq-eyy
  zz=nyq-ezz
  yz=nyz+eyz
  a=exx
  b=-Q*(yy+zz)+2.0d0*nyz*yz-exzq-exyq+nzq*yy+nyq*zz
  c=-Q*yy*zz-2.0d0*yz*exy*exz-exyq*zz-exzq*yy+yz*yz*Q
  det=b**2-4.d0*a*c
  root=sqrt(det)
  ntq(1)=-(b+root)/(TWO*a)
  ntq(2)=-(b-root)/(TWO*a)
  nxq(1)=ntq(1)
  nxq(2)=ntq(2)
  nx(1)=sqrt(nxq(1))
  nx(2)=sqrt(nxq(2))
  if(dimag(nx(1)) < 0) nx(1)=-nx(1)
  if(dimag(nx(2)) < 0) nx(2)=-nx(2)
  Nx1=nx(1)
  Nx2=nx(2)
  P1=(nyq*nxq(1)-exyq-Q*(nxq(1)+nzq-eyy))/ &
  (-ny*nz*nxq(1)-ny*Nx1*exz+exy*nz*Nx1+exy*exz-Q*(ny*nz+eyz))
  P2=(nyq*nxq(2)-exyq-Q*(nxq(2)+nzq-eyy))/ &
  (-ny*nz*nxq(2)-ny*Nx2*exz+exy*nz*Nx2+exy*exz-Q*(ny*nz+eyz))

  end subroutine
