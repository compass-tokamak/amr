# Microsoft Developer Studio Project File - Name="ebe_integration_nag" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=ebe_integration_nag - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ebe_integration_nag.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ebe_integration_nag.mak" CFG="ebe_integration_nag - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ebe_integration_nag - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "ebe_integration_nag - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
F90=df.exe
RSC=rc.exe

!IF  "$(CFG)" == "ebe_integration_nag - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE F90 /compile_only /nologo /warn:nofileopt
# ADD F90 /compile_only /define:"_MCLIB" /define:"_IMSLXXX" /define:"_IMSL" /fpp /imsl /nologo /warn:nofileopt
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x405 /d "NDEBUG"
# ADD RSC /l 0x405 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /pdb:"ebe_integration_nag.pdb" /machine:I386 /out:"ebe_integration_nag.exe"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "ebe_integration_nag - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE F90 /check:bounds /compile_only /dbglibs /debug:full /nologo /traceback /warn:argument_checking /warn:nofileopt
# ADD F90 /check:bounds /compile_only /dbglibs /debug:full /define:"_CERNLIB" /define:"_MCLIB" /fpp /nologo /traceback /warn:argument_checking /warn:nofileopt
# SUBTRACT F90 /imsl
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE RSC /l 0x405 /d "_DEBUG"
# ADD RSC /l 0x405 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib /nologo /subsystem:console /incremental:no /debug /machine:I386 /nodefaultlib:"libc.lib" /nodefaultlib:"libcmt.lib" /nodefaultlib:"msvcrt.lib" /nodefaultlib:"libcmtd.lib" /nodefaultlib:"msvcrtd.lib" /out:"ebe_integration.exe" /pdbtype:sept
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "ebe_integration_nag - Win32 Release"
# Name "ebe_integration_nag - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat;f90;for;f;fpp"
# Begin Source File

SOURCE=..\source\akima_surf.f
# End Source File
# Begin Source File

SOURCE=..\source\analytical_profiles.F90
DEP_F90_ANALY=\
	".\Debug\equilibrium.mod"\
	".\Debug\shot_parameters.mod"\
	".\Debug\utilities.mod"\
	".\Debug\vectors.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\bspline90_22.F90
# End Source File
# Begin Source File

SOURCE=..\source\cmlib.F90
# End Source File
# Begin Source File

SOURCE=..\source\collisionFrequency.F90
DEP_F90_COLLI=\
	".\Debug\bspline.mod"\
	".\Debug\generalConstants.mod"\
	".\Debug\utilities.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\constants.F90
DEP_F90_CONST=\
	".\Debug\generalConstants.mod"\
	".\Debug\shot_parameters.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\density_temperature.F90
DEP_F90_DENSI=\
	"..\source\density_temperature\LCFS_scale_profile.f90"\
	".\Debug\analytical_profiles.mod"\
	".\Debug\bspline.mod"\
	".\Debug\cmlib.mod"\
	".\Debug\equilibrium.mod"\
	".\Debug\fileNames.mod"\
	".\Debug\m_mrgref.mod"\
	".\Debug\shot_parameters.mod"\
	".\Debug\utilities.mod"\
	".\Debug\vectors.mod"\
	".\Debug\zeroin.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\disp_consts.F90
# End Source File
# Begin Source File

SOURCE=..\source\ebe_integration.F90
DEP_F90_EBE_I=\
	".\Debug\density_temperature.mod"\
	".\Debug\equilibrium.mod"\
	".\Debug\fileNames.mod"\
	".\Debug\shot_parameters.mod"\
	".\Debug\utilities.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\equilibrium.F90
DEP_F90_EQUIL=\
	"..\source\equilibrium\exportPsiThetaEquil.f90"\
	"..\source\equilibrium\fluxTrace.f90"\
	"..\source\equilibrium\LLintegFun.f90"\
	"..\source\equilibrium\LLintegVars.f90"\
	"..\source\equilibrium\Psi2DSpline.f90"\
	"..\source\equilibrium\readFluxData.f90"\
	".\Debug\bspline.mod"\
	".\Debug\LLquadrat.mod"\
	".\Debug\rkf45.mod"\
	".\Debug\utilities.mod"\
	".\Debug\vectors.mod"\
	".\Debug\zeroin.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\fileNames.F90
# End Source File
# Begin Source File

SOURCE=..\source\generalConstants.F90
# End Source File
# Begin Source File

SOURCE=..\source\i1mach.f
# End Source File
# Begin Source File

SOURCE=..\source\import_psi_d_t.F90
DEP_F90_IMPOR=\
	".\Debug\collisionFrequency.mod"\
	".\Debug\density_temperature.mod"\
	".\Debug\equilibrium.mod"\
	".\Debug\fileNames.mod"\
	".\Debug\shot_parameters.mod"\
	".\Debug\travis_config_int.mod"\
	".\Debug\travis_green_func_int.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\integrate_beams.F90
DEP_F90_INTEG=\
	".\Debug\bspline.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\iso_varying_string.F90
# End Source File
# Begin Source File

SOURCE=..\source\LLquadrat.F90
# End Source File
# Begin Source File

SOURCE=..\source\m_mrgref.F90
# End Source File
# Begin Source File

SOURCE=..\source\quanc8.F
# End Source File
# Begin Source File

SOURCE=..\source\rays.F90
DEP_F90_RAYS_=\
	".\Debug\density_temperature.mod"\
	".\Debug\equilibrium.mod"\
	".\Debug\generalConstants.mod"\
	".\Debug\utilities.mod"\
	".\Debug\vectors.mod"\
	".\Debug\zeroin.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\rkf45.F90
# End Source File
# Begin Source File

SOURCE=..\source\shot_parameters.F90
DEP_F90_SHOT_=\
	".\Debug\disp_consts.mod"\
	".\Debug\generalConstants.mod"\
	".\Debug\utilities.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\sort_pick.F90
# End Source File
# Begin Source File

SOURCE=..\source\travis_config_ext.F90
DEP_F90_TRAVI=\
	".\Debug\equilibrium.mod"\
	".\Debug\travis_const_and_precisions.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\travis_config_int.F90
DEP_F90_TRAVIS=\
	".\Debug\travis_config_ext.mod"\
	".\Debug\travis_const_and_precisions.mod"\
	".\Debug\travis_mymathlib.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\travis_const_and_precisions.F90
# End Source File
# Begin Source File

SOURCE=..\source\travis_green_func_ext.F90
DEP_F90_TRAVIS_=\
	".\Debug\travis_const_and_precisions.mod"\
	".\Debug\travis_green_func_int.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\travis_green_func_int.F90
DEP_F90_TRAVIS_G=\
	".\Debug\travis_config_int.mod"\
	".\Debug\travis_const_and_precisions.mod"\
	".\Debug\travis_mymathlib.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\travis_mymathlib.F90
DEP_F90_TRAVIS_M=\
	".\Debug\travis_const_and_precisions.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\tripack.f
# End Source File
# Begin Source File

SOURCE=..\source\utilities.F90
DEP_F90_UTILI=\
	".\Debug\generalConstants.mod"\
	".\Debug\m_mrgref.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\vectors.F90
DEP_F90_VECTO=\
	".\Debug\generalConstants.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\zeroin.F90
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;fi;fd"
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# Begin Source File

SOURCE=..\lib\mconf.lib
# End Source File
# Begin Source File

SOURCE=..\lib\AMRLIB_WIN_STATIC.lib
# End Source File
# Begin Source File

SOURCE=..\lib\BLAS_LIB.lib
# End Source File
# Begin Source File

SOURCE=..\lib\mathlib.lib
# End Source File
# Begin Source File

SOURCE=..\lib\kernlib.lib
# End Source File
# End Target
# End Project
