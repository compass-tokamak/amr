use PlasmaSlab

implicit none

complex(8) gauss_int_24_1f,gauss_int_24_2f,gauss_int_24_0fq,gauss_int_24_1fq,gauss_int_24_2fq

real(8) x,h
! real(8) ny,nz
complex(8) iu
! complex(8) exx(24),exy(24),exz(24),eyy(24),eyz(24),ezz(24),q(24)
! complex(8) q0,exy0,exz0
real(8) g0p(24),g0m(24),g1p(24),g1m(24),dg0p(24),dg0m(24),dg1p(24),dg1m(24)

! common /nynz/ ny,nz
! common /epsilon/ exx,exy,exz,eyy,eyz,ezz,q
! common /q0/ q0,exy0,exz0
common /fe/ g0p,g0m,g1p,g1m,dg0p,dg0m,dg1p,dg1m

parameter (iu=(0.0d0,1.0d0))

real(8) :: Ny,Nz
complex(8) :: Ey,Ez

Ny = getSlabNy()
Nz = getSlabNz()
Ey = getSlabEy()
Ez = getSlabEz()
