## @package ebeResult6
#  Python module with output handling classes.

## Class for handling ebe_run outputs.
class ebeResult:
    def __init__(self):
        self.iAntenna = -1
        self.iConfiguration = -1
        self.iRay = -1
        self.time = -1.0
        self.conv_eff1 = 0
        self.conv_eff2 = 0
        self.Trad = 0
        self.intensity = 0
        self.Npar = 0
        self.Npol = 0
        self.visible_area = 0
        self.transm_coef = 0
        self.x_waist = 0
        self.y_waist = 0
        self.rt_pos_R = 0
        self.rt_pos_phi = 0
        self.rt_pos_Z = 0
        self.rt_pos_rho = 0
        self.rt_coll_absp = 0

          # outType = 'ce','rt','wd'
          # checkCommon=True ==> test common values in putput files
    def importData(self,outType,dataLine,checkCommon=False):
        res = True
        # data line from ebe_run
        # 0       ,1             ,2   ,3        ,4   ,5        ,6        ,7
        # iAntenna,iConfiguration,freq,shot_time,iRay,conv_eff1,conv_eff2,Trad
        dataArray = dataLine.split()
        if not(checkCommon):
            self.iAntenna = int(float(dataArray[0]))
            self.iConfiguration = int(float(dataArray[1]))
            self.iRay = int(float(dataArray[4]))
            self.time = float(dataArray[3])
            self.frequency = float(dataArray[2])
        else:
            if (self.iAntenna != int(float(dataArray[0]))) | \
               (self.iConfiguration != int(float(dataArray[1]))) | \
               (self.iRay != int(float(dataArray[4]))) | \
               (self.time != float(dataArray[3])) | \
               (self.frequency != float(dataArray[2])) :
                res = False
        if outType=='ce':
            self.conv_eff1 = float(dataArray[5])
            self.conv_eff2 = float(dataArray[6])
        if outType=='rt':
            self.Trad = float(dataArray[5])
            self.rt_pos_R = float(dataArray[6])
            self.rt_pos_phi = float(dataArray[7])
            self.rt_pos_Z = float(dataArray[8])
            self.rt_pos_rho = float(dataArray[9])
            self.rt_coll_absp = float(dataArray[10])
        if outType=='wd':
            # intensity,Npar,Npol,visible_area,transm_coef,x_waist,y_waist
            self.intensity = float(dataArray[5])
            self.power = float(dataArray[6])
            self.Npar = float(dataArray[7])
            self.Npol = float(dataArray[8])
            self.visible_area = float(dataArray[9])
            self.transm_coef = float(dataArray[10])
            self.x_waist = float(dataArray[11])
            self.y_waist = float(dataArray[12])
        return res
## Class for handling ebe_integration profile (power deposition, current) outputs.
class intProfileResult:
    # r_eff[m],dPdr_total[kW/m],Pd_total[kW/m3],j_total[A/cm2],I_total[A],dPdr_N1[kW/m],dPdr_N2[kW/m]'', &
    #   &  '',dPdr_c1[kW/m],j_c1[A/cm2]'', &
    #   &  '',dPdr_c2[kW/m],j_c2[A/cm2]
    
    ## Default constructor.
    def __init__(self,n_components):
        self.n_components = n_components
        
    def setType(self,type):
        self.type = type.lower()
        if (self.type!='reff') & (self.type!='r'):
            print 'error: wrong intProfileResult type'
            sys.exit(1)

    ## Import data from a single output line
    def importData(self,dataLine,iAntenna,iConfiguration,time):
        self.dPdr_component = []
        self.j_component = []
        darray = map(float,dataLine.split())
# 0       ,1         ,2       ,3               ,4              ,5             ,6         ,7            ,8       
# r_eff[m],volume[m3],area[m2],dPdr_total[kW/m],Pd_total[kW/m3],j_total[A/cm2],I_total[A],dPdr_N1[kW/m],dPdr_N2[kW/m]'', &
# 9            ,10
# dPdr_c1[kW/m],j_c1[A/cm2]
# ... components
        if self.type=='reff':
            n_per_comp = 2
            n_common = 9
            self.volume  = darray[1]
            self.area    = darray[2]
            self.dPdr_total = darray[3]
            self.Pd_total= darray[4]
            self.j_total = darray[5]
            self.I_total = darray[6]
            self.dPdr_N1 = darray[7]
            self.dPdr_N2 = darray[8]
        elif self.type=='r':
            n_per_comp = 1
            n_common = 4
            self.dPdr_total = darray[1]
            self.dPdr_N1 = darray[2]
            self.dPdr_N2 = darray[3]
        self.iAntenna = iAntenna
        self.iConfiguration = iConfiguration
        self.time = time
        self.coordinate = darray[0]
        # no. of columns per one component
        for i in range(self.n_components):
            self.dPdr_component.append(darray[n_common+n_per_comp*i])
            if self.type=='reff':
                # print 'index = ' + str(n_common+1+n_per_comp*i) + '; len = ' + str(len(darray))
                self.j_component.append(darray[n_common+1+n_per_comp*i])
        
    ##Define  += operator
    def __iadd__(self, other):
        if self.iAntenna == -1:
            self = other
        elif self.type=='reff':
            self.dPdr_total += other.dPdr_total
            self.Pd_total   += other.Pd_total
            self.j_total    += other.j_total
            self.I_total    += other.I_total
            self.dPdr_N1    += other.dPdr_N1
            self.dPdr_N2    += other.dPdr_N2
            for i in range(len(self.dPdr_component)):
                self.dPdr_component[i] += other.dPdr_component[i]
                self.j_component[i]    += other.j_component[i]
        elif self.type=='r':
            self.dPdr_total += other.dPdr_total
            self.dPdr_N1    += other.dPdr_N1
            self.dPdr_N2    += other.dPdr_N2
            for i in range(len(self.dPdr_component)):
                self.dPdr_component[i] += other.dPdr_component[i]
        else:
            print 'error: types do not match in __iadd__'
            sys.exit(1)
        return self
    
    def writeln(self,fileHandle):
        if self.type=='reff':
            line = '%+.6e %+.6e %+.6e %+.6e %+.6e %+.6e %+.6e ' % \
            (self.coordinate,self.dPdr_total,self.Pd_total,self.j_total,self.I_total,self.dPdr_N1,self.dPdr_N2)
            for i in range(len(self.dPdr_component)):
                line+= '%+.6e %+.6e ' % (self.dPdr_component[i],self.j_component[i])
        elif self.type=='r':
            line = '%+.6e %+.6e %+.6e %+.6e ' % (self.coordinate,self.dPdr_total,self.dPdr_N1,self.dPdr_N2)
            for i in range(len(self.dPdr_component)):
                line+= '%+.6e ' % (self.dPdr_component[i])
        line+= '\n'
        fileHandle.write(line)

def getFileType(fname):
    if '_reff' in fname.lower():
        return 'reff'
    else:
        return 'r'
