ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine dimg(
     &     eqdsk, ierr
     &     , mw, mh, nbdry, limitr
     &     )
c GET DIMENSIONS OF ARRAYS IN GEQDSK SO WE CAN PASS THEM TO PYTHON
c
      parameter (npoint=500)
      character case_v*10
      dimension case_v(6)
      character eqdsk*256
      data neqdsk/30/
c
      do i=1,256
         if (ichar(eqdsk(i:i)) .eq. 0 ) then
            leqdsk=i-1
            goto 5433
         endif
      enddo
 5433 open(unit=neqdsk,file=eqdsk(1:leqdsk),status='OLD',
     .     form='FORMATTED',err=9999)
      read (neqdsk,2000,err=3000) (case_v(i),i=1,6),idum,mw,mh
      if (case_v(1)(3:7) .ne. 'EFITD') then
c         print *,eqdsk(1:leqdsk),' IS NOT A GEQDSK'
c         goto 9999
         print *, eqdsk(1:leqdsk),' WAS NOT GENERATED BY EFIT'
         print *, 'SOURCE: ', case_v(1)(3:7),
     &        ' - PROCEEDING WITH TREPIDATION'
      endif
      read (neqdsk,2020,err=3000) xdum,xdum,xdum,xdum,xdum
      read (neqdsk,2020,err=3000) xdum,xdum,xdum,xdum,xdum
      read (neqdsk,2020,err=3000) xdum,xdum,xdum,xdum,xdum
      read (neqdsk,2020,err=3000) xdum,xdum,xdum,xdum,xdum
      read (neqdsk,2020,err=3000) (xdum,i=1,mw)
      read (neqdsk,2020,err=3000) (xdum,i=1,mw)
      read (neqdsk,2020,err=3000) (xdum,i=1,mw)
      read (neqdsk,2020,err=3000) (xdum,i=1,mw)
      read (neqdsk,2020,err=3000) ((xdum,i=1,mw),j=1,mh)
      read (neqdsk,2020,err=3000) (xdum,i=1,mw)
      read (neqdsk,2022,err=3000)  nbdry,limitr
      if (nbdry.gt.npoint ) then
         print *,'NBDRY IN FILE ',nbdry,
     &         'EXCEEDS MAXIMUM DIMENSION ',npoint
         goto 9999
      endif
      goto 916
c----------------------------------------------------------------------
c--  input file binary format                                        --
c----------------------------------------------------------------------
 3000 continue
      close(unit=neqdsk)
      open(unit=neqdsk,file=eqdsk(1:leqdsk),status='OLD'
     &    ,form='UNFORMATTED',err=9999)
      read (neqdsk,err=9999) (case_v(i),i=1,6),idum,mw,mh
      if (case_v(1)(3:7) .ne. 'EFITD') then
         print *,eqdsk(1:leqdsk),' IS NOT A GEQDSK'
         goto 9999
      endif
      read (neqdsk,err=9999) xdum,xdum,xdum,xdum,xdum
      read (neqdsk,err=9999) xdum,xdum,xdum,xdum,xdum
      read (neqdsk,err=9999) xdum,xdum,xdum,xdum,xdum
      read (neqdsk,err=9999) xdum,xdum,xdum,xdum,xdum
      read (neqdsk,err=9999) (xdum,i=1,mw)
      read (neqdsk,err=9999) (xdum,i=1,mw)
      read (neqdsk,err=9999) (xdum,i=1,mw)
      read (neqdsk,err=9999) (xdum,i=1,mw)
      read (neqdsk,err=9999) ((xdum,i=1,mw),j=1,mh)
      read (neqdsk,err=9999) (xdum,i=1,mw)
      read (neqdsk,err=9999)  nbdry,limitr
      if (nbdry.gt.npoint ) then
         print *,'NBDRY IN FILE ',nbdry,
     &         'EXCEEDS MAXIMUM DIMENSION ',npoint
         goto 9999
      endif

 916  continue
      close(unit=neqdsk)
      ierr=0
      return
c
 9999 ierr=1
      close(unit=neqdsk)
      return
c
 2000 format (6a8,3i4)
 2020 format (5e16.9)
 2026 format (i5)
 2022 format (2i5)
 2024 format (i5,e16.9,i5)

	end subroutine dimg
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine readg(
     &  eqdsk,ierr
     & ,volp,pprime,pres,ffprim,fpol
     & ,qpsi,psirz,rgrid
     & ,pressw,pwprim,dmion
     & ,zgrid,rhovn,epoten
     & ,xlim,ylim
     & ,rbdry,zbdry
     & ,idat,rdat
     & ,nw,nh,nlimit
     & ,xEE2BDRY,xEEBDRY,xEEKNT
     & ,xFF2BDRY,xFFBDRY,xFFKNT
     & ,xKEE2BDRY,xKEEBDRY
     & ,xKFF2BDRY,xKFFBDRY
     & ,xKPP2BDRY,xKPPBDRY
     & ,xKWW2BDRY,xKWWBDRY
     & ,xPP2BDRY,xPPBDRY,xPPKNT
     & ,xWW2BDRY,xWWBDRY,xWWKNT
     & ,ibasis
     & )
c
      parameter (npoint=500,nercur=18,npcurn=18+18)
c
      dimension 
     &  volp(nw),pprime(nw),pres(nw),ffprim(nw),fpol(nw)
     & ,qpsi(nw),psirz(nw*nh),rgrid(nw)
     & ,pressw(nw),pwprim(nw),dmion(nw)
     & ,zgrid(nh),rhovn(nw),epoten(nw)
      dimension xlim(nlimit),ylim(nlimit)
      dimension rbdry(npoint),zbdry(npoint)
      character case_v*10
      dimension case_v(6)
      character eqdsk*256,vernum*8
      character uday*10,mfvers*5
      dimension mfvers(2)
      data neqdsk/30/
c LOCAL VARIABLES NEEDED TO READ NAMELIST
      dimension
     &  EE2BDRY(nercur),EEBDRY(nercur),EEKNT(nercur)
     & ,FF2BDRY(npcurn),FFBDRY(npcurn),FFKNT(npcurn)
     & ,KEE2BDRY(nercur),KEEBDRY(nercur)
     & ,KFF2BDRY(npcurn),KFFBDRY(npcurn)
     & ,KPP2BDRY(npcurn),KPPBDRY(npcurn)
     & ,KWW2BDRY(npcurn),KWWBDRY(npcurn)
     & ,PP2BDRY(npcurn),PPBDRY(npcurn),PPKNT(npcurn)
     & ,WW2BDRY(npcurn),WWBDRY(npcurn),WWKNT(npcurn)
c DUMMY VARIABLES PASSED IN FROM C (PYHON INTERFACE)
      dimension
     &  xEE2BDRY(nercur),xEEBDRY(nercur),xEEKNT(nercur)
     & ,xFF2BDRY(npcurn),xFFBDRY(npcurn),xFFKNT(npcurn)
     & ,xKEE2BDRY(nercur),xKEEBDRY(nercur)
     & ,xKFF2BDRY(npcurn),xKFFBDRY(npcurn)
     & ,xKPP2BDRY(npcurn),xKPPBDRY(npcurn)
     & ,xKWW2BDRY(npcurn),xKWWBDRY(npcurn)
     & ,xPP2BDRY(npcurn),xPPBDRY(npcurn),xPPKNT(npcurn)
     & ,xWW2BDRY(npcurn),xWWBDRY(npcurn),xWWKNT(npcurn)
c
      parameter(nidat = 16)
      dimension idatl(nidat),idat(nidat)
      equivalence 
     &  (idatl( 1),mw),     (idatl( 2),mh)
     & ,(idatl( 3),nbdry),  (idatl( 4),limitr)
     & ,(idatl( 5),ktor),   (idatl( 6),nmass)
     & ,(idatl( 7),keecur), (idatl( 8),ishot)
     & ,(idatl( 9),KEEFNC), (idatl(10),KEEKNT)
     & ,(idatl(11),KFFFNC), (idatl(12),KFFKNT)
     & ,(idatl(13),KPPFNC), (idatl(14),KPPKNT)
     & ,(idatl(15),KWWFNC), (idatl(16),KWWKNT)

      parameter(nrdat=16)
      dimension rdatl(nrdat),rdat(nrdat)
      equivalence 
     &  (rdatl( 1),xdim),   (rdatl( 2),zdim)
     & ,(rdatl( 3),rzero),  (rdatl( 4),zmid)
     & ,(rdatl( 5),rmaxis), (rdatl( 6),zmaxis)
     & ,(rdatl( 7),ssimag), (rdatl( 8),ssibry)
     & ,(rdatl( 9),bcentr), (rdatl(10),cpasma)
     & ,(rdatl(11),rvtor),  (rdatl(12),time)
     & ,(rdatl(13),EETENS), (rdatl(14),FFTENS) 
     & ,(rdatl(15),PPTENS), (rdatl(16),WWTENS)
c
      namelist /BASIS/
     &  EE2BDRY,EEBDRY,EEKNT,EETENS 
     & ,FF2BDRY,FFBDRY,FFKNT,FFTENS 
     & ,KEE2BDRY,KEEBDRY,KEEFNC,KEEKNT
     & ,KFF2BDRY,KFFBDRY,KFFFNC,KFFKNT 
     & ,KPP2BDRY,KPPBDRY,KPPFNC,KPPKNT 
     & ,KWW2BDRY,KWWBDRY,KWWFNC,KWWKNT 
     & ,PP2BDRY,PPBDRY,PPKNT,PPTENS
     & ,WW2BDRY,WWBDRY,WWKNT,WWTENS

c     Zero out extendable boundary array to avoid funny values going back to python
      do i=1,npoint
         rbdry(i)=0.0
         zbdry(i)=0.0
      enddo
c     Zero out optional data arrays to avoid funny values going back to python
      do i=1,nw
         pressw(i)=0.0
         pwprim(i)=0.0
         epoten(i)=0.0
         dmion(i)=0.0
      enddo

      do i=1,256
         if (ichar(eqdsk(i:i)) .eq. 0 ) then
            leqdsk=i-1
            goto 5433
         endif
      enddo
 5433 continue

      open(unit=neqdsk,file=eqdsk(1:leqdsk),status='OLD',
     .     form='FORMATTED',err=9999)
      read (neqdsk,2000,err=3000) (case_v(i),i=1,6),idum,mw,mh
      if (case_v(1)(3:7) .ne. 'EFITD') then
c         print *,eqdsk(1:leqdsk),' IS NOT A GEQDSK'
c         goto 9999
         print *, eqdsk(1:leqdsk),' WAS NOT GENERATED BY EFIT'
         print *, 'SOURCE: ', case_v(1)(3:7),
     &        ' - PROCEEDING WITH TREPIDATION'
      endif
      if (mw.gt.nw ) then
         print *,'MW IN FILE ',mw,'EXCEEDS MAXIMUM DIMENSION ',nw
         goto 9999
      endif
      if (mh.gt.nh ) then
         print *,'MH IN FILE ',mh,'EXCEEDS MAXIMUM DIMENSION ',nh
         goto 9999
      endif
      read (case_v(4),1070) ishot1
      read (case_v(4),1073) ishot2
      ishot = max(ishot1,ishot2)
      read (case_v(5),1080) itime
      time = float(itime)
      read (neqdsk,2020,err=3000) xdim,zdim,rzero,rgrid(1),zmid
      read (neqdsk,2020,err=3000) rmaxis,zmaxis,ssimag,ssibry,bcentr
      read (neqdsk,2020,err=3000) cpasma,ssimag,xdum,rmaxis,xdum
      read (neqdsk,2020,err=3000) zmaxis,xdum,ssibry,xdum,xdum
      read (neqdsk,2020,err=3000) (fpol(i),i=1,mw)
      read (neqdsk,2020,err=3000) (pres(i),i=1,mw)
      read (neqdsk,2020,err=3000) (ffprim(i),i=1,mw)
      read (neqdsk,2020,err=3000) (pprime(i),i=1,mw)
      read (neqdsk,2020,err=3000) ((psirz(i+(j-1)*mw),i=1,mw),j=1,mh)
      read (neqdsk,2020,err=3000) (qpsi(i),i=1,mw)
      read (neqdsk,2022,err=3000)  nbdry,limitr
      if (nbdry.gt.npoint ) then
         print *,'NBDRY IN FILE ',nbdry,
     &         'EXCEEDS MAXIMUM DIMENSION ',npoint
         goto 9999
      endif
      if (limitr.gt.nlimit ) then
         print *,'LIMITR IN FILE ',limitr,
     &          'EXCEEDS MAXIMUM DIMENSION ',nlimit
         goto 9999
      endif
      read (neqdsk,2020,err=3000) (rbdry(i),zbdry(i),i=1,nbdry)
      read (neqdsk,2020,err=3000) (xlim(i),ylim(i),i=1,limitr)
      read (neqdsk,2024,err=9166,end=9166) ktor,rvtor,nmass
      if (ktor.gt.0) then
         read (neqdsk,2020,err=3000) (pressw(i),i=1,mw)
         read (neqdsk,2020,err=3000) (pwprim(i),i=1,mw)
      endif
      if (nmass.gt.0) then
         read (neqdsk,2020,err=3000) ( dmion(i),i=1,mw)
      endif
      read (neqdsk,2020,err=9166,end=9166) (rhovn(i),i=1,mw)
      read (neqdsk,2026,err=9166,end=9166) keecur
      if (keecur.gt.0) then
         read (neqdsk,2020,err=3000) (epoten(i),i=1,mw)
      endif
c THE BASIS DATA IS ONLY AVAILABLE AS NAMELIST DATA IN FORMATTED OUTPUT
 9166 continue
      if (ibasis.eq.1) then
         read (neqdsk,BASIS,err=3002,end=3002)
         goto 3003
 3002    ibasis = 0
         goto 916
 3003    continue
         do k=1,nercur
            xEE2BDRY(k)  = EE2BDRY(k)
            xEEBDRY(k)   = EEBDRY(k)
            xEEKNT(k)    = EEKNT(k)
            xKEE2BDRY(k) = KEE2BDRY(k)
            xKEEBDRY(k)  = KEEBDRY(k)
         enddo
         do k=1,npcurn
            xFF2BDRY(k)  = FF2BDRY(k)
            xFFBDRY(k)   = FFBDRY(k)
            xFFKNT(k)    = FFKNT(k)
            xKFF2BDRY(k) = KFF2BDRY(k)
            xKFFBDRY(k)  = KFFBDRY(k)
            xKPP2BDRY(k) = KPP2BDRY(k)
            xKPPBDRY(k)  = KPPBDRY(k)
            xKWW2BDRY(k) = KWW2BDRY(k)
            xKWWBDRY(k)  = KWWBDRY(k)
            xPP2BDRY(k)  = PP2BDRY(k)
            xPPBDRY(k)   = PPBDRY(k)
            xPPKNT(k)    = PPKNT(k)
            xWW2BDRY(k)  = WW2BDRY(k)
            xWWBDRY(k)   = WWBDRY(k)
            xWWKNT(k)    = WWKNT(k)
         enddo
      endif
      goto 916
c----------------------------------------------------------------------
c--  input file binary format                                        --
c----------------------------------------------------------------------
 3000 continue
      close(unit=neqdsk)
      open(unit=neqdsk,file=eqdsk(1:leqdsk),status='OLD'
     &     ,form='UNFORMATTED',err=9999)
      read (neqdsk,err=9999) (case_v(i),i=1,6),idum,mw,mh
      if (case_v(1)(3:7) .ne. 'EFITD') then
         print *,eqdsk(1:leqdsk),' IS NOT A GEQDSK'
         goto 9999
      endif
      if (mw.gt.nw ) then
         print *,'MW IN FILE ',mw,'EXCEEDS MAXIMUM DIMENSION ',nw
         goto 9999
      endif
      if (mh.gt.nh ) then
         print *,'MH IN FILE ',mh,'EXCEEDS MAXIMUM DIMENSION ',nh
         goto 9999
      endif
      read (case_v(4),1070) ishot1
      read (case_v(4),1073) ishot2
      ishot = max(ishot1,ishot2)
      read (case_v(5),1080) itime
      time = float(itime)
      read (neqdsk,err=9999) xdim,zdim,rzero,rgrid(1),zmid
      read (neqdsk,err=9999) rmaxis,zmaxis,ssimag,ssibry,bcentr
      read (neqdsk,err=9999) cpasma,ssimag,xdum,rmaxis,xdum
      read (neqdsk,err=9999) zmaxis,xdum,ssibry,xdum,xdum
      read (neqdsk,err=9999) (fpol(i),i=1,mw)
      read (neqdsk,err=9999) (pres(i),i=1,mw)
      read (neqdsk,err=9999) (ffprim(i),i=1,mw)
      read (neqdsk,err=9999) (pprime(i),i=1,mw)
      read (neqdsk,err=9999) ((psirz(i+(j-1)*mw),i=1,mw),j=1,mh)
      read (neqdsk,err=9999) (qpsi(i),i=1,mw)
      read (neqdsk,err=9999) nbdry,limitr
      if (nbdry.gt.npoint ) then
         print *,'NBDRY IN FILE ',nbdry,
     &         'EXCEEDS MAXIMUM DIMENSION ',npoint
         goto 9999
      endif
      if (limitr.gt.nlimit ) then
         print *,'LIMITR IN FILE ',limitr,
     &          'EXCEEDS MAXIMUM DIMENSION ',nlimit
         goto 9999
      endif
      read (neqdsk,err=9999) (rbdry(i),zbdry(i),i=1,nbdry)
      read (neqdsk,err=9999) (xlim(i),ylim(i),i=1,limitr)
      read (neqdsk,err=916) ktor,rvtor,nmass
      if (ktor.gt.0) then
         read (neqdsk,err=9999) (pressw(i),i=1,mw)
         read (neqdsk,err=9999) (pwprim(i),i=1,mw)
      endif
      if (nmass.gt.0) then
         read (neqdsk,err=9999) ( dmion(i),i=1,mw)
      endif
      read (neqdsk,err=916) (rhovn(i),i=1,mw)
      read (neqdsk,err=916) keecur
      if (keecur.gt.0) then
         read (neqdsk,err=9999) (epoten(i),i=1,mw)
      endif

      ibasis=0

 916  continue
      close(unit=neqdsk)
      ierr=0
      do i=1,nidat
         idat(i)=idatl(i)
      enddo
      do i=1,nrdat
         rdat(i)=rdatl(i)
      enddo
      dx=xdim/float(mw-1)
      do i=1,mw
         rgrid(i) = rgrid(1) + float(i-1)*dx
      enddo
      dz=zdim/float(mh-1)
      z0=zmid-0.5*zdim
      do i=1,mh
         zgrid(i) = z0 + float(i-1)*dz
      enddo

      return
c
 9999 ierr=1
      close(unit=neqdsk)
      return
c
 
 1070 format (3x,i5)
 1073 format (2x,i6)
 1080 format (2x,i4)

 2000 format (6a8,3i4)
 2020 format (5e16.9)
 2026 format (i5)
 2022 format (2i5)
 2024 format (i5,e16.9,i5)

	end subroutine readg
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine writeg(
     &       eqdsk,ierr,imode
     &       ,volp,pprime,pres,ffprim,fpol
     &       ,qpsi,psirz,rgrid
     &       ,pressw,pwprim,dmion
     &       ,zgrid,rhovn,epoten
     &       ,xlim,ylim
     &       ,rbdry,zbdry
     &       ,idat,rdat
     &       ,xEE2BDRY,xEEBDRY,xEEKNT
     &       ,xFF2BDRY,xFFBDRY,xFFKNT
     &       ,xKEE2BDRY,xKEEBDRY
     &       ,xKFF2BDRY,xKFFBDRY
     &       ,xKPP2BDRY,xKPPBDRY
     &       ,xKWW2BDRY,xKWWBDRY
     &       ,xPP2BDRY,xPPBDRY,xPPKNT
     &       ,xWW2BDRY,xWWBDRY,xWWKNT
     &       ,ibasis
     &       )
c
      parameter (npoint=500,nercur=18,npcurn=18+18)
      parameter (nw=129,nh=129,nwnh=nw*nh)
      parameter (nlimit=300)
c
      dimension volp(nw),pprime(nw),pres(nw),ffprim(nw),fpol(nw)
     .     ,qpsi(nw),psirz(nwnh),rgrid(nw)
     .     ,pressw(nw),pwprim(nw),dmion(nw)
     .     ,zgrid(nh),rhovn(nw),epoten(nw)
      dimension xlim(nlimit),ylim(nlimit)
      dimension rbdry(npoint),zbdry(npoint)
      character case_v*10
      dimension case_v(6)
      character eqdsk*256,vernum*8
      character uday*10,mfvers*5
      dimension mfvers(2)
      data neqdsk/30/

c LOCAL VARIABLES NEEDED TO READ NAMELIST
      dimension
     &  EE2BDRY(nercur),EEBDRY(nercur),EEKNT(nercur)
     & ,FF2BDRY(npcurn),FFBDRY(npcurn),FFKNT(npcurn)
     & ,KEE2BDRY(nercur),KEEBDRY(nercur)
     & ,KFF2BDRY(npcurn),KFFBDRY(npcurn)
     & ,KPP2BDRY(npcurn),KPPBDRY(npcurn)
     & ,KWW2BDRY(npcurn),KWWBDRY(npcurn)
     & ,PP2BDRY(npcurn),PPBDRY(npcurn),PPKNT(npcurn)
     & ,WW2BDRY(npcurn),WWBDRY(npcurn),WWKNT(npcurn)
c DUMMY VARIABLES PASSED IN FROM C (PYHON INTERFACE)
      dimension
     &  xEE2BDRY(nercur),xEEBDRY(nercur),xEEKNT(nercur)
     & ,xFF2BDRY(npcurn),xFFBDRY(npcurn),xFFKNT(npcurn)
     & ,xKEE2BDRY(nercur),xKEEBDRY(nercur)
     & ,xKFF2BDRY(npcurn),xKFFBDRY(npcurn)
     & ,xKPP2BDRY(npcurn),xKPPBDRY(npcurn)
     & ,xKWW2BDRY(npcurn),xKWWBDRY(npcurn)
     & ,xPP2BDRY(npcurn),xPPBDRY(npcurn),xPPKNT(npcurn)
     & ,xWW2BDRY(npcurn),xWWBDRY(npcurn),xWWKNT(npcurn)

      parameter(nidat = 16)
      dimension idatl(nidat),idat(nidat)
      equivalence 
     &  (idatl( 1),mw),    (idatl( 2),mh)
     & ,(idatl( 3),nbdry), (idatl( 4),limitr)
     & ,(idatl( 5),ktor),  (idatl( 6),nmass)
     & ,(idatl( 7),keecur),(idatl( 8),ishot)
     & ,(idatl( 9),KEEFNC), (idatl(10),KEEKNT)
     & ,(idatl(11),KFFFNC), (idatl(12),KFFKNT)
     & ,(idatl(13),KPPFNC), (idatl(14),KPPKNT)
     & ,(idatl(15),KWWFNC), (idatl(16),KWWKNT)

      parameter(nrdat=16)
      dimension rdatl(nrdat),rdat(nrdat)
      equivalence 
     &  (rdatl( 1),xdim),   (rdatl( 2),zdim)
     & ,(rdatl( 3),rzero),  (rdatl( 4),zmid)
     & ,(rdatl( 5),rmaxis), (rdatl( 6),zmaxis)
     & ,(rdatl( 7),ssimag), (rdatl( 8),ssibry)
     & ,(rdatl( 9),bcentr), (rdatl(10),cpasma)
     & ,(rdatl(11),rvtor),  (rdatl(12),time)
     & ,(rdatl(13),EETENS), (rdatl(14),FFTENS) 
     & ,(rdatl(15),PPTENS), (rdatl(16),WWTENS)
c
      namelist /BASIS/
     &  EE2BDRY,EEBDRY,EEKNT,EETENS 
     & ,FF2BDRY,FFBDRY,FFKNT,FFTENS 
     & ,KEE2BDRY,KEEBDRY,KEEFNC,KEEKNT
     & ,KFF2BDRY,KFFBDRY,KFFFNC,KFFKNT 
     & ,KPP2BDRY,KPPBDRY,KPPFNC,KPPKNT 
     & ,KWW2BDRY,KWWBDRY,KWWFNC,KWWKNT 
     & ,PP2BDRY,PPBDRY,PPKNT,PPTENS
     & ,WW2BDRY,WWBDRY,WWKNT,WWTENS
c
      do i=1,nidat
         idatl(i)=idat(i)
      enddo
      do i=1,nrdat
         rdatl(i)=rdat(i)
      enddo

      do i=1,256
         if (ichar(eqdsk(i:i)) .eq. 0 ) then
            leqdsk=i-1
            goto 5433
         endif
      enddo
 5433 continue

      uday = '24-Oct-10 '
      mfvers(1) = '01/21'
      mfvers(2) = '/2000'
      idum = 7
      xdum = 0.0
      write (case_v(1),1040)
      write (case_v(2),1050) mfvers(1)
      write (case_v(3),1060) mfvers(2)
      if (ishot.le.99999) then
       write (case_v(4),1070) ishot
      else
       write (case_v(4),1073) ishot
      endif
      write (case_v(5),1080) int(time)
      case_v(6)=' '

      if (imode.eq.1) then

      open(unit=neqdsk,file=eqdsk(1:leqdsk),status='REPLACE',
     .     form='FORMATTED',err=9999)
      write (neqdsk,2000) (case_v(i),i=1,6),idum,mw,mh
      write (neqdsk,2020) xdim,zdim,rzero,rgrid(1),zmid
      write (neqdsk,2020) rmaxis,zmaxis,ssimag,ssibry,bcentr
      write (neqdsk,2020) cpasma,ssimag,xdum,rmaxis,xdum
      write (neqdsk,2020) zmaxis,xdum,ssibry,xdum,xdum
      write (neqdsk,2020) (fpol(i),i=1,mw)
      write (neqdsk,2020) (pres(i),i=1,mw)
      write (neqdsk,2020) (ffprim(i),i=1,mw)
      write (neqdsk,2020) (pprime(i),i=1,mw)
      write (neqdsk,2020) ((psirz(i+(j-1)*mw),i=1,mw),j=1,mh)
      write (neqdsk,2020) (qpsi(i),i=1,mw)
      write (neqdsk,2022)  nbdry,limitr
      write (neqdsk,2020) (rbdry(i),zbdry(i),i=1,nbdry)
      write (neqdsk,2020) (xlim(i),ylim(i),i=1,limitr)
      write (neqdsk,2024) ktor,rvtor,nmass
      if (ktor.gt.0) then
         write (neqdsk,2020) (pressw(i),i=1,mw)
         write (neqdsk,2020) (pwprim(i),i=1,mw)
      endif
      if (nmass.gt.0) then
         write (neqdsk,2020) ( dmion(i),i=1,mw)
      endif
      write (neqdsk,2020) (rhovn(i),i=1,mw)
      write (neqdsk,2026) keecur
      if (keecur.gt.0) then
         write (neqdsk,2020) (epoten(i),i=1,mw)
      endif
      if (ibasis.eq.1) then
         do k=1,nercur
            EE2BDRY(k)  = xEE2BDRY(k)
            EEBDRY(k)   = xEEBDRY(k)
            EEKNT(k)    = xEEKNT(k)
            KEE2BDRY(k) = xKEE2BDRY(k)
            KEEBDRY(k)  = xKEEBDRY(k)
         enddo
         do k=1,npcurn
            FF2BDRY(k)  = xFF2BDRY(k)
            FFBDRY(k)   = xFFBDRY(k)
            FFKNT(k)    = xFFKNT(k)
            KFF2BDRY(k) = xKFF2BDRY(k)
            KFFBDRY(k)  = xKFFBDRY(k)
            KPP2BDRY(k) = xKPP2BDRY(k)
            KPPBDRY(k)  = xKPPBDRY(k)
            KWW2BDRY(k) = xKWW2BDRY(k)
            KWWBDRY(k)  = xKWWBDRY(k)
            PP2BDRY(k)  = xPP2BDRY(k)
            PPBDRY(k)   = xPPBDRY(k)
            PPKNT(k)    = xPPKNT(k)
            WW2BDRY(k)  = xWW2BDRY(k)
            WWBDRY(k)   = xWWBDRY(k)
            WWKNT(k)    = xWWKNT(k)
         enddo
         write (neqdsk,BASIS)
      endif
c----------------------------------------------------------------------
c--  input file binary format                                        --
c----------------------------------------------------------------------
      else
      close(unit=neqdsk)
      open(unit=neqdsk,file=eqdsk(1:leqdsk),status='REPLACE'
     &     ,form='UNFORMATTED',err=9999)
      write (neqdsk) (case_v(i),i=1,6),idum,mw,mh
      write (neqdsk) xdim,zdim,rzero,rgrid(1),zmid
      write (neqdsk) rmaxis,zmaxis,ssimag,ssibry,bcentr
      write (neqdsk) cpasma,ssimag,xdum,rmaxis,xdum
      write (neqdsk) zmaxis,xdum,ssibry,xdum,xdum
      write (neqdsk) (fpol(i),i=1,mw)
      write (neqdsk) (pres(i),i=1,mw)
      write (neqdsk) (ffprim(i),i=1,mw)
      write (neqdsk) (pprime(i),i=1,mw)
      write (neqdsk) ((psirz(i+(j-1)*mw),i=1,mw),j=1,mh)
      write (neqdsk) (qpsi(i),i=1,mw)
      write (neqdsk) nbdry,limitr
      write (neqdsk) (rbdry(i),zbdry(i),i=1,nbdry)
      write (neqdsk) (xlim(i),ylim(i),i=1,limitr)
      write (neqdsk) ktor,rvtor,nmass
      if (ktor.gt.0) then
         write (neqdsk) (pressw(i),i=1,mw)
         write (neqdsk) (pwprim(i),i=1,mw)
      endif
      if (nmass.gt.0) then
         write (neqdsk) ( dmion(i),i=1,mw)
      endif
      write (neqdsk) (rhovn(i),i=1,mw)
      write (neqdsk) keecur
      if (keecur.gt.0) then
         write (neqdsk) (epoten(i),i=1,mw)
      endif

      endif

      close(unit=neqdsk)
      ierr=0
      return
c
 9999 ierr=1
      close(unit=neqdsk)
      return
c
 1020 format (1hx,i5,1h.,i3)
 1040 format (8h  EFITD )
 1042 format (1x,a42,1x,a3)
 1050 format (3h   ,a5)
 1060 format (a5,3h   )
 1070 format (3h # ,i5)
 1073 format (2h #,i6)
 1080 format (2h  ,i4,2hms)

 2000 format (6a8,3i4)
 2020 format (5e16.9)
 2026 format (i5)
 2022 format (2i5)
 2024 format (i5,e16.9,i5)

	end subroutine writeg
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
