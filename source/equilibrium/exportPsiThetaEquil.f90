!> Export psi,theta-grid equilibrium
!!
!! \param prefix filenames prefix
!! \param nt theta dimension
!! \praam np psi dimension
subroutine exportPsiThetaEquil(prefix,np,nt,psi_LCFS_out,n_psi_out)

  character(*),intent(in) :: prefix
  integer(4),intent(in) :: np,nt
  real(8),intent(out) :: psi_LCFS_out
  integer(4),intent(out) :: n_psi_out
  integer(4) :: ip,it,i,Ntrace
  real(8),allocatable :: psigrid(:),thetagrid(:),Rtrace(:),Ztrace(:),Ltrace(:)
  real(8) :: DLcoeff,w,x,x0,theta,theta0,B(3)
  integer(4),parameter :: saffac = 3
  real(8),allocatable ::   ptR(:,:),ptZ(:,:),ptBR(:,:),ptBPHI(:,:),ptBZ(:,:),pt_psi_test(:,:)
  logical :: isClosed
  integer(4) :: f_R,f_Z,f_th,f_psi,np_out,f_BR,f_BZ,f_BPHI,fvals,n_B_extr
  character(len=18) :: fmt

  real(8),parameter :: tol_fluxtrace = 1d-6

  type(vector) :: gpsi !,zeroPsi_start
  real(8) :: t1,cyl(3)

  if (equilibriumType /= equilibrium2D) then
    write(*,*)'warning: exportPsiThetaEquil works only for 2D equilibrium, no output generated'
    write(getProtocolUnit(),*)'warning: exportMatlabEquilibrium works only for 2D equilibrium, no output generated'
    return
  endif

  allocate (Rtrace(maxSurfDim*saffac),Ztrace(maxSurfDim*saffac),Ltrace(maxSurfDim*saffac))
  allocate (psigrid(np),thetagrid(nt))
  allocate (ptR(np,nt),ptZ(np,nt),ptBR(np,nt),ptBPHI(np,nt),ptBZ(np,nt),pt_psi_test(np,nt))

  psigrid(1) = psiMagAxis
  thetagrid(1) = 0
  ptR(1,:)   = RMagAxis
  ptZ(1,:)   = ZMagAxis
  B = getB(RMagAxis,0.d0,ZMagAxis)
  ptBR(1,:)   = B(1)
  ptBPHI(1,:) = B(2)
  ptBZ(1,:)   = B(3)
  pt_psi_test(1,:) = 0
  forall(i=2:np) psigrid(i) = psiMagAxis + (i-1) * (Psi_LCFS - psiMagAxis) / (np - 1)
  forall(i=2:nt) thetagrid(i) = 0 + (i-1) * (2*Pi) / (nt - 1)
  np_out = np
  do ip=2,np
    DLcoeff = getreff(psigrid(ip))*2*Pi/(saffac*nt)
    call fluxTrace(psigrid(ip),DLcoeff,Rtrace,Ztrace,Ltrace,NTrace,isClosed,tol_fluxtrace,n_B_extr)
    if (n_B_extr>2) then
      write(*,'(''warning: over 2 B extrems, flux surface '',I3,''/'',I3'', psi='',E16.8)'),ip,np,psigrid(ip)
      write(*,*)'exporting anyway' !only previous flux surfaces'
      !np_out = ip-1
      !exit
    endif
    if (.not. isClosed) then
      if (ip<np) then
        write(*,'(''warning: unclosed flux surface '',I3,''/'',I3'', psi='',E16.8)'),ip,np,psigrid(ip)
        write(*,*)'exporting only closed flux surfaces'
        np_out = ip-1
        exit
      else
        do i=9,1,-1
          psigrid(ip) = psigrid(ip-1) + i*0.1d0*(psigrid(ip)-psigrid(ip-1))
          call fluxTrace(psigrid(ip),DLcoeff,Rtrace,Ztrace,Ltrace,NTrace,isClosed,tol_fluxtrace)
          if (isClosed) then
            write(*,*)'exportPsiThetaEquil: psi_LCFS corrected by ',i*0.1
            exit
          endif
        enddo
      endif
      if (.not. isClosed) then
        write(*,'(''warning: unclosed flux surface '',I3,''/'',I3'', psi='',E16.8)'),ip,np,psigrid(ip)
        write(*,*)'exporting only closed flux surfaces'
        np_out = ip-1
        exit
      endif
    endif

    theta0 = 0
    i = 2
    theta = atan2(Ztrace(i)-ZMagAxis,Rtrace(i)-RMagAxis)
    if (theta<theta) theta = 2*Pi + theta
    it = 1
    ptR(ip,it) = Rtrace(1)
    ptZ(ip,it) = Ztrace(1)
    B = getB(ptR(ip,it),0.d0,ptZ(ip,it))
    ptBR(ip,it)   = B(1)
    ptBPHI(ip,it) = B(2)
    ptBZ(ip,it)   = B(3)
    pt_psi_test(ip,it) = 0
    do it=2,nt-1
      do while (.not.(thetagrid(it)>=theta0 .and. thetagrid(it)<=theta))
        i = i+1
        if (i>NTrace) then
          write(*,*)'error: psi-theta equilibrium export failed'
          stop
        endif
        theta0 = theta
        theta = atan2(Ztrace(i)-ZMagAxis,Rtrace(i)-RMagAxis)
        if (theta<theta0) theta = 2*Pi + theta
      enddo
      w = (thetagrid(it)-theta0)/(theta-theta0)
      !x = sqrt((Rtrace(i)  -RMagAxis)*(Rtrace(i)  -RMagAxis) + (Ztrace(i)  -ZMagAxis)*(Ztrace(i)  -ZMagAxis))
      !x0= sqrt((Rtrace(i-1)-RMagAxis)*(Rtrace(i-1)-RMagAxis) + (Ztrace(i-1)-ZMagAxis)*(Ztrace(i-1)-ZMagAxis))
      !ptR(ip,it) = RMagAxis + cos(thetagrid(it))*(x*w + x0*(1-w))
      !ptZ(ip,it) = ZMagAxis + sin(thetagrid(it))*(x*w + x0*(1-w))
      ptR(ip,it) = Rtrace(i)*w + Rtrace(i-1)*(1-w)
      ptZ(ip,it) = Ztrace(i)*w + Ztrace(i-1)*(1-w)
      ! refine
      if (abs(psigrid(ip))>tol_fluxtrace) then
        pt_psi_test(ip,it) = abs((getPsi(ptR(ip,it),0d0,ptZ(ip,it))-psigrid(ip))/psigrid(ip))
      else
        pt_psi_test(ip,it) = abs(getPsi(ptR(ip,it),0.d0,ptZ(ip,it)) - psigrid(ip))
      endif

      if (pt_psi_test(ip,it)>tol_fluxtrace) then
        x0 = pt_psi_test(ip,it)
        diffPsiUserVec_PsiValue = psigrid(ip)
        diffPsiUserVec_pos0 = cylindricalVector(ptR(ip,it),0d0,ptZ(ip,it))
        call getGradPsi(diffPsiUserVec_pos0,gpsi)
        gpsi = sign(1.d0,psigrid(ip)-getPsi(ptR(ip,it),0d0,ptZ(ip,it)))*gpsi
        diffPsiUserVec_vec = gpsi
        t1=tol_fluxtrace*5d1
        do while (diffPsiUserVec(t1)*diffPsiUserVec(0.d0)>0)
          t1 = t1 + tol_fluxtrace*5d1
        enddo
        t1 = findRoot(0.d0,t1,diffPsiUserVec,tol_fluxtrace)
        cyl = toCylindricalArray(diffPsiUserVec_pos0 + t1*gpsi)
        ptR(ip,it) = cyl(1)
        ptZ(ip,it) = cyl(3)
        if (abs(psigrid(ip))>tol_fluxtrace) then
          pt_psi_test(ip,it) = abs((getPsi(ptR(ip,it),0d0,ptZ(ip,it))-psigrid(ip))/psigrid(ip))
        else
          pt_psi_test(ip,it) = abs(getPsi(ptR(ip,it),0.d0,ptZ(ip,it)) - psigrid(ip))
        endif
!        if (pt_psi_test(ip,it)>tol_fluxtrace) then
!          write(*,*)'refined:',x0,pt_psi_test(ip,it)
!          write(*,*)'ip,it',ip,it
!        endif
      endif

      B = getB(ptR(ip,it),0.d0,ptZ(ip,it))
      ptBR(ip,it)   = B(1)
      ptBPHI(ip,it) = B(2)
      ptBZ(ip,it)   = B(3)
    enddo
  enddo
  ! repeat (2*Pi = 0)
  ptR(:,nt) = ptR(:,1) 
  ptZ(:,nt) = ptZ(:,1)
  ptBR(:,nt)   = ptBR(:,1)
  ptBPHI(:,nt) = ptBPHI(:,1)
  ptBZ(:,nt)   = ptBZ(:,1)
  pt_psi_test(:,nt) = pt_psi_test(:,1)

  write(*,*)'max err=',maxval(pt_psi_test(:,:))

  write(fmt,'(''('',I4,''(SP,1PE16.8))'')')np_out
  f_R = openFile(prefix//'_R.dat')
  f_Z = openFile(prefix//'_Z.dat')
  f_th = openFile(prefix//'_theta.dat')
  f_BR = openFile(prefix//'_BR.dat')
  f_BZ = openFile(prefix//'_BZ.dat')
  f_BPHI = openFile(prefix//'_Bphi.dat')
  do it=1,nt
    write(f_R,fmt)ptR(1:np_out,it)
    write(f_Z,fmt)ptZ(1:np_out,it)
    write(f_BR,fmt)ptBR(1:np_out,it)
    write(f_BZ,fmt)ptBZ(1:np_out,it)
    write(f_BPHI,fmt)ptBPHI(1:np_out,it)
    write(f_th,'(E16.8)')thetagrid(it)
  enddo
  close(f_R)
  close(f_Z)
  close(f_BR)
  close(f_BZ)
  close(f_BPHI)
  close(f_th)

  f_psi = openFile(prefix//'_Psi.dat')
  do i=1,np_out
    write(f_psi,'(E16.8)')psigrid(i)*SGNBPOL
  enddo
  close(f_psi)
  psi_LCFS_out = psigrid(np_out)
  n_psi_out = np_out

  fvals = openFile(prefix // '_values.dat')
  write(fvals,'(SP,1PE16.8)')RMagAxis
  write(fvals,'(SP,1PE16.8)')ZMagAxis
  write(fvals,'(SP,1PE16.8)')SGNBPOL*psigrid(np_out)
  close(fvals)

  deallocate (psigrid,thetagrid,Rtrace,Ztrace,Ltrace)
  deallocate (ptR,ptZ,ptBR,ptBPHI,ptBZ)

!contains
!
!  real(8) function zeroPsi(t)
!    real(8),intent(in) :: t
!
!    zeroPsi = psigrid(ip) - getPsi(zeroPsi_start + t*gpsi)
!  end function


end subroutine
