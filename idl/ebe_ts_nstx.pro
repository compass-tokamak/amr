pro ebe_ts_nstx,shot,time,ts_data

  tsdata=ts3(shot)
  if (n_elements(tsdata) eq 0) then tsdata=ts(shot)
  itime=where(abs(time - tsdata.time) eq min(abs(time - tsdata.time)))
  ts_time = tsdata.time(itime)
  if (ts_data eq 0) then begin
    r = tsdata.radius
    den = tsdata.nef[itime,*]
    te = tsdata.tef[itime,*]
    dr = tsdata.dr
    dden = tsdata.dnef[itime,*]
    dte = tsdata.dtef[itime,*]
  endif else begin
    r = tsdata.rs
    den = tsdata.nes[itime,*]
    te = tsdata.tes[itime,*]
  endelse

  ; convert den from cm^-3 to 10^19 m^-3
	; R from cm to m
  ; te is in keV
  r=r*1.0e-2
  den=den*1.0e-13
  dr=dr*1.0e-2
  dden=dden*1.0e-13
  
  phi = 0.0
  Z = 0.0
  dphi = 0.0
  dZ = 0.0
  out_fname = inp_fname(shot,time,'ts')
  openw,1,out_fname
  printf,1,'#R(m),phi(rad),Z(m),dR(m),dphi(rad),dZ(m),den(1e19m-3),Te(keV),dden(1e19m-3),dTe(keV)'
  for i=0,n_elements(r)-1 do begin
	; test for NaNs
	; if NaN appears, it's replaced by -1
	if ( ~(finite(den[i]))) then den[i]=-1
	if ( ~(finite(dden[i]))) then begin
          den[i]=-1
          dden[i]=-1
        endif
	if ( ~(finite(te[i]))) then te[i]=-1
	if ( ~(finite(dte[i]))) then begin
          te[i]=-1
          dte[i]=-1
        endif
      printf,1,r[i],phi,Z,dr[i],dphi,dZ,den[i],te[i],dden[i],dte[i],format='(10(E15.7))'
  endfor
    
  close,1

end
