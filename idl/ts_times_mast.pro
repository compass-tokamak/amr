pro ts_times_mast

openr,2,'shot_no.in'
readf,2,shotn
readf,2,str_ts_data_no
close,2

shot=long(shotn)
ts_data_no=long(str_ts_data_no)

source = 'MAST::' + strtrim(string(shot),2)

if (ts_data_no eq 0) then begin
	; reading ruby laser TS data
	trace = getdata('ats_ne', source)
	ts_times = trace.time
end
if (ts_data_no eq 1) then begin
	; reading Nd YAG laser TS data
	trace = getdata('atm_ne', source)
	ts_times = trace.time
end

if (shot ge 100000) then fname=string(shot,format='(''ts_times_'',I6,''.out'')') $
    else if (shot ge 10000) then fname=string(shot,format='(''ts_times_0'',I5,''.out'')') $
    else if (shot ge 1000) then fname=string(shot,format='(''ts_times_00'',I4,''.out'')') $
    else if (shot ge 100) then fname=string(shot,format='(''ts_times_000'',I3,''.out'')') $
    else if (shot ge 10) then fname=string(shot,format='(''ts_times_0000'',I2,''.out'')') $
    else fname=string(shot,format='(''ts_times_00000'',I1,''.out'')')

openw,1,fname
for i=0,n_elements(ts_times)-1 do begin
    printf,1,ts_times(i)
endfor
close,1

end
