!> \file absorb2.f90

!> calculate the absorbed power from incident and reflected wave amplitudes
!! \param Aincy y-amplitude of the incident wave
!! \param Aincz z-amplitude of the incident wave
!! \param Cy y-amplitude of the reflected wave
!! \param Cz z-amplitude of the reflected wave
!! \param Ny Ny (wave vector) of the incident wave
!! \param Nz Nz (wave vector) of the incident wave
real(8) function absorb2(Aincy,Aincz,Cy,Cz,Ny,Nz)
  implicit none
  complex(8) Aincy,Aincz,Cy,Cz
  real(8) :: pi,Ny,Nz,k_eta2,k_eta
  real(8) :: pabsorb,pinc

  pi=2.0d0*asin(1.0d0)
  k_eta2=ny*ny+nz*nz
  k_eta=sqrt(k_eta2)
  if (k_eta > 1e-4 .AND. k_eta < 0.9995) then

  ! the incident wave is propagating

  ! nx=sqrt(1.0d0-k_eta2)
    pabsorb=((1.0d0-Nz*Nz)*(Aincy*conjg(Aincy)-Cy*conjg(Cy))+ &
    (1.0d0-Ny*Ny)*(Aincz*conjg(Aincz)-Cz*conjg(Cz)) &
    +2.0d0*Ny*Nz*real(Aincy*conjg(Aincz)-Cy*conjg(Cz)))
    pinc=(1.0d0-Nz*Nz)*Aincy*conjg(Aincy)+ &
    (1.0d0-Ny*Ny)*Aincz*conjg(Aincz)+ &
    2.0d0*Ny*Nz*real(Aincy*conjg(Aincz))

    absorb2=pabsorb/pinc
  ! write(*,*)'Aincy,Aincz,Cy,Cz = ',Aincy,Aincz,Cy,Cz
  endif

end function
