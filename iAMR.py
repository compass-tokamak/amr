'''iAMR = interactive AMR.

1) Put AMR path to PYTHONPATH environment variable
2) Run ipython --pylab (or any preffered python environment)
3) Run iAMR by one of:
     * execfile(amrpath+'/iAMR.py')
       (either define amrpath before or just type here)
     * from iAMR import *
'''
##@package iAMR
## starts interactive AMR session

def iPrompt(text,defval=''):
    '''Interactive user input with a default value.'''
    ans = raw_input(text + ' [' + str(defval) + '] : ')
    ans = ans.strip()
    if ans=='': ans = defval
    return ans

import os,os.path,sys
# Find AMR path
amrpath = ''
try:
    import pyAMR.pyAMR as pyamr
    amrpath = os.path.dirname(os.path.dirname(os.path.realpath(pyamr.__file__)))
except ImportError,NameError:
    if os.getenv('AMR'): 
        amrpath = os.getenv('AMR').split()[-1]
        amrpath = os.path.dirname(amrpath) if not os.path.isdir(amrpath) else amrpath
if not amrpath: amrpath = iPrompt('Specify AMR path')
print('AMR path: ' + amrpath)
iamrpath = os.path.join(amrpath,'iAMR')
if amrpath not in sys.path: sys.path.append(amrpath)
import pyAMR.ebeResult6 as ebeResult6
import pyAMR.pyLUKE as pyLUKE
import pyAMR.fileNames6 as fn
import pyAMR.amrplot as amrplot
import matplotlib
from matplotlib.pylab import *
from numpy import *
try:
    haveSciPy = True
    import scipy as S
    import scipy.interpolate
except ImportError:
    haveSciPy = False

if haveSciPy:
    def fwhm(x,y):
    #    if abs(y.max())<abs(y.min()): 
    #        y=-y
    #    print y.max()
        y = abs(y)
        yih = S.interpolate.InterpolatedUnivariateSpline(x,y-y.max()/2,k=3)
        rs = yih.roots()
        if len(rs)==0: rs=[0,0]
        if len(rs)==1:
            if yih(x[-1])>0:
                rs=[rs[0],x[-1]]
            else:
                rs=[x[0],rs[0]]
        w = rs[-1]-rs[0]
        pts = (rs[0],rs[-1],)
        return w,pts

print('\n-= This iAMR - interactive AMR =-\n')

# regular font for math text

if 'mathtext.default' in matplotlib.rcParams: matplotlib.rc('mathtext',default='regular')


# get configuration file
filelist = os.listdir('.')
filelist2 = filter(lambda x: x.endswith('.cfg') and not x.lower().startswith('antenna'),filelist)
if len(filelist2)==1:
    cfgfilename = filelist2[0]
else:
    if 'ebe6.cfg' in filelist:
        cfgfilename = 'ebe6.cfg'
    else:
        for filename in filelist:
            if os.path.splitext(filename)[1]=='.cfg' and 'anten' not in os.path.splitext(filename)[0].lower():
                cfgfilename = filename
                break
            else:
                cfgfilename = 'ebe6.cfg'
    cfgfilename = iPrompt('cfg file name',cfgfilename)
defaults_cfgfilename = os.path.join(amrpath,'ebe6_defaults.cfg')

amr_cfg = pyamr.readConfig(cfgfilename,defaults_cfgfilename)
print('AMR configuration stored in amr_cfg')
antennas = pyamr.initAntennas(amr_cfg)
print('Antennas stored in antennas')
s = 'Antenna id\'s: '
for ant in antennas:
    s += ant.id + ' '
print(s)
ebe_times = pyamr.initShotTimes(amr_cfg)
print('Simulation shot times stored in ebe_times: ' + str(array(ebe_times)))

# read test_profiles output
prof_exist = False
flux_coord = amr_cfg['figures']['flux_coordinate']
if amr_cfg['output']['test_profiles'] | amr_cfg['figures']['test_profiles']:
    prof_exist = True
    (prof0d,prof1d,prof1d_comp,prof2d,profpsi,profpsi_comp,profexp,prof3d,profequat) = pyamr.readProfiles(amr_cfg,ebe_times)
print('Plasma profiles stored in prof0d, prof1d, prof1d_comp, prof2d, profpsi, profpsi_comp, profexp arrays[time]')

# read ray and beam results
indir = 'output/'
raydata = amrplot.amrdata()
raydata.readFile(indir + 'results_ray.dat')
beamdata = amrplot.amrdata()
beamdata.readFile(indir + 'results_beam.dat')
print('Rays/beam results stored in raydata, beamdata')

# check for frequency scan
freq_scan = True
freqs = []
for ant in antennas:
    freqs_a = []
    for iConf in range(ant.getNumberOfConfigurations()):
        freqs_a.append(ant.getBeamWaist(iConf).freq)
    if freqs==[]: freqs=freqs_a
    if freqs!=freqs_a:
        freq_scan = False
        break
del(freqs_a)
if not len(freqs)>1: freqs_scan = False
if freq_scan:
    print 'Frequency scan detected, frequencies in freqs, freq_scan = True'
else:
    del(freqs)

# read power deposition results
# indices [time,atenna,configuration] ([time,config] for summed profiles)
pd_data = []
sum_pd_data = []
indir = 'ebe_integration/output/'
for [t,it] in zip(ebe_times,range(len(ebe_times))):
    pd_data.append([])
    sum_pd_data.append([])
    if amr_cfg['output']['sum_antennas'] and len(amr_cfg['antenna']['antenna_ids'])>1:
        # plot summed profiles
        for iConf in range(antennas[0].getNumberOfConfigurations()):
            sum_pd_data[-1].append(amrplot.amrdata())
            sum_pd_data[-1][-1].readFile('output/'+fn.summedIntProfiles(iConf,t)[0])
    for iAnt in range(len(amr_cfg['antenna']['antenna_ids'])):
        pd_data[-1].append([])
        for iConf in range(antennas[iAnt].getNumberOfConfigurations()):
            freq = antennas[iAnt].getBeamWaist(iConf).freq
            pd_data[-1][-1].append(amrplot.amrdata())
            pd_data[-1][-1][-1].readFile(indir+fn.getIntProfFileName(iAnt,iConf,t,'reff'))
print('Power deposition results stored in pd_data, sum_pd_data arrays[time,antenna,configuration]')

# read LUKE results
luke_results = []
for [t,it] in zip(ebe_times,range(len(ebe_times))):
    luke_results.append([])
    for iAnt in range(len(antennas)):
        luke_results[-1].append([])
        for iConf in range(antennas[iAnt].getNumberOfConfigurations()):
            fname = fn.getLUKEFileName(amr_cfg['input']['experiment'],amr_cfg['input']['shot_number'],t,iAnt,iConf)
            luke_results[-1][-1].append(pyLUKE.readLUKE(fname))
print('LUKE results stored in luke_results array[time,antenna,configuration]')

# read ray tracing outputs
RTRayData = []
for [t,it] in zip(ebe_times,range(len(ebe_times))):
    RTRayData.append([])
    for iAnt,ant in enumerate(antennas):
        RTRayData[-1].append([])
        for iConf in range(ant.getNumberOfConfigurations()):
            RTRayData[-1][-1].append([])
            for iRay in range(ant.getBeamWaist(iConf).getNumberOfRays()):
                RTRayData[-1][-1][-1].append(amrplot.amrdata())
                RTRayData[-1][-1][-1][-1].readFile(os.path.join('ebe_run','output',\
                                                   fn.getRTFileName(amr_cfg['input']['shot_number'], t, iAnt, iConf, iRay+1)))
print 'Ray tracing results stored in RTRayData[time,iAnt,iConf,iRay]'
DPRayData = []
for [t,it] in zip(ebe_times,range(len(ebe_times))):
    DPRayData.append([])
    for iAnt,ant in enumerate(antennas):
        DPRayData[-1].append([])
        for iConf in range(ant.getNumberOfConfigurations()):
            DPRayData[-1][-1].append([])
            for iRay in range(ant.getBeamWaist(iConf).getNumberOfRays()):
                DPRayData[-1][-1][-1].append(amrplot.amrdata())
                DPRayData[-1][-1][-1][-1].readFile(os.path.join('ebe_run','output',\
                                                   fn.getDPFileName(amr_cfg['input']['shot_number'], t, iAnt, iConf, iRay+1)))
print 'Ray tracing power deposition results stored in DPRayData[time,iAnt,iConf,iRay]'

# perform some analysis
for [t,it] in zip(ebe_times,range(len(ebe_times))):
    for iAnt,ant in enumerate(antennas):
        for iConf in range(ant.getNumberOfConfigurations()):
            for iRay in range(ant.getBeamWaist(iConf).getNumberOfRays()):
                rtd = RTRayData[it][iAnt][iConf][iRay]
                if rtd.hasData():
                    r_damped = rtd.getData('damping_factor').min()
                    r_max_d = abs(rtd.getData('Dtest')).max()
                    # print 'Time %g, antenna %i, configuration %i, ray %i damped %g' % (t,iAnt,iConf,iRay+1,r_damped)
                    # print 'Time %g, antenna %i, configuration %i, ray %i max D %g' % (t,iAnt,iConf,iRay+1,r_max_d)
                    if r_damped > amr_cfg['ray_tracing']['power_limit']:
                        print 'Time %g, antenna %i, configuration %i, ray %i damped only %g' % (t,iAnt,iConf,iRay+1,1-r_damped)
                    if r_max_d > amr_cfg['ray_tracing']['accuracy']:
                        print 'Time %g, antenna %i, configuration %i, ray %i low accuracy %g' % (t,iAnt,iConf,iRay+1,r_max_d)

if raydata.hasData():
    ri_ce_exc = raydata.getData('conv_eff2') + raydata.getData('conv_eff1') > 1
    if ri_ce_exc.any():
        print 'Conversion efficiency exceed 100 % in certain cases'
        print 'iAntenna iConf iRay'
        print column_stack((raydata.getData('iantenna')[ri_ce_exc],\
                              raydata.getData('iconf')[ri_ce_exc],\
                              raydata.getData('iray')[ri_ce_exc],))
    ri_ce_neg = (raydata.getData('conv_eff2')<0) | (raydata.getData('conv_eff1')<0)
    if ri_ce_neg.any():
        print 'Conversion efficiency NEGATIVE in certain cases'
        print 'iAntenna iConf iRay'
        print column_stack((raydata.getData('iantenna')[ri_ce_exc],\
                              raydata.getData('iconf')[ri_ce_exc],\
                              raydata.getData('iray')[ri_ce_exc],))

# latex slows down considerably
uselatex = False

rho_LCFS = amr_cfg['figures']['rho_LCFS']

# example plot fpe(R)
# plot(prof1d[0].getData('R'),prof1d[0].getData('fpe'))

# choose the first time / antenna / configuration
it = 0
t = ebe_times[it]
iAnt = 0
iConf = 0

def charfrq(it=0):
    '''Plot characteristic frequencies.
    
    it - shot time index in ebe_times.
    '''
    t = ebe_times[it]
    title = 'Characteristic frequencies, shot %i, time %.3f s' % (amr_cfg['input']['shot_number'],t)
    RLCFS_LFS = prof0d[it].getData('RLCFS_LFS')[0]
    amrplot.plotfreqs('',prof1d[it],'R',title=title, \
                      fpe='fpe',fUHR='fUHR',fce='fce',nfce=[1],xLCFS=RLCFS_LFS, \
                      xlabel='R [m]',ylabel='GHz', \
                      fpecolor = 'green',fUHRcolor = 'magenta', \
                      fcecolor = 'blue',
                      selectors=[],latex=uselatex)

def rho2D(it=0):
    '''Plot rho contours.
    
    it - shot time index in ebe_times.
    '''
    t = ebe_times[it]
    psi_LCFS = prof0d[it].getData('psi_LCFS')[0]
    psi_axis = prof0d[it].getData('psi_axis')[0]
    R_axis = prof0d[it].getData('R_axis')[0]
    Z_axis = prof0d[it].getData('Z_axis')[0]
    RLCFS_LFS = prof0d[it].getData('RLCFS_LFS')[0]
    title = '$\\rho $ contours, shot %i, time %.3f s' % (amr_cfg['input']['shot_number'],t)
    amrplot.plotRZ('',title=title, \
                   R=prof2d[it].getData('R'),Z=prof2d[it].getData('Z'),psi=prof2d[it].getData('rho') ,\
                   psilabels=False,psilabel='$\\rho_{\\rm pol}$',\
                   psiLCFS=rho_LCFS,RZaxis=[R_axis,Z_axis],\
                   ncin=5,ncout=5 ,\
                   latex=uselatex)

def profiles(it=0):
    '''Plot plasma density and temperature profiles.
    
    it - shot time index in ebe_times.
    '''
    t = ebe_times[it]
    title = 'Electron density and temperature, shot %i, time %.3f s' % (amr_cfg['input']['shot_number'],t)
    figname=''
    psi_LCFS = prof0d[it].getData('psi_LCFS')[0]
    psi_axis = prof0d[it].getData('psi_axis')[0]
    R_axis = prof0d[it].getData('R_axis')[0]
    Z_axis = prof0d[it].getData('Z_axis')[0]
    RLCFS_LFS = prof0d[it].getData('RLCFS_LFS')[0]
    title = 'Electron density and temperature, shot %i, time %.3f s' % (amr_cfg['input']['shot_number'],t)
    amrplot.plotdente(figname,profpsi[it],'rho',title=title,xLCFS=rho_LCFS, \
          den='denRZ',temp='TempRZ', \
          xlabel='$\\rho$',ydenlabel='${\\sf 10^{19}\\; m^{-3}}$',\
          ytemplabel='keV',\
          denlabel='${\\sf n_e}$',templabel='${\\sf T_e}$', \
          dencolor = 'blue',tempcolor = 'red', \
          selectors=[],latex=uselatex)
    if (not amr_cfg['profiles']['analytical_profiles']) and amr_cfg['figures']['experimental_data']:
        if amr_cfg['figures']['errbars']:
            amrplot.plotdente(figname,prof1d[it],'R',title=title,xLCFS=RLCFS_LFS, \
                  den='denRZ',temp='TempRZ', \
                  xlabel='R [m]',ydenlabel='${\\sf 10^{19}\\; m^{-3}}$',ytemplabel='keV',\
                  denlabel='${\\sf n_e}$',templabel='${\\sf T_e}$', \
                  dencolor = 'blue',tempcolor = 'red', \
                  expdata=profexp[it],expx='R(m)',expden='den(1e19m-3)',exptemp='Te(keV)', \
                  denerr='dden(1e19m-3)',temperr='dTe(keV)',expxerr='dR(m)',\
                  selectors=[],latex=uselatex)
        else:
            amrplot.plotdente(figname,prof1d[it],'R',title=title,xLCFS=RLCFS_LFS, \
                  den='denRZ',temp='TempRZ', \
                  xlabel='R [m]',ydenlabel='${\\sf 10^{19}\\; m^{-3}}$',ytemplabel='keV',\
                  denlabel='${\\sf n_e}$',templabel='${\\sf T_e}$', \
                  dencolor = 'blue',tempcolor = 'red', \
                  expdata=profexp[it],expx='R(m)',expden='den(1e19m-3)',exptemp='Te(keV)', \
                  selectors=[],latex=uselatex)
    else:
        amrplot.plotdente(figname,prof1d[it],'R',title=title,xLCFS=RLCFS_LFS, \
              den='denRZ',temp='TempRZ', \
              xlabel='R [m]',ydenlabel='${\\sf 10^{19}\\; m^{-3}}$',ytemplabel='keV',\
              denlabel='${\\sf n_e}$',templabel='${\\sf T_e}$', \
              dencolor = 'blue',tempcolor = 'red', \
              selectors=[],latex=uselatex)           

def plotRT_RZ(it=0,iAnts=[0],iConf=0,iRays=[],titles=['','']):
    t = ebe_times[it]
    beamWaist = antennas[iAnts[0]].getBeamWaist(iConf)
    if not any(iRays): iRays=range(1,beamWaist.getNumberOfRays()+1)
    rays = []
    for iAnt in iAnts:
        for iRay in iRays:
            if RTRayData[it][iAnt][iConf][iRay-1].hasData():
                raydetails = RTRayData[it][iAnt][iConf][iRay-1]
                raycolor = 'darkgreen'
                if iRay==1:
                    raycolor = 'red'
                ray = {'R':raydetails.getData('R'),'Z':raydetails.getData('Z'),'name':'ray %i' % iRay,\
                       'color':raycolor,'linestyle':'-',\
                       'Npar':raydetails.getData('Npar'),\
                       'x':raydetails.getData('x'),'y':raydetails.getData('y')}
                if len(amr_cfg['figures']['rt_power_dep_markers'])>0 and len(raydetails['damping_factor'])>0:
                    ray['spots'] = [abs(raydetails['damping_factor']-p).argmin() for p in amr_cfg['figures']['rt_power_dep_markers']]
                rays.append(ray)
    title = titles[0]
    rays.reverse()
    if prof2d != [] and prof2d[it].hasData():
        R_axis = prof0d[it].getData('R_axis')[0]
        Z_axis = prof0d[it].getData('Z_axis')[0]
        if raydetails.hasData():
            nfce = range(int(raydetails.getData('om/omc').round(0).min()),int(raydetails.getData('om/omc').round(0).max())+1)
        else:
            nfce = []
        fcevals = (beamWaist.freq*ones(len(nfce))).tolist()
        amrplot.plotRZ('',title=title, \
                       R=prof2d[it].getData('R'),Z=prof2d[it].getData('Z'),psi=prof2d[it].getData('rho') ,\
                       psilabels=False,psilabel='$\\rho $',\
                       psiLCFS=rho_LCFS,RZaxis=[R_axis,Z_axis],\
                       raysRZ = rays, raylabels=False, \
                       fce = prof2d[it].getData('Fce'),nfce=nfce,fcevals=fcevals,\
                       ncin=5,ncout=5 ,\
                       latex=amr_cfg['figures']['uselatex'],\
                       beam = beamWaist,plotWaist=amr_cfg['figures']['plot_waists'])
    title = titles[1]
    amrplot.plotxy('',None,[r['R'] for r in rays],[r['Npar'] for r in rays],'R[m]',['' for r in rays],\
                   colors=[r['color'] for r in rays],title=title,xLCFS=None,yuplimit=None,ylowlimit=None, \
                   grid=False,latex=False)
    figure()
    for r in rays: plot(r['R'],r['Npar'])
