; 'input' section is for entering input data for EBE simualtion
[input]
device = stellarator
; name of the experiment, i.e. MAST, NSTX
experiment = WEGA
shot_number = 1
start_time = 1
end_time = 1
; use TS times between start_time and end_time
; 'ebe_times_XXXXXX.dat' is used when run_IDL=no or run_code=yes)
; and must be produced manually (from 'ts_times_XXXXXX.out')
; 'ts_times_XXXXXX.out' is used in the testing phase and is produced automatically when run_IDL is 'yes'
ts_times = no
; or use equidistant times between start_time and end_time
; if time_samples <= 1, only start_time is considered
time_samples = 1
; run IDL programs to get input data (TS, EFIT ...)
; if set to no, all files must be provided prior to execution of the program
run_IDL = no

; 'output' section for selecting which output data are needed
; All elements must be 'yes' or 'no' if nothing else indicated
[output]
; use only the central ray in simulations
central_ray_only = no
; Output each ray's path
ray_details = yes
; power deposition profiles
power_deposition = yes
; current density calculation
; none, simple, hansen, curba
current_density = none
; Output conversion eff. for each freq/angle/time/ray
; If conv_eff = no and temperature = yes, temperature with conv_eff=100% is considered
conv_eff = yes
; Output radiative temperature for each freq/angle/time
temperature = yes
; Ouput plasma profiles
test_profiles = yes
; Run the prepared code
run_code = yes
; postprocess the data
postprocess = yes
; sum power deposition of individual antennas
sum_antennas = yes
luke = no

[cluster]
; if use=cluster set to 'no', the code will run on the local machine
use_cluster = no
; job = beam distributes jobs by inidividual beam = 41 rays, i.e. one job = 41 rays
; job = optimum distributes optimally, i.e. there will be ncpu jobs submitted to cluster
; optimum should be faster, but would be harder to get partial results if the code
; is interrupted (for whatever reason)
job = optimum
que = sque ; sunfire, sque, kestrel
ncpu = 5
; check for job completition in chech_int intervals
; in minutes, can be floating point, recommended values > 1
check_int = 1
; priority on the cluster
priority = 0
; master_priority = priority class of the python and all child processes
; 0 = lowest, 1 = low, 2 = default
master_priority = 0

[adaptive]
coll_freq = 0.0005
n_init = 350
accuracy = 0.001
; extension_xend_search<=1, S_RZmax = getxPsiMin()*Extension_xend_search
extension_xend_search = 1.0

[ray_tracing]
Zeff = 1.0
; Zeff=0.0
; Choose which root to use to start the EBW ray-tracing
; 1 - small root, makes a U-turn on EBW path
; 2 - large root, no U-turn
Nperp = 1
; accuracy of the Runge-Kutta integration
accuracy = 1.e-5
; power limit to stop the ray-tracing
power_limit = 1.e-2

[test_profiles]
R_min = 0.5
R_max = 0.9
Z_min = -0.15
Z_max = 0.15
# phi in degrees
phi_test = -55
phi_min = -55
phi_max = 360
R_dim = 50
Z_dim = 50
phi_dim = 1
# wkb_curve_ray - for which ray(s) produce wkb curve file
# 0 - no wkb curve, 50 - all rays with wkbcurve (only for non zero shot_time_test)
wkb_curve_ray = 1
# shot_time_test - 0 for all the times
shot_time_test = 0
# usualy Nparal on the plasma boundary (SOL) from Jray_iray_f.dat,
#     above harmonics Nparal oscilates
Nparal_above=0.5
# Nparal at absorption from end_of_ray_eff.dat
#  below harmonics Nparal increases
Nparal_below=0.8

[profiles]
; set equilibrium version by equil_ver
; NSTX - efit01, efit02
; stellarators - bc filename
equil_ver = wega_383_542_00_e1
; input_type = default | idl | mclib | matlab
input_type = default
; export = none | matlab
export = none
export_nR = 100
export_nZ = 100
export_npsi = 100

; -- stellarators --
; B_axis [T] - on axis magnetic field (can be math expression) at phi=phi0
B_axis = 0.65*2.45/28.0
; B_axis = 0.48
; phi0 in degrees
phi0 = -55
; maximum normalized toroidal flux to resize mesh to (for stellarators) -old psi
s_max = 10
; s (psi) of the LCFS (to shift the LCFS), original LCFS = 1
s_LCFS = 2.5
; spectrum truncation and mesh accuracy
epsTrunc = 1.0e-8
epsA = 1.0e-6
; compute mesh, must be done once for each equil_ver, eps and s_max
compute_mesh = no
; -- stellaratos --

; ts_data = raw or spline for nstx
; ruby or ndyag for mast
ts_data = ruby
; number of components (e.g. 2 for bulk + tail)
n_components = 2
# imethodofspline =1->Bspline, =2-> Akima cubic spline, =3-> CSCON, =4-> cubic splines
ims=4
; monotonize the density profile up to the critical density for the monotonize_freq frequency
monotonize = no
; monotonize_freq [GHz]
; 0 - use maximum simulated frequency
monotonize_freq = 0
; error tolerance
errorToleranceDen = 1
errorToleranceTe = 1
; profiles averaging
; from the last datapoint along the data line backwards the specified distance
; possibilities: none, mean, median, constant
averageProfilesDen = none
averageProfilesTe = none
averageDistanceDen = 0.2
averageDistanceTe = 0.1
; profiles extension
; possibilities: none, exp, constant
extendProfilesDen = exp
extendProfilesTe = exp
; start the extension before the last data points
extendDistanceDen = 0
extendDistanceTe = 0
; number of datapoints to add
extDataPoints = 5
; scale length for exp profiles extension 
LSOL_den=0.1
LSOL_Te=0.1
; SOL width
LSOL = 0.035
ne_jump_LCFS = no
analytical_profiles = yes

[analytical_profiles]
type = wega
; phi (in degrees) coordinate of the profiles
phi = -55
; number of grid points in psi grid for splines
nGrid = 100
; format [[p(1,1), p(1,2),...],[p(2,1),p(2,2),...],...]
; where p(c,n) in the n-th parameter for c-th component
; 1 component, 3 parameters example: [[1,0.2,3.3]]
; 2 components,2 parameters example: [[1,0.2],[2.2,4]]
; [N0[10^19 m-3] ]
; [Te0[keV]]
den_parameters = [[0.1],[0.2]]
Te_parameters = [[0.01],[0.3]]

[antenna]
cfg_file = antenna_WEGA.cfg
; which antennas to use - white space separated list of antenna id's
ids = left right

[integration]
; full = perform integration of teh results
; power = use the already integrated ray power
type = power
R_min = 0.5
R_max = 0.9
r_eff_min = 0.02
r_eff_max = 0.2
tabdim = 91
