real(8),private :: LLintegLambda !< lambda parameter for the LLintegFun, set by setLLintegLambda
real(8),private :: LLintegBmax   !< Bmax parameter for the LLintegFun, set by setLLintegBmax
real(8),allocatable,dimension(:) :: LLdHRtrace,LLdHZtrace,LLdHLtrace !< flux traces for LLdHFun
integer(4) :: LLdHNTrace !< flux trace dimension for LLdHFun
integer(4),parameter :: LLGridDim = 20 !< Grid size for lambda-splines of the H-function (for McGregor CD)
real(8),parameter :: LLintegTol = 1.d-5 !< Integration error tolerance in McGregor CD
real(8),private :: LLHFun_lambda_psi !< psi value for LLHFun_lambda

real(8),allocatable,private :: LLlambda_spline_knots(:)
real(8),allocatable,private :: LLHfun_spline_coef(:)
real(8),allocatable,private :: LLfcFun_spline_coef(:)
integer(4),private :: NLLlambda_spline_knots

