/*
  Copyright (C) 2004 Thomas H. Osborne (osborne@fusion.gat.com)
 
  This program is part of the pyD3D package
 
     This program is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation; either version 2 of the License, or
     (at your option) any later version.
 
     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.
 
     You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/* ROUTINE TO GET PTDATA AND RETURN VALUES IN A 2xN DIMENSIONAL PYTHON  */
/* ARRAY OBJECT WHERE N IS THE NUMBER OF POINTS RETURNED, FIRST ROW IS */
/* TIME (INDEX 0) AND SECOND ROW IS DATA (INDEX 1) */
/* T. Osborne osborne@fusion.gat.com */

#include "Python.h"
#include "numpy/arrayobject.h"
 
#define SOF sizeof(float)
#define SOI sizeof(int)
#include <stdlib.h>
#include <stdio.h>

#ifdef NO_APPEND_FORTRAN
#define DIMA dima
#define READA reada
#define WRITEA writea
#define DIMG dimg
#define READG readg
#define WRITEG writeg
#else
#define DIMA dima_
#define READA reada_
#define WRITEA writea_
#define DIMG dimg_
#define READG readg_
#define WRITEG writeg_
#endif

extern void DIMA(char *eqdsk, int *ierr
		 , int *mco2v, int *mco2r
		 , int *nsilop0, int *magpri0, int *nfcoil0, int *nesum0
		 );
extern void READA( char *eqdsk, int *ierr
		   ,float *csilop, float *crogow, float *cmpr2, float *ccbrsp, float *eccurt
		   ,float *rco2r, float *rco2v, float *chordv , float *dco2r, float *dco2v
		   ,float *rseps, float *zseps
		   ,float *rdat, int *idat
		   ,int *nsilop, int *magpri, int *nfcoil, int *nesum, int *nco2r, int *nco2v
 		   ,char *qmflag,char *limloc
		   );
extern void WRITEA( char *eqdsk, int *ierr, int *mode
		    ,float *csilop, float *crogow, float *cmpr2, float *ccbrsp, float *eccurt
		    ,float *rco2r, float *rco2v, float *chordv , float *dco2r, float *dco2v
		    ,float *rseps, float *zseps
		    ,float *rdat, int *idat
		    ,char *qmflag, char *limloc
		    );

extern void DIMG( char *eqdsk, int *ierr
		  , int *mw, int *mh, int *nbdry, int *limitr
		  );
extern void READG( char *eqdsk, int *ierr
		   ,float *volp ,float *pprime ,float *pres ,float *ffprim ,float *fpol
		   ,float *qpsi ,float *psirz ,float *rgrid
		   ,float *pressw ,float *pwprim ,float *dmion
		   ,float *zgrid ,float *rhovn ,float *epoten
		   ,float *xlim ,float *ylim
		   ,float *rbdry ,float *zbdry
		   ,int *idat ,float *rdat
		   ,int *nw, int *nh, int *nlimit
		   ,float *ee2bdry ,float *eebdry,float *eeknt
		   ,float *ff2bdry ,float *ffbdry,float *ffknt
		   ,int *kee2bdry,int *keebdry
		   ,int *kff2bdry,int *kffbdry
		   ,int *kpp2bdry,int *kppbdry
		   ,int *kww2bdry,int *kwwbdry
		   ,float *pp2bdry ,float *ppbdry,float *ppknt
		   ,float *ww2bdry ,float *wwbdry,float *wwknt
		   ,int *read_basis
		  );
extern void WRITEG(char *eqdsk,int *ierr,int *imode
		   , float *volp, float *pprime, float *pres, float *ffprim, float *fpol
		   , float *qpsi, float *psirz, float *rgrid
		   , float *pressw, float *pwprim, float *dmion
		   , float *zgrid, float *rhovn, float *epoten
		   , float *xlim, float *ylim
		   , float *rbdry, float *zbdry
		   , int *idat, float *rdat
		   ,float *ee2bdry ,float *eebdry,float *eeknt
		   ,float *ff2bdry ,float *ffbdry,float *ffknt
		   ,int *kee2bdry,int *keebdry
		   ,int *kff2bdry,int *kffbdry
		   ,int *kpp2bdry,int *kppbdry
		   ,int *kww2bdry,int *kwwbdry
		   ,float *pp2bdry ,float *ppbdry,float *ppknt
		   ,float *ww2bdry ,float *wwbdry,float *wwknt
		   ,int *write_basis
		   );

/* DIMENSION PARAMETERS FOR AFILE ARRAYS */
#define NFCOIL 18
#define NSILOP 44
#define MAGPRI 68
#define NROGOW 1
#define NESUM  6
#define NCO2V  3
#define NCO2R  1
/* NUMBER OF REAL, INTEGER AND REAL ARRAY DATA VALUES IN A FILE */
#define AF_NRDAT 121
#define AF_NIDAT 12
#define AF_NADAT 12
#define AF_NSDAT 2

/* NAMES OF INTEGER VALUES IN A FILE */
static char *af_inames[AF_NIDAT] = {
    "ishot",   "jflag"   
    , "lflag",   "mco2v"
    , "mco2r"
    , "nlold",   "nlnew"
    , "nsilop0", "magpri0"
    , "nfcoil0", "nesum0"
    , "nvernum" 
  };
/* NAMES OF FLOAT VARAIABLES IN A FILE */
static char *af_rnames[AF_NRDAT] ={ 
    "time"
    , "tsaisq", "rcencm"
    , "bcentr", "pasmat"
    , "cpasma", "rout" 
    , "zout",   "aout"
    ,  "eout",   "doutu" 
    , "doutl",  "vout"
    , "rcurrt", "zcurrt" 
    , "qsta",   "betat"
    , "betap",  "ali" 
    ,  "oleft",  "oright"
    , "otop",   "obott" 
    , "qpsib",  "vertn"
    , "shearb", "bpolav" 
    , "s1",     "s2"
    ,  "s3",     "qout" 
    , "olefs",  "orighs"
    , "otops",  "sibdry" 
    , "areao",  "wplasm"
    , "terror", "elongm" 
    ,  "qqmagx", "cdflux"
    , "alpha",  "rttt" 
    , "psiref", "xndnt"
    , "sepexp", "obots" 
    , "btaxp",  "btaxv"
    ,  "aaq1",   "aaq2" 
    , "aaq3",   "seplim"
    , "rmagx",  "zmagx" 
    , "simagx", "taumhd"
    , "betapd", "betatd" 
    ,  "wplasmd","fluxx"
    , "vloopt", "taudia" 
    , "qmerci", "tavem"
    , "pbinj",  "rvsin"
    , "zvsin",  "rvsout"
    ,  "zvsout", "vsurfa"
    , "wpdot",  "wbdot"
    , "slantu", "slantl"
    , "zuperts","chipre"
    , "cjor95", "pp95"
    ,  "ssep",   "yyy2"
    , "xnnc",   "cprof"
    , "oring",  "cjor0"
    , "fexpan", "qqmin"
    , "chigamt","ssi01"
    ,  "fexpvs", "sepnose"
    , "ssi95",  "rqqmin"
    , "cjor99", "cj1ave"
    , "rmidin", "rmidout"
    , "psurfa", "peak"
    ,  "dminux", "dminlx"
    , "dolubaf","dolubafm"
    , "diludom","diludomm"
    , "ratsol", "rvsiu"
    , "zvsiu",  "rvsid"
    ,  "zvsid",  "rvsou"
    , "zvsou",  "rvsod"
    , "zvsod",  "condno"
    , "psin32", "psin21"
    , "rq32in", "rq21top"
    , "chilibt", "ali3"
  };

static char *af_anames[AF_NADAT] = {  "csilop","crogow","cmpr2","ccbrsp","ecurrt",
			   "rco2r","rco2v","chordv","dco2r","dco2v",
			   "rseps","zseps"};

static char *af_snames[AF_NSDAT] = { "qmflag", "limloc" };

/* DIMENSION PARAMETERS FOR GFILE ARRAYS */
#define NPOINT 500
#define NW 257
#define NH 257
#define NLIMIT 300
#define NFFCUR 18
#define NPPCUR 18
#define NPCURN NFFCUR+NPPCUR
#define NERCUR 18

/* NUMBER OF REAL, INTEGER AND REAL ARRAY DATA VALUES IN G FILE */
#define GF_NIBASIS 8
#define GF_NIDAT 8+GF_NIBASIS
#define GF_NRBASIS 4
#define GF_NRDAT 12+GF_NRBASIS
#define GF_NABASIS 20
#define GF_NADAT 18+GF_NABASIS

/* NAMES OF INTEGER VALUES IN G FILE */
static char *gf_inames[GF_NIDAT] = {  "mw"     , "mh"
				    , "nbdry"  , "limitr"
				    , "ktor"   , "nmass"
				    , "keecur" , "ishot"

				    , "keefnc", "keeknt"
				    , "kfffnc", "kffknt"
				    , "kppfnc", "kppknt"
				    , "kwwfnc", "kwwknt"

};
/* NAMES OF REAL VALUES IN G FILE */
static char *gf_rnames[GF_NRDAT] = {   "xdim"   , "zdim"
				     , "rzero"  , "zmid"
				     , "rmaxis" , "zmaxis"
				     , "ssimag" , "ssibry"
				     , "bcentr" , "cpasma"
				     , "rvtor"  , "time"

				     , "eetens", "fftens" 
				     , "pptens", "wwtens"
};
/* NAMES OF ARRAY VALUES IN G FILE */
static char *gf_anames[GF_NADAT] = {  "volp"   , "pprime" , "pres"  , "ffprim" , "fpol"
				    , "qpsi"   , "psirz"  , "rgrid"
				    , "pressw" , "pwprim" , "dmion" , "zgrid" , "rhovn"
				    , "epoten" ,  "xlim"  , "ylim"  , "rbdry" , "zbdry"

				    , "ee2bdry" , "eebdry", "eeknt"
				    , "ff2bdry" , "ffbdry", "ffknt"
				    , "kee2bdry", "keebdry"
				    , "kff2bdry", "kffbdry"
				    , "kpp2bdry", "kppbdry"
				    , "kww2bdry", "kwwbdry"
				    , "pp2bdry" , "ppbdry", "ppknt"
				    , "ww2bdry" , "wwbdry", "wwknt"
};

/* READA **************************************************************************/
static PyObject *
_reada( PyObject *self, PyObject *args ) 
{
  /* EQUIVALENCE DATA FOR FLOATS AND INTS*/
  float rdat[AF_NRDAT];
  int idat[AF_NIDAT];
  PyArrayObject *pidat[AF_NIDAT], *prdat[AF_NRDAT];

  /* ARRAYS */
  float *csilop, *crogow, *cmpr2, *ccbrsp, *ecurrt;
  float *rco2r, *rco2v, *chordv, *dco2r, *dco2v;
  float *rseps, *zseps;
  int adatdim[AF_NADAT]={ NSILOP, NROGOW, MAGPRI, NFCOIL, NESUM,
		     NCO2R, NCO2V, NCO2V, NCO2R, NCO2V,
		     2, 2 };
  int mco2v,mco2r,nsilop0,magpri0,nfcoil0,nesum0;
  PyArrayObject *padat[AF_NADAT];

  /* STRINGS */
  char *sdat[AF_NSDAT];
  char qmflag[4]={0,0,0,0},limloc[5]={0,0,0,0,0};
  PyObject *psdat[AF_NSDAT];

  PyObject *eqdsk_list, *peqdsk, *aeqdsk;
  char *eqdsk;
  int num_eqdsks=0;

  npy_intp dimensions[2];
  int ierr=0,quiet=0;
  int nidat_count=0,nrdat_count=0,nadat_count=0,nsdat_count=0;

  char error_string[256];

  register int i,j;

  /* PASS LIST OF EQDSK NAMES (STRINGS) FROM PYTHON */
  if (!PyArg_ParseTuple( args,"O|i",&eqdsk_list,&quiet )) {return NULL;}

  if ( !PyList_Check(eqdsk_list) ) {
	  PyErr_SetString(PyExc_TypeError,"FIRST ARGUMENT MUST BE LIST OF STRINGS");
	  return NULL;
  }

  /* NUMBER OF EQDSKS TO READ */
  num_eqdsks = PyList_Size(eqdsk_list);
  if ( num_eqdsks == 0 ) {
    PyErr_SetString(PyExc_TypeError,"EQDSK LIST IS 0 LENGTH");
    return NULL;
  }  
  dimensions[0]= (npy_intp)num_eqdsks;

  /* CREATE DICTIONARY TO HOLD RESULTS */
  aeqdsk = PyDict_New();
  if (aeqdsk == NULL) {
    PyErr_SetString(PyExc_RuntimeError,"CANNOT CREATE AEQDSK DICTIONARY");
    Py_XDECREF(aeqdsk);
    return NULL;
  } 
 
  /* GET DIMENSIONS OF VARIOUS QUANTATIES FROM FIRST EQDSK */
  peqdsk = PyList_GetItem( eqdsk_list, 0 );
  if ( !PyString_Check(peqdsk) ) {
    PyErr_SetString(PyExc_TypeError,"EQDSK NAME MUST BE STRING");
    Py_XDECREF(aeqdsk);
    return NULL;
  }  
  eqdsk = PyString_AsString( peqdsk );
  DIMA(eqdsk,&ierr
       ,&mco2v,&mco2r
       ,&nsilop0,&magpri0,&nfcoil0,&nesum0);
  if (ierr !=0) {
    sprintf( error_string, "ERROR READING %s", eqdsk );
    PyErr_SetString(PyExc_IOError,error_string);
    Py_XDECREF(aeqdsk);
    return NULL;
  }      
  adatdim[ 0] = nsilop0;
  adatdim[ 1] = NROGOW;
  adatdim[ 2] = magpri0;
  adatdim[ 3] = nfcoil0;
  adatdim[ 4] = nesum0;
  adatdim[ 5] = mco2r;
  adatdim[ 6] = mco2v;
  adatdim[ 7] = mco2v;
  adatdim[ 8] = mco2r;
  adatdim[ 9] = mco2v;
  adatdim[10] = 2;
  adatdim[11] = 2;

  /* CREATE PYTHON ARRAYS TO HOLD INTEGER DATA IN EQUIVALENCE */
  nidat_count=0;
  for (i=0;i<AF_NIDAT;i++) {
    pidat[i] = (PyArrayObject *)PyArray_SimpleNew(1, dimensions, PyArray_INT);
    if ( pidat[i] == NULL ) {
      nidat_count=i+1;
      PyErr_SetString(PyExc_RuntimeError,"FAIL ON CREATING INTEGER ARRAY FOR RESULTS IN READA");
      goto READA_FAIL;
    }
    if( PyDict_SetItemString( aeqdsk, af_inames[i], (PyObject *)pidat[i] ) < 0 ){
      nidat_count=i+1;
      PyErr_SetString(PyExc_RuntimeError,"FAIL ON ADDING INTEGER ARRAY TO RESULTS DICT IN READA");
      goto READA_FAIL;
    }
  }
  nidat_count=AF_NIDAT;
  /* CREATE PYTHON ARRAYS TO HOLD FLOAT DATA IN EQUIVALENCE */
  nrdat_count=0;
  for (i=0;i<AF_NRDAT;i++) {
    prdat[i] = (PyArrayObject *)PyArray_SimpleNew(1, dimensions, PyArray_FLOAT);
    if ( prdat[i] == NULL ) {
      nrdat_count=i+1;
      PyErr_SetString(PyExc_RuntimeError,"FAIL ON CREATING REAL ARRAY FOR RESULTS IN READA");
      goto READA_FAIL;
    }
    if( PyDict_SetItemString( aeqdsk, af_rnames[i], (PyObject *)prdat[i] ) < 0 ){
      nrdat_count=i+1;
      PyErr_SetString(PyExc_RuntimeError,"FAIL ON ADDING REAL ARRAY TO RESULTS DICT IN READA");
      goto READA_FAIL;
    }
  }
  nrdat_count=AF_NRDAT;
  /* CREATE PYTHON ARRAYS TO HOLD ARRAY DATA */
  nadat_count=0;
  for (i=0;i<AF_NADAT;i++) {
    dimensions[1] = (npy_intp)adatdim[i];
    padat[i] = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, PyArray_FLOAT);
    if ( padat[i] == NULL ) {
      nadat_count=i+1;
      PyErr_SetString(PyExc_RuntimeError,"FAIL ON CREATING 2D ARRAY FOR RESULTS IN READA");
      goto READA_FAIL;
    }
    if( PyDict_SetItemString( aeqdsk, af_anames[i], (PyObject *)padat[i] ) < 0 ){
      nadat_count=i+1;
      PyErr_SetString(PyExc_RuntimeError,"FAIL ON ADDING 2D ARRAY TO RESULTS DICT IN READA");
      goto READA_FAIL;
    }
  }
  nadat_count=AF_NADAT;
  nsdat_count=0;
  /* CREATE LIST TO HOLD STRING DATA */
  for (i=0;i<AF_NSDAT;i++) {
    psdat[i] = PyList_New( num_eqdsks );
    if ( psdat[i] == NULL ) {
      nsdat_count=i+1;
      PyErr_SetString(PyExc_RuntimeError,"FAIL ON CREATING LIST OF STRINGS FOR RESULTS IN READA");
      goto READA_FAIL;
    }
    if( PyDict_SetItemString( aeqdsk, af_snames[i], psdat[i] ) < 0 ){
      nsdat_count=i+1;
      PyErr_SetString(PyExc_RuntimeError,"FAIL ON ADDING LIST OF STRINGS TO RESULTS DICT IN READA");
      goto READA_FAIL;
    }
  }
  nsdat_count=AF_NSDAT;

  sdat[0] = (char *)qmflag;
  sdat[1] = (char *)limloc;

      
  for (i=0;i<num_eqdsks;i++) {

    peqdsk = PyList_GetItem( eqdsk_list, i);
    if ( !PyString_Check(peqdsk) ) {
      PyErr_SetString(PyExc_TypeError,"EQDSK NAME MUST BE STRING");
      goto READA_FAIL;
    }  
    eqdsk = PyString_AsString( peqdsk );
    if (quiet==0) {printf("%s\n",eqdsk);}

    csilop = (float *)( padat[ 0]->data + i*padat[ 0]->strides[0] );
    crogow = (float *)( padat[ 1]->data + i*padat[ 1]->strides[0] );
    cmpr2  = (float *)( padat[ 2]->data + i*padat[ 2]->strides[0] );
    ccbrsp = (float *)( padat[ 3]->data + i*padat[ 3]->strides[0] );
    ecurrt = (float *)( padat[ 4]->data + i*padat[ 4]->strides[0] );
    rco2r  = (float *)( padat[ 5]->data + i*padat[ 5]->strides[0] );
    rco2v  = (float *)( padat[ 6]->data + i*padat[ 6]->strides[0] );
    chordv = (float *)( padat[ 7]->data + i*padat[ 7]->strides[0] );
    dco2r  = (float *)( padat[ 8]->data + i*padat[ 8]->strides[0] );
    dco2v  = (float *)( padat[ 9]->data + i*padat[ 9]->strides[0] );
    rseps  = (float *) (padat[10]->data + i*padat[10]->strides[0] );
    zseps  = (float *) (padat[11]->data + i*padat[11]->strides[0] );

    READA( eqdsk, &ierr 
	   ,csilop, crogow, cmpr2, ccbrsp, ecurrt
	   ,rco2r, rco2v, chordv, dco2r, dco2v
	   ,rseps, zseps
	   ,rdat, idat
	   ,&nsilop0, &magpri0, &nfcoil0, &nesum0, &mco2r, &mco2v
	   ,qmflag, limloc
	   );

    if (ierr !=0) {
      sprintf( error_string, "ERROR READING %s", eqdsk );
      PyErr_SetString(PyExc_IOError,error_string);
      goto READA_FAIL;
    }      

    for (j=0;j<AF_NIDAT;j++) {
      *(int *)(pidat[j]->data + i*pidat[j]->strides[0] ) = idat[j];
    }
    for (j=0;j<AF_NRDAT;j++) {
      *(float *)(prdat[j]->data + i*prdat[j]->strides[0] ) = rdat[j];
    }
    for (j=0;j<AF_NSDAT;j++) {
      PyList_SetItem( psdat[j], i , PyString_FromString( sdat[j] ) );
    }
  }

  /* SINCE ARRAYS ARE IN DICTIONARY LOWER REF COUNT BY 1 */
  for (i=0;i<AF_NIDAT;i++) {Py_DECREF(pidat[i]);}
  for (i=0;i<AF_NRDAT;i++) {Py_DECREF(prdat[i]);}
  for (i=0;i<AF_NADAT;i++) {Py_DECREF(padat[i]);}
  for (i=0;i<AF_NSDAT;i++) {Py_DECREF(psdat[i]);}

  return aeqdsk;

 READA_FAIL:
  PyDict_Clear( aeqdsk );
  Py_DECREF( aeqdsk );
  for (i=0;i<nidat_count;i++) {Py_DECREF(pidat[i]);}
  for (i=0;i<nrdat_count;i++) {Py_DECREF(prdat[i]);}
  for (i=0;i<nadat_count;i++) {Py_DECREF(padat[i]);}
  for (i=0;i<nsdat_count;i++) {Py_DECREF(psdat[i]);}
  return NULL;
}
/* WRITEA **************************************************************************/
static PyObject *
_writea( PyObject *self, PyObject *args ) 
{

  /* EQUIVALENCE DATA FOR FLOATS AND INTS*/
  float rdat[AF_NRDAT];
  int idat[AF_NIDAT];
  PyArrayObject *pidat[AF_NIDAT], *prdat[AF_NRDAT];

  /* ARRAYS */
  float *csilop, *crogow, *cmpr2, *ccbrsp, *ecurrt;
  float *rco2r, *rco2v, *chordv, *dco2r, *dco2v;
  float *rseps, *zseps;
  int adatdim[AF_NADAT]={ NSILOP, NROGOW, MAGPRI, NFCOIL, NESUM,
		     NCO2R, NCO2V, NCO2V, NCO2R, NCO2V,
		     2, 2 };
  PyArrayObject *padat[AF_NADAT], *paobj;

  int mco2v,mco2r,nsilop0,magpri0,nfcoil0,nesum0;
  char *adimsnames[6] = {"nsilop0","magpri0","nfcoil0","nesum0","mco2r","mco2v"};
  int *adims[6]={ &nsilop0, &magpri0, &nfcoil0, &nesum0, &mco2r, &mco2v };

  /* STRINGS */
  char *sdat[AF_NSDAT];
  PyObject *psdat[AF_NSDAT];
  char *qmflag={"   "},*limloc={"    "};

  PyObject *eqdsk_list, *peqdsk, *aeqdsk, *mode_list, *pmode, *pobj;
  char *eqdsk;
  int num_eqdsks=0;

  npy_intp dimensions[2];
  int mode=0,quiet=0;

  register int i,j, k;

  int ierr=0;
  char error_string[256];

  /* PASS LIST OF EQDSK NAMES (STRINGS), SAVE MODE AND DATA DICTIONARY FROM PYTHON */
  if (!PyArg_ParseTuple( args,"OOO|i",&eqdsk_list, &mode_list, &aeqdsk, &quiet)) {return NULL;}

  if ( !PyList_Check(eqdsk_list) ) {
	  PyErr_SetString(PyExc_TypeError,"FIRST ARGUMENT MUST BE LIST OF STRINGS");
	  return NULL;
  }
  if ( !PyList_Check(mode_list) ) {
	  PyErr_SetString(PyExc_TypeError,"SECOND ARGUMENT MUST BE LIST OF INTEGERS");
	  return NULL;
  }
  if ( !PyDict_Check(aeqdsk) ) {
	  PyErr_SetString(PyExc_TypeError,"THIRD ARGUMENT MUST BE DICTIONARY OF ARRAYS");
	  return NULL;
  }

  num_eqdsks = PyList_Size(eqdsk_list);
  if ( num_eqdsks == 0 ) {
    PyErr_SetString(PyExc_TypeError,"EQDSK LIST IS 0 LENGTH");
    return NULL;
  }  
  if (num_eqdsks != PyList_Size(eqdsk_list)){
	  PyErr_SetString(PyExc_RuntimeError,"LENGTH OF EQDSK LIST MUST = LENGTH OF SAVE_MODE LIST");
	  return NULL;
  }

  /* GET nsilop0,magpri0,nfcoil0,nesum0,mco2r,mco2v FROM DICTIONARY IN CASE OTHER ENTRIES 
     NEED TO BE CONSTRUCTED */
  for (i=0;i<6;i++){
    pobj = PyDict_GetItemString( aeqdsk, adimsnames[i] );
    if ( pobj == NULL) {
      sprintf(error_string,"%s NOT IN DICTIONARY",adimsnames[i]);
      PyErr_SetString(PyExc_RuntimeError,error_string);
      return NULL;
    }
    paobj = (PyArrayObject *)pobj;
    *adims[i] = *(int *)(paobj->data);
    for (j=0;j<(int)(paobj->dimensions[0]);j++) {
      if (*adims[i] != *(int *)(paobj->data + j*paobj->strides[0]) ) {
	sprintf(error_string,"%s NOT THE SAME FOR ALL ENTRIES",adimsnames[i]);
	PyErr_SetString(PyExc_RuntimeError,error_string);
	return NULL;
      }
    }
  }
/*   printf("XXXXXXX %i %i %i %i %i %i\n",nsilop0,magpri0,nfcoil0,nesum0,mco2r,mco2v); */
  adatdim[ 0] = nsilop0;
  adatdim[ 1] = NROGOW;
  adatdim[ 2] = magpri0;
  adatdim[ 3] = nfcoil0;
  adatdim[ 4] = nesum0;
  adatdim[ 5] = mco2r;
  adatdim[ 6] = mco2v;
  adatdim[ 7] = mco2v;
  adatdim[ 8] = mco2r;
  adatdim[ 9] = mco2v;
  adatdim[10] = 2;
  adatdim[11] = 2;

  /* INT EQUIVALENCE DATA FROM DICTIONARY */
  dimensions[0] = (npy_intp)num_eqdsks;
  for (i=0;i<AF_NIDAT;i++) {
    pobj = PyDict_GetItemString( aeqdsk, af_inames[i] );
    Py_XINCREF(pobj);
    if ( pobj == NULL ) {
      if(quiet==0){printf("WARNING %s NOT IN DATA DICTIONARY, SETING TO 0\n",af_inames[i]);}
      paobj = (PyArrayObject *)PyArray_SimpleNew(1, dimensions, PyArray_INT);
      for (j=0;j<num_eqdsks;j++) { *(int *)(paobj->data + j*paobj->strides[0]) = 0;}
      Py_XDECREF(pobj);
      pobj = (PyObject *)paobj;
    }
    if ( !PyArray_Check(pobj) ) {
      PyErr_SetString(PyExc_TypeError,"INT AND FLOAT ELEMENTS OF DICTIONARY MUST BE ARRAYS");
      Py_XDECREF(pobj);
      goto FAIL_WRITEA;
    }
    pidat[i] = (PyArrayObject *)pobj;
    if ((int)(pidat[i]->dimensions[0]) != num_eqdsks) {
	  PyErr_SetString(PyExc_RuntimeError,"LENGTH OF EQDSK DATA ARRAY MUST = LENGTH OF EQDSK LIST");
	  goto FAIL_WRITEA;
    }
  }
  /* FLOAT EQUIVALENCE DATA FROM DICTIONARY */
  for (i=0;i<AF_NRDAT;i++) {
    pobj = PyDict_GetItemString( aeqdsk, af_rnames[i] );
    Py_XINCREF(pobj);
    if ( pobj == NULL ) {
      if(quiet==0){printf("WARNING %s NOT IN DATA DICTIONARY, SETING TO 0.0\n",af_rnames[i]);}
      paobj = (PyArrayObject *)PyArray_SimpleNew(1, dimensions, PyArray_FLOAT);
      for (j=0;j<num_eqdsks;j++) { *(float *)(paobj->data + j*paobj->strides[0]) = 0.0;}
      Py_XDECREF(pobj);
      pobj = (PyObject *)paobj;
    }
    if ( !PyArray_Check(pobj) ) {
      PyErr_SetString(PyExc_TypeError,"INT AND FLOAT ELEMENTS OF DICTIONARY MUST BE ARRAYS");
      Py_XDECREF(pobj);
      goto FAIL_WRITEA;
    }
    prdat[i] = (PyArrayObject *)pobj;
    if ((int)(prdat[i]->dimensions[0]) != num_eqdsks) {
	  PyErr_SetString(PyExc_RuntimeError,"LENGTH OF EQDSK DATA ARRAY MUST = LENGTH OF EQDSK LIST");
	  goto FAIL_WRITEA;
    }
  }
  /* ARRAY DATA FROM DICTIONARY */
  for (i=0;i<AF_NADAT;i++) {
    pobj = PyDict_GetItemString( aeqdsk, af_anames[i] );
    Py_XINCREF(pobj);
    if ( pobj == NULL ) {
      dimensions[1] = (npy_intp)adatdim[i];
      if(quiet==0){printf("WARNING %s NOT IN DATA DICTIONARY, SETING TO 0.0\n",af_anames[i]);}
      paobj = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, PyArray_FLOAT);
      for (j=0;j<num_eqdsks;j++) { 
	for (k=0;k<adatdim[i];k++){
	  *(float *)(paobj->data + j*paobj->strides[0] + k*paobj->strides[1]) = 0.0;
	}
      }
      Py_XDECREF(pobj);
      pobj = (PyObject *)paobj;
    }
    if ( !PyArray_Check(pobj) ) {
      PyErr_SetString(PyExc_TypeError,"INT AND FLOAT ELEMENTS OF DICTIONARY MUST BE ARRAYS");
      Py_XDECREF(pobj);
      goto FAIL_WRITEA;
    }
    padat[i] = (PyArrayObject *)pobj;
    if ((int)(padat[i]->dimensions[0]) != num_eqdsks) {
	  PyErr_SetString(PyExc_RuntimeError,"LENGTH OF EQDSK ARRAY MUST = LENGTH OF EQDSK LIST");
	  goto FAIL_WRITEA;
    }
  }
  /* STRING DATA FROM DICTIONARY */  
  sdat[0] = (char *)qmflag;
  sdat[1] = (char *)limloc;
  for (i=0;i<AF_NSDAT;i++) {
    pobj = PyDict_GetItemString( aeqdsk, af_snames[i] );
    Py_XINCREF(pobj);
    if ( pobj == NULL ) {
      if(quiet==0){printf("WARNING %s NOT IN DATA DICTIONARY, SETING TO BLANKS\n",af_snames[i]);}
      Py_XDECREF(pobj);
      pobj = PyList_New(num_eqdsks);
      for (j=0;j<num_eqdsks;j++) {
	PyList_SetItem( pobj, j, PyString_FromString( sdat[i] ) );
      }
    }
    if ( !PyList_Check(pobj) ) {
      PyErr_SetString(PyExc_TypeError,"INT AND FLOAT ELEMENTS OF DICTIONARY MUST BE ARRAYS");
      Py_XDECREF(pobj);
      goto FAIL_WRITEA;
    }
    psdat[i] = pobj;
    if (PyList_Size(psdat[i]) != num_eqdsks) {
	  PyErr_SetString(PyExc_RuntimeError,
			  "LENGTH OF EQDSK STRING DATA LIST MUST = LENGTH OF EQDSK LIST");
	  goto FAIL_WRITEA;
    }
  }

  for (i=0;i<num_eqdsks;i++) {

    peqdsk = PyList_GetItem( eqdsk_list, i);
    if ( !PyString_Check(peqdsk) ) {
      PyErr_SetString(PyExc_TypeError,"EQDSK NAME MUST BE STRING");
      goto FAIL_WRITEA;
    }  
    eqdsk = PyString_AsString( peqdsk );

    pmode = PyList_GetItem( mode_list, i);
    if ( !PyInt_Check(pmode) ) {
      PyErr_SetString(PyExc_TypeError,"MODE TYPE MUST BE INTEGER");
      goto FAIL_WRITEA;
    }  
    mode = (int)PyInt_AsLong( pmode);
    if (quiet==0) {printf("%s %i\n",eqdsk,mode);}

    csilop = (float *)( padat[ 0]->data + i*padat[ 0]->strides[0] );
    crogow = (float *)( padat[ 1]->data + i*padat[ 1]->strides[0] );
    cmpr2  = (float *)( padat[ 2]->data + i*padat[ 2]->strides[0] );
    ccbrsp = (float *)( padat[ 3]->data + i*padat[ 3]->strides[0] );
    ecurrt = (float *)( padat[ 4]->data + i*padat[ 4]->strides[0] );
    rco2r  = (float *)( padat[ 5]->data + i*padat[ 5]->strides[0] );
    rco2v  = (float *)( padat[ 6]->data + i*padat[ 6]->strides[0] );
    chordv = (float *)( padat[ 7]->data + i*padat[ 7]->strides[0] );
    dco2r  = (float *)( padat[ 8]->data + i*padat[ 8]->strides[0] );
    dco2v  = (float *)( padat[ 9]->data + i*padat[ 9]->strides[0] );
    rseps  = (float *) (padat[10]->data + i*padat[10]->strides[0] );
    zseps  = (float *) (padat[11]->data + i*padat[11]->strides[0] );

    for (j=0;j<AF_NIDAT;j++) {
      idat[j] = *(int *)(pidat[j]->data + i*pidat[j]->strides[0] );
    }
    for (j=0;j<AF_NRDAT;j++) {
      rdat[j] = *(float *)(prdat[j]->data + i*prdat[j]->strides[0] );
    }
    for (j=0;j<AF_NSDAT;j++) {
      sdat[j] = PyString_AsString( PyList_GetItem(psdat[j],i) );
    }

    WRITEA( eqdsk, &ierr, &mode
	    ,csilop, crogow, cmpr2, ccbrsp, ecurrt
	    ,rco2r, rco2v, chordv, dco2r, dco2v
	    ,rseps, zseps
	    ,rdat, idat
	    ,sdat[0], sdat[1] 
	    );

    if (ierr !=0) {
      sprintf( error_string, "ERROR WRITING %s", eqdsk );
      PyErr_SetString(PyExc_IOError,error_string);
      goto FAIL_WRITEA;
    }      

  }
  /* SINCE ARRAYS ARE IN DICTIONARY LOWER REF COUNT BY 1 */
  for (i=0;i<AF_NIDAT;i++) {Py_DECREF(pidat[i]);}
  for (i=0;i<AF_NRDAT;i++) {Py_DECREF(prdat[i]);}
  for (i=0;i<AF_NADAT;i++) {Py_DECREF(padat[i]);}
  for (i=0;i<AF_NSDAT;i++) {Py_DECREF(psdat[i]);}

  Py_INCREF(Py_None); 
  return(Py_None);

 FAIL_WRITEA:
  for (i=0;i<AF_NIDAT;i++) {Py_XDECREF(pidat[i]);}
  for (i=0;i<AF_NRDAT;i++) {Py_XDECREF(prdat[i]);}
  for (i=0;i<AF_NADAT;i++) {Py_XDECREF(padat[i]);}
  for (i=0;i<AF_NSDAT;i++) {Py_XDECREF(psdat[i]);}
  return NULL;
}
/* READG **************************************************************************/
static PyObject *
_readg( PyObject *self, PyObject *args ) 
{
  /* EQUIVALENCE DATA FOR FLOATS AND INTS*/
  float rdat[GF_NRDAT];
  int idat[GF_NIDAT];
  PyArrayObject *pidat[GF_NIDAT], *prdat[GF_NRDAT];

  /* ARRAYS */
  float *volp=NULL, *pprime=NULL, *pres=NULL, *ffprim=NULL, *fpol=NULL;
  float *qpsi=NULL, *psirz=NULL, *rgrid=NULL;
  float *pressw=NULL, *pwprim=NULL, *dmion=NULL, *zgrid=NULL, *rhovn=NULL;
  float *epoten=NULL, *xlim=NULL, *ylim=NULL, *rbdry=NULL, *zbdry=NULL;
  float *ee2bdry=NULL  , *eebdry=NULL , *eeknt=NULL;
  float *ff2bdry=NULL  , *ffbdry=NULL , *ffknt=NULL;
  int  *kee2bdry=NULL , *keebdry=NULL;
  int  *kff2bdry=NULL , *kffbdry=NULL;
  int  *kpp2bdry=NULL , *kppbdry=NULL;
  int  *kww2bdry=NULL , *kwwbdry=NULL;
  float *pp2bdry=NULL  , *ppbdry=NULL , *ppknt=NULL;
  float *ww2bdry=NULL  , *wwbdry=NULL , *wwknt=NULL;
  int adatdim0[GF_NADAT] = {  
      NW     , NW     , NW     , NW      , NW
    , NW     , NW     , NW
    , NW     , NW     , NW     , NH     , NW
    , NW     , NLIMIT , NLIMIT , NPOINT , NPOINT
    , NERCUR , NERCUR , NERCUR
    , NPCURN , NPCURN , NPCURN
    , NERCUR , NERCUR
    , NPCURN , NPCURN
    , NPCURN , NPCURN
    , NPCURN , NPCURN
    , NPCURN , NPCURN , NPCURN
    , NPCURN , NPCURN , NPCURN
  };
  int adatdim1[GF_NADAT] = {  
      0 , 0  , 0 , 0 , 0
    , 0 , NH , 0
    , 0 , 0  , 0 , 0 , 0
    , 0 , 0  , 0 , 0 , 0 
    , 0 , 0  , 0
    , 0 , 0  , 0
    , 0 , 0
    , 0 , 0
    , 0 , 0
    , 0 , 0
    , 0 , 0  , 0
    , 0 , 0  , 0
  };
  int adattype[GF_NADAT] = {  
      PyArray_FLOAT , PyArray_FLOAT  , PyArray_FLOAT , PyArray_FLOAT , PyArray_FLOAT
    , PyArray_FLOAT , PyArray_FLOAT  , PyArray_FLOAT
    , PyArray_FLOAT , PyArray_FLOAT  , PyArray_FLOAT , PyArray_FLOAT , PyArray_FLOAT
    , PyArray_FLOAT , PyArray_FLOAT  , PyArray_FLOAT , PyArray_FLOAT , PyArray_FLOAT 
    , PyArray_FLOAT , PyArray_FLOAT  , PyArray_FLOAT
    , PyArray_FLOAT , PyArray_FLOAT  , PyArray_FLOAT
    , PyArray_INT   , PyArray_INT
    , PyArray_INT   , PyArray_INT
    , PyArray_INT   , PyArray_INT
    , PyArray_INT   , PyArray_INT
    , PyArray_FLOAT , PyArray_FLOAT  , PyArray_FLOAT
    , PyArray_FLOAT , PyArray_FLOAT  , PyArray_FLOAT
  };
  int mw,mh,nbdry,limitr;
  PyArrayObject *padat[GF_NADAT], *prbdry=NULL, *pzbdry=NULL;

  PyObject *eqdsk_list=NULL, *peqdsk=NULL, *aeqdsk=NULL;
  char *eqdsk=NULL;
  int num_eqdsks=0;

  npy_intp dimensions[3];
  int ierr=0,quiet=0,nbdy_max=0;

  char error_string[256];

  int read_basis=0,gf_nidat=GF_NIDAT,gf_nrdat=GF_NRDAT,gf_nadat=GF_NADAT;
  int basis_readable=1,nidat_count=0,nrdat_count=0,nadat_count=0;

  register int i,j;

  /* PASS LIST OF EQDSK NAMES (STRINGS) FROM PYTHON */
  if (!PyArg_ParseTuple( args,"O|ii",&eqdsk_list,&quiet,&read_basis )) {return NULL;}

  if ( !PyList_Check(eqdsk_list) ) {
	  PyErr_SetString(PyExc_TypeError,"FIRST ARGUMENT MUST BE LIST OF STRINGS");
	  return NULL;
  }
  if ( read_basis==0 ) {
    gf_nidat = GF_NIDAT - GF_NIBASIS;
    gf_nrdat = GF_NRDAT - GF_NRBASIS; 
    gf_nadat = GF_NADAT - GF_NABASIS; 
  }

  /* NUMBER OF EQDSKS TO READ */
  num_eqdsks = PyList_Size(eqdsk_list);
  if ( num_eqdsks == 0 ) {
    PyErr_SetString(PyExc_TypeError,"EQDSK LIST IS 0 LENGTH");
    return NULL;
  }  
  dimensions[0]=(npy_intp)num_eqdsks;

  /* GET DIMENSIONS OF VARIOUS QUANTATIES FROM FIRST EQDSK */
  peqdsk = PyList_GetItem( eqdsk_list, 0 );
  if ( !PyString_Check(peqdsk) ) {
    PyErr_SetString(PyExc_TypeError,"EQDSK NAME MUST BE STRING");
    return NULL;
  }  
  eqdsk = PyString_AsString( peqdsk );
  DIMG(eqdsk,&ierr
       ,&mw,&mh,&nbdry,&limitr );
  if (ierr !=0) {
    sprintf( error_string, "ERROR READING %s", eqdsk );
    PyErr_SetString(PyExc_IOError,error_string);
    return NULL;
  }
  for (i=0;i<14;i++) {adatdim0[i] = mw;}
  adatdim0[11] = mh;
  adatdim0[14] = limitr;
  adatdim0[15] = limitr;
  adatdim1[ 6] = mw;
  adatdim0[ 6] = mh;

  /* CREATE DICTIONARY TO HOLD RESULTS */
  aeqdsk = PyDict_New();
  if (aeqdsk == NULL) {
    PyErr_SetString(PyExc_RuntimeError,"FAIL ON CREATE DICTIONARY TO HOLD RESULTS IN READG");
    return NULL;
  } 
  PyDict_Clear(aeqdsk);
 
  /* CREATE PYTHON ARRAYS TO HOLD INTEGER DATA IN EQUIVALENCE */
  nidat_count=0;
  for (i=0;i<gf_nidat;i++) {
    pidat[i] = (PyArrayObject *)PyArray_SimpleNew(1, dimensions, PyArray_INT);
    if ( pidat[i] == NULL ) {
      nidat_count=i+1;
      PyErr_SetString(PyExc_RuntimeError,"FAIL ON CREATING INTEGER ARRAY FOR RESULTS IN READG");
      goto READG_FAIL;
    }
    if( PyDict_SetItemString( aeqdsk, gf_inames[i], (PyObject *)pidat[i] ) < 0 ) {
      nidat_count=i+1;
      PyErr_SetString(PyExc_RuntimeError,"FAIL ON ADDING INTEGER ARRAY TO RESULTS DICT IN READG");
      goto READG_FAIL;
    }
  }
  nidat_count = gf_nidat;
  /* CREATE PYTHON ARRAYS TO HOLD FLOAT DATA IN EQUIVALENCE */
  nrdat_count=0;
  for (i=0;i<gf_nrdat;i++) {
    prdat[i] = (PyArrayObject *)PyArray_SimpleNew(1, dimensions, PyArray_FLOAT);
    if ( prdat[i] == NULL ) {
      nrdat_count=i+1;
      PyErr_SetString(PyExc_RuntimeError,"FAIL ON CREATING REAL ARRAY FOR RESULTS IN READG");
      goto READG_FAIL;
    }
    if( PyDict_SetItemString( aeqdsk, gf_rnames[i], (PyObject *)prdat[i] ) < 0 ){
      nrdat_count=i+1;
      PyErr_SetString(PyExc_RuntimeError,"FAIL ON ADDING REAL ARRAY TO RESULTS DICT IN READG");
      goto READG_FAIL;
    }
  }
  nrdat_count = gf_nrdat;
  /* CREATE PYTHON ARRAYS TO HOLD ARRAY DATA */
  nadat_count=0;
  for (i=0;i<gf_nadat;i++) {
    dimensions[1] = (npy_intp)adatdim0[i];
    if (adatdim1[i] == 0) {
      padat[i] = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, adattype[i] );
      if ( padat[i] == NULL ) {
	nadat_count=i+1;
	PyErr_SetString(PyExc_RuntimeError,"FAIL ON CREATING 2D ARRAY FOR RESULTS IN READG");
	goto READG_FAIL;
      }
    }
    else {
      dimensions[2] = adatdim1[i];
      padat[i] = (PyArrayObject *)PyArray_SimpleNew(3, dimensions, adattype[i] );
      if ( padat[i] == NULL ) {
	nadat_count=i+1;
	PyErr_SetString(PyExc_RuntimeError,"FAIL ON CREATING 3D ARRAY FOR RESULTS IN READG");
	goto READG_FAIL;
      }
    }
    if( PyDict_SetItemString( aeqdsk, gf_anames[i], (PyObject *)padat[i] ) < 0 ){
      nadat_count=i+1;
      PyErr_SetString(PyExc_RuntimeError,"FAIL ON ADDING 2D OR 3D ARRAY TO RESULTS DICT IN READG");
      goto READG_FAIL;
    }
  }
  nadat_count = gf_nadat;

  basis_readable = read_basis;
  for (i=0;i<num_eqdsks;i++) {

    peqdsk = PyList_GetItem( eqdsk_list, i);
    if ( !PyString_Check(peqdsk) ) {
      PyErr_SetString(PyExc_TypeError,"EQDSK NAME MUST BE STRING");
      goto READG_FAIL;
    }  
    eqdsk = PyString_AsString( peqdsk );
    if (quiet==0) {printf("%s\n",eqdsk);}

    volp =   (float *)( padat[ 0]->data + i*padat[ 0]->strides[0] );
    pprime = (float *)( padat[ 1]->data + i*padat[ 1]->strides[0] );
    pres =   (float *)( padat[ 2]->data + i*padat[ 2]->strides[0] );
    ffprim = (float *)( padat[ 3]->data + i*padat[ 3]->strides[0] );
    fpol =   (float *)( padat[ 4]->data + i*padat[ 4]->strides[0] );
    qpsi =   (float *)( padat[ 5]->data + i*padat[ 5]->strides[0] );
    psirz =  (float *)( padat[ 6]->data + i*padat[ 6]->strides[0] );
    rgrid =  (float *)( padat[ 7]->data + i*padat[ 7]->strides[0] );
    pressw = (float *)( padat[ 8]->data + i*padat[ 8]->strides[0] );
    pwprim = (float *)( padat[ 9]->data + i*padat[ 9]->strides[0] );
    dmion =  (float *)( padat[10]->data + i*padat[10]->strides[0] );
    zgrid =  (float *)( padat[11]->data + i*padat[11]->strides[0] );
    rhovn =  (float *)( padat[12]->data + i*padat[12]->strides[0] );
    epoten = (float *)( padat[13]->data + i*padat[13]->strides[0] );
    xlim =   (float *)( padat[14]->data + i*padat[14]->strides[0] );
    ylim =   (float *)( padat[15]->data + i*padat[15]->strides[0] );
    rbdry =  (float *)( padat[16]->data + i*padat[16]->strides[0] );
    zbdry =  (float *)( padat[17]->data + i*padat[17]->strides[0] );
    if ( read_basis ) {
      ee2bdry= (float *)( padat[18]->data + i*padat[18]->strides[0] ); 
      eebdry=  (float *)( padat[19]->data + i*padat[19]->strides[0] );
      eeknt=   (float *)( padat[20]->data + i*padat[20]->strides[0] );
      ff2bdry= (float *)( padat[21]->data + i*padat[21]->strides[0] ); 
      ffbdry=  (float *)( padat[22]->data + i*padat[22]->strides[0] );
      ffknt=   (float *)( padat[23]->data + i*padat[23]->strides[0] );
      kee2bdry=(int *)( padat[24]->data + i*padat[24]->strides[0] );
      keebdry= (int *)( padat[25]->data + i*padat[25]->strides[0] );
      kff2bdry=(int *)( padat[26]->data + i*padat[26]->strides[0] );
      kffbdry= (int *)( padat[27]->data + i*padat[27]->strides[0] );
      kpp2bdry=(int *)( padat[28]->data + i*padat[28]->strides[0] );
      kppbdry= (int *)( padat[29]->data + i*padat[29]->strides[0] );
      kww2bdry=(int *)( padat[30]->data + i*padat[30]->strides[0] );
      kwwbdry= (int *)( padat[31]->data + i*padat[31]->strides[0] );
      pp2bdry= (float *)( padat[32]->data + i*padat[32]->strides[0] ); 
      ppbdry=  (float *)( padat[33]->data + i*padat[33]->strides[0] );
      ppknt=   (float *)( padat[34]->data + i*padat[34]->strides[0] );
      ww2bdry= (float *)( padat[35]->data + i*padat[35]->strides[0] ); 
      wwbdry=  (float *)( padat[36]->data + i*padat[36]->strides[0] );
      wwknt=   (float *)( padat[37]->data + i*padat[37]->strides[0] );
    }

    READG(
	   eqdsk, &ierr
	  , volp, pprime, pres, ffprim, fpol
	  , qpsi, psirz, rgrid
	  , pressw, pwprim, dmion
	  , zgrid, rhovn, epoten
	  , xlim, ylim
	  , rbdry, zbdry
	  , idat, rdat
	  , &mw, &mh, &limitr
	  , ee2bdry , eebdry, eeknt
	  , ff2bdry , ffbdry, ffknt
	  , kee2bdry, keebdry
	  , kff2bdry, kffbdry
	  , kpp2bdry, kppbdry
	  , kww2bdry, kwwbdry
	  , pp2bdry , ppbdry, ppknt
	  , ww2bdry , wwbdry, wwknt
	  , &basis_readable
          );

    if (ierr !=0) {
      sprintf( error_string, "ERROR READING %s", eqdsk );
      PyErr_SetString(PyExc_IOError,error_string);
      goto READG_FAIL;
    }

    if ( basis_readable == 0 && read_basis == 1 ) {
/*       printf( "readg: WRARNING, CANNOT READ BASIS NAMELIST IN GFILE\n" ); */
/*       printf( "                 NO BASIS DATA WILL BE RETURNED\n" ); */
      read_basis = 0;
      basis_readable = 0;
      for ( j=GF_NIDAT - GF_NIBASIS;j<GF_NIDAT;j++ ) {
	PyDict_DelItemString( aeqdsk, gf_inames[j] );
	Py_DECREF( pidat[j] ); 
      }
      for ( j=GF_NRDAT - GF_NRBASIS;j<GF_NRDAT;j++ ) { 
	PyDict_DelItemString( aeqdsk, gf_rnames[j] );
	Py_DECREF( prdat[j] ); 
      }
      for ( j=GF_NADAT - GF_NABASIS;j<GF_NADAT;j++ ) { 
	PyDict_DelItemString( aeqdsk, gf_anames[j] );
	Py_DECREF( padat[j] ); 
	ee2bdry= NULL;
	eebdry=  NULL;
	eeknt=   NULL;
	ff2bdry= NULL;
	ffbdry=  NULL;
	ffknt=   NULL;
	kee2bdry=NULL;
	keebdry= NULL;
	kff2bdry=NULL;
	kffbdry= NULL;
	kpp2bdry=NULL;
	kppbdry= NULL;
	kww2bdry=NULL;
	kwwbdry= NULL;
	pp2bdry= NULL;
	ppbdry=  NULL;
	ppknt=   NULL;
	ww2bdry= NULL;
	wwbdry=  NULL;
	wwknt=   NULL;
      }
      gf_nidat = GF_NIDAT - GF_NIBASIS;
      gf_nrdat = GF_NRDAT - GF_NRBASIS; 
      gf_nadat = GF_NADAT - GF_NABASIS;
      nidat_count = gf_nidat;
      nrdat_count = gf_nrdat;
      nadat_count = gf_nadat;
    }

    for (j=0;j<gf_nidat;j++) {
      *(int *)(pidat[j]->data + i*pidat[j]->strides[0] ) = idat[j];
    }
    for (j=0;j<gf_nrdat;j++) {
      *(float *)(prdat[j]->data + i*prdat[j]->strides[0] ) = rdat[j];
    }

    if ( nbdy_max < idat[2] ) { nbdy_max = idat[2]; }
  }

  /* FIX RBOUNDARY SO THAT ARRAY IS NO LAGER THAN THE MAX LENGHT OF THE TIMES READ IN */
  PyDict_DelItemString( aeqdsk, "rbdry");
  PyDict_DelItemString( aeqdsk, "zbdry");
  dimensions[1] = (npy_intp)nbdy_max;
  prbdry = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, PyArray_FLOAT);
  if ( prbdry == NULL ) {
    PyErr_SetString(PyExc_RuntimeError,"FAIL ON CREATING prbdry ARRAY FOR RESULTS IN READG");
    goto READG_FAIL;
  }
  for (i=0;i<num_eqdsks;i++) {
    for (j=0;j<nbdy_max;j++) {
      *(float *)(prbdry->data + i*prbdry->strides[0] + j*prbdry->strides[1]) =
	*(float *)(padat[16]->data + i*padat[16]->strides[0] + j*padat[16]->strides[1]);
    }
  }
  if( PyDict_SetItemString( aeqdsk, "rbdry", (PyObject *)prbdry ) < 0 ){
    PyErr_SetString(PyExc_RuntimeError,"FAIL ON ADDING rbdry ARRAY TO RESULTS DICT IN READG");
    goto READG_FAIL;
  }
  Py_DECREF(prbdry);
  pzbdry = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, PyArray_FLOAT);
  if ( pzbdry == NULL ) {
    PyErr_SetString(PyExc_RuntimeError,"FAIL ON CREATING pzbdry ARRAY FOR RESULTS IN READG");
    goto READG_FAIL;
  }
  for (i=0;i<num_eqdsks;i++) {
    for (j=0;j<nbdy_max;j++) {
      *(float *)(pzbdry->data + i*pzbdry->strides[0] + j*pzbdry->strides[1]) =
	*(float *)(padat[17]->data + i*padat[17]->strides[0] + j*padat[17]->strides[1]);
    }
  }
  if( PyDict_SetItemString( aeqdsk, "zbdry", (PyObject *)pzbdry ) < 0 ) {
    PyErr_SetString(PyExc_RuntimeError,"FAIL ON ADDING zbdry ARRAY TO RESULTS DICT IN READG");
    goto READG_FAIL;
  }
  Py_DECREF(pzbdry);

  /* SINCE ARRAYS ARE IN DICTIONARY LOWER REF COUNT BY 1 */
  for (i=0;i<gf_nidat;i++) {Py_DECREF(pidat[i]);}
  for (i=0;i<gf_nrdat;i++) {Py_DECREF(prdat[i]);}
  for (i=0;i<gf_nadat;i++) {Py_DECREF(padat[i]);}

  return aeqdsk;

 READG_FAIL:
  PyDict_Clear( aeqdsk );
  Py_DECREF( aeqdsk );
  for (i=0;i<nidat_count;i++) {Py_DECREF(pidat[i]);}
  for (i=0;i<nrdat_count;i++) {Py_DECREF(prdat[i]);}
  for (i=0;i<nadat_count;i++) {Py_DECREF(padat[i]);}
  Py_XDECREF( prbdry );
  Py_XDECREF( pzbdry );
  return NULL;
}
/* WRITEG **************************************************************************/
static PyObject *
_writeg( PyObject *self, PyObject *args ) 
{
  /* EQUIVALENCE DATA FOR FLOATS AND INTS*/
  float rdat[GF_NRDAT];
  int idat[GF_NIDAT];
  PyArrayObject *pidat[GF_NIDAT], *prdat[GF_NRDAT];

  /* ARRAYS */
  float *volp, *pprime, *pres, *ffprim, *fpol;
  float *qpsi, *psirz, *rgrid;
  float *pressw, *pwprim, *dmion, *zgrid, *rhovn;
  float *epoten, *xlim, *ylim, *rbdry, *zbdry;
  float *ee2bdry=NULL  , *eebdry=NULL , *eeknt=NULL;
  float *ff2bdry=NULL  , *ffbdry=NULL , *ffknt=NULL;
  int  *kee2bdry=NULL , *keebdry=NULL;
  int  *kff2bdry=NULL , *kffbdry=NULL;
  int  *kpp2bdry=NULL , *kppbdry=NULL;
  int  *kww2bdry=NULL , *kwwbdry=NULL;
  float *pp2bdry=NULL  , *ppbdry=NULL , *ppknt=NULL;
  float *ww2bdry=NULL  , *wwbdry=NULL , *wwknt=NULL;
  int adatdim0[GF_NADAT] = {  
      NW     , NW     , NW     , NW      , NW
    , NW     , NW     , NW
    , NW     , NW     , NW     , NH     , NW
    , NW     , NLIMIT , NLIMIT , NPOINT , NPOINT
    , NERCUR , NERCUR , NERCUR
    , NPCURN , NPCURN , NPCURN
    , NERCUR , NERCUR
    , NPCURN , NPCURN
    , NPCURN , NPCURN
    , NPCURN , NPCURN
    , NPCURN , NPCURN , NPCURN
    , NPCURN , NPCURN , NPCURN
  };
  int adatdim1[GF_NADAT] = {  
      0 , 0  , 0 , 0 , 0
    , 0 , NH , 0
    , 0 , 0  , 0 , 0 , 0
    , 0 , 0  , 0 , 0 , 0 
    , 0 , 0  , 0
    , 0 , 0  , 0
    , 0 , 0
    , 0 , 0
    , 0 , 0
    , 0 , 0
    , 0 , 0  , 0
    , 0 , 0  , 0
  };
  int adattype[GF_NADAT] = {  
      PyArray_FLOAT , PyArray_FLOAT  , PyArray_FLOAT , PyArray_FLOAT , PyArray_FLOAT
    , PyArray_FLOAT , PyArray_FLOAT  , PyArray_FLOAT
    , PyArray_FLOAT , PyArray_FLOAT  , PyArray_FLOAT , PyArray_FLOAT , PyArray_FLOAT
    , PyArray_FLOAT , PyArray_FLOAT  , PyArray_FLOAT , PyArray_FLOAT , PyArray_FLOAT 
    , PyArray_FLOAT , PyArray_FLOAT  , PyArray_FLOAT
    , PyArray_FLOAT , PyArray_FLOAT  , PyArray_FLOAT
    , PyArray_INT   , PyArray_INT
    , PyArray_INT   , PyArray_INT
    , PyArray_INT   , PyArray_INT
    , PyArray_INT   , PyArray_INT
    , PyArray_FLOAT , PyArray_FLOAT  , PyArray_FLOAT
    , PyArray_FLOAT , PyArray_FLOAT  , PyArray_FLOAT
  };
  PyArrayObject *padat[GF_NADAT];
  int mw,mh,nbdry,limitr,nnbdry;
  int *adims[4]={ &mw, &mh, &limitr, &nbdry };
  char *adimsnames[4]={ "mw","mh","limitr", "nbdry" };

  PyObject *eqdsk_list, *peqdsk, *aeqdsk, *mode_list, *pobj, *pmode;
  PyArrayObject *paobj;
  char *eqdsk;
  int num_eqdsks=0,mode=1;

  npy_intp dimensions[3];
  int ierr=0,quiet=0;

  int write_basis=0,gf_nidat=GF_NIDAT,gf_nrdat=GF_NRDAT,gf_nadat=GF_NADAT;

  char error_string[256];

  register int i,j,k,m;

  /* PASS LIST OF EQDSK NAMES (STRINGS), SAVE MODE AND DATA DICTIONARY FROM PYTHON */
  if (!PyArg_ParseTuple( args,"OOO|ii",&eqdsk_list, &mode_list, &aeqdsk, &quiet, &write_basis)) {return NULL;}

  if ( write_basis==0 ) {
    gf_nidat = GF_NIDAT - GF_NIBASIS;
    gf_nrdat = GF_NRDAT - GF_NRBASIS; 
    gf_nadat = GF_NADAT - GF_NABASIS; 
  }

  if ( !PyList_Check(eqdsk_list) ) {
	  PyErr_SetString(PyExc_TypeError,"FIRST ARGUMENT MUST BE LIST OF STRINGS");
	  return NULL;
  }
  if ( !PyList_Check(mode_list) ) {
	  PyErr_SetString(PyExc_TypeError,"SECOND ARGUMENT MUST BE LIST OF INTEGERS");
	  return NULL;
  }
  if ( !PyDict_Check(aeqdsk) ) {
	  PyErr_SetString(PyExc_TypeError,"THIRD ARGUMENT MUST BE DICTIONARY OF ARRAYS");
	  return NULL;
  }

  num_eqdsks = PyList_Size(eqdsk_list);
  if ( num_eqdsks == 0 ) {
    PyErr_SetString(PyExc_TypeError,"EQDSK LIST IS 0 LENGTH");
    return NULL;
  }  
  if (num_eqdsks != PyList_Size(eqdsk_list)){
	  PyErr_SetString(PyExc_RuntimeError,"LENGTH OF EQDSK LIST MUST = LENGTH OF SAVE_MODE LIST");
	  return NULL;
  }

  /* GET MW,MH,LIMITR AND NBDRY FROM DICTIONARY IN CASE OTHER ENTRIES 
     NEED TO BE CONSTRUCTED */
  for (i=0;i<3;i++){
    pobj = PyDict_GetItemString( aeqdsk, adimsnames[i] );
    if ( pobj == NULL) {
      sprintf(error_string,"%s NOT IN DICTIONARY",adimsnames[i]);
      PyErr_SetString(PyExc_RuntimeError,error_string);
      return NULL;
    }
    paobj = (PyArrayObject *)pobj;
    *adims[i] = *(int *)(paobj->data);
    for (j=0;j<(int)(paobj->dimensions[0]);j++) {
      if (*adims[i] != *(int *)(paobj->data + j*paobj->strides[0]) ) {
	sprintf(error_string,"%s NOT THE SAME FOR ALL ENTRIES",adimsnames[i]);
	PyErr_SetString(PyExc_RuntimeError,error_string);
	return NULL;
      }
    }
  }
  pobj = PyDict_GetItemString( aeqdsk, adimsnames[3] );
  if ( pobj == NULL) {
    sprintf(error_string,"%s NOT IN DICTIONARY",adimsnames[3]);
    PyErr_SetString(PyExc_RuntimeError,error_string);
    return NULL;
  }
  paobj = (PyArrayObject *)pobj;
  *adims[3] = *(int *)(paobj->data);
  for (j=0;j<(int)(paobj->dimensions[0]);j++) {
    nnbdry = *(int *)(paobj->data + j*paobj->strides[0]);
    if (nnbdry > *adims[3]) {*adims[3] = nnbdry;}
  }
  for (i=0;i<14;i++) {adatdim0[i] = mw;}
  adatdim0[11] = mh;
  adatdim0[14] = limitr;
  adatdim0[15] = limitr;
  adatdim1[ 6] = mw;
  adatdim0[ 6] = mh;
  adatdim0[16] = nbdry;
  adatdim0[17] = nbdry;

  if ( write_basis == 1                         &&
       PyDict_GetItemString( aeqdsk, "ppbdry" ) == NULL &&
       PyDict_GetItemString( aeqdsk, "PPBDRY" ) == NULL    ) {
/*     printf( "writeg: WARNING, NO BASIS NAMELIST DATA TO WRITE TO GFILE\n" ); */
    write_basis = 0;
    gf_nidat = GF_NIDAT - GF_NIBASIS;
    gf_nrdat = GF_NRDAT - GF_NRBASIS; 
    gf_nadat = GF_NADAT - GF_NABASIS; 
  }

  /* INT EQUIVALENCE DATA FROM DICTIONARY */
  dimensions[0] = (npy_intp)num_eqdsks;
  for (i=0;i<gf_nidat;i++) {
    pobj = PyDict_GetItemString( aeqdsk, gf_inames[i] );
    Py_XINCREF(pobj);
    if ( pobj == NULL ) {
      if(quiet==0){printf("WARNING %s NOT IN DATA DICTIONARY, SETING TO 0\n",gf_inames[i]);}
      paobj = (PyArrayObject *)PyArray_SimpleNew(1, dimensions, PyArray_INT );
      for (j=0;j<num_eqdsks;j++) { *(int *)(paobj->data + j*paobj->strides[0]) = 0;}
      Py_XDECREF(pobj);
      pobj = (PyObject *)paobj;
    }
    if ( !PyArray_Check(pobj) ) {
      PyErr_SetString(PyExc_TypeError,"INT AND FLOAT ELEMENTS OF DICTIONARY MUST BE ARRAYS");
      Py_XDECREF(pobj);
      goto FAIL_WRITEG;
    }
    pidat[i] = (PyArrayObject *)pobj;
    if ((int)(pidat[i]->dimensions[0]) != num_eqdsks) {
	  PyErr_SetString(PyExc_RuntimeError,"LENGTH OF EQDSK DATA ARRAY MUST = LENGTH OF EQDSK LIST");
	  goto FAIL_WRITEG;
    }
  }
  /* FLOAT EQUIVALENCE DATA FROM DICTIONARY */
  for (i=0;i<gf_nrdat;i++) {
    pobj = PyDict_GetItemString( aeqdsk, gf_rnames[i] );
    Py_XINCREF(pobj);
    if ( pobj == NULL ) {
      if(quiet==0){printf("WARNING %s NOT IN DATA DICTIONARY, SETING TO 0.0\n",gf_rnames[i]);}
      paobj = (PyArrayObject *)PyArray_SimpleNew(1, dimensions, PyArray_FLOAT );
      for (j=0;j<num_eqdsks;j++) { *(float *)(paobj->data + j*paobj->strides[0]) = 0.0;}
      Py_XDECREF(pobj);
      pobj = (PyObject *)paobj;
    }
    if ( !PyArray_Check(pobj) ) {
      PyErr_SetString(PyExc_TypeError,"INT AND FLOAT ELEMENTS OF DICTIONARY MUST BE ARRAYS");
      Py_XDECREF(pobj);
      goto FAIL_WRITEG;
    }
    prdat[i] = (PyArrayObject *)pobj;
    if ((int)(prdat[i]->dimensions[0]) != num_eqdsks) {
	  PyErr_SetString(PyExc_RuntimeError,"LENGTH OF EQDSK DATA ARRAY MUST = LENGTH OF EQDSK LIST");
	  goto FAIL_WRITEG;
    }
  }

  /* ARRAY DATA FROM DICTIONARY */
  for (i=0;i<gf_nadat;i++) {
    pobj = PyDict_GetItemString( aeqdsk, gf_anames[i] );
    Py_XINCREF(pobj);
    if ( pobj == NULL ) {
      if(quiet==0){printf("WARNING %s NOT IN DATA DICTIONARY, SETING TO 0.0\n",gf_anames[i]);}
      dimensions[1] = (npy_intp)adatdim0[i];
      if (adatdim0[i] == 0){
	paobj = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, adattype[i] );
	for (j=0;j<num_eqdsks;j++) { 
	  for (k=0;k<adatdim0[i];k++){
	    *(float *)(paobj->data + j*paobj->strides[0] + k*paobj->strides[1]) = 0.0;
	  }
	}
      }
      else {
	dimensions[2] = (npy_intp)adatdim1[i]; 
	paobj = (PyArrayObject *)PyArray_SimpleNew(3, dimensions, adattype[i] );
	for (j=0;j<num_eqdsks;j++) { 
	  for (k=0;k<adatdim0[i];k++){
	    for (m=0;m<adatdim1[i];m++)
	    *(float *)(paobj->data 
		       + j*paobj->strides[0] 
		       + k*paobj->strides[1]
		       + m*paobj->strides[2] ) = 0.0;
	  }
	}
      }
      Py_XDECREF(pobj);
      pobj = (PyObject *)paobj;
    }
    if ( !PyArray_Check(pobj) ) {
      PyErr_SetString(PyExc_TypeError,"INT AND FLOAT ELEMENTS OF DICTIONARY MUST BE ARRAYS");
      Py_XDECREF(pobj);
      goto FAIL_WRITEG;
    }
    padat[i] = (PyArrayObject *)pobj;
    if ((int)(padat[i]->dimensions[0]) != num_eqdsks) {
	  PyErr_SetString(PyExc_RuntimeError,"LENGTH OF EQDSK ARRAY MUST = LENGTH OF EQDSK LIST");
	  goto FAIL_WRITEG;
    }
  }

  for (i=0;i<num_eqdsks;i++) {

    peqdsk = PyList_GetItem( eqdsk_list, i);
    if ( !PyString_Check(peqdsk) ) {
      PyErr_SetString(PyExc_TypeError,"EQDSK NAME MUST BE STRING");
      goto FAIL_WRITEG;
    }  
    eqdsk = PyString_AsString( peqdsk );

    pmode = PyList_GetItem( mode_list, i);
    if ( !PyInt_Check(pmode) ) {
      PyErr_SetString(PyExc_TypeError,"MODE TYPE MUST BE INTEGER");
      goto FAIL_WRITEG;
    }  
    mode = (int)PyInt_AsLong( pmode);
    if (mode==0 && write_basis==1) {
      PyErr_SetString(PyExc_RuntimeError,"CANNOT WRITE BASIS DATA IN BINARY FILE MODE");
      goto FAIL_WRITEG;
    }  

    if (quiet==0) {printf("%s %i\n",eqdsk,mode);}

    volp =   (float *)( padat[ 0]->data + i*padat[ 0]->strides[0] );
    pprime = (float *)( padat[ 1]->data + i*padat[ 1]->strides[0] );
    pres =   (float *)( padat[ 2]->data + i*padat[ 2]->strides[0] );
    ffprim = (float *)( padat[ 3]->data + i*padat[ 3]->strides[0] );
    fpol =   (float *)( padat[ 4]->data + i*padat[ 4]->strides[0] );
    qpsi =   (float *)( padat[ 5]->data + i*padat[ 5]->strides[0] );
    psirz =  (float *)( padat[ 6]->data + i*padat[ 6]->strides[0] );
    rgrid =  (float *)( padat[ 7]->data + i*padat[ 7]->strides[0] );
    pressw = (float *)( padat[ 8]->data + i*padat[ 8]->strides[0] );
    pwprim = (float *)( padat[ 9]->data + i*padat[ 9]->strides[0] );
    dmion =  (float *)( padat[10]->data + i*padat[10]->strides[0] );
    zgrid =  (float *)( padat[11]->data + i*padat[11]->strides[0] );
    rhovn =  (float *)( padat[12]->data + i*padat[12]->strides[0] );
    epoten = (float *)( padat[13]->data + i*padat[13]->strides[0] );
    xlim =   (float *)( padat[14]->data + i*padat[14]->strides[0] );
    ylim =   (float *)( padat[15]->data + i*padat[15]->strides[0] );
    rbdry =  (float *)( padat[16]->data + i*padat[16]->strides[0] );
    zbdry =  (float *)( padat[17]->data + i*padat[17]->strides[0] );
    if ( write_basis ) {
      ee2bdry= (float *)( padat[18]->data + i*padat[18]->strides[0] ); 
      eebdry=  (float *)( padat[19]->data + i*padat[19]->strides[0] );
      eeknt=   (float *)( padat[20]->data + i*padat[20]->strides[0] );
      ff2bdry= (float *)( padat[21]->data + i*padat[21]->strides[0] ); 
      ffbdry=  (float *)( padat[22]->data + i*padat[22]->strides[0] );
      ffknt=   (float *)( padat[23]->data + i*padat[23]->strides[0] );
      kee2bdry=(int *)( padat[24]->data + i*padat[24]->strides[0] );
      keebdry= (int *)( padat[25]->data + i*padat[25]->strides[0] );
      kff2bdry=(int *)( padat[26]->data + i*padat[26]->strides[0] );
      kffbdry= (int *)( padat[27]->data + i*padat[27]->strides[0] );
      kpp2bdry=(int *)( padat[28]->data + i*padat[28]->strides[0] );
      kppbdry= (int *)( padat[29]->data + i*padat[29]->strides[0] );
      kww2bdry=(int *)( padat[30]->data + i*padat[30]->strides[0] );
      kwwbdry= (int *)( padat[31]->data + i*padat[31]->strides[0] );
      pp2bdry= (float *)( padat[32]->data + i*padat[32]->strides[0] ); 
      ppbdry=  (float *)( padat[33]->data + i*padat[33]->strides[0] );
      ppknt=   (float *)( padat[34]->data + i*padat[34]->strides[0] );
      ww2bdry= (float *)( padat[35]->data + i*padat[35]->strides[0] ); 
      wwbdry=  (float *)( padat[36]->data + i*padat[36]->strides[0] );
      wwknt=   (float *)( padat[37]->data + i*padat[37]->strides[0] );
    }

    for (j=0;j<gf_nidat;j++) {
      idat[j] = *(int *)(pidat[j]->data + i*pidat[j]->strides[0] );
    }
    for (j=0;j<gf_nrdat;j++) {
      rdat[j] = *(float *)(prdat[j]->data + i*prdat[j]->strides[0] );
    }

    WRITEG(eqdsk,&ierr,&mode
           ,volp,pprime,pres,ffprim,fpol
           ,qpsi,psirz,rgrid
           ,pressw,pwprim,dmion
           ,zgrid,rhovn,epoten
           ,xlim,ylim
           ,rbdry,zbdry
           ,idat,rdat
	  , ee2bdry , eebdry, eeknt
	  , ff2bdry , ffbdry, ffknt
	  , kee2bdry, keebdry
	  , kff2bdry, kffbdry
	  , kpp2bdry, kppbdry
	  , kww2bdry, kwwbdry
	  , pp2bdry , ppbdry, ppknt
	  , ww2bdry , wwbdry, wwknt
	  , &write_basis
	   );

    if (ierr !=0) {
      sprintf( error_string, "ERROR WRITING %s", eqdsk );
      PyErr_SetString(PyExc_IOError,error_string);
      goto FAIL_WRITEG;

    }      

  }
  /* SINCE ARRAYS ARE IN DICTIONARY LOWER REF COUNT BY 1 */
  for (i=0;i<gf_nidat;i++) {Py_DECREF(pidat[i]);}
  for (i=0;i<gf_nrdat;i++) {Py_DECREF(prdat[i]);}
  for (i=0;i<gf_nadat;i++) {Py_DECREF(padat[i]);}

  Py_INCREF(Py_None); 
  return(Py_None);

 FAIL_WRITEG:
  for (i=0;i<gf_nidat;i++) {Py_XDECREF(pidat[i]);}
  for (i=0;i<gf_nrdat;i++) {Py_XDECREF(prdat[i]);}
  for (i=0;i<gf_nadat;i++) {Py_XDECREF(padat[i]);}
  return NULL;
}
/****************************************************************************/


static char reada__doc__[]= {"\n\
--------------------------------------------------------------\n\
| s = reada( eqdsk_list, optional:quiet )                    |\n\
|  eqdsk_list = list of eqdsk file names(strings)            |\n\
|  quiet(optional) = 0(default) print diagnostics, 1=do not  |\n\
|  s = dictionary of results: keys=efit parameter names,     |\n\
|     values = arrays[number of files,,,other parameters]    |\n\
|     for string values a list is used rather than an array  |\n\
--------------------------------------------------------------\n\
"};
static char writea__doc__[]= {"\n\
--------------------------------------------------------------\n\
| writea( eqdsk_list, mode_list, s, opt:quiet )              |\n\
|  eqdsk_list = list of eqdsk file names(strings)            |\n\
|  mode_list = list of write modes: 0=binary,1=formatted     |\n\
|  s = dictionary: keys=efit parameter names,                |\n\
|     values = arrays[number of files,,,other parameters]    |\n\
|     for string values a list is used rather than an array  |\n\
|  quiet(optional) = 0(default) print diagnostics, 1=do not  |\n\
--------------------------------------------------------------\n\
"};
static char readg__doc__[]= {"\n\
--------------------------------------------------------------\n\
| s = readg( eqdsk_list, optional:quiet,read_basis )         |\n\
|  eqdsk_list = list of eqdsk file names(strings)            |\n\
|  quiet(optional) = 0(default) print diagnostics, 1=do not  |\n\
|  read_basis(optional)= 1 read p' ,ff' basis function       |\n\
|                        data from namelist in ascii gfile   |\n\
|                        0(default)=do not                   |\n\
|  s = dictionary of results: keys=efit parameter names,     |\n\
|     values = arrays[number of files,,,other parameters]    |\n\
|     for string values a list is used rather than an array  |\n\
--------------------------------------------------------------\n\
"};
static char writeg__doc__[]= {"\n\
--------------------------------------------------------------\n\
| writeg( eqdsk_list, mode_list, s, opt:quiet, write_basis ) |\n\
|  eqdsk_list = list of eqdsk file names(strings)            |\n\
|  mode_list = list of write modes: 0=binary,1=formatted     |\n\
|  s = dictionary: keys=efit parameter names,                |\n\
|     values = arrays[number of files,,,other parameters]    |\n\
|     for string values a list is used rather than an array  |\n\
|  quiet(optional) = 0(default) print diagnostics, 1=do not  |\n\
|  write_basis(optional)= 1 write p' ,ff' basis function     |\n\
|                        data to namelist in ascii gfile     |\n\
|                        0(default)=do not                   |\n\
--------------------------------------------------------------\n\
"};


/* DEFINE MODULE METHODS - ONLY GETDAT */
static PyMethodDef pytaMethods[] = {
  {"reada",  _reada,  METH_VARARGS, reada__doc__},
  {"writea", _writea, METH_VARARGS, writea__doc__},
  {"readg",  _readg,  METH_VARARGS, readg__doc__},
  {"writeg", _writeg, METH_VARARGS, writeg__doc__},
  {NULL, NULL}
};

/* INIT ROUTINE FOR MODULE PPTDATA */
void initrwefit(void) {
  static char rwefit__doc__[] = {"\n\
----------------------------------------------------------------------\n\
| rwefit: read and write efit a and g eqdisks                        |\n\
|    methods:                                                        |\n\
|      s = reada( eqdsk_list, optional:quiet )                       |\n\
|         writea( eqdsk_list, mode_list, s, opt:quiet )              |\n\
|      s = readg( eqdsk_list, optional:quiet,read_basis)             |\n\
|         writeg( eqdsk_list, mode_list, s, opt:quiet,write_basis)   |\n\
|        eqdsk_list = list of eqdsk file names(strings)              |\n\
|        mode_list = list of write modes: 0=binary,1=formatted       |\n\
|        quiet(optional) = 0(default) print diagnostics, 1=do not    |\n\
|        read,write_basis= 0(default), 1=read or write p',ff' basis  |\n\
|                          function namelist in gfile                |\n\
|        s = dictionary of results: keys=efit parameter names,       |\n\
|            values = arrays[number of files,,,other parameters]     |\n\
|            for string values a list is used rather than an array   |\n\
|                                                                    |\n\
|     mw,mh,limitr and nbdry arrays must be in a gfile s dictionary  |\n\
|     to write the data to a file. Error if not present.             |\n\
|     The maxmimum # of boundary points(nbdry) = 500                 |\n\
|     If any needed values are not in the dictionary zeros are       |\n\
|     written to the file.                                           |\n\
|     All array and lists in s must have the same 0 dimension(time)  |\n\
|     All afiles to read must have the same number of probes, etc.   |\n\
|     All gfiles must have the same grid (mw,mh) and number of       |\n\
|     limiter points, # boundary points can vary to maximum.         |\n\
----------------------------------------------------------------------\n\
"};
  (void) Py_InitModule3("rwefit",pytaMethods,rwefit__doc__);
  import_array();
}
