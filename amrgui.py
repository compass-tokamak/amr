# -*- coding: utf-8 -*-
"""
Created on Wed Dec 02 09:58:25 2009

@author: jurban
"""

from formlayout import fedit
from ConfigParser import SafeConfigParser
import re,os,sys

def index(seq, f):
    """Return the index of the first item in seq where f(item) == True."""
    for index in (i for i in xrange(len(seq)) if f(seq[i])):
        return index

def index_sec(seq, sec):
    return index(seq, lambda s: re.search('^\s*\[\s*'+sec+'\s*\]',s))

def find_any_sec(seq):
    si = index(seq, lambda s: re.search('^\s*\[\s*\S+\s*\]',s))
    if si == None: return None,None
    return si, re.search('^\s*\[\s*(\S+)\s*\]',seq[si]).group(1)

def sec_des(seq):
    lni = index(seq, lambda s: re.search('^\s*#sec\s+',s))
    if lni == None: return ''
    ln = seq[lni]
    return ln[re.search('^\s*#sec\s+',ln).end():].strip()

def index_opt(seq, opt):
    return index(seq, lambda s: re.search('^\s*'+opt+'\s*=',s))

if __name__=='__main__':
    maxItems = 20
    
    simulation_path = os.getcwd()
    amr_path = sys.path[0]
    if len(sys.argv)>1:
        cfgfilename = sys.argv[1]
    else:
        cfgfilename = 'ebe6.cfg'
    defaults_cfgfilename = os.path.join(amr_path,'ebe6_defaults.cfg')
    
    my_conf = SafeConfigParser()
    my_conf.read([defaults_cfgfilename,cfgfilename])
    
    f_conf = open(defaults_cfgfilename,'r')
    lns = f_conf.readlines()
    f_conf.close()
    lns = map(lambda s: s.strip(),lns)
    
    sections = []
    sec_lns = []
    while True:
        if len(sec_lns)==0:
            seci,sec = find_any_sec(lns)
        else:
            seci,sec = find_any_sec(lns[sec_lns[-1]+1:])
        if seci==None: break
        sections.append(sec)
        sec_lns.append((seci if len(sec_lns)==0 else seci+sec_lns[-1]+1))
        
    sec_dess = []
    for isec,sec in enumerate(sections):
        if isec==0:
            sec_dess.append(sec_des(lns[0:sec_lns[isec]]))
        else:
            sec_dess.append(sec_des(lns[sec_lns[isec-1]:sec_lns[isec]]))
    
    sec_fields = []
    for isec in range(len(sections)):
        lnmax = (len(lns) if isec==len(sections)-1 else sec_lns[isec+1])
        field_des = ''
        field_type = ''
        sec_fields.append([])
        for ln in lns[sec_lns[isec]+1:lnmax]:
            # m = re.match('#f\s+(.*)$',ln)
            # field_des = (m.group(1) if m else '')
            
            # field_type = (m.group(1) if m else '')
            if ln.startswith('#f'): field_des = ln[2:].strip()
            if ln.startswith('#type'): field_type = ln[5:].strip()
            #m = re.match('#type\s+(\S+)\s*(.*)$',ln)
            
            if ln and ln[0]!='#' and ln[0]!=';':
                m = re.match('(\S+)\s*=\s*(\S*)',ln)
                field_name = m.group(1)
                field_val = my_conf.get(sections[isec],field_name) # m.group(2)
                if field_type.lower() in ('int','integer'):
                    field_val = (0 if field_val=='' else int(field_val))
                elif field_type.lower() in ('float','real'):
                    field_val = (0.0 if field_val=='' else float(field_val))
                elif field_type.lower() in ('bool','boolean'):
                    field_val = (False if field_val.lower() in ('f','false','no') else bool(field_val))
                elif field_type.lower().startswith('list'):
                    pos = field_type.split()[1:]
                    pos_l = field_type.lower().split()[1:]
                    if field_val.lower() in pos_l:
                        field_val = [pos_l.index(field_val.lower())] + pos
                    else:
                        field_val = [0] + pos
                elif field_type.lower() in ('str','string','text',''):
                    field_val = field_val
                else:
                    print 'Unknown type: ' + field_val
                field = dict()
                field['name'] = field_name
                field['value'] = field_val
                field['description'] = field_des
                field['type'] = field_type
                sec_fields[-1].append(field)
                field_des = ''
                field_type = ''
    
    datagroup = []
    for isec in range(len(sections)):
        i0 = 0
        i1 = maxItems
        while i0<len(sec_fields[isec]):
            datalist = []
            for field in sec_fields[isec][i0:i1]:
                datalist.append((field['name'],field['value']))
            subsec = ('_%i' % (i0 // maxItems + 1) if i0>0 else '')
            datagroup.append((datalist,sections[isec]+subsec,sec_dess[isec]))
            i0 = i1
            i1 = i0 + maxItems
    
    # formlayout bug fixed (QSpinBox.setRange called before setValue)
    cfgdata = fedit(datagroup, title="AMR config")
    
    if cfgdata is not None:
        f_conf = open(cfgfilename,'w')
        isec_data=0
        for isec in range(len(sections)):
            f_conf.write('#sec ' + sec_dess[isec] + '\n')
            f_conf.write('[' + sections[isec] + ']\n')
            i0 = 0
            i1 = maxItems
            while i0<len(sec_fields[isec]):
                datalist = []
                for ifield,field in enumerate(sec_fields[isec][i0:i1]):
                    field['value'] = cfgdata[isec_data][ifield]
                    f_conf.write('#f ' + field['description'] + '\n')
                    f_conf.write(field['name'] + ' = ' + str(field['value']) + '\n')
                i0 = i1
                i1 = i0 + maxItems
                isec_data += 1
            f_conf.write('\n')
        f_conf.close()
    
    

