# Microsoft Developer Studio Project File - Name="ebe_run_nag" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=ebe_run_nag - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ebe_run_nag.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ebe_run_nag.mak" CFG="ebe_run_nag - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ebe_run_nag - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "ebe_run_nag - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
F90=df.exe
RSC=rc.exe

!IF  "$(CFG)" == "ebe_run_nag - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE F90 /compile_only /nologo /warn:nofileopt
# ADD F90 /compile_only /define:"_MCLIB" /define:"_IMSL" /fpp /imsl /nologo /warn:nofileopt
# SUBTRACT F90 /threads
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x405 /d "NDEBUG"
# ADD RSC /l 0x405 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /machine:I386 /nodefaultlib:"libcmt.lib" /nodefaultlib:"msvcrt.lib" /nodefaultlib:"libcd.lib" /nodefaultlib:"libcmtd.lib" /nodefaultlib:"msvcrtd.lib" /out:"ebe_run.exe"

!ELSEIF  "$(CFG)" == "ebe_run_nag - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE F90 /check:bounds /compile_only /dbglibs /debug:full /nologo /traceback /warn:argument_checking /warn:nofileopt
# ADD F90 /check:bounds /compile_only /dbglibs /debug:full /define:"_CERNLIB" /define:"_MCLIB" /fpp /include:"../lib" /nologo /traceback /warn:argument_checking /warn:nofileopt /warn:truncated_source
# SUBTRACT F90 /imsl /warn:unused
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE RSC /l 0x405 /d "_DEBUG"
# ADD RSC /l 0x405 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib /nologo /subsystem:console /incremental:no /debug /machine:I386 /nodefaultlib:"libc.lib" /nodefaultlib:"libcmt.lib" /nodefaultlib:"msvcrt.lib" /nodefaultlib:"libcmtd.lib" /nodefaultlib:"msvcrtd.lib" /out:"ebe_run.exe" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "ebe_run_nag - Win32 Release"
# Name "ebe_run_nag - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat;f90;for;f;fpp"
# Begin Source File

SOURCE=..\source\absorb2.F90
# End Source File
# Begin Source File

SOURCE=..\source\adaptive.F90
DEP_F90_ADAPT=\
	"..\source\parameters.f90"\
	".\Debug\generalConstants.mod"\
	".\Debug\PlasmaSlab.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\akima_surf.F
# End Source File
# Begin Source File

SOURCE=..\source\alphaphi_jd.F90
DEP_F90_ALPHA=\
	".\Debug\bessfn_jd.mod"\
	".\Debug\travis_const_and_precisions.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\analytical_profiles.F90
DEP_F90_ANALY=\
	".\Debug\Equilibrium.mod"\
	".\Debug\shot_parameters.mod"\
	".\Debug\utilities.mod"\
	".\Debug\vectors.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\besselfun.F90
# End Source File
# Begin Source File

SOURCE=..\source\bessfn_jd.F90
# End Source File
# Begin Source File

SOURCE=..\source\bspline90_22.F90
# End Source File
# Begin Source File

SOURCE=..\source\cmlib.F90
# End Source File
# Begin Source File

SOURCE=..\source\collisionFrequency.F90
DEP_F90_COLLI=\
	".\Debug\bspline.mod"\
	".\Debug\generalConstants.mod"\
	".\Debug\utilities.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\config_test.F90
DEP_F90_CONFI=\
	".\Debug\density_temperature.mod"\
	".\Debug\Equilibrium.mod"\
	".\Debug\FavouredCombinations.mod"\
	".\Debug\fileNames.mod"\
	".\Debug\generalConstants.mod"\
	".\Debug\local_param.mod"\
	".\Debug\shot_parameters.mod"\
	".\Debug\travis_config_ext.mod"\
	".\Debug\utilities.mod"\
	".\Debug\vectors.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\constants.F90
DEP_F90_CONST=\
	".\Debug\generalConstants.mod"\
	".\Debug\shot_parameters.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\ConversionEfficiency.F90
DEP_F90_CONVE=\
	".\Debug\analytical_profiles.mod"\
	".\Debug\Constants.mod"\
	".\Debug\density_temperature.mod"\
	".\Debug\Equilibrium.mod"\
	".\Debug\fileNames.mod"\
	".\Debug\generalConstants.mod"\
	".\Debug\PlasmaSlab.mod"\
	".\Debug\rays.mod"\
	".\Debug\shot_parameters.mod"\
	".\Debug\utilities.mod"\
	".\Debug\vectors.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\cpusec.f95.F90
# End Source File
# Begin Source File

SOURCE=..\source\density_temperature.F90
DEP_F90_DENSI=\
	"..\source\density_temperature\LCFS_scale_profile.f90"\
	".\Debug\analytical_profiles.mod"\
	".\Debug\bspline.mod"\
	".\Debug\cmlib.mod"\
	".\Debug\Equilibrium.mod"\
	".\Debug\fileNames.mod"\
	".\Debug\m_mrgref.mod"\
	".\Debug\shot_parameters.mod"\
	".\Debug\utilities.mod"\
	".\Debug\vectors.mod"\
	".\Debug\zeroin.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\disp_consts.F90
# End Source File
# Begin Source File

SOURCE=..\source\ebe_run.F90
DEP_F90_EBE_R=\
	".\Debug\Constants.mod"\
	".\Debug\fileNames.mod"\
	".\Debug\gamma_int.mod"\
	".\Debug\rays.mod"\
	".\Debug\rt_output.mod"\
	".\Debug\shot_parameters.mod"\
	".\Debug\utilities.mod"\
	".\Debug\vectors.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\ebw_init.F90
DEP_F90_EBW_I=\
	".\Debug\analytical_profiles.mod"\
	".\Debug\Constants.mod"\
	".\Debug\Equilibrium.mod"\
	".\Debug\FavouredCombinations.mod"\
	".\Debug\fileNames.mod"\
	".\Debug\generalConstants.mod"\
	".\Debug\PlasmaSlab.mod"\
	".\Debug\rays.mod"\
	".\Debug\shot_parameters.mod"\
	".\Debug\vectors.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\EBWDispDecker.F90
DEP_F90_EBWDI=\
	".\Debug\generalConstants.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\EBWDispSaveliev.F90
DEP_F90_EBWDIS=\
	".\Debug\generalConstants.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\equilibrium.F90
DEP_F90_EQUIL=\
	"..\source\equilibrium\exportPsiThetaEquil.f90"\
	"..\source\equilibrium\fluxTrace.f90"\
	"..\source\equilibrium\LLintegFun.f90"\
	"..\source\equilibrium\LLintegVars.f90"\
	"..\source\equilibrium\Psi2DSpline.f90"\
	"..\source\equilibrium\readFluxData.f90"\
	".\Debug\bspline.mod"\
	".\Debug\LLquadrat.mod"\
	".\Debug\rkf45.mod"\
	".\Debug\utilities.mod"\
	".\Debug\vectors.mod"\
	".\Debug\zeroin.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\favoured_combinations.F90
DEP_F90_FAVOU=\
	".\Debug\collisionFrequency.mod"\
	".\Debug\Constants.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\favoured_combinations_rt.F90
DEP_F90_FAVOUR=\
	".\Debug\collisionFrequency.mod"\
	".\Debug\Constants.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\fileNames.F90
# End Source File
# Begin Source File

SOURCE=..\source\find_mesh_inp.F90
# End Source File
# Begin Source File

SOURCE=..\source\find_xwkb.F90
DEP_F90_FIND_=\
	".\Debug\Constants.mod"\
	".\Debug\FavouredCombinations.mod"\
	".\Debug\generalConstants.mod"\
	".\Debug\PlasmaSlab.mod"\
	".\Debug\shot_parameters.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\find_xwkb_test.F90
DEP_F90_FIND_X=\
	".\Debug\Constants.mod"\
	".\Debug\FavouredCombinations.mod"\
	".\Debug\fileNames.mod"\
	".\Debug\generalConstants.mod"\
	".\Debug\PlasmaSlab.mod"\
	".\Debug\shot_parameters.mod"\
	".\Debug\vectors.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\gamma_int.F90
DEP_F90_GAMMA=\
	".\Debug\alphaphi_jd.mod"\
	".\Debug\besselfun.mod"\
	".\Debug\collisionFrequency.mod"\
	".\Debug\Constants.mod"\
	".\Debug\density_temperature.mod"\
	".\Debug\disp_consts.mod"\
	".\Debug\EBWDispDecker.mod"\
	".\Debug\EBWDispSaveliev.mod"\
	".\Debug\Equilibrium.mod"\
	".\Debug\FavouredCombinations_rt.mod"\
	".\Debug\generalConstants.mod"\
	".\Debug\LLdrive.mod"\
	".\Debug\local_param.mod"\
	".\Debug\shot_parameters.mod"\
	".\Debug\vectors.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\gauss_int_24.F90
# End Source File
# Begin Source File

SOURCE=..\source\generalConstants.F90
# End Source File
# Begin Source File

SOURCE=..\source\i1mach.f
# End Source File
# Begin Source File

SOURCE=..\source\import_psi_d_t.F90
DEP_F90_IMPOR=\
	".\Debug\collisionFrequency.mod"\
	".\Debug\density_temperature.mod"\
	".\Debug\Equilibrium.mod"\
	".\Debug\fileNames.mod"\
	".\Debug\shot_parameters.mod"\
	".\Debug\travis_config_int.mod"\
	".\Debug\travis_green_func_int.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\init_gauss_fe.F90
# End Source File
# Begin Source File

SOURCE=..\source\init_ray.F90
DEP_F90_INIT_=\
	".\Debug\Constants.mod"\
	".\Debug\Equilibrium.mod"\
	".\Debug\FavouredCombinations_rt.mod"\
	".\Debug\local_param.mod"\
	".\Debug\shot_parameters.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\l2dev.F90
DEP_F90_L2DEV=\
	"..\source\parameters.f90"\
	
# End Source File
# Begin Source File

SOURCE=..\source\LLdrive.F90
DEP_F90_LLDRI=\
	".\Debug\density_temperature.mod"\
	".\Debug\Equilibrium.mod"\
	".\Debug\LLglob.mod"\
	".\Debug\LLinteg.mod"\
	".\Debug\LLquadrat.mod"\
	".\Debug\vectors.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\LLglob.F90
# End Source File
# Begin Source File

SOURCE=..\source\LLinteg.F90
DEP_F90_LLINT=\
	".\Debug\Equilibrium.mod"\
	".\Debug\LLglob.mod"\
	".\Debug\LLquadrat.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\LLquadrat.F90
# End Source File
# Begin Source File

SOURCE=..\source\local_param3.F90
DEP_F90_LOCAL=\
	".\Debug\density_temperature.mod"\
	".\Debug\Equilibrium.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\loccom24.F90
DEP_F90_LOCCO=\
	".\Debug\PlasmaSlab.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\m_mrgref.F90
# End Source File
# Begin Source File

SOURCE=..\source\make_matrix.F90
DEP_F90_MAKE_=\
	"..\source\coeff_include.f90"\
	".\Debug\PlasmaSlab.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\max_min_value.F90
# End Source File
# Begin Source File

SOURCE=..\source\plasmaslab.F90
DEP_F90_PLASM=\
	".\Debug\Constants.mod"\
	".\Debug\density_temperature.mod"\
	".\Debug\Equilibrium.mod"\
	".\Debug\FavouredCombinations.mod"\
	".\Debug\generalConstants.mod"\
	".\Debug\rays.mod"\
	".\Debug\utilities.mod"\
	".\Debug\vectors.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\precision_mod.F90
# End Source File
# Begin Source File

SOURCE=..\source\quanc8.f
# End Source File
# Begin Source File

SOURCE=..\source\rays.F90
DEP_F90_RAYS_=\
	".\Debug\density_temperature.mod"\
	".\Debug\Equilibrium.mod"\
	".\Debug\generalConstants.mod"\
	".\Debug\utilities.mod"\
	".\Debug\vectors.mod"\
	".\Debug\zeroin.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\RayTracing.F90
DEP_F90_RAYTR=\
	".\Debug\analytical_profiles.mod"\
	".\Debug\Constants.mod"\
	".\Debug\disp_consts.mod"\
	".\Debug\EBWDispSaveliev.mod"\
	".\Debug\Equilibrium.mod"\
	".\Debug\FavouredCombinations_rt.mod"\
	".\Debug\fileNames.mod"\
	".\Debug\gamma_int.mod"\
	".\Debug\local_param.mod"\
	".\Debug\rays.mod"\
	".\Debug\rt_output.mod"\
	".\Debug\RTDebug.mod"\
	".\Debug\shot_parameters.mod"\
	".\Debug\utilities.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\RD_cB_oblique.F90
DEP_F90_RD_CB=\
	".\Debug\Constants.mod"\
	".\Debug\disp_consts.mod"\
	".\Debug\Equilibrium.mod"\
	".\Debug\FavouredCombinations.mod"\
	".\Debug\generalConstants.mod"\
	".\Debug\local_param.mod"\
	".\Debug\shot_parameters.mod"\
	".\Debug\utilities.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\rd_cb_oblique_rt.F90
DEP_F90_RD_CB_=\
	".\Debug\alphaphi_jd.mod"\
	".\Debug\besselfun.mod"\
	".\Debug\Constants.mod"\
	".\Debug\density_temperature.mod"\
	".\Debug\disp_consts.mod"\
	".\Debug\EBWDispDecker.mod"\
	".\Debug\EBWDispSaveliev.mod"\
	".\Debug\Equilibrium.mod"\
	".\Debug\FavouredCombinations_rt.mod"\
	".\Debug\local_param.mod"\
	".\Debug\RTDebug.mod"\
	".\Debug\shot_parameters.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\readInputLine.F90
DEP_F90_READI=\
	".\Debug\rays.mod"\
	".\Debug\vectors.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\real_impl.F90
DEP_F90_REAL_=\
	".\Debug\precision_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\real_mod.F90
DEP_F90_REAL_M=\
	".\Debug\precision_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\rhsebw.F90
DEP_F90_RHSEB=\
	".\Debug\Constants.mod"\
	".\Debug\Equilibrium.mod"\
	".\Debug\local_param.mod"\
	".\Debug\shot_parameters.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\ribesl.f
# End Source File
# Begin Source File

SOURCE=..\source\rkf45.F90
# End Source File
# Begin Source File

SOURCE=..\source\rt_output.F90
DEP_F90_RT_OU=\
	".\Debug\vectors.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\RTDebug.F90
DEP_F90_RTDEB=\
	".\Debug\utilities.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\shot_parameters.F90
DEP_F90_SHOT_=\
	".\Debug\disp_consts.mod"\
	".\Debug\generalConstants.mod"\
	".\Debug\utilities.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\solve.F90
DEP_F90_SOLVE=\
	"..\source\parameters.f90"\
	".\Debug\MakeMatrix.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\sort_pick.F90
# End Source File
# Begin Source File

SOURCE=..\source\stopwatch.F90
# End Source File
# Begin Source File

SOURCE=..\source\support.f
# End Source File
# Begin Source File

SOURCE=..\source\test_profiles.F90
DEP_F90_TEST_=\
	".\Debug\analytical_profiles.mod"\
	".\Debug\Constants.mod"\
	".\Debug\fileNames.mod"\
	".\Debug\generalConstants.mod"\
	".\Debug\PlasmaSlab.mod"\
	".\Debug\shot_parameters.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\TorGA_curba.F90
DEP_F90_TORGA=\
	".\Debug\precision_mod.mod"\
	".\Debug\real_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\torlib2.F90
DEP_F90_TORLI=\
	".\Debug\precision_mod.mod"\
	".\Debug\real_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\travis_config_ext.F90
DEP_F90_TRAVI=\
	".\Debug\Equilibrium.mod"\
	".\Debug\travis_const_and_precisions.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\travis_config_int.F90
DEP_F90_TRAVIS=\
	".\Debug\travis_config_ext.mod"\
	".\Debug\travis_const_and_precisions.mod"\
	".\Debug\travis_mymathlib.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\travis_const_and_precisions.F90
# End Source File
# Begin Source File

SOURCE=..\source\travis_green_func_ext.F90
NODEP_F90_TRAVIS_=\
	".\Debug\travis_const_and_precisions.mod"\
	".\Debug\travis_green_func_int.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\travis_green_func_int.F90
NODEP_F90_TRAVIS_G=\
	".\Debug\travis_config_int.mod"\
	".\Debug\travis_const_and_precisions.mod"\
	".\Debug\travis_mymathlib.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\travis_mymathlib.F90
NODEP_F90_TRAVIS_M=\
	".\Debug\travis_const_and_precisions.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\tripack.F
# End Source File
# Begin Source File

SOURCE=..\source\utilities.F90
DEP_F90_UTILI=\
	".\Debug\generalConstants.mod"\
	".\Debug\m_mrgref.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\vectors.F90
DEP_F90_VECTO=\
	".\Debug\generalConstants.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\waist_data.F90
DEP_F90_WAIST=\
	".\Debug\Constants.mod"\
	".\Debug\fileNames.mod"\
	".\Debug\PlasmaSlab.mod"\
	".\Debug\rays.mod"\
	".\Debug\shot_parameters.mod"\
	".\Debug\utilities.mod"\
	".\Debug\vectors.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\windowProjection.F90
DEP_F90_WINDO=\
	".\Debug\Constants.mod"\
	".\Debug\fileNames.mod"\
	".\Debug\generalConstants.mod"\
	".\Debug\rays.mod"\
	".\Debug\utilities.mod"\
	".\Debug\vectors.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\wkb.F90
DEP_F90_WKB_F=\
	".\Debug\PlasmaSlab.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\source\zeroin.F90
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;fi;fd"
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# Begin Source File

SOURCE=..\lib\mconf.lib
# End Source File
# Begin Source File

SOURCE=..\lib\BLAS_LIB.lib
# End Source File
# Begin Source File

SOURCE=..\lib\LAPACK_LIB.lib
# End Source File
# Begin Source File

SOURCE=..\lib\kernlib.lib
# End Source File
# Begin Source File

SOURCE=..\lib\mathlib.lib
# End Source File
# Begin Source File

SOURCE=..\lib\AMRLIB_WIN_STATIC.lib
# End Source File
# End Target
# End Project
