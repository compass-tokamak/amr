!=============================================================================
! loccom24
!
! computes dielectric tensor components in the points
! used by gauss integration
!
!=============================================================================
subroutine loccom24(x0,h,exx,exy,exz,eyy,eyz,ezz,q)
  use PlasmaSlab
	implicit none
	integer j
	real(8),intent(in) :: x0,h
  real(8) :: x,arggauss(24)
	complex(8),intent(out) :: exx(24),exy(24),exz(24),eyy(24),eyz(24),ezz(24),q(24)
 	common/arggauss/ arggauss

  do j=1,24
	  x = x0 + arggauss(j)*h
    call loccom(x,exx(j),exy(j),exz(j),eyy(j),eyz(j),ezz(j))
    q(j) = getSlabNy()**2 + getSlabNz()**2 - exx(j)
  enddo
end subroutine

