C                                                                       DRKG  10
C     ..................................................................DRKG  20
C                                                                       DRKG  30
C        SUBROUTINE DRKGS                                               DRKG  40
C                                                                       DRKG  50
C        PURPOSE                                                        DRKG  60
C           TO SOLVE A SYSTEM OF FIRST ORDER ORDINARY DIFFERENTIAL      DRKG  70
C           EQUATIONS WITH GIVEN INITIAL VALUES.                        DRKG  80
C                                                                       DRKG  90
C        USAGE                                                          DRKG 100
C           CALL DRKGS (PRMT,Y,DERY,NDIM,IHLF,FCT,OUTP,AUX)             DRKG 110
C           PARAMETERS FCT AND OUTP REQUIRE AN EXTERNAL STATEMENT.      DRKG 120
C                                                                       DRKG 130
C        DESCRIPTION OF PARAMETERS                                      DRKG 140
C           PRMT   - DOUBLE PRECISION INPUT AND OUTPUT VECTOR WITH      DRKG 150
C                    DIMENSION GREATER THAN OR EQUAL TO 5, WHICH        DRKG 160
C                    SPECIFIES THE PARAMETERS OF THE INTERVAL AND OF    DRKG 170
C                    ACCURACY AND WHICH SERVES FOR COMMUNICATION BETWEENDRKG 180
C                    OUTPUT SUBROUTINE (FURNISHED BY THE USER) AND      DRKG 190
C                    SUBROUTINE DRKGS. EXCEPT PRMT(5) THE COMPONENTS    DRKG 200
C                    ARE NOT DESTROYED BY SUBROUTINE DRKGS AND THEY ARE DRKG 210
C           PRMT(1)- LOWER BOUND OF THE INTERVAL (INPUT),               DRKG 220
C           PRMT(2)- UPPER BOUND OF THE INTERVAL (INPUT),               DRKG 230
C           PRMT(3)- INITIAL INCREMENT OF THE INDEPENDENT VARIABLE      DRKG 240
C                    (INPUT),                                           DRKG 250
C           PRMT(4)- UPPER ERROR BOUND (INPUT). IF ABSOLUTE ERROR IS    DRKG 260
C                    GREATER THAN PRMT(4), INCREMENT GETS HALVED.       DRKG 270
C                    IF INCREMENT IS LESS THAN PRMT(3) AND ABSOLUTE     DRKG 280
C                    ERROR LESS THAN PRMT(4)/50, INCREMENT GETS DOUBLED.DRKG 290
C                    THE USER MAY CHANGE PRMT(4) BY MEANS OF HIS        DRKG 300
C                    OUTPUT SUBROUTINE.                                 DRKG 310
C           PRMT(5)- NO INPUT PARAMETER. SUBROUTINE DRKGS INITIALIZES   DRKG 320
C                    PRMT(5)=0. IF THE USER WANTS TO TERMINATE          DRKG 330
C                    SUBROUTINE DRKGS AT ANY OUTPUT POINT, HE HAS TO    DRKG 340
C                    CHANGE PRMT(5) TO NON-ZERO BY MEANS OF SUBROUTINE  DRKG 350
C                    OUTP. FURTHER COMPONENTS OF VECTOR PRMT ARE        DRKG 360
C                    FEASIBLE IF ITS DIMENSION IS DEFINED GREATER       DRKG 370
C                    THAN 5. HOWEVER SUBROUTINE DRKGS DOES NOT REQUIRE  DRKG 380
C                    AND CHANGE THEM. NEVERTHELESS THEY MAY BE USEFUL   DRKG 390
C                    FOR HANDING RESULT VALUES TO THE MAIN PROGRAM      DRKG 400
C                    (CALLING DRKGS) WHICH ARE OBTAINED BY SPECIAL      DRKG 410
C                    MANIPULATIONS WITH OUTPUT DATA IN SUBROUTINE OUTP. DRKG 420
C           Y      - DOUBLE PRECISION INPUT VECTOR OF INITIAL VALUES    DRKG 430
C                    (DESTROYED). LATERON Y IS THE RESULTING VECTOR OF  DRKG 440
C                    DEPENDENT VARIABLES COMPUTED AT INTERMEDIATE       DRKG 450
C                    POINTS X.                                          DRKG 460
C           DERY   - DOUBLE PRECISION INPUT VECTOR OF ERROR WEIGHTS     DRKG 470
C                    (DESTROYED). THE SUM OF ITS COMPONENTS MUST BE     DRKG 480
C                    EQUAL TO 1. LATERON DERY IS THE VECTOR OF          DRKG 490
C                    DERIVATIVES, WHICH BELONG TO FUNCTION VALUES Y AT  DRKG 500
C                    INTERMEDIATE POINTS X.                             DRKG 510
C           NDIM   - AN INPUT VALUE, WHICH SPECIFIES THE NUMBER OF      DRKG 520
C                    EQUATIONS IN THE SYSTEM.                           DRKG 530
C           IHLF   - AN OUTPUT VALUE, WHICH SPECIFIES THE NUMBER OF     DRKG 540
C                    BISECTIONS OF THE INITIAL INCREMENT. IF IHLF GETS  DRKG 550
C                    GREATER THAN 10, SUBROUTINE DRKGS RETURNS WITH     DRKG 560
C                    ERROR MESSAGE IHLF=11 INTO MAIN PROGRAM. ERROR     DRKG 570
C                    MESSAGE IHLF=12 OR IHLF=13 APPEARS IN CASE         DRKG 580
C                    PRMT(3)=0 OR IN CASE SIGN(PRMT(3)).NE.SIGN(PRMT(2)-DRKG 590
C                    PRMT(1)) RESPECTIVELY.                             DRKG 600
C           FCT    - THE NAME OF AN EXTERNAL SUBROUTINE USED. THIS      DRKG 610
C                    SUBROUTINE COMPUTES THE RIGHT HAND SIDES DERY OF   DRKG 620
C                    THE SYSTEM TO GIVEN VALUES X AND Y. ITS PARAMETER  DRKG 630
C                    LIST MUST BE X,Y,DERY. SUBROUTINE FCT SHOULD       DRKG 640
C                    NOT DESTROY X AND Y.                               DRKG 650
C           OUTP   - THE NAME OF AN EXTERNAL OUTPUT SUBROUTINE USED.    DRKG 660
C                    ITS PARAMETER LIST MUST BE X,Y,DERY,IHLF,NDIM,PRMT.DRKG 670
C                    NONE OF THESE PARAMETERS (EXCEPT, IF NECESSARY,    DRKG 680
C                    PRMT(4),PRMT(5),...) SHOULD BE CHANGED BY          DRKG 690
C                    SUBROUTINE OUTP. IF PRMT(5) IS CHANGED TO NON-ZERO,DRKG 700
C                    SUBROUTINE DRKGS IS TERMINATED.                    DRKG 710
C           AUX    - DOUBLE PRECISION AUXILIARY STORAGE ARRAY WITH 8    DRKG 720
C                    ROWS AND NDIM COLUMNS.                             DRKG 730
C                                                                       DRKG 740
C        REMARKS                                                        DRKG 750
C           THE PROCEDURE TERMINATES AND RETURNS TO CALLING PROGRAM, IF DRKG 760
C           (1) MORE THAN 10 BISECTIONS OF THE INITIAL INCREMENT ARE    DRKG 770
C               NECESSARY TO GET SATISFACTORY ACCURACY (ERROR MESSAGE   DRKG 780
C               IHLF=11),                                               DRKG 790
C           (2) INITIAL INCREMENT IS EQUAL TO 0 OR HAS WRONG SIGN       DRKG 800
C               (ERROR MESSAGES IHLF=12 OR IHLF=13),                    DRKG 810
C           (3) THE WHOLE INTEGRATION INTERVAL IS WORKED THROUGH,       DRKG 820
C           (4) SUBROUTINE OUTP HAS CHANGED PRMT(5) TO NON-ZERO.        DRKG 830
C                                                                       DRKG 840
C        SUBROUTINES AND FUNCTION SUBPROGRAMS REQUIRED                  DRKG 850
C           THE EXTERNAL SUBROUTINES FCT(X,Y,DERY) AND                  DRKG 860
C           OUTP(X,Y,DERY,IHLF,NDIM,PRMT) MUST BE FURNISHED BY THE USER.DRKG 870
C                                                                       DRKG 880
C        METHOD                                                         DRKG 890
C           EVALUATION IS DONE BY MEANS OF FOURTH ORDER RUNGE-KUTTA     DRKG 900
C           FORMULAE IN THE MODIFICATION DUE TO GILL. ACCURACY IS       DRKG 910
C           TESTED COMPARING THE RESULTS OF THE PROCEDURE WITH SINGLE   DRKG 920
C           AND DOUBLE INCREMENT.                                       DRKG 930
C           SUBROUTINE DRKGS AUTOMATICALLY ADJUSTS THE INCREMENT DURING DRKG 940
C           THE WHOLE COMPUTATION BY HALVING OR DOUBLING. IF MORE THAN  DRKG 950
C           10 BISECTIONS OF THE INCREMENT ARE NECESSARY TO GET         DRKG 960
C           SATISFACTORY ACCURACY, THE SUBROUTINE RETURNS WITH          DRKG 970
C           ERROR MESSAGE IHLF=11 INTO MAIN PROGRAM.                    DRKG 980
C           TO GET FULL FLEXIBILITY IN OUTPUT, AN OUTPUT SUBROUTINE     DRKG 990
C           MUST BE FURNISHED BY THE USER.                              DRKG1000
C           FOR REFERENCE, SEE                                          DRKG1010
C           RALSTON/WILF, MATHEMATICAL METHODS FOR DIGITAL COMPUTERS,   DRKG1020
C           WILEY, NEW YORK/LONDON, 1960, PP.110-120.                   DRKG1030
C                                                                       DRKG1040

      subroutine drkgs( prmt, y, dery, ndim, ihlf, fct, outp, aux )
c to solve a system of first order differential equations with given
c initial values by means of 4th order Runge-Kutta method.
c ... for description, see IBM SSP, page 333.
c
	implicit real*8(a-h,o-z)
      REAL*8 Y(6), DERY(6), AUX(8,6),  PRMT(20), t
	External fct, outp
      real*8 A, B, C, X, XEND, H, AJ, BJ, CJ, R1, R2, DELT
      dimension  A(4), B(4), C(4)
c
        do 1  I = 1, NDIM
1       AUX(8,I) = 0.066666666666666667D0 * DERY(I)
        X = PRMT(1)
        XEND = PRMT(2)
        H = PRMT(3)
        PRMT(5) = 0.0D0
        call FCT( X, Y, DERY )
c
        if( H*( XEND-X ) )      38, 37, 2
c
2       A(1) = 0.5D0
        A(2) = 0.29289321881345248D0
        A(3) = 1.7071067811865475D0
        A(4) = 0.16666666666666667D0
        B(1) = 2.0D0
        B(2) = 1.0D0
        B(3) = 1.0D0
        B(4) = 2.0D0
        C(1) = 0.5D0
        C(2) = 0.29289321881345248D0
        C(3) = 1.7071067811865475D0
        C(4) = 0.5D0
c
        do 3 i = 1, NDIM
        AUX(1,i) = Y(i)
        AUX(2,i) = DERY(i)
        AUX(3,i) = 0.0D0
3       AUX(6,i) = 0.0D0
        irec = 0
        H = H + H
        IHLF = -1
        ISTEP = 0
        IEND = 0
c
4       if( ( X+H-XEND )*H )    7, 6, 5
5       H = XEND - X
6       IEND = 1
c
7       call OUTP( X, Y, DERY, IREC, NDIM, PRMT )
        if( PRMT(5) )          40, 8, 40
8       itest = 0
9       ISTEP = ISTEP + 1
c
        j = 1
10      AJ = A(j)
        BJ = B(j)
        CJ = C(j)
        do 11  i = 1, NDIM
        R1 = H * DERY(i)
        R2 = AJ * ( R1 - BJ*AUX(6,i) )
        Y(i) = Y(i) + R2
        R2 = R2 + R2 + R2
11      AUX(6,i) = AUX(6,i) + R2 - CJ*R1
        if( j-4 )               12, 15, 15
12      j = j + 1
        if( j-3 )               13, 14, 13
13      X = X + 0.5D0*H
14      call FCT( X, Y, DERY )
        go to 10
c
15      if( itest )             16, 16, 20
c
16      do 17 i = 1, NDIM
17      AUX(4,i) = Y(i)
        itest = 1
        ISTEP = ISTEP + ISTEP - 2
18      IHLF  = IHLF + 1
        X = X - H
        H = 0.5D0 * H
        do 19  i = 1, NDIM
        Y(i) = AUX(1,i)
        DERY(i) = AUX(2,i)
19      AUX(6,i) = AUX(3,i)
        go to 9
c
20      imod = ISTEP / 2
        if( ISTEP-imod-imod )   21, 23, 21
21      call FCT( X, Y, DERY )
        do 22 i = 1, NDIM
        AUX(5,i) = Y(i)
22      AUX(7,i) = DERY(i)
        go to 9
c
23      delt = 0.D0
        do 24  i = 1, NDIM
24      delt = delt + AUX(8,i) * dabs( AUX(4,i) - Y(i) )
        if( delt - PRMT(4) )    28, 28, 25
c
25      if( IHLF - 30 )         26, 36, 36
26      do 27  i = 1, NDIM
27      AUX(4,i) = AUX(5,i)
        ISTEP = ISTEP + ISTEP - 4
        X = X - H
        IEND = 0
        go to 18
c
28      call FCT( X, Y, DERY )
        do 29  i = 1, NDIM
        AUX(1,i) = Y(i)
        AUX(2,i) = DERY(i)
        AUX(3,i) = AUX(6,i)
        Y(i) = AUX(5,i)
29      DERY(i) = AUX(7,i)
        call OUTP( X-H, Y, DERY, IHLF, NDIM, PRMT )
        if( PRMT(5) )           40, 30, 40
30      do 31 i = 1, NDIM
        Y(I) = AUX(1,i)
31      DERY(I) = AUX(2,i)
        irec = IHLF
        if( IEND )              32, 32, 39
c
32      IHLF = IHLF - 1
        ISTEP = ISTEP / 2
        H = H + H
        if( IHLF )              4, 33, 33
33      imod = ISTEP / 2
        if( ISTEP-imod-imod )   4, 34, 4
34      if( DELT-0.02D0*PRMT(4) )       35, 35, 4
35      IHLF = IHLF - 1
        ISTEP = ISTEP / 2
        H = H + H
        go to 4
c
36      IHLF = 31
        call FCT( X, Y, DERY )
        go to 39
37      IHLF = 32
        go to 39
38      IHLF = 33
39      call OUTP( X, Y, DERY, IHLF, NDIM, PRMT )
40      return
        end
