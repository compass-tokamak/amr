'''Main AMR program
'''
## @package ebe6
## Main AMR program


# Import modules
import string,os,sys,time
import subprocess
import pyAMR.ebeResult6 as ebeResult6 #@UnresolvedImport
import pyAMR.pyLUKE as pyLUKE
import pyAMR.fileNames6 as fn #@UnresolvedImport
from pyAMR.pyAMR import * #@UnresolvedImport
try:
    import pp
    use_pp = True
except ImportError:
    use_pp = False
import pyAMR.which as which #@UnresolvedImport

#if __name__ == '__main__':
#    pass

# setup directories and paths
simulation_path = os.getcwd()
amr_path = sys.path[0]
dir_list = ['input', \
            'output', \
            'idl', \
            'ebe_run', \
            'ebe_integration', \
            os.path.join('output','luke'), \
            os.path.join('output','figures'), \
            os.path.join('output','figures_profiles'), \
            os.path.join('output','figures_rt_details'), \
            os.path.join('output','figures_wkbcurves'), \
            os.path.join('ebe_run','output'), \
            os.path.join('ebe_run','protocol'), \
            os.path.join('ebe_integration','output'), \
            os.path.join('ebe_integration','protocol'), \
            ]
for dir in dir_list:
    if not os.path.isdir(dir): os.mkdir(dir)

# Get actual time
time1=time.time()

# Open configuration files
if len(sys.argv)>1:
    cfgfilename = sys.argv[1]
else:
    cfgfilename = 'ebe6.cfg'
defaults_cfgfilename = os.path.join(amr_path,'ebe6_defaults.cfg')

# Open the protocol file
p_file = open('output/protocol.txt','w')
protocol(p_file,'AMR run from '+simulation_path+', AMR location: '+amr_path)
protocol(p_file,'Config file: '+cfgfilename+', default config: '+defaults_cfgfilename)
# Write current date & time
protocol(p_file,time.strftime("%a, %d %b %Y %H:%M:%S %Z"))
# Write current SVN release
get = subprocess.Popen(['svn','info',amr_path], stdout=subprocess.PIPE).stdout
protocol(p_file,'AMR code subversion information:')
for svninfo in get.readlines():
    if ('URL:' in svninfo) | ('Revision:' in svninfo):
        protocol(p_file,svninfo.rstrip())
get.close()
get = subprocess.Popen(['svnversion',amr_path], stdout=subprocess.PIPE).stdout
for svninfo in get.readlines():
    protocol(p_file,'svnversion: '+svninfo.rstrip())
get.close()
protocol(p_file,'----------------------------')

amr_cfg = readConfig(cfgfilename,defaults_cfgfilename)
if 'MATLABPATH' in os.environ:
    os.environ['MATLABPATH'] += os.pathsep + amr_path
else:
    os.environ['MATLABPATH'] = amr_path
antennas = initAntennas(amr_cfg)
ebe_times = initShotTimes(amr_cfg)

cluster_run = (amr_cfg['output']['ray_tracing'] or amr_cfg['output']['conv_eff']) and (amr_cfg['cluster']['use_cluster'])
setpriority(amr_cfg['cluster']['master_priority'])

maxfreq = 0
for (ant,iAnt) in zip(antennas,range(len(antennas))):
    protocol(p_file,'Antenna %i configuration:' % iAnt)
    protocol(p_file,ant.dump(False))
    maxfreq = max(ant.frequencies+[maxfreq])

# calculate total number of rays
total_rays = 0
for Ant in antennas:
    if amr_cfg['output']['central_ray_only']:
        total_rays += Ant.getNumberOfConfigurations()
    else:
        total_rays += Ant.getNumberOfRays()
    total_rays *= len(ebe_times)

protocol(p_file,'------------------------------')
protocol(p_file,'Total rays = '+str(total_rays))

# check / adjust monotonization frequency
if amr_cfg['profiles']['monotonize']:
    if amr_cfg['profiles']['monotonize_freq'] == 0:
        amr_cfg['profiles']['monotonize_freq'] = maxfreq
    elif amr_cfg['profiles']['monotonize_freq'] < maxfreq:
        protocol(p_file,'Warning: Density monotonization turned on, but for a lower frequency then the maximum simulated')

# Writes 'find_mesh.dat' file
f_find_mesh=open('input/find_mesh.dat','w')
f_find_mesh.write('%e collision frequency\n' % amr_cfg['adaptive']['coll_freq'])
f_find_mesh.write('%i n_init\n' % amr_cfg['adaptive']['n_init'])
f_find_mesh.write('%e accuracy\n' % amr_cfg['adaptive']['accuracy'])
f_find_mesh.write('%e accuracy_ap\n' % amr_cfg['adaptive']['accuracy_ap'])
f_find_mesh.close()

# Runs IDL programs ('ebe_run.pro') to get plasma profiles
if (amr_cfg['input']['run_IDL']) :
    f_idl_in=open('idl/ebe_data.in','w')
    f_idl_in.write('%i\n' % amr_cfg['input']['shot_number'])
    f_idl_in.write('%i\n' % amr_cfg['profiles']['ts_data_no'])
    f_idl_in.write(amr_cfg['profiles']['equil_ver'] + '\n')
    for t in ebe_times:
        f_idl_in.write('%5.3f\n' % t)
    f_idl_in.close()
    # os.spawnlp(os.P_WAIT,'idl','idl','ebe_run.pro')
    run_idl('ebe_run_'+amr_cfg['input']['experiment']+'.pro',amr_path)
    # get = os.popen('mv *.out input')

preAMR(amr_cfg,ebe_times)

# Write 'shot_parameters.dat' file
writeShotParamsFile(amr_cfg,'input/shot_parameters.dat')

# Compile the code if desired
# if makeall:
#    get = os.popen('make all')

protocol(p_file,'------------------------------')
protocol(p_file,'Shot number: ')
protocol(p_file,amr_cfg['input']['shot_number'])
protocol(p_file,'Times to simulate:')
protocol(p_file,map(round3,ebe_times))

# Construct batch files ebe_xxxx for each processor (id)
pid=0
rays_in_pid=0
use_pp = use_pp and amr_cfg['cluster']['use_pp']
job_server = None
if amr_cfg['cluster']['ncpu']==0:
    if cluster_run:
        protocol(p_file,'warning: setting number of cluster jobs to 5 (default value)')
        amr_cfg['cluster']['ncpu'] = 5
    else:
        if use_pp:
            job_server = pp.Server(ppservers=amr_cfg['cluster']['pp_servers'])
            time.sleep(0.2) # for some reason a delay is necessary for get_active_nodes
            ppcpus = job_server.get_active_nodes()
            protocol(p_file, 'pp active nodes: '+str(ppcpus))
            amr_cfg['cluster']['ncpu'] = sum((i for i in ppcpus.itervalues()))
            # job_server.destroy()
        else:
            amr_cfg['cluster']['ncpu'] = detectCPUs()
elif not use_pp:
    amr_cfg['cluster']['ncpu'] = min(detectCPUs(),amr_cfg['cluster']['ncpu']) # force maximum CPUs when not using pp
    
if amr_cfg['cluster']['ncpu']==1: use_pp = False

if amr_cfg['cluster']['jobDistribution'] == 'optimum':
    # optimum distribution
    rays_per_cpu = total_rays // amr_cfg['cluster']['ncpu']
    if total_rays % amr_cfg['cluster']['ncpu'] > 0: rays_per_cpu = rays_per_cpu + 1
    protocol(p_file,"Rays per job (CPU) = %i" % rays_per_cpu)
    if amr_cfg['output']['run_code']:
        [f_batch,f_init] = ebe_files_init(amr_cfg,pid,amr_cfg['output']['ray_tracing'],amr_cfg['output']['conv_eff'],cluster_run)
    rays_in_pid = 0
    for t in ebe_times:
        for iAnt in range(len(antennas)):
            if antennas[iAnt].hasWindow:
                writeWindowFile(iAnt,antennas[iAnt])
            for iConf in range(antennas[iAnt].getNumberOfConfigurations()):
                runWindowProjection = antennas[iAnt].hasWindow
                beamWaist = antennas[iAnt].getBeamWaist(iConf)
                iray0 = 1
                while True:
                    if amr_cfg['output']['central_ray_only']:
                        nBeamRays = 1
                    else:
                        nBeamRays = beamWaist.getNumberOfRays()
                    if nBeamRays - iray0 + 1 <= rays_per_cpu - rays_in_pid:
                        # vejdou se vsechny zbyvajici tohoto svazku
                        if amr_cfg['output']['run_code']:
                            f_init.write(ebeInputLine(t,iAnt,antennas[iAnt],iConf,beamWaist,iray0,nBeamRays,runWindowProjection))
                        rays_in_pid += nBeamRays - iray0 + 1
                        iray0 = 1
                    else:
                        # vejde se jenom cast
                        if amr_cfg['output']['run_code']:
                            f_init.write(ebeInputLine(t,iAnt,antennas[iAnt],iConf,beamWaist,iray0,iray0+(rays_per_cpu-rays_in_pid)-1,runWindowProjection))
                        iray0 = iray0+(rays_per_cpu-rays_in_pid)
                        rays_in_pid = rays_per_cpu
                    if rays_in_pid == rays_per_cpu:
                        # naplnili jsme tento job
                        if amr_cfg['output']['run_code']:
                            f_init.close()
                            if cluster_run:
                                f_batch.close()
                                os.chmod(ebe_bname(pid),511)
                            runWindowProjection = False
                            [f_batch,f_init] = ebe_files_init(amr_cfg,pid+1,amr_cfg['output']['ray_tracing'],amr_cfg['output']['conv_eff'],cluster_run)
                        pid = pid+1
                        rays_in_pid = 0
                    if iray0 == 1:
                        break
    if amr_cfg['output']['run_code']:
        f_init.close()
        if cluster_run:
            f_batch.close()
            try:
                os.chmod(ebe_bname(pid),511)
            except OSError:
                protocol(p_file,'error: os.chmod failed for ' + ebe_bname(pid))
    npid = pid+1
    if (rays_in_pid == 0):
    # the last file is empty
        if amr_cfg['output']['run_code']:
            try:
                if cluster_run: os.remove(ebe_bname(pid))
                os.remove('input/' + ebe_fname(pid))
            except OSError:
                print 'err'
                protocol(p_file,'error: os.remove failed for ' + ebe_bname(pid) + ' or ' + 'input/' + ebe_fname(pid))
        npid = npid-1
    protocol(p_file,"Number of jobs = %i" % npid)

if amr_cfg['cluster']['jobDistribution'] == 'beam':
    # beam distribution
    protocol(p_file,"Beam job distribution")
    pid = 0
    for t in ebe_times:
        for iAnt in range(len(antennas)):
            runWindowProjection = antennas[iAnt].hasWindow
            if antennas[iAnt].hasWindow:
                writeWindowFile(iAnt,antennas[iAnt].getWindow())
            for iConf in range(antennas[iAnt].getNumberOfConfigurations()):
                beamWaist = antennas[iAnt].getBeamWaist(iConf)
                if amr_cfg['output']['run_code']:
                    [f_batch,f_init] = ebe_files_init(amr_cfg,pid,amr_cfg['output']['ray_tracing'],amr_cfg['output']['conv_eff'],cluster_run)
                    if amr_cfg['output']['central_ray_only']:
                        f_init.write(ebeInputLine(t,iAnt,antennas[iAnt],iConf,beamWaist,1,1,runWindowProjection))
                    else:
                        f_init.write(ebeInputLine(t,iAnt,antennas[iAnt],iConf,beamWaist,1,beamWaist.getNumberOfRays(),runWindowProjection))
                    f_init.close()
                    if cluster_run:
                        f_batch.close()
                        os.chmod(ebe_bname(pid),493)
                pid = pid+1
    npid = pid
    protocol(p_file,"Number of jobs = %i" % npid)


#for t in ebe_times:
#    print "time " + str(t)
#    for iAnt in range(len(antennas)):
#        print "Antenna " + str(antennas[iAnt].id)
#        for iConf in range(antennas[iAnt].getNumberOfConfigurations()):
#            print "iConf " + str(iConf)
#            for iRay in range(1,antennas[iAnt].getBeamWaist(iConf).getNumberOfRays()+1):
#                print "iRay " + str(iRay)
#                print str(findRayOutput(amr_cfg,rays_per_cpu,t,ebe_times,antennas,iAnt,iConf,iRay))

# Run test_profiles if desired
# write the test_profiles input file with all the freqs, times etc.
if amr_cfg['output']['test_profiles']:
    f_test_in=open('input/ebe_test_in.dat','w')
    f_test_in.write(ebeFileHeader)
    for t in ebe_times:
        for iAnt in range(len(antennas)):
            for iConf in range(antennas[iAnt].getNumberOfConfigurations()):
                beamWaist = antennas[iAnt].getBeamWaist(iConf)
                if amr_cfg['output']['central_ray_only']:
                    f_test_in.write(ebeInputLine(t,iAnt,antennas[iAnt],iConf,beamWaist,1,1))
                else:
                    f_test_in.write(ebeInputLine(t,iAnt,antennas[iAnt],iConf,beamWaist,1,beamWaist.getNumberOfRays()))
    f_test_in.close()

    f_tp=open('input/test_profiles.dat','w')
    f_tp.write('%i %f %f Rdim Rmin Rmax\n' % (amr_cfg['test_profiles']['R_dim'],amr_cfg['test_profiles']['R_min'],amr_cfg['test_profiles']['R_max']))
    f_tp.write('%i %f %f Zdim Zmin Zmax\n' % (amr_cfg['test_profiles']['Z_dim'],amr_cfg['test_profiles']['Z_min'],amr_cfg['test_profiles']['Z_max']))
    f_tp.write('%i %f %f phidim phimin phimax\n' % (amr_cfg['test_profiles']['phi_dim'],amr_cfg['test_profiles']['phi_min'],amr_cfg['test_profiles']['phi_max']))
    f_tp.write('%f phi_test\n' % amr_cfg['test_profiles']['phi_test'])
    f_tp.write('%i ir_ray_test - if 0, all rays, no wkbcurve  - if 50 all rays with wkbcurve (only for non zero shot_time_test)\n' % amr_cfg['test_profiles']['wkb_curve_ray'])
    f_tp.write('%f shot_time_test - if zero all times \n' % amr_cfg['test_profiles']['shot_time_test'])
    f_tp.write('%f tp_Nparal_above - usualy Nparal on the plasma boundary (SOL) from Jray_iray_f.dat \n' % amr_cfg['test_profiles']['Nparal_above'])
    f_tp.write('%f tp_Nparal_below - Nparal at absorption from end_of_ray_eff.dat \n' % amr_cfg['test_profiles']['Nparal_below'])
    f_tp.close()

# pre-run or test_profiles
if amr_cfg['output']['run_code'] or amr_cfg['output']['test_profiles']:
    # clear LUKE inputs
    if amr_cfg['luke']['outputs']:
        for f in os.listdir(u'output/luke'):
            ff = u'output/luke/'+f
            if os.path.isfile(ff): os.remove(ff)

# things that run locally only
if cluster_run:
    protocol(p_file,'Running local programs')
    runFlags = 0
    runFlags |= runFlagsConst['ProfilesExport']
    if amr_cfg['output']['test_profiles']:
        runFlags |= runFlagsConst['TestProfiles']
    if runFlags > 0:
        os.chdir('ebe_run')
        tp_pid = os.spawnl(os.P_NOWAIT,os.path.join(amr_path,"ebe_run.exe"),\
                                       os.path.join(amr_path,"ebe_run.exe"),str(runFlags))
        os.waitpid(tp_pid,0)
        # alternative with subprocess
        # retcode = subprocess.call([os.path.join(amr_path,"ebe_run.exe"),str(runFlagsConst['TestProfiles'])])
        os.chdir('..')
    protocol(p_file,'Finished local programs')
    
if (cluster_run and amr_cfg['output']['run_code']):
    qids = []
    # que the jobs and get their id's into qids
    for i in range(min(amr_cfg['cluster']['ncpu'],npid)):
        get = subprocess.Popen(['qsub',ebe_bname(i)], stdout=subprocess.PIPE).stdout
        qids.append(string.strip(get.readline()))
        get.close()
    # if no. of cpu's < no. of processes, some of the jobs must be submitted later
    pids_waiting = []
    for i in range(amr_cfg['cluster']['ncpu'],npid):
        pids_waiting.append(i)

    protocol(p_file,'Jobs in que:')
    protocol(p_file,qids)
    if pids_waiting:
        protocol(p_file,'%i job(s) waiting to be submitted' % len(pids_waiting))
    # wait while there are jobs in the que (or running)
    while qids:
        time.sleep(amr_cfg['cluster']['check_int']*60)
        for qid in qids:
            # get = os.popen('qstat -a | grep '+qid)
            p1 = subprocess.Popen(["qstat","-a"], stdout=subprocess.PIPE)
            p2 = subprocess.Popen(["grep", qid], stdin=p1.stdout, stdout=subprocess.PIPE)
            if p2.communicate()[0]=='': qids.remove(qid)
        if pids_waiting:
            for i in range(min(amr_cfg['cluster']['ncpu']-len(qids),len(pids_waiting))):
                # get = os.popen('qsub '+ebe_bname(pids_waiting[0]))
                get = subprocess.Popen(['qsub',ebe_bname(pids_waiting[0])], stdout=subprocess.PIPE).stdout
                qids.append(string.strip(get.readline()))
                pids_waiting[0:1]=[]
                get.close()
        if qids:
            protocol(p_file,'Checking ... ' + str(len(qids)) + ' job(s) still in que: ')
            protocol(p_file,qids)
        if pids_waiting:
            protocol(p_file,'%i job(s) waiting to be submitted' % len(pids_waiting))

elif (amr_cfg['output']['run_code'] or amr_cfg['output']['test_profiles']):
    """
    Run locally
    """
    protocol(p_file,'###############################')
    protocol(p_file,'  Running on this machine ...')
    protocol(p_file,'###############################')
    runFlags = 0
    runFlags |= runFlagsConst['ProfilesExport'] # run export by default
    if amr_cfg['output']['run_code']:
        if amr_cfg['output']['ray_tracing']:
            runFlags |= runFlagsConst['RayTracing']
            runFlags |= runFlagsConst['WaistData']
        if amr_cfg['output']['conv_eff']:
            runFlags |= runFlagsConst['ConvEff']
            runFlags |= runFlagsConst['WaistData']
    if amr_cfg['output']['test_profiles']:
        runFlags |= runFlagsConst['TestProfiles']
    if runFlags>0:
        if use_pp:
            if not job_server: job_server = pp.Server(ppservers=amr_cfg['cluster']['pp_servers']) #,ncpus=amr_cfg['cluster']['ncpu'])
            protocol(p_file,'Using ParallelPython, nCPUs=%i' %  amr_cfg['cluster']['ncpu'])
        os.chdir('ebe_run')
        pp_jobs = []
        def pp_call(params,dir):
            # import subprocess,os
            os.chdir(dir)
            stdo = open('protocol/stdout_job_'+params[2]+'.txt','w')
            p = subprocess.Popen(params, stdin=subprocess.PIPE, stdout=stdo,stderr=stdo)
            # for l in p.stdout: print(l.strip())
            stdo.close()
            return p.wait()
        stdo=[]
        for i in range(npid):
            run_prog = os.path.join(amr_path,"ebe_run","ebe_run.exe")
            flags = (runFlags if i==0 else (runFlags & ~runFlagsConst['TestProfiles'] & ~runFlagsConst['ProfilesExport']))
            run_params = [run_prog,str(flags),str(i)]
            dir = os.getcwd()
            if use_pp:
#                pp_jobs.append(job_server.submit(subprocess.call,(run_params,),modules=('subprocess',)))
                pp_jobs.append(job_server.submit(pp_call,(run_params,dir),modules=('subprocess',)))
            else:
                # retcode = subprocess.call(run_params)
                stdo.append(open('protocol/stdout_job_'+run_params[2]+'.txt','w'))
                pp_jobs.append(subprocess.Popen(run_params,stdin=subprocess.PIPE,stdout=stdo[-1],stderr=stdo[-1]))
        if use_pp:
            protocol(p_file,'Wating for pp jobs to finish')
            job_server.wait()
            for i,job in enumerate(pp_jobs):
                print('Job %i returned %i' % (i,job()))
            protocol(p_file,str(job_server.get_stats()))
            # job_server.destroy()
        else:
            for i,job in enumerate(pp_jobs): 
                print('Job %i returned %i' % (i,job.wait()))
                stdo[i].close()
#        tp_pid = os.spawnl(os.P_NOWAIT,os.path.join(amr_path,"ebe_run","ebe_run.exe"), \
#                           os.path.join(amr_path,"ebe_run","ebe_run.exe"),str(runFlags))
#        os.waitpid(tp_pid,0)
        # alternative with subprocess
        # retcode = subprocess.call([os.path.join(amr_path,"ebe_run","ebe_run.exe"),str(runFlagsConst['TestProfiles'])])
        os.chdir('..')

# output eaten time
time2=time.time()-time1
protocol(p_file,'Ugh ... finished with the codes.')
protocol(p_file,'Time eaten: '+str(int(time2)//3600)+'h:'+str((int(time2)%3600)//60)+'m:'+str((int(time2)%60))+'s.')

# ===========================================
# post processing - output files, integration
# ===========================================

integrace = (((amr_cfg['output']['ray_tracing'] or amr_cfg['output']['conv_eff']) and not(amr_cfg['output']['central_ray_only'])) and amr_cfg['output']['postprocess'])

if (amr_cfg['output']['postprocess']):
    # waist_data = False
    waist_data = amr_cfg['output']['conv_eff'] | amr_cfg['output']['ray_tracing']
    skip_line=False
    # ebe_results is an array of all ebe_run results
    ebe_results = []
    for pid in range(npid):
        # do out_files dame dvojice [typ_vystupu,file_handle]
        out_files = []
        if amr_cfg['output']['conv_eff']:
            ce_out_file = open('ebe_run/output/conv_eff_'+pidstr(pid)+'.dat','r')
            ce_out_file.readline()
            out_files.append(['ce',ce_out_file])
        if amr_cfg['output']['ray_tracing']:
            rt_out_file = open('ebe_run/output/Trad_'+pidstr(pid)+'.dat','r')
            rt_out_file.readline()
            out_files.append(['rt',rt_out_file])
        if waist_data:
            wd_out_file = open('ebe_run/output/waist_data_'+pidstr(pid)+'.dat','r')
            wd_out_file.readline()
            out_files.append(['wd',wd_out_file])
        # prvni soubor z out_files
        iLine = 0
        for d_in in out_files[0][1].readlines():
            iLine += 1
#            print 'iLine '+str(iLine)
#            print d_in
            # ebe_result is a single result from ebe_run
            ebe_result=ebeResult6.ebeResult()
            ebe_result.importData(out_files[0][0],d_in,False)
            # ostatni soubory z out_files
            for out_file in out_files[1:]:
                d_in = out_file[1].readline()
                res = ebe_result.importData(out_file[0],d_in,True)
                if not(res):
                    protocol(p_file,'error: mismatch in ebe output files')
                    sys.exit(3)
            if [pid,iLine] != findRayOutput(amr_cfg,rays_per_cpu,ebe_result.time,ebe_times,antennas,ebe_result.iAntenna,ebe_result.iConfiguration,ebe_result.iRay):
                protocol(p_file,'error: inconsistent data')
                protocol(p_file,'ebe_out_'+pidstr(pid)+'.dat, line %i: iAntenna=%i, iConf=%i, iRay=%i' % \
                         (iLine,ebe_result.iAntenna,ebe_result.iConfiguration,ebe_result.iRay))
                protocol(p_file,'should be in [pid,line]=' +
                         str(findRayOutput(amr_cfg,rays_per_cpu,ebe_result.time,ebe_times,antennas,ebe_result.iAntenna,ebe_result.iConfiguration,ebe_result.iRay)))
                sys.exit(3)
                # sem muze jit algoritmus na opravu
            ebe_results.append(ebe_result)
        if amr_cfg['output']['conv_eff']:
            ce_out_file.close()
        if amr_cfg['output']['ray_tracing']:
            rt_out_file.close()
        if waist_data:
            wd_out_file.close()
    # ebe_results now contains all the ebe_run results
    ray_file = open('output/results_ray.dat','w')
    ray_file.write('#time,iAntenna,iConf,tor_angle,pol_angle,E_eq_angle,freq,iRay,conv_eff1,conv_eff2,conv_eff1+2,Trad,coll_absp,intensity,power,Npar,Npol,visible_area,transm_coef,x_waist,y_waist\n')
    if amr_cfg['luke']['outputs']:
        luke_t_prev = None
        luke_iAnt_prev = None
        luke_iConf_prev = None
        luke_ray_file = None
    for ebe_result in ebe_results:
        t = ebe_result.time
        iAnt = ebe_result.iAntenna
        iConf = ebe_result.iConfiguration
        iRay = ebe_result.iRay
        ant = antennas[iAnt]
        conf = ant.getBeamWaist(iConf)
        angles = ant.getAngles(iConf)
        ray_file.write('%+.6e %+5i %+5i ' % (t,iAnt,iConf) + \
                       '%+.6e %+.6e %+.6e %+.6e %+5i ' % \
                       (angles[0],angles[1],angles[2],conf.freq,iRay) + \
                       '%+.6e %+.6e %+.6e %+.6e ' % \
                       (ebe_result.conv_eff1,ebe_result.conv_eff2,\
                        ebe_result.conv_eff1+ebe_result.conv_eff2,ebe_result.Trad) + \
                        '%.6e ' % ebe_result.rt_coll_absp + \
                       '%+.6e %+.6e %+.6e %+.6e %+.6e %+.6e %+.6e %+.6e ' % \
                       (ebe_result.intensity,ebe_result.power,ebe_result.Npar,ebe_result.Npol, \
                        ebe_result.visible_area,ebe_result.transm_coef,ebe_result.x_waist,ebe_result.y_waist) + \
                       '\n')
        if amr_cfg['luke']['outputs']:
            if (luke_t_prev!=t) or (luke_iAnt_prev!=iAnt) or (luke_iConf_prev!=iConf):
                luke_t_prev = t
                luke_iAnt_prev = iAnt
                luke_iConf_prev = iConf
                if luke_ray_file!=None: luke_ray_file.close()
                luke_ray_file = open('output/luke/luke_rays' + \
                                     fn.getAntConfSuffix(iAnt,iConf) + \
                                     fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],t) + \
                                     '.dat','w')
            # put 100% conversion when not calculated
            if not amr_cfg['output']['conv_eff']:
                luke_conv_eff = 1.0
            else:
                luke_conv_eff = min(ebe_result.conv_eff1+ebe_result.conv_eff2,1.0)
            # full power to the central ray if central_ray_only
            if amr_cfg['output']['central_ray_only']:
                luke_power = antennas[iAnt].power
            else:
                luke_power = ebe_result.power
            if amr_cfg['ray_tracing']['collisional_damping_model']!=CollisionalDampingModels['none']:
                luke_power *= (1-ebe_result.rt_coll_absp)
            luke_ray_file.write('%.3f %2i %4i ' % (t,iAnt,iConf) + \
                                '%+.6e %+.6e ' % (conf.freq,luke_power) + \
                                '%+.6e %+.6e ' % (luke_conv_eff,ebe_result.transm_coef) + \
                                '%+.6e %+.6e %+.6e %+.6e \n' % (conf.x_center,conf.y_center,conf.z_center,conf.waist_radius))
    ray_file.close()
    if amr_cfg['luke']['outputs']: luke_ray_file.close()

# sys.exit(0)

can_sum = False
if (integrace and amr_cfg['output']['postprocess']):
    # no_of_rays and n_window_points identical for all antennas and configurations
    no_of_rays = antennas[0].getBeamWaist(0).getNumberOfRays()
    f_ic=open('input/int_config.dat','w')
    f_ic.write('%i no_of_rays\n' % no_of_rays)
    f_ic.write('%i window_points\n' % antennas[0].n_window_points)
    if amr_cfg['integration']['integrate_R']:
        f_ic.write('T integrate_R\n')
        f_ic.write('%g R_min\n' % amr_cfg['integration']['R_min'])
        f_ic.write('%g R_max\n' % amr_cfg['integration']['R_max'])
    else:
        f_ic.write('F integrate_R\n')
    if amr_cfg['integration']['integrate_flux']:
        f_ic.write('T integrate_flux\n')
        f_ic.write(amr_cfg['integration']['flux_coordinate']+'\n')
        f_ic.write('%g flux_min\n' % amr_cfg['integration']['flux_min'])
        f_ic.write('%g flux_max\n' % amr_cfg['integration']['flux_max'])
    else:
        f_ic.write('F integrate_flux\n')
    f_ic.write('%i tabdim\n' % amr_cfg['integration']['tabdim'])
    f_ic.write(amr_cfg['integration']['type']+'\n')
    f_ic.close()
    protocol(p_file,'Integrating ...')
    os.chdir('ebe_integration')
    int_pid = os.spawnl(os.P_NOWAIT,os.path.join(amr_path,"ebe_integration","ebe_integration.exe"),\
                                    os.path.join(amr_path,"ebe_integration","ebe_integration.exe"))
    os.waitpid(int_pid,0)
    os.chdir('..')
    if amr_cfg['output']['power_deposition'] & amr_cfg['output']['sum_antennas']:
        protocol(p_file,'Processing power deposition profiles ...')
        nconf = antennas[0].getNumberOfConfigurations()
        can_sum = True
        for ant in antennas[1:]:
            if ant.getNumberOfConfigurations() != nconf:
                protocol(p_file,'warning: antennas do not have same number of configurations, cannot sum power depositions')
                can_sum = False
                break
        if len(antennas)==1:
            protocol(p_file,'warning: only a single antenna found, power deposition summing useless')
            can_sum = False
        if can_sum:
            profile = ebeResult6.intProfileResult(amr_cfg['profiles']['n_components'])
            summed_profile = ebeResult6.intProfileResult(amr_cfg['profiles']['n_components'])
            for t in ebe_times:
                for iConf in range(nconf):
                    # pd_fileNames = []
                    pd_files = []
                    pd_file_types = []
                    # zjisteni jmen souboru a otevreni jednotlivych souboru
                    summed_files = []
                    for spdfname in fn.summedIntProfiles(iConf,t):
                        summed_files.append(open('output/'+spdfname,'w'))
                    for iAnt in range(len(antennas)):
                        # pdfs = []
                        # pd_fileNames.append(fn.intProfiles(iAnt, iConf, t))
                        # for pdfname in pd_fileNames[-1]:
                        pd_files.append([])
                        pd_file_types.append([])
                        for pdfname in fn.intProfiles(iAnt, iConf, t):
                            # pdfs.append(open(pdfname,'r'))
                            if ((ebeResult6.getFileType(pdfname)=='r') and amr_cfg['integration']['integrate_R']) or \
                            ((ebeResult6.getFileType(pdfname)=='reff') and amr_cfg['integration']['integrate_flux']):
                                pd_files[-1].append(open(pdfname,'r'))
                                pd_file_types[-1].append(ebeResult6.getFileType(pdfname))
                        for i in range(len(pd_files[-1])):
                            if iAnt==0:
                                summed_files[i].write(pd_files[-1][i].readline())
                            else:
                                pd_files[-1][i].readline()
                    for i in range(len(pd_files[0])):
                        for dline in pd_files[0][i].readlines():
                            summed_profile.setType(pd_file_types[0][i])
                            summed_profile.importData(dline,0,iConf,t)
                            for iAnt in range(1,len(antennas)):
                                dline2 = pd_files[iAnt][i].readline()
                                profile.setType(pd_file_types[iAnt][i])
                                profile.importData(dline2,iAnt,iConf,t)
                                summed_profile += profile
                            summed_profile.writeln(summed_files[i])
                        for iAnt in range(len(antennas)):
                            pd_files[iAnt][i].close()
                for sf in summed_files:
                    sf.close()
#
if amr_cfg['output']['run_luke']:
    protocol(p_file,'Running LUKE')
    if use_pp and not job_server:
        job_server = pp.Server(ncpus=amr_cfg['cluster']['ncpu'])
        time.sleep(0.2) # for some reason a delay is necessary for get_active_nodes
        ppcpus = job_server.get_active_nodes()
        protocol(p_file, 'pp active nodes: '+str(ppcpus))
    pp_jobs = []
    def pp_call(params,path):
        os.chdir(path)
        p = subprocess.Popen(params, stdin=subprocess.PIPE, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        for l in p.stdout: print(l.strip())
        return p.wait()
    os.environ['AMR'] = amr_path
    if 'LUKE' in os.environ:
        if 'MATLABPATH' in os.environ:
            os.environ['MATLABPATH'] = os.environ['MATLABPATH'] + os.pathsep + \
            os.path.join(os.environ['LUKE'],'Project_DKE','Modules','AMR')
        else:
            os.environ['MATLABPATH'] = os.path.join(os.environ['LUKE'],'Project_DKE','Modules','AMR')
    else:
        protocol(p_file,'environment variable LUKE must be defined')
        sys.exit(5)
    for t in ebe_times:
        for iAnt in range(len(antennas)):
            for iConf in range(antennas[iAnt].getNumberOfConfigurations()):
                luke_id_simul = ''
                luke_script_call = pyLUKE.callString(amr_cfg['input']['experiment'],luke_id_simul,amr_cfg['input']['shot_number'],\
                                                     t,iAnt,iConf,amr_cfg['luke']['np'],amr_cfg['luke']['nt'],\
                                                     amr_cfg['luke']['dsmin'],amr_cfg['luke']['kmode'],amr_cfg['luke']['id_dkeparam'])
                protocol(p_file,luke_script_call)
                if use_pp:
                    protocol(p_file,'Launching LUKE, pp job %i' % (len(pp_jobs)+1))
                    pp_jobs.append(job_server.submit(pp_call,(luke_script_call.split(),os.getcwd()),modules=('subprocess','os')))
                else:
                    # warning! this is not optimized to launch only ncpu jobs at a time
                    # pp_jobs.append(subprocess.Popen(luke_script_call.split()))
                    pp_jobs.append(luke_script_call.split())
    if use_pp:
        job_server.wait()
        for i,job in enumerate(pp_jobs):
            print('Job %i returned %i' % (i,job()))
        # job_server.destroy()
    else:
        # launch ncpu jobs
        running_jobs = [subprocess.Popen(pp_jobs.pop()) for i in range(min(amr_cfg['cluster']['ncpu'],len(pp_jobs)))]
        while running_jobs!=[]:
            time.sleep(amr_cfg['cluster']['check_int']*60)
            for i,job in enumerate(running_jobs):
                if job.poll()!=None:
                    if pp_jobs!=[]:
                        running_jobs[i] = subprocess.Popen(pp_jobs.pop())
                    else:
                        running_jobs[i:i+1] = []
#        for i,job in enumerate(pp_jobs): 
#            print('Job %i returned %i' % (i,job.wait()))
    
    
    protocol(p_file,'Finished LUKE, use Matlab(+LUKE) to see the results')
#
if amr_cfg['figures']['create']:
    from numpy import *
    import matplotlib
    matplotlib.use('Agg')
    if 'mathtext.default' in matplotlib.rcParams: matplotlib.rc('mathtext',default='regular')
    import matplotlib.cm
    import pyAMR.amrplot as amrplot #@UnresolvedImport
    protocol(p_file,'Generating figures ...')
    try:
        haveSciPy = True
        from scipy.interpolate import interpolate
        from scipy.optimize import optimize
    except ImportError:
        haveSciPy = False
        protocol(p_file,"SciPy cannot be imported---some figures may be missing")
    plot_ext = '.' + amr_cfg['figures']['ext']
    plot_exp_data = amr_cfg['figures']['experimental_data']
    rho_LCFS = amr_cfg['figures']['rho_LCFS']
    plot_bw = amr_cfg['figures']['plot_bw']
    prof_exist = False
    flux_coord = amr_cfg['figures']['flux_coordinate']
    if amr_cfg['output']['test_profiles'] | amr_cfg['figures']['test_profiles']:
        prof_exist = True
        (prof0d,prof1d,prof1d_comp,prof2d,profpsi,profpsi_comp,profexp,prof3d,profequat) = readProfiles(amr_cfg,ebe_times)
        if amr_cfg['profiles']['n_components']>1:
            den_comp_colors = ['blue','green','cyan']
            temp_comp_colors = ['brown','red','yellow']
    # plot profiles
    if amr_cfg['figures']['test_profiles']:
        outdir = 'output/figures_profiles/'
        for [t,it] in zip(ebe_times,range(len(ebe_times))):
            psi_LCFS = prof0d[it].getData('psi_LCFS')[0]
            psi_axis = prof0d[it].getData('psi_axis')[0]
            R_axis = prof0d[it].getData('R_axis')[0]
            Z_axis = prof0d[it].getData('Z_axis')[0]
            RLCFS_LFS = prof0d[it].getData('RLCFS_LFS')[0]
            title = '$\\rho $ contours, shot %i, time %.3f s' % (amr_cfg['input']['shot_number'],t)
            amrplot.plotRZ(outdir + 'rho' + fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],t) + plot_ext,title=title, \
                           R=prof2d[it].getData('R'),Z=prof2d[it].getData('Z'),psi=prof2d[it].getData('rho') ,\
                           psilabels=False,psilabel='$\\rho $',\
                           psiLCFS=rho_LCFS,RZaxis=[R_axis,Z_axis],\
                           ncin=5,ncout=5 ,\
                           latex=amr_cfg['figures']['uselatex'])
    #        psi = (adata.getData('psi')-psi_axis)/(psi_LCFS-psi_axis)
    #        amrplot.plotRZ(outdir + 'psi_2D_%06i_%1.3f.png' % (amr_cfg['input']['shot_number'],t), \
    #                       R=adata.getData('R'),Z=adata.getData('Z'),psi=psi,\
    #                       psilabels=False,psilabel='$\\psi $',\
    #                       psiLCFS=1,RZaxis=None,ncin=5,ncout=5 ,\
    #                       latex=True)
            figname = outdir + 'freqs_R' + fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],t) + plot_ext
            title = 'Characteristic frequencies, shot %i, time %.3f s' % (amr_cfg['input']['shot_number'],t)
            hfreqs = []
            for ant in antennas: hfreqs += ant.getFrequencies().tolist()
            amrplot.plotfreqs(figname,prof1d[it],'R',title=title,xLCFS=RLCFS_LFS, \
                              fpe='fpe',fUHR='fUHR',fce='fce',nfce=[1,2,3,4],fce_limit=prof1d[it]['fUHR'].max(), \
                              xlabel='R [m]',ylabel='GHz', \
                              fpecolor = 'green',fUHRcolor = 'magenta', \
                              fcecolor = 'blue',
                              hfreqs = hfreqs, \
                              selectors=[],latex=amr_cfg['figures']['uselatex'])  
            # density and temperature profiles 
            figname = outdir + 'dente_R' + fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],t) + plot_ext
            title = 'Electron density and temperature, shot %i, time %.3f s' % (amr_cfg['input']['shot_number'],t)
            if (not amr_cfg['profiles']['analytical_profiles']) and plot_exp_data and len(profexp)>0:
                if amr_cfg['figures']['errbars']:
                    amrplot.plotdente(figname,prof1d[it],'R',title=title,xLCFS=RLCFS_LFS, \
                          den='denRZ',temp='TempRZ', \
                          xlabel='R [m]',ydenlabel='$10^{19}\\ m^{-3} $',ytemplabel='keV',\
                          denlabel='$n_e $',templabel='$T_e $', \
                          dencolor = 'blue',tempcolor = 'red', \
                          expdata=profexp[it],expx='R(m)',expden='den(1e19m-3)',exptemp='Te(keV)', \
                          denerr='dden(1e19m-3)',temperr='dTe(keV)',expxerr='dR(m)',\
                          selectors=[],latex=amr_cfg['figures']['uselatex'])
                else:
                    amrplot.plotdente(figname,prof1d[it],'R',title=title,xLCFS=RLCFS_LFS, \
                          den='denRZ',temp='TempRZ', \
                          xlabel='R [m]',ydenlabel='$10^{19}\\ m^{-3} $',ytemplabel='keV',\
                          denlabel='$n_e $',templabel='$T_e $', \
                          dencolor = 'blue',tempcolor = 'red', \
                          expdata=profexp[it],expx='R(m)',expden='den(1e19m-3)',exptemp='Te(keV)', \
                          selectors=[],latex=amr_cfg['figures']['uselatex'])
            else:
                amrplot.plotdente(figname,prof1d[it],'R',title=title,xLCFS=RLCFS_LFS, \
                      den='denRZ',temp='TempRZ', \
                      xlabel=r'R [m]',ydenlabel=r'$10^{19}\ m^{-3} $',ytemplabel=r'keV',\
                      denlabel=r'$n_e $',templabel=r'$T_e $', \
                      dencolor = 'blue',tempcolor = 'red', \
                      selectors=[],latex=amr_cfg['figures']['uselatex'])           
            # density and temperature profiles fo individual components
            if prof1d_comp!=[]:
                den_comp_colors = ['blue','green','cyan']
                temp_comp_colors = ['brown','red','yellow']
                figname = outdir + 'dente_R_comp' + fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],t) + plot_ext
                title = 'Electron density and temperature, shot %i, time %.3f s' % (amr_cfg['input']['shot_number'],t)
                dens = []
                denlabels = []
                temps = []
                templabels = []
                expdens = []
                exptemps = []
                expdenerrs = []
                exptemperrs = []
                for icomp in range(1,amr_cfg['profiles']['n_components']+1):
                    dens.append('denRZ_c%i' % icomp)
                    denlabels.append('$n_{e%i} $' % icomp)
                    temps.append('TempRZ_c%i' % icomp)
                    templabels.append('$T_{e%i} $' % icomp)
                    expdens.append('den_c%i(1e19m-3)' % icomp)
                    exptemps.append('Te_c%i(keV)' % icomp)
                    expdenerrs.append('dden_c%i(1e19m-3)' % icomp)
                    exptemperrs.append('dTe_c%i(keV)' % icomp)
                if not(amr_cfg['profiles']['analytical_profiles']):
                    if amr_cfg['figures']['errbars']:
                        amrplot.plotdente(figname,prof1d[it],'R',title=title,xLCFS=RLCFS_LFS, \
                              den=dens,temp=temps, \
                              xlabel='R [m]',ydenlabel='$10^{19}\\ m^{-3} $',ytemplabel='keV',\
                              denlabel=denlabels,templabel=templabels, \
                              dencolor = den_comp_colors,tempcolor = temp_comp_colors, \
                              expdata=profexp[it],expx='R(m)',expden=expdens,exptemp=exptemps, \
                              denerr=expdenerrs,temperr=exptemperrs,expxerr='dR(m)',\
                              selectors=[],latex=amr_cfg['figures']['uselatex'])
                    else:
                        amrplot.plotdente(figname,prof1d[it],'R',title=title,xLCFS=RLCFS_LFS, \
                              den=dens,temp=temps, \
                              xlabel='R [m]',ydenlabel='$10^{19}\\ m^{-3} $',ytemplabel='keV',\
                              denlabel=denlabels,templabel=templabels, \
                              dencolor = den_comp_colors,tempcolor = temp_comp_colors, \
                              expdata=profexp[it],expx='R(m)',expden=expdens,exptemp=exptemps, \
                              selectors=[],latex=amr_cfg['figures']['uselatex'])
                else:
                    amrplot.plotdente(figname,prof1d_comp[it],'R',title=title,xLCFS=RLCFS_LFS, \
                          den=dens,temp=temps, \
                          xlabel='R [m]',ydenlabel='$10^{19}\\ m^{-3} $',ytemplabel='keV',\
                          denlabel=denlabels,templabel=templabels, \
                          dencolor = den_comp_colors,tempcolor = temp_comp_colors, \
                          selectors=[],latex=amr_cfg['figures']['uselatex'])           
            # 1D profiles on psi
            figname = outdir + 'dente_rho' + fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],t) + plot_ext
            title = 'Electron density and temperature, shot %i, time %.3f s' % (amr_cfg['input']['shot_number'],t)
            amrplot.plotdente(figname,profpsi[it],'rho',title=title,xLCFS=rho_LCFS, \
                  den='denRZ',temp='TempRZ', \
                  xlabel=r'$\rho $',ydenlabel=r'$10^{19}\ m^{-3} $',\
                  ytemplabel=r'keV',\
                  denlabel=r'$n_e $',templabel=r'$T_e $', \
                  dencolor = 'blue',tempcolor = 'red', \
                  selectors=[],latex=amr_cfg['figures']['uselatex'])
            # 1D profiles on psi individual components
            if profpsi_comp!=[]:
                figname = outdir + 'dente_rho_comp' + fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],t) + plot_ext
                title = 'Electron density and temperature, shot %i, time %.3f s' % (amr_cfg['input']['shot_number'],t)
                dens = []
                denlabels = []
                temps = []
                templabels = []
                for icomp in range(1,amr_cfg['profiles']['n_components']+1):
                    dens.append('denRZ_c%i' % icomp)
                    denlabels.append('$n_{e%i} $' % icomp)
                    temps.append('TempRZ_c%i' % icomp)
                    templabels.append('$T_{e%i} $' % icomp)
                amrplot.plotdente(figname,profpsi_comp[it],'rho',title=title,xLCFS=rho_LCFS, \
                      den=dens,temp=temps, \
                      xlabel='$\\rho $',ydenlabel='$10^{19}\\ m^{-3} $',\
                      ytemplabel='keV',\
                      denlabel=denlabels,templabel=templabels, \
                      dencolor = den_comp_colors,tempcolor = temp_comp_colors, \
                      selectors=[],latex=amr_cfg['figures']['uselatex'])
    # read LUKE results
    plot_luke_results = amr_cfg['figures']['luke_results']
    if plot_luke_results:
        luke_results = []
        for [t,it] in zip(ebe_times,range(len(ebe_times))):
            luke_results.append([])
            for iAnt in range(len(antennas)):
                luke_results[-1].append([])
                for iConf in range(antennas[iAnt].getNumberOfConfigurations()):
                    fname = fn.getLUKEFileName(amr_cfg['input']['experiment'],amr_cfg['input']['shot_number'],t,iAnt,iConf)
                    luke_results[-1][-1].append(pyLUKE.readLUKE(fname))
    
    # plot ray results    
    indir = 'output/'
    outdir = 'output/figures/'   
    raydata = amrplot.amrdata()
    raydata.readFile(indir + 'results_ray.dat')
    cmap = None
    if plot_bw: cmap = matplotlib.cm.gray
    if amr_cfg['figures']['conv_eff_angular_ray']:
        for t in unique(raydata.getData('time')):
            for iAnt in unique(raydata.getData('iAntenna')):
                # check for angle scan
                if len(unique(raydata.getData('tor_angle')))>1 and len(unique(raydata.getData('pol_angle')))>1: 
                    zar = []
                    freqs = []
                    opt_angles = []
                    for freq in unique(raydata.getData('freq')):
                        freqs.append(freq)
                        for E_eq in unique(raydata.getData('E_eq_angle')):
                            # conversion efficiency angle scan
                            selectors = [['freq',freq],['iAntenna',iAnt],['time',t],['E_eq_angle',E_eq],['iRay',1]]
                            [x,y,z] = raydata.selectData2D('tor_angle','pol_angle','conv_eff1+2',selectors=selectors)
                            zar.append(z)
                            figname = outdir + 'conveff_ray_ant%02i_%1.3fs_%.1fGHz_%.1fEeq' % (iAnt,t,freq,E_eq) + plot_ext 
                            amrplot.plotContour(figname,x,'toroidal angle',y,'poloidal angle',[z],labels=[''],\
                                                filled=True,plotlabels=False,cbars=True,cmap=cmap,latex=amr_cfg['figures']['uselatex'])
                            [x,y,zT] = raydata.selectData2D('tor_angle','pol_angle','Trad',selectors=selectors)
                            figname = outdir + 'Trad_ant%02i_%1.3fs_%.1fGHz_%.1fEeq' % (iAnt,t,freq,E_eq) + plot_ext 
                            amrplot.plotContour(figname,x,'toroidal angle',y,'poloidal angle',[zT],labels=[''],\
                                                filled=True,plotlabels=False,cbars=True,cmap=cmap,latex=amr_cfg['figures']['uselatex'])
                            figname = outdir + 'Trad_ce_ant%02i_%1.3fs_%.1fGHz_%.1fEeq' % (iAnt,t,freq,E_eq) + plot_ext 
                            amrplot.plotContour(figname,x,'toroidal angle',y,'poloidal angle',[z*zT],labels=[''],\
                                                filled=True,plotlabels=False,cbars=True,cmap=cmap,latex=amr_cfg['figures']['uselatex'])
                            if haveSciPy:
                                zi = interpolate.interp2d(x,y,z,kind='cubic')
                                opt_angles.append(optimize.fmin(lambda xa: -zi(xa[0],xa[1]), array([(x[0]+x[-1])/2,(y[0]+y[-1])/2])))
                                protocol(p_file,'Optimum %g GHz = [%g,%g]' % (freq,opt_angles[-1][0],opt_angles[-1][1]))
                    figname = outdir + 'conveff90_ray_ant%02i_%1.3fs' % (iAnt,t) + plot_ext
                    n = len(zar)
                    # colors = [[(0.0,0.0,0.3+(n-i-1)*0.7/(n-1))] for i in range(n)]
                    colors = [[(0.0+(n-i-1)*0.4/n,0.0+(n-i-1)*0.4/n,0.0+(n-i-1)*0.4/n)] for i in range(n)]
                    fmts = [{0.9:'%g'%freq} for freq in freqs]
                    if haveSciPy:
                        splts = [[a[0:1],a[1:2],"s=20,c="+str(c)+",marker='+',edgecolors="+str(c)] for a,c in zip(opt_angles,colors)]
                    else:
                        splts = []
                    amrplot.plotContour(figname,x,'toroidal angle',y,'poloidal angle',zar,contours=[[0.9] for i in range(n)],\
                                        colors=colors,filled=False,plotlabels=False,fmts=fmts,cbars=False,\
                                        splts=splts,cmap=cmap,latex=amr_cfg['figures']['uselatex'])
                    if haveSciPy:
                        oa = average(opt_angles,axis=0)
                        protocol(p_file,'Average optimum: [%g,%g]' % (oa[0],oa[1]))
    # plot beam results    
    indir = 'output/'
    outdir = 'output/figures/'   
    beamdata = amrplot.amrdata()
    beamdata.readFile(indir + 'results_beam.dat')
    if amr_cfg['output']['conv_eff']:
        Trad_col = 'T_rad_p12'
    else:
        Trad_col = 'T_rad100'
    for t in unique(beamdata.getData('time')):
        for iAnt in unique(beamdata.getData('iAntenna')):
            if amr_cfg['figures']['ebe_Trad_time']:
                selectors = [['iAntenna',iAnt],['time',t]]
                [x,y] = beamdata.selectData('freq',Trad_col,selectors=selectors)
                figname = outdir + 'ebe_Trad_time%02i_%1.3fs' % (iAnt,t) + plot_ext 
                amrplot.plotxy(figname, None, x, [y], 'GHz', ['Trad[keV]'], ['blue'], \
                               title='EBE spectrum time %f1.3' % t, latex=amr_cfg['figures']['uselatex'])

    # power deposition
    indir = 'ebe_integration/output/'
    outdir = 'output/figures/'   
    if amr_cfg['figures']['power_deposition']:
        pd_data = []
        pd_data_ce = []
        sum_pd_data = []
        for [t,it] in zip(ebe_times,range(len(ebe_times))):
            pd_data.append([])
            pd_data_ce.append([])
            sum_pd_data.append([])
            if prof_exist:
                reff_LCFS = rho_LCFS*prof0d[it].getData('minor_radius')[0]
                psi_LCFS = prof0d[it].getData('psi_LCFS')[0]
            else:
                reff_LCFS = None
                psi_LCFS = None
            if flux_coord=='r_eff':
                flux_coord_LCFS = reff_LCFS
            elif flux_coord=='psi':
                flux_coord_LCFS = psi_LCFS
            elif flux_coord=='rho':
                flux_coord_LCFS = rho_LCFS
            if amr_cfg['output']['power_deposition'] & amr_cfg['output']['sum_antennas'] & (len(antennas)>1):
                # plot summed profiles
                for iConf in range(antennas[0].getNumberOfConfigurations()):
                    sum_pd_data[-1].append(amrplot.amrdata())
                    sum_pd_data[-1][-1].readFile('output/'+fn.summedIntProfiles(iConf,t)[0])
                    if sum_pd_data[-1][-1].hasData():
                        figname = outdir + 'sum_pd' + fn.getConfSuffix(iConf) + fn.getTimeSuffix(t) + plot_ext
                        amrplot.plotx2y(figname,sum_pd_data[-1][-1],possible_flux_coord[flux_coord],\
                                        ['Pd_total[kW/m3]'],['dPdr_total[kW/m]'],\
                                        xlabel = flux_coord_plt_labels[flux_coord],\
                                        y1labels = ['Pdtotal[kW/m3]'], y2labels=['dPdrtotal[kW/m]'],\
                                        yaxis1_label='Pd [kW/m3]',yaxis2_label='dPdr [kW/m]',\
                                        colors1=['blue'],colors2=['green'],title='Power deposition',\
                                        xLCFS=flux_coord_LCFS, \
                                        grid=False,latex=amr_cfg['figures']['uselatex'])
                        figname = outdir + 'sum_current' + fn.getConfSuffix(iConf) + fn.getTimeSuffix(t) + plot_ext
                        amrplot.plotx2y(figname,sum_pd_data[-1][-1],possible_flux_coord[flux_coord],\
                                        ['j_total[A/cm2]'],['I_total[A]'],\
                                        xlabel = flux_coord_plt_labels[flux_coord],\
                                        y1labels = ['jtotal[A/cm2]'], y2labels=['Itotal[A]'],\
                                        yaxis1_label='j [A/cm2]',yaxis2_label='I [A]',\
                                        colors1=['blue'],colors2=['green'],title='Driven current',\
                                        xLCFS=flux_coord_LCFS, \
                                        grid=False,latex=amr_cfg['figures']['uselatex'])               
            for iAnt in range(len(antennas)):
                pd_data[-1].append([])
                pd_data_ce[-1].append([])
                antfreqs = []               
                for iConf in range(antennas[iAnt].getNumberOfConfigurations()):
                    freq = antennas[iAnt].getBeamWaist(iConf).freq
                    antfreqs.append(freq)
                    pd_data[-1][-1].append(amrplot.amrdata())
                    pd_data_ce[-1][-1].append(amrplot.amrdata())
                    pd_data[-1][-1][-1].readFile(indir+fn.getIntProfFileName(iAnt,iConf,t,'reff'))
                    pd_data_ce[-1][-1][-1].readFile(indir+fn.getIntProfFileName(iAnt,iConf,t,'reff_ce'))
                    figname = outdir + 'pd' + fn.getAntConfSuffix(iAnt, iConf) + fn.getTimeSuffix(t) + plot_ext
                    if plot_luke_results and luke_results[it][iAnt][iConf] is not None:
                        arx = pyLUKE.fluxCoordinate(luke_results[it][iAnt][iConf],flux_coord)
                        ary1s = [pyLUKE.Pd(luke_results[it][iAnt][iConf])]
                        ary2s = [pyLUKE.dPdflux(luke_results[it][iAnt][iConf],flux_coord)]
                        # equilDKE.xdV_2piRp_dke*equilDKE.Rp*2*pi
                        arcolors1=['red']
                        arcolors2=['orange']
                        ary1labels=['LUKE Pd']
                        ary2labels=['LUKE dPdr']
                    else:
                        arx = None
                        ary1s = []
                        ary2s = []
                        arcolors1=None
                        arcolors2=None
                        ary1labels=[]
                        ary2labels=[]
                    if pd_data[-1][-1][-1].hasData() or (plot_luke_results and luke_results[it][iAnt][iConf] is not None):
                        amrplot.plotx2y(figname,pd_data[-1][-1][-1],possible_flux_coord[flux_coord],\
                                        ['Pd_total[kW/m3]'],['dPdr_total[kW/m]'],\
                                        xlabel = flux_coord_plt_labels[flux_coord],\
                                        y1labels = ['Pdtotal[kW/m3]'], y2labels=['dPdrtotal[kW/m]'],\
                                        yaxis1_label='Pd [kW/m3]',yaxis2_label='dPdr [kW/m]',\
                                        colors1=['blue'],colors2=['green'], \
                                        arx = arx, ary1s = ary1s, ary2s = ary2s, \
                                        arcolors1 = arcolors1, arcolors2 = arcolors2, ary1labels = ary1labels, ary2labels = ary2labels, \
                                        title='Power deposition %gGHz' % freq,\
                                        xLCFS=flux_coord_LCFS, \
                                        grid=False,latex=amr_cfg['figures']['uselatex'])
                    figname = outdir + 'pd_ce' + fn.getAntConfSuffix(iAnt, iConf) + fn.getTimeSuffix(t) + plot_ext
                    if pd_data_ce[-1][-1][-1].hasData():
                        amrplot.plotx2y(figname,pd_data_ce[-1][-1][-1],possible_flux_coord[flux_coord],\
                                        ['Pd_total[kW/m3]'],['dPdr_total[kW/m]'],\
                                        xlabel = flux_coord_plt_labels[flux_coord],\
                                        y1labels = ['Pdtotal[kW/m3]'], y2labels=['dPdrtotal[kW/m]'],\
                                        yaxis1_label='Pd [kW/m3]',yaxis2_label='dPdr [kW/m]',\
                                        colors1=['blue'],colors2=['green'], \
                                        arx = arx, ary1s = ary1s, ary2s = ary2s, \
                                        arcolors1 = arcolors1, arcolors2 = arcolors2, ary1labels = ary1labels, ary2labels = ary2labels, \
                                        title='Power deposition %gGHz' % freq,\
                                        xLCFS=flux_coord_LCFS, \
                                        grid=False,latex=amr_cfg['figures']['uselatex'])
                    if amr_cfg['figures']['power_deposition_comp'] and amr_cfg['profiles']['n_components']==2:
                        figname = outdir + 'pd_comp' + fn.getAntConfSuffix(iAnt, iConf) + fn.getTimeSuffix(t) + plot_ext
                        amrplot.plotxy(figname,pd_data[-1][-1][-1],possible_flux_coord[flux_coord],\
                                        ['dPdr_c1[kW/m]','dPdr_c2[kW/m]'],\
                                        xlabel = flux_coord_plt_labels[flux_coord],\
                                        ylabels = ['dPdr c1[kW/m]','dPdr c2[kW/m]'],\
                                        colors=['blue','red'], \
                                        title='Power deposition by components, %gGHz' % freq,\
                                        xLCFS=flux_coord_LCFS, \
                                        grid=False,latex=amr_cfg['figures']['uselatex'])

                    figname = outdir + 'current' + fn.getAntConfSuffix(iAnt, iConf) + fn.getTimeSuffix(t) + plot_ext
                    if plot_luke_results:
                        arx = pyLUKE.fluxCoordinate(luke_results[it][iAnt][iConf],flux_coord)
                        # LUKE current relative B
                        # AMR relative to B
                        # sgn I_phi = -sgn psi (plasma current sign)
                        ary1s = [pyLUKE.j(luke_results[it][iAnt][iConf])]
                        ary2s = [pyLUKE.I(luke_results[it][iAnt][iConf])]
                        arcolors1=['red']
                        arcolors2=['orange']
                        ary1labels=['LUKE j']
                        ary2labels=['LUKE I']
                    else:
                        arx = None
                        ary1s = []
                        ary2s = []
                        arcolors1=None
                        arcolors2=None
                        ary1labels=[]
                        ary2labels=[]
                    if pd_data[-1][-1][-1].hasData() or (plot_luke_results and luke_results[it][iAnt][iConf] is not None):
                        amrplot.plotx2y(figname,pd_data[-1][-1][-1],possible_flux_coord[flux_coord],\
                                        ['j_total[A/cm2]'],['I_total[A]'],\
                                        xlabel = flux_coord_plt_labels[flux_coord],\
                                        y1labels = ['jtotal[A/cm2]'], y2labels=['Itotal[A]'],\
                                        yaxis1_label='j_parallel [A/cm2]',yaxis2_label='I_parallel [A]',\
                                        colors1=['blue'],colors2=['green'],\
                                        arx = arx, ary1s = ary1s, ary2s = ary2s, \
                                        arcolors1 = arcolors1, arcolors2 = arcolors2, ary1labels = ary1labels, ary2labels = ary2labels, \
                                        title='Driven current',\
                                        xLCFS=flux_coord_LCFS, \
                                        grid=False,latex=amr_cfg['figures']['uselatex'])
                    figname = outdir + 'current_ce' + fn.getAntConfSuffix(iAnt, iConf) + fn.getTimeSuffix(t) + plot_ext
                    if pd_data_ce[-1][-1][-1].hasData() or (plot_luke_results and luke_results[it][iAnt][iConf] is not None):
                        amrplot.plotx2y(figname,pd_data_ce[-1][-1][-1],possible_flux_coord[flux_coord],\
                                        ['j_total[A/cm2]'],['I_total[A]'],\
                                        xlabel = flux_coord_plt_labels[flux_coord],\
                                        y1labels = ['jtotal[A/cm2]'], y2labels=['Itotal[A]'],\
                                        yaxis1_label='j_parallel [A/cm2]',yaxis2_label='I_parallel [A]',\
                                        colors1=['blue'],colors2=['green'],\
                                        arx = arx, ary1s = ary1s, ary2s = ary2s, \
                                        arcolors1 = arcolors1, arcolors2 = arcolors2, ary1labels = ary1labels, ary2labels = ary2labels, \
                                        title='Driven current',\
                                        xLCFS=flux_coord_LCFS, \
                                        grid=False,latex=amr_cfg['figures']['uselatex'])
                    # power deposition individual rays
                    if amr_cfg['figures']['power_deposition_individual']:
                        indir = 'ebe_integration/output/'
                        outdir_ind = 'output/figures_rt_details/'
                        beamWaist = antennas[iAnt].getBeamWaist(iConf)
                        raypd_flux = amrplot.amrdata()
                        raypd_R = amrplot.amrdata()
                        if not amr_cfg['output']['central_ray_only']:
                            nrays = beamWaist.getNumberOfRays()
                            for iRay in range(1,nrays+1):
                                if amr_cfg['integration']['integrate_flux']:
                                    raypd_flux.readFile(indir + fn.getRayPDFileName(iAnt,iConf,amr_cfg['input']['shot_number'],t,iRay,'reff'))
                                    figname = outdir_ind + 'pd' + fn.getAntConfSuffix(iAnt, iConf) + fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],t) + fn.getIRaySuffix(iRay) + plot_ext
                                    amrplot.plotx2y(figname,raypd_flux,possible_flux_coord[flux_coord],\
                                                    ['Pd_total[kW/m3]'],['dPdr_total[kW/m]'],\
                                                    xlabel = flux_coord_plt_labels[flux_coord],\
                                                    y1labels = ['Pdtotal[kW/m3]'], y2labels=['dPdrtotal[kW/m]'],\
                                                    yaxis1_label='Pd [kW/m3]',yaxis2_label='dPdr [kW/m]',\
                                                    colors1=['blue'],colors2=['green'],title='Power deposition %gGHz' % freq,\
                                                    xLCFS=flux_coord_LCFS, \
                                                    grid=False,latex=amr_cfg['figures']['uselatex'])
                                if amr_cfg['integration']['integrate_R']:
                                    raypd_R.readFile(indir + fn.getRayPDFileName(iAnt,iConf,amr_cfg['input']['shot_number'],t,iRay,'R'))
                # power deposition spectrum
                if amr_cfg['figures']['power_deposition_spectra']:
                    figname = outdir + 'pd_spectrum' + fn.getAntSuffix(iAnt) + fn.getTimeSuffix(t) + plot_ext
                    x = pd_data[-1][iAnt][0].getData(possible_flux_coord[flux_coord])
                    y = array(antfreqs)
                    z = array([d.getData('Pd_total[kW/m3]') for d in pd_data[-1][iAnt]])
                    amrplot.implot(figname, x, y, z.T, None, latex=amr_cfg['figures']['uselatex'])
                    figname = outdir + 'dPdr_spectrum_' + fn.getAntSuffix(iAnt) + fn.getTimeSuffix(t) + plot_ext
                    #x = pd_data[-1][iAnt][0].getData(possible_flux_coord[flux_coord])
                    #y = array(antfreqs)
                    z = array([d.getData('dPdr_total[kW/m]') for d in pd_data[-1][iAnt]])
                    amrplot.implot(figname, x, y, z.T, None, latex=amr_cfg['figures']['uselatex'])

    # ray-tracing details
    if amr_cfg['figures']['ray_tracing_details']:
        indir = 'ebe_run/output/'
        outdir = 'output/figures_rt_details/'
        raydetails = amrplot.amrdata()
        for [t,it] in zip(ebe_times,range(len(ebe_times))):
            for iAnt in range(len(antennas)):
                for iConf in range(antennas[iAnt].getNumberOfConfigurations()):
                    beamWaist = antennas[iAnt].getBeamWaist(iConf)
                    if amr_cfg['output']['central_ray_only']:
                        nrays = 1
                    else:
                        nrays = beamWaist.getNumberOfRays()
                    rays = []
                    for iRay in range(1,nrays+1):
                        if os.access(indir + fn.getRTFileName(amr_cfg['input']['shot_number'],t,iAnt,iConf,iRay),os.F_OK):
                            raydetails.readFile(indir + fn.getRTFileName(amr_cfg['input']['shot_number'],t,iAnt,iConf,iRay))
                            raycolor = 'darkgreen'
                            if iRay==1:
                                raycolor = 'red'
                            ray = {'R':raydetails.getData('R'),'Z':raydetails.getData('Z'),'name':'ray %i' % iRay,\
                                   'color':raycolor,'linestyle':'-',\
                                   'Npar':raydetails.getData('Npar'),\
                                   'x':raydetails.getData('x'),'y':raydetails.getData('y')}
                            if len(amr_cfg['figures']['rt_power_dep_markers'])>0 and len(raydetails['damping_factor'])>0:
                                ray['spots'] = [abs(raydetails['damping_factor']-p).argmin() for p in amr_cfg['figures']['rt_power_dep_markers']]
                            rays.append(ray)
                            if amr_cfg['figures']['ray_tracing_details_individual']:
                                # ray RZ plots
                                if not amr_cfg['output']['central_ray_only']:
                                    figname = outdir + 'RT_RZ' + fn.getRTFileSuffix(amr_cfg['input']['shot_number'],t,iAnt,iConf,iRay) + plot_ext
                                    title = 'Ray tracing, time %3g s, antenna %i, config %i (%g GHz), ray %i' % \
                                            (t,iAnt,iConf,beamWaist.freq,iRay)
                                    if prof2d != [] and prof2d[it].hasData():
                                        R_axis = prof0d[it].getData('R_axis')[0]
                                        Z_axis = prof0d[it].getData('Z_axis')[0]
                                        nfce = range(int(floor(raydetails.getData('om/omc').min())),int(ceil(raydetails.getData('om/omc').max()))+1)
                                        fcevals = (beamWaist.freq*ones(len(nfce))).tolist()
                                        amrplot.plotRZ(figname,title=title, \
                                                       R=prof2d[it].getData('R'),Z=prof2d[it].getData('Z'),psi=prof2d[it].getData('rho') ,\
                                                       psilabels=False,psilabel='$\\rho $',\
                                                       psiLCFS=rho_LCFS,RZaxis=[R_axis,Z_axis],\
                                                       raysRZ = [ray], raylabels=True, \
                                                       fce = prof2d[it].getData('Fce'),nfce=nfce,fcevals=fcevals,\
                                                       ncin=5,ncout=5 ,\
                                                       latex=amr_cfg['figures']['uselatex'],
                                                       beam = beamWaist,plotWaist=amr_cfg['figures']['plot_waists'])
                                    else:
                                        amrplot.plotRZ(figname,title=title, \
                                                       R=prof2d[it].getData('R'),Z=prof2d[it].getData('Z'), \
                                                       raysRZ = [ray], raylabels=True, \
                                                       ncin=5,ncout=5, \
                                                       latex=amr_cfg['figures']['uselatex'],
                                                       beam = beamWaist,plotWaist=amr_cfg['figures']['plot_waists'])
                                    figname = outdir + 'RT_XY' + fn.getRTFileSuffix(amr_cfg['input']['shot_number'],t,iAnt,iConf,iRay) + plot_ext
                                    title = 'Time %g s, antenna %i, config %i (%g GHz), ray %i' % \
                                            (t,iAnt,iConf,beamWaist.freq,iRay)
                                    if profequat != [] and profequat[it].hasData():
                                        nfce = range(int(floor(raydetails.getData('om/omc').min())),int(ceil(raydetails.getData('om/omc').max()))+1)
                                        fcevals = (beamWaist.freq*ones(len(nfce))).tolist()
                                        amrplot.plotXY(figname,title=title, \
                                                       X=profequat[it].getData('x'),Y=profequat[it].getData('y'),psi=profequat[it].getData('rho') ,\
                                                       psilabels=False,psilabel='$\\rho $',\
                                                       psiLCFS=rho_LCFS, \
                                                       raysRZ = [ray], raylabels=True, \
                                                       fce = profequat[it].getData('Fce'),nfce=nfce,fcevals=fcevals,\
                                                       ncin=5,ncout=0 ,\
                                                       latex=amr_cfg['figures']['uselatex'],
                                                       beam = beamWaist,plotWaist=amr_cfg['figures']['plot_waists'],
                                                       legend_pos = '')
                                # ray time evolution plots
                                figname = outdir + 'RT_time' + fn.getRTFileSuffix(amr_cfg['input']['shot_number'],t,iAnt,iConf,iRay) + plot_ext
                                title = 'Ray tracing, time %3g s, antenna %i, config %i (%g GHz), ray %i' % \
                                        (t,iAnt,iConf,beamWaist.freq,iRay)
                                x = 't'
                                xlabel = 'time [s]'
                                ys = ['Damping_factor','Npar','Nperp',1.0/raydetails.getData('om/omc'),'rho','R']
                                scaley = [True for ii in ys]
                                scaley[3] = 'int'
                                scaley[1] = 'int'
                                ylabels = ['Damping factor','Npar','Nperp','omc/om','rho','R']
                                colors=['black','red','green','blue','cyan','magenta']
                                amrplot.plotxys(figname,raydetails,x,ys,xlabel,ylabels,\
                                                scalex=True,scaley=scaley,colors=colors,title=title,xLCFS=None,\
                                                grid=True,latex=amr_cfg['figures']['uselatex'])
                                # rays vs. distance plots
                                figname = outdir + 'RT_dist' + fn.getRTFileSuffix(amr_cfg['input']['shot_number'],t,iAnt,iConf,iRay) + plot_ext
                                title = 'Ray tracing, time %3g s, antenna %i, config %i (%g GHz), ray %i' % \
                                        (t,iAnt,iConf,beamWaist.freq,iRay)
                                s = sqrt(diff(raydetails['x'])**2 + diff(raydetails['y'])**2 + diff(raydetails['Z'])**2)
                                s = cumsum(hstack((array([0]),s)))
                                xlabel = 'distance [m]'
                                Te_scale = raydetails['TempRZ'].max()
                                ys = ['Damping_factor',1.0/raydetails.getData('om/omc'),raydetails['TempRZ']/Te_scale,'ImD']
                                ylabels = ['Damping factor','omc/om','Te/%.3g'%Te_scale,'ImD']
                                colors=['black','cyan','pink','gray']
                                linestyles = ['-','-','-','-']
                                x = [s, s, s, s]
                                scaley = [True for ii in ys]
                                scaley[0] = False; scaley[1] = 'int'; scaley[2] = False; scaley[3] = False
                                if plot_luke_results:
                                    ys += [pyLUKE.rayP(luke_results[it][iAnt][iConf], iRay), \
                                          1.0/pyLUKE.rayom_omc(luke_results[it][iAnt][iConf],iRay),\
                                          pyLUKE.rayTe(luke_results[it][iAnt][iConf],iRay)/Te_scale,\
                                          pyLUKE.rayimD(luke_results[it][iAnt][iConf],iRay)]
                                    ylabels += ['LUKE P','LUKE omc/om','LUKE Te/%.3g'%Te_scale,'LUKE imD']
                                    colors += ['red','blue','magenta','yellow']
                                    linestyles += ['--','--','--','--']
                                    x += [pyLUKE.raydist(luke_results[it][iAnt][iConf],iRay) for i in range(4)]
                                    scaley += [False,'int',False,False]
                                amrplot.plotxys(figname,raydetails,x,ys,xlabel,ylabels,\
                                                scalex=False,scaley=scaley,colors=colors,title=title,xLCFS=None,\
                                                linestyles=linestyles,grid=True,latex=amr_cfg['figures']['uselatex'])
                    # all rays for one antenna configuration
                    figname = outdir + 'RT_RZ' + \
                              fn.getAntConfSuffix(iAnt,iConf) + \
                              fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],t) + \
                              plot_ext
                    title = 'Ray tracing, time %.3g s, antenna %i, config %i (%g GHz)' % \
                            (t,iAnt,iConf,beamWaist.freq)
                    rays.reverse()
                    if prof2d != [] and prof2d[it].hasData():
                        R_axis = prof0d[it].getData('R_axis')[0]
                        Z_axis = prof0d[it].getData('Z_axis')[0]
                        if raydetails.hasData():
                            nfce = range(int(raydetails.getData('om/omc').round(0).min()),int(raydetails.getData('om/omc').round(0).max())+1)
                        else:
                            nfce = []
                        fcevals = (beamWaist.freq*ones(len(nfce))).tolist()
                        amrplot.plotRZ(figname,title=title, \
                                       R=prof2d[it].getData('R'),Z=prof2d[it].getData('Z'),psi=prof2d[it].getData('rho') ,\
                                       psilabels=False,psilabel='$\\rho $',\
                                       psiLCFS=rho_LCFS,RZaxis=[R_axis,Z_axis],\
                                       raysRZ = rays, raylabels=False, \
                                       fce = prof2d[it].getData('Fce'),nfce=nfce,fcevals=fcevals,\
                                       ncin=5,ncout=5 ,\
                                       latex=amr_cfg['figures']['uselatex'],\
                                       beam = beamWaist,plotWaist=amr_cfg['figures']['plot_waists'])
                    else:
                        protocol(p_file,'Cannot plot rays in poloidal cut---missing 2D profiles')
#                         amrplot.plotRZ(figname,title=title, \
#                                        R=prof2d[it].getData('R'),Z=prof2d[it].getData('Z'), \
#                                        raysRZ = rays, raylabels=False, \
#                                        ncin=5,ncout=5, \
#                                        latex=amr_cfg['figures']['uselatex'],\
#                                        beam = beamWaist,plotWaist=amr_cfg['figures']['plot_waists'])
                    figname = outdir + 'RT_XY' + \
                              fn.getAntConfSuffix(iAnt,iConf) + \
                              fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],t) + \
                              plot_ext
                    title = 'time %g s, antenna %i, config %i (%g GHz)' % \
                            (t,iAnt,iConf,beamWaist.freq)
                    if profequat != [] and profequat[it].hasData():
                        if raydetails.hasData():
                            nfce = range(int(floor(raydetails.getData('om/omc').min())),int(ceil(raydetails.getData('om/omc').max()))+1)
                        else:
                            nfce = range(int(floor(beamWaist.freq/profequat[it].getData('Fce').max())),int(ceil(beamWaist.freq/profequat[it].getData('Fce').max()))+1)
                        fcevals = (beamWaist.freq*ones(len(nfce))).tolist()
                        amrplot.plotXY(figname,title=title, \
                                       X=profequat[it].getData('x'),Y=profequat[it].getData('y'),psi=profequat[it].getData('rho') ,\
                                       psilabels=False,psilabel='$\\rho $',\
                                       psiLCFS=rho_LCFS, \
                                       raysRZ = rays, raylabels=True, \
                                       fce = profequat[it].getData('Fce'),nfce=nfce,fcevals=fcevals,\
                                       ncin=5,ncout=0 ,\
                                       latex=amr_cfg['figures']['uselatex'],
                                       beam = beamWaist,plotWaist=amr_cfg['figures']['plot_waists'],
                                       legend_pos = '')
#                    else:
#                        protocol(p_file,'Cannot plot rays in toroidal cut---missing equatorial profiles (3D test profiles)')
#                        amrplot.plotXY(figname,title=title, \
#                                       X=profequat[it].getData('x'),Y=profequat[it].getData('y'),psi=profequat[it].getData('rho') ,\
#                                       raysRZ = rays, raylabels=True, \
#                                       ncin=5,ncout=0 ,\
#                                       latex=amr_cfg['figures']['uselatex'],
#                                       beam = beamWaist,plotWaist=amr_cfg['figures']['plot_waists'],
#                                       legend_pos = '')
                    figname = outdir + 'RT_Npar' + \
                              fn.getAntConfSuffix(iAnt,iConf) + \
                              fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],t) + \
                              plot_ext
                    title = 'Npar, time %.3g s, antenna %i, config %i (%g GHz)' % \
                            (t,iAnt,iConf,beamWaist.freq)
                    amrplot.plotxy(figname,None,[r['R'] for r in rays],[r['Npar'] for r in rays],'R[m]',['' for r in rays],\
                                   colors=[r['color'] for r in rays],title='',xLCFS=None,yuplimit=None,ylowlimit=None, \
                                   grid=False,latex=False)
                    rays.reverse()
    # WKB curves plots
    if amr_cfg['figures']['wkb_curves']!='none' and amr_cfg['figures']['wkb_curves']!='no':
        if amr_cfg['figures']['wkb_curves']=='all':
            plot_wkb_curves = 'all'
        else:
            plot_wkb_curves = 'c'
        indir = 'ebe_run/output/'
        outdir = 'output/figures_wkbcurves/'
        wkb_data = amrplot.amrdata()
        for [t,it] in zip(ebe_times,range(len(ebe_times))):
            for iAnt in range(len(antennas)):
                for iConf in range(antennas[iAnt].getNumberOfConfigurations()):
                    beamWaist = antennas[iAnt].getBeamWaist(iConf)
                    if amr_cfg['output']['central_ray_only'] or plot_wkb_curves=='c':
                        nrays = 1
                    else:
                        nrays = beamWaist.getNumberOfRays()
                    for iRay in range(1,nrays+1):
                        if os.access(indir + fn.getWKBcurveFileName(iAnt,iConf,amr_cfg['input']['shot_number'],t,iRay),os.F_OK):
                            figname = outdir + 'wkb' + \
                                      fn.getAntConfSuffix(iAnt,iConf) + \
                                      fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],t) + \
                                      fn.getIRaySuffix(iRay) + \
                                      plot_ext
                            wkb_data.readFile(indir + fn.getWKBcurveFileName(iAnt,iConf,amr_cfg['input']['shot_number'],t,iRay))
                            amrplot.plotxy(figname,wkb_data,'SS(i)',['Re(Nxfast)','Im(Nxfast)','Re(Nxslow)','Im(Nxslow)','-Re(Nxfast)','-Im(Nxfast)','-Re(Nxslow)','-Im(Nxslow)'],\
                                           'x[m]',['Re(Nxfast)','Im(Nxfast)','Re(Nxslow)','Im(Nxslow)','','','',''],\
                                           colors=['blue','cyan','red','pink','blue','cyan','red','pink'],title='',yuplimit=5,ylowlimit=-5,\
                                           grid=False,latex=amr_cfg['figures']['uselatex'])

    protocol(p_file,'plotting not fully implemented yet. sorry.')
protocol(p_file,'Mission completed.')

time2=time.time()-time1
protocol(p_file,'Time eaten: '+str(int(time2)//3600)+'h:'+str((int(time2)%3600)//60)+'m:'+str((int(time2)%60))+'s.')

p_file.close()
if job_server: job_server.destroy()
