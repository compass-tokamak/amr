% indexes in the output files
freqindex = 7;
toraindex = 4;
polaindex = 5;
ceindex = 11;
Tradindex = 12;
eqfile = 'Psi_UHR_Fce_000021_1.000.dat';
psia = -1.925568813526706E-002;
psib =  0.0;
rayfiles = {'rt26.00_00_000021_1.000.dat','rt28.00_00_000021_1.000.dat','rt30.00_00_000021_1.000.dat',...
    'rt32.00_00_000021_1.000.dat','rt34.00_00_000021_1.000.dat','rt36.00_00_000021_1.000.dat',...
    'rt38.00_00_000021_1.000.dat','rt40.00_00_000021_1.000.dat'};
legs = {'\rho_{pol}','26 GHz','28 GHz','30 GHz',...
    '32 GHz','34 GHz','36 GHz',...
    '38 GHz','40 GHz'};

% refine mesh meshfactor-times for interpolation
meshfactor = 1;
% number of countours to plot
ncontours = 8;
% maximum contour value to plot
maxcontour = psib;
vcontours = psia:(psib-psia)/ncontours:psib;
rhocontours = 0.1:0.1:1;

if not(exist('readdata'))
    fid = fopen(eqfile, 'r');
    filedata = textscan(fid, '%f %f %f %f %f %f %f %f %f', 'headerlines', 1);
    fclose(fid);
    alldata = zeros(length(filedata{1}),length(filedata));
    for i=1:size(alldata,2)
        alldata(:,i) = filedata{i};
    end
    clear('filedata');

    R = unique(alldata(:,1));
    Z = unique(alldata(:,2));
    nR = length(R);
    nZ = length(Z);

    psidata = zeros(nZ,nR);
    fcedata = zeros(nZ,nZ);
    for i=1:nR
        for j=1:nZ
            psidata(j,i) = alldata(find( alldata(:,1)==R(i) & alldata(:,2)==Z(j)),4);
            fcedata(j,i) = alldata(find( alldata(:,1)==R(i) & alldata(:,2)==Z(j)),6);
            rhodata(j,i) = sqrt((psidata(j,i)-psia)/(psib-psia));
        end
    end
    readdata = 1
end

scrsz = get(0,'ScreenSize');
fig1 = figure('Position',[10 10 scrsz(3)-10 scrsz(4)-100]);
% construct meshes
[X,Y]=meshgrid(R,Z);
[XI,YI]=meshgrid(R(1):(R(length(R))-R(1))/(nR*meshfactor):R(length(R)),...
                 Z(1):(Z(length(Z))-Z(1))/(nZ*meshfactor):Z(length(Z)));
% interpolate data
ZI = interp2(X,Y,rhodata,XI,YI,'cubic');
% contourf(X,Y,cedata',ncontours);
contour(XI,YI,ZI,[1.0,0.1],'r','LineWidth',2);
hold on
contour(XI,YI,ZI,[0.9,0.1],'w','LineWidth',2);
[C,h] = contour(XI,YI,ZI,rhocontours,'k');
% set equal color scaling
axis equal tight;
axis([0.35 0.8 -0.3 0.25])
% text_handle = clabel(C,h);
% set(text_handle,'BackgroundColor',[1 1 .6],'Edgecolor',[.7 .7 .7]);
% title({'Conversion efficiency contours',strcat('frequency = ',num2str(freq(fi)),' GHz')})
% colorbar

hold on

cmrays = [1 1 0; 1 0 0; 0 1 0; 0 0 1; 1 0 1; 0 1 1; .500 0 0; 1 0.62 0.40];
colormap(cmrays);
contour(R,Z,fcedata,[26,28,30,32,34,36,38,40],'LineWidth',1.5);
caxis([26 40]);
xlabel('R [m]');
ylabel('Z [m]');
hrays = zeros(1,length(rayfiles)+1);
hrays(1) = h;
for j=1:length(rayfiles)
    fid = fopen(rayfiles{j}, 'r');
    filedata = textscan(fid, '%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f', 'headerlines', 1);
    fclose(fid);
    alldata = zeros(length(filedata{1}),length(filedata));
    for i=1:size(alldata,2)
        alldata(:,i) = filedata{i};
    end
    clear('filedata');
    % legend(legend_handle);
    hrays(j+1) = plot(alldata(:,2),alldata(:,3),'Color',cmrays(j,:),'LineWidth',2);
    % legend(legend_handle,h(j),legs{j});
end
% legend(h,legs);
legend_handle = legend(hrays,legs);
