'''
Current-drive analysis---read AMR+LUKE results and store in cd_results_all[sim_id][ifreq].
'''
#import numpy as N
#import os

from scipy.interpolate import interp1d
try:
    cd_results_all
    if not cd_results_all: cd_results_all = {}
except NameError:
    print 'resetting cd_results_all'
    cd_results_all = {}

simul_id = os.path.basename(os.getcwd())
print 'Simulation id: '+simul_id
print 'Assuming only one antenna, only frequency scan (one config per frequency)'
ant=antennas[0]

cd_results_all[simul_id]=[]
for i,f in enumerate(freqs):
    print('%i, %f' % (i,f))
    res={}
    res['freq'] = f
    res['thtor'] = ant.getAngles(i)[0];
    res['thpol'] = ant.getAngles(i)[1];
    res['Z'] = ant.getBeamWaist(i).z_center
    res['P0'] = ant.power;
    res['LP0'] = sum([pyLUKE.rayP0(luke_results[0][0][i],iray+1) for iray in range(ant.getBeamWaist(0).getNumberOfRays())])
    res['LPd'] = pyLUKE.Pd(luke_results[0][0][i])
    res['LdV'] = pyLUKE.dV(luke_results[0][0][i])
    res['Lrho'] = pyLUKE.fluxCoordinate(luke_results[0][0][i],'rho')
    res['LdP'] = res['LPd'] * res['LdV']
    res['Lj'] = pyLUKE.j(luke_results[0][0][i])
    res['LI'] = pyLUKE.I(luke_results[0][0][i])
    res['LItot'] = res['LI'][-1]
    res['LPtot'] = sum(res['LdP'])
    res['fwhm_LPd'] = fwhm(res['Lrho'],res['LPd'])
    res['fwhm_Lj'] = fwhm(res['Lrho'],res['Lj'])
    res['zeta'] = (pyLUKE.dI(luke_results[0][0][i])*pyLUKE.eqne(luke_results[0][0][i])/pyLUKE.eqTe(luke_results[0][0][i])).sum()\
                  *1e-3*3.27*pyLUKE.eqR0(luke_results[0][0][i])/res['LPtot']
    res['rays'] = []
    LdPharm = pyLUKE.dPharm(luke_results[0][0][i])
    Ptotrays = 0.0
    for iRay,r in enumerate(RTRayData[0][0][i]):
        d = {}
        d['Npar'] = zeros(res['Lrho'].shape)
        d['w_wce'] = zeros(res['Lrho'].shape)
        # uppper/lower bin boundaries
        ubound = res['Lrho'] + diff(hstack((res['Lrho'],[1])))/2.0
        ubound[-1] = 1.0
        lbound = res['Lrho'] - diff(hstack(([0],res['Lrho'])))/2.0
        lbound[0] = 0.0
        rrho = pyLUKE.rayrho(luke_results[0][0][i],iRay+1)
        rNpar = pyLUKE.rayNpar(luke_results[0][0][i],iRay+1)
        rw_wce = pyLUKE.rayom_omc(luke_results[0][0][i],iRay+1)
        if pyLUKE.rayQLzS(luke_results[0][0][i],iRay+1) is None:
            # LUKE data missing
            print('warning: LUKE ray data missing: %s, %g GHz, ray %i' % (simul_id, f, iRay+1))
            rdP = zeros(rrho.shape)
            rdPharm = zeros((len(res['Lrho']),pyLUKE.nharm(luke_results[0][0][i]))) # tile(rdP,(pyLUKE.nharm(luke_results[0][0][i]),1)).T
        else:
            rdP = pyLUKE.rayQLdPinterp(luke_results[0][0][i],iRay+1)
            rdPharm = pyLUKE.rayQLdPharm(luke_results[0][0][i],iRay+1)
        for irho,rho in enumerate(res['Lrho']):
            rhom = (rrho<=ubound[irho]) & (rrho>=lbound[irho])
            if any(rhom):
                # simple average
                # nans if no linear damping - error for multipass through flux surface
                Npar = nan # average(r['Npar'][rhom])
                w_wce = nan # average(r['om/omc'][rhom])
                # weighted by linear damping
                if sum(rdP[rhom])>0:
                    Npar = average(rNpar[rhom],weights=rdP[rhom])
                    w_wce = average(rw_wce[rhom],weights=rdP[rhom])
                    # rdPharm[irho,:] = sum(rdP[rhom])*LdPharm[irho,:]
                    Ptotrays += sum(rdPharm[irho,:])
            else:
                Npar = nan
                w_wce = nan

            d['Npar'][irho] = Npar
            d['w_wce'][irho] = w_wce
            # d['dP'] = pyLUKE.rayQLdP(luke_results[0][0][i],iRay+1)
        # d['dPharm'] = pyLUKE.rayQLdPharm(luke_results[0][0][i],iRay+1)
        d['dPharm'] = rdPharm
        res['rays'].append(d)
    # res['dPharm'] = sum([r['dPharm'] for r in res['rays']])
    for r in res['rays']:
        r['dPharm'] = r['dPharm']/Ptotrays*res['LPtot']
    cd_results_all[simul_id].append(res)

del(res)
