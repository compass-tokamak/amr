include ../make.flags

OBJDIR = ../obj#
MODDIR = ../mod#
SRCDIR = ../source#
vpath %.f90 $(SRCDIR)
vpath %.F90 $(SRCDIR)
vpath %.f $(SRCDIR)
vpath %.F $(SRCDIR)
vpath %.o $(OBJDIR)
vpath %.mod $(MODDIR)

.PHONY: all clean realclean depend

OBJS = $(patsubst %.c,%.o,$(patsubst %.f,%.o,$(patsubst %.F,%.o,$(patsubst %.f90,%.o,$(patsubst %.F90,%.o,$(ALLFILES))))))
OBJSVPATH = $(OBJS:%=${OBJDIR}/%)
MAKEFILE_INC = $(CURDIR)/makedeps.inc
F_makedepend=../util/sfmakedepend --objdir $(OBJDIR) --moddir $(OBJDIR) $(PREFLAGS) --file - 

all: $(NAME)
	@mkdir -p output protocol

$(NAME): $(OBJSVPATH)
	$(F) $(OBJSVPATH) $(COPTS) $(LOPTS) -o $(NAME)

$(OBJDIR)/%.o: %.f90
	$(F) $(COPTS) -c $< -o $@

$(OBJDIR)/%.o: %.F90
	$(F) $(COPTS) -c $< -o $@

$(OBJDIR)/%.o: %.f
	$(F) $(COPTS) -c $< -o $@

$(OBJDIR)/%.o: %.F
	$(F) $(COPTS) -c $< -o $@

$(OBJDIR)/%.o: %.c
	$(C) -c $< -o $@

clean:
	@rm -f $(OBJDIR)/*.o
	@rm -f $(MODDIR)/*.mod
	@rm -f $(MAKEFILE_INC)
	@rm -f *.i
	@rm -f *.i90

realclean: clean
	@rm -f $(NAME)

show:
	@echo "Compilation: $(F) $(COPTS)
	@echo "Linking: $(F) $(COPTS) $(LOPTS)"
	@echo "Source : $(ALLFILES)"

depend $(MAKEFILE_INC):
	cd ../source; $(F_makedepend) $(F90SRC) $(FSRC) > $(MAKEFILE_INC)

-include $(MAKEFILE_INC)
