## @package amrplot
##  Python module for LUKE handling.

import os,os.path,sys
from which import which

def callString(experiment,id_simul,shot_number,time,iAnt,iConf,np,nt,dsmin,kmode,id_dkeparam):
    opt_save = 0
    if os.getenv('AMR_MATLAB'):
        matlab = os.getenv('AMR_MATLAB')
    else:
        matlab = which('matlab')
    return matlab + ' -nodisplay -r ' + \
           'rundke_AMR(\'%s\',\'%s\',%i,%g,%i,%i,%i,%i,%g,%i,%i,\'%s\')' % \
           (experiment,id_simul,shot_number,time,iAnt,iConf,np,nt,dsmin,kmode,opt_save,id_dkeparam)

def extractConfig(infile,outfile):
    import ConfigParser   
    my_conf = ConfigParser.SafeConfigParser()
    my_conf.read(infile)    
    fout = open(outfile,'w')
    for sec in my_conf.sections():
        fout.write('['+sec+']\n')
        for it in my_conf.items(sec):
            # if not it[1]: print sec + ': ' + it[0].lower()
            fout.write(it[0].lower() + '=' + it[1].lower() + '\n')
    fout.close()

if __name__ == "__main__":
    if len(sys.argv)==1:
        print 'Usage: pyLUKE extractConfig input output'
        sys.exit(-2)
    elif sys.argv[1].lower() == 'extractconfig':
        fin = sys.argv[2]
        fout = sys.argv[3]
        if os.path.isfile(fin): extractConfig(fin,fout)
    sys.exit(0)

try:
    isNumPy = True
    from numpy import *
except ImportError:
    print 'warning: pyLUKE cannot import numpy'
    isNumPy = False

if isNumPy:

    def readLUKE(fname):
        if not os.path.isfile(fname):
            return None
        try:
            from scipy.io import loadmat
        except ImportError:
            print 'error: scipy.io.loadmat cannot be imported'
            return None
        lukeres = dict()
        fluke=loadmat(fname,struct_as_record=True)
        lukeres['equilDKE']=fluke['equilDKE'][0][0]
        lukeres['equil_magnetic']=fluke['equil_magnetic'][0][0]
        # lukeres['dke_out']=fluke['dke_out'][0]
        lukeres['waves']=fluke['dke_out'][0]['waves'][0][0][0][0][0]
        lukeres['Zcurr']=fluke['Zcurr'][0][0]
        lukeres['ZP0']=fluke['ZP0'][0][0]
        try:
            lukeres['zS']=fluke['dke_out'][0]['zS'][0][0]
            lukeres['zP_0_2piRp_mod']=fluke['dke_out'][0]['zP_0_2piRp_mod'][0][0]
        except ValueError:
            lukeres['zS']=None
            lukeres['zP_0_2piRp_mod']=None
        lukeres['yb'] = hstack((fluke['dke_out'][0,0]['yb'][0]-1,(lukeres['ZP0']['xyn_rf_fsav'].shape[1],)))
        lukeres['mksa']=fluke['mksa'][0][0]
        lukeres['radialDKE']=fluke['radialDKE'][0][0]
        # MW/m3 -> kW/m3
        lukeres['mksa']['P_ref'][0][0] = lukeres['mksa']['P_ref'][0][0]*1e3
        # MA/m2 -> A/cm2
        lukeres['mksa']['j_ref'][0][0] = lukeres['mksa']['j_ref'][0][0]*1e2
        return lukeres
    
    def fluxCoordinate(lukeres,coord='rho'):
        if coord == 'rho':
            return lukeres['equilDKE']['xrhoP'].reshape(-1)
        return None
    
    def Pd(lukeres):
        return lukeres['ZP0']['x_rf_fsav'].reshape(-1) * lukeres['mksa']['P_ref'][0][0]

    def Pdharm(lukeres):
        return sum(lukeres['ZP0']['xyn_rf_fsav'],axis=1) * lukeres['mksa']['P_ref'][0][0]
    
    def dPdflux(lukeres,coord):
        if coord == 'rho':
            drho = diff(sqrt(lukeres['radialDKE']['xpsin_S_dke'].reshape(-1)))
            return Pd(lukeres) * dV(lukeres) / drho
        return None

    def dP(lukeres):
        return Pd(lukeres) * dV(lukeres)
    
    def nharm(lukeres):
        return lukeres['ZP0']['n_rf_list'].size
        
    def dPharm(lukeres):
        return Pdharm(lukeres)*tile(dV(lukeres),(nharm(lukeres),1)).T 
        
    def dV(lukeres):
        return lukeres['equilDKE']['xdV_2piRp_dke'].reshape(-1) * 2 * pi * lukeres['equilDKE']['Rp'][0][0]

    def dS(lukeres):
        return lukeres['equilDKE']['xdA_dke'].reshape(-1)
    
    def j(lukeres):
        # x_0_fsav = integral(v_par*f)
        # x_0_fsav*j_ref = j_parallel (along the magnetic field)
        return lukeres['Zcurr']['x_0_fsav'].reshape(-1) * lukeres['mksa']['j_ref'][0][0]
    
    def I(lukeres):
        return progsum((lukeres['Zcurr']['x_0_fsav'].reshape(-1) * \
                        lukeres['mksa']['j_ref'][0][0]) * \
                        lukeres['equilDKE']['xdA_dke'].reshape(-1)*1e4)

    def dI(lukeres):
        return (lukeres['Zcurr']['x_0_fsav'].reshape(-1) * \
                        lukeres['mksa']['j_ref'][0][0]) * \
                        lukeres['equilDKE']['xdA_dke'].reshape(-1)*1e4

    def rayR(lukeres,iRay):
        return lukeres['waves']['rays'][0][iRay-1][0][0]['sx'][0]+lukeres['equilDKE']['Rp'][0][0]

    def rayZ(lukeres,iRay):
        return lukeres['waves']['rays'][0][iRay-1][0][0]['sy'][0]+lukeres['equilDKE']['Zp'][0][0]

    def rayP(lukeres,iRay):
        return lukeres['waves']['rays'][0][iRay-1][0][0]['sP_2piRp'][0] / lukeres['waves']['rays'][0][iRay-1][0][0]['P0_2piRp'][0][0]

    def raydP(lukeres,iRay):
        return -hstack(([0],diff(rayP(lukeres,iRay))))

    def rayP0(lukeres,iRay):
        # convert W -> kW
        return lukeres['waves']['rays'][0][iRay-1][0][0]['P0_2piRp'][0][0]*2*pi*lukeres['equilDKE']['Rp'][0][0]*1e-3

    # ray path length at LUKE radial grid points    
    def rayQLzS(lukeres,iRay):
        if lukeres['zS'] is not None and len(lukeres['zS'])>=iRay:
            return lukeres['zS'][iRay-1][-1]
        else:
            return None
    # quasilinear power along ray
    def rayQLP(lukeres,iRay):
        # return lukeres['ZP0']['xy_rf_fsav'][:,iRay-1] * dV(lukeres) * lukeres['mksa']['P_ref'][0][0]
        if lukeres['zP_0_2piRp_mod'] is not None and len(lukeres['zP_0_2piRp_mod'])>=iRay:
            return lukeres['zP_0_2piRp_mod'][iRay-1][-1]/ lukeres['waves']['rays'][0][iRay-1][0][0]['P0_2piRp'][0][0] 
        else:
            return None

    def rayQLPinterp(lukeres,iRay):
        from scipy.interpolate import interp1d
        x = raydist(lukeres,iRay)
        f = interp1d(hstack(((x[0],),rayQLzS(lukeres,iRay),(x[-1],))),hstack(((1,),rayQLP(lukeres,iRay),(0,))))
        return f(x)
    def rayQLdPinterp(lukeres,iRay):
        return -hstack(([0],diff(rayQLPinterp(lukeres,iRay))))

    def rayQLdPharm(lukeres,iRay):
        try:
            res = sum(lukeres['ZP0']['xyn_rf_fsav'][:,lukeres['yb'][iRay-1]:lukeres['yb'][iRay],:],axis=1) \
                 * tile(dV(lukeres),(nharm(lukeres),1)).T * lukeres['mksa']['P_ref'][0][0]
        except IndexError:
            print('warning: ray data missing in LUKE (rayQLdPharm)')
            res = zeros(lukeres['ZP0']['xyn_rf_fsav'].shape)
            raw_input('Hit enter to continue')
        return res

    def rayNpar(lukeres,iRay):
        return lukeres['waves']['rays'][0][iRay-1][0][0]['sNpar'][0]

    def rayNperp(lukeres,iRay):
        return lukeres['waves']['rays'][0][iRay-1][0][0]['sNperp'][0]

    def rayne(lukeres,iRay):
        return lukeres['waves']['rays'][0][iRay-1][0][0]['sne'][0]

    def rayTe(lukeres,iRay):
        return lukeres['waves']['rays'][0][iRay-1][0][0]['sTe'][0]
    
    def rayrho(lukeres,iRay):
        return sqrt(lukeres['waves']['rays'][0][iRay-1][0][0]['spsin'][0])

    def rayB(lukeres,iRay):
        return lukeres['waves']['rays'][0][iRay-1][0][0]['sB'][0]
    
    def rayom_omc(lukeres,iRay):
        return lukeres['waves']['omega_rf'][0][0]/(rayB(lukeres,iRay)*175882010927.9415)

    def raydist(lukeres,iRay):
        return lukeres['waves']['rays'][0][iRay-1][0][0]['ss'][0]

    def rayimD(lukeres,iRay):
        # imD = salphaphi_lin / kvac
        return lukeres['waves']['rays'][0][iRay-1][0][0]['salphaphi_lin'][0] / lukeres['waves']['omega_rf'][0][0] * 2.99792458e8

    def eqrho(lukeres):
        return lukeres['equilDKE']['xrhoP'].reshape(-1)
    
    def eqR(lukeres):
        return lukeres['equilDKE']['xx0'].reshape(-1)+lukeres['equilDKE']['Rp'][0][0]
    
    def eqR0(lukeres):
        return lukeres['equilDKE']['Rp'][0][0]
    
    def eqne(lukeres):
        return lukeres['equilDKE']['xne'].reshape(-1)*1e-19

    def eqTe(lukeres):
        return lukeres['equilDKE']['xTe'].reshape(-1)

    def progsum(a):
        res = array(a)
        for i in range(1,len(res)):
            res[i] += res[i-1]
        return res
    
    
