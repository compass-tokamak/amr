!> Spline 2D equilibrium data.
!!
!! Construct 2D splines of psi(R,Z) and 1D splines F(psi),V(psi),S(psi)
subroutine Psi2DSpline(nR,nZ,nF,constructPsiGrid,psiSurfDim,fluxTraceDLcoeff,psi_scale,Btor_scale,psi_R_shift)

use LLquadrat
! use stopwatch

#ifdef _IMSL
! use IMSL
#else
use bspline
#endif

integer(4),intent(in) :: nR,nZ,nF,psiSurfDim
logical(4),intent(in) :: constructPsiGrid
real(8),intent(in) :: fluxTraceDLcoeff,psi_scale,Btor_scale,psi_R_shift

integer :: i,j,IPARAM(7)
real(8) :: FSCALE,RPARAM(7),X_min(2),XGUESS(2),XSCALE(2),F_min,A,B,dpsi

real(8) :: NAGWORK(26),NAGRUSER(1),dF_min(2)
integer :: NAGIWORK(3),NAGIUSER(1),IFAIL,NAGITER,ierr
real(8) :: E1,E2
integer :: MAXCAL
logical :: isClosed

real(8),allocatable :: BFluxAvGrid(:),BsqFluxAvGrid(:),BmaxFluxAvGrid(:)
real(8),allocatable :: LLHFunPsiLambdaGrid(:,:),LLfcFunPsiGrid(:),LLlambdaGrid(:)

! type (watchtype) :: watch

fluxPsiGridDim = psiSurfDim

if (allocated(VolPsiGrid)) then
  deallocate (VolPsiGrid,AreaPsiGrid)
endif
if (allocated(Psi2D_spline_Rknots)) then
  deallocate (Psi2D_spline_Rknots,Psi2D_spline_Zknots)
  deallocate (Psi2D_spline_coef)
endif
if (allocated(F_spline_knots)) then
  deallocate(F_spline_knots,F_spline_coef,Vol_spline_coef,Area_spline_coef)
  deallocate(BFluxAvGrid,BsqFluxAvGrid,BmaxFluxAvGrid,BsqFluxAv_spline_coef,BFluxAv_spline_coef,BmaxFluxAv_spline_coef)
endif
if (allocated(LLfcFunPsiGrid)) then
  deallocate (LLfcFunPsiGrid,LLHFunPsiLambdaGrid,LLlambdaGrid)
  deallocate(LLlambda_spline_knots,LLHfun_spline_coef,LLfcFun_spline_coef)
endif

allocate(VolPsiGrid(fluxPsiGridDim),AreaPsiGrid(fluxPsiGridDim))
allocate(BFluxAvGrid(fluxPsiGridDim),BsqFluxAvGrid(fluxPsiGridDim),BmaxFluxAvGrid(fluxPsiGridDim))
allocate (LLfcFunPsiGrid(fluxPsiGridDim),LLHFunPsiLambdaGrid(fluxPsiGridDim,LLGridDim),LLlambdaGrid(LLGridDim))

! call create_watch(watch)

if (PsiMagAxis>Psi_LCFS) then
  ! psi is decreasing from the axis -> reverse psi and set SIGNBPOL to -1
  PsiRZGrid = - PsiRZGrid
  Psi_LCFS  = - Psi_LCFS
  PsiMagAxis= - PsiMagAxis
  SGNBPOL   = - 1
else
  SGNBPOL   = + 1
endif
! Bpol scaling
PsiRZGrid = abs(psi_scale) * PsiRZGrid
Psi_LCFS  = abs(psi_scale) * Psi_LCFS
PsiMagAxis= abs(psi_scale) * PsiMagAxis
SGNBPOL = SGNBPOL * sign(1.d0,psi_scale)
! Btor scaling
BvacR = BvacR * Btor_scale
FPsiGrid = FPsiGrid * Btor_scale
! Maximum psi along Z=0
!! TO BE IMPROVED
PsiMax = maxval(PsiRZGrid(:,nZ/2+1))
PsiMaxLFS = maxval(PsiRZGrid(nR/2+1:nR,nZ/2+1))

! Splining
! Interpolating of Psi 2D
allocate (Psi2D_spline_Rknots(nR+PsiSplineOrder),Psi2D_spline_Zknots(nZ+PsiSplineOrder))
allocate (Psi2D_spline_coef(nR*nZ))
RGrid = RGrid + psi_R_shift
RMagAxis = RMagAxis + psi_R_shift
call DBSNAK(nR,RGrid,PsiSplineOrder,Psi2D_spline_Rknots)
call DBSNAK(nZ,ZGrid,PsiSplineOrder,Psi2D_spline_Zknots)
call DBS2IN(nR,RGrid,nZ,ZGrid,PsiRZGrid,nR,PsiSplineOrder,PsiSplineOrder,&
            Psi2D_spline_Rknots,Psi2D_spline_Zknots,Psi2D_spline_coef)
! Record (R,Z) domain for Psi spline
PsiRZDomain(1) = minval(RGrid)
PsiRZDomain(2) = maxval(RGrid)
PsiRZDomain(3) = minval(ZGrid)
PsiRZDomain(4) = maxval(ZGrid)
! take maximu on Z=0 axis
PsiOutOfDomainValue = maxval(PsiRZGrid(:,nZ/2+1))

! exact minimum of Psi in RZ plane
#ifdef _IMSL
! IMSL UMING/DUMING (Single/Double precision)
! Minimize a function of N variables using a quasi-Newton method and a user supplied gradient.
IPARAM(1) = 0 ! set DUMING to default parameters
XSCALE = (/1.0d0, 1.0d00/)
FSCALE = 1.0d0
XGUESS(1)=RMagAxis
XGUESS(2)=ZMagAxis
CALL DUMING (getPsiRZ_DUMING,getGradPsiRZ_DUMING,2,XGUESS,XSCALE,FSCALE,IPARAM,RPARAM,X_min,F_min)
RMagAxis=X_min(1)
ZMagAxis=X_min(2)
PsiMagAxis=F_min
#else
! Using NAG E04DGF
IFAIL = 0
XGUESS(1)=RMagAxis
XGUESS(2)=ZMagAxis
call E04DKF('Print Level = 0')
call E04DGF(2,getPsiRZ_NAG,NAGITER,F_min,dF_min,XGUESS,NAGIWORK,NAGWORK,NAGIUSER,NAGRUSER,IFAIL)
RMagAxis=XGUESS(1)
ZMagAxis=XGUESS(2)
PsiMagAxis=F_min
#endif

if (constructPsiGrid) then
  ! construct Psi Grid
  allocate (PsiGrid(nF))
  dPsi=(Psi_LCFS - PsiMagAxis)/(nF - 1)
  do i=1,nF
    PsiGrid(i)=PsiMagAxis + dPsi*(i-1)
  enddo
else
  PsiGrid = abs(psi_scale) * SGNBPOL * PsiGrid
  ! correct exact axis value
  PsiGrid(1) = PsiMagAxis
endif

! contour tracing
write(*,*)'Contour tracing'
if (allocated(fluxRtrace)) deallocate(fluxRtrace,fluxZtrace,fluxLtrace,fluxPsiGrid,fluxNTrace)
if (allocated(LLdHRtrace)) deallocate(LLdHRtrace,LLdHZtrace,LLdHLtrace)
allocate(fluxRtrace(fluxPsiGridDim,maxSurfDim),fluxZtrace(fluxPsiGridDim,maxSurfDim),fluxLtrace(fluxPsiGridDim,maxSurfDim),&
         fluxPsiGrid(fluxPsiGridDim),fluxNTrace(fluxPsiGridDim))
allocate(LLdHRtrace(maxSurfDim),LLdHZtrace(maxSurfDim),LLdHLtrace(maxSurfDim))
! construct flux psi grid
do i=fluxPsiGridDim,2,-1
  ! fluxPsiGrid(i) = psiMagAxis + (psi_LCFS - psiMagAxis) / (fluxPsiGridDim-1) * (i-1)
  ! fluxPsiGrid(i) = getPsi(RMagAxis + (RLCFS_LFS-RMagAxis) / (fluxPsiGridDim) * i, ZMagAxis)
  if (i==fluxPsiGridDim) then
    fluxPsiGrid(i) = Psi_LCFS
  else
    fluxPsiGrid(i) = getPsi(RMagAxis + (RLCFS_LFS-RMagAxis) / (fluxPsiGridDim-1) * (i-1), ZMagAxis)
  endif
  call fluxTrace(fluxPsiGrid(i),fluxTraceDLcoeff,fluxRtrace(i,:),fluxZtrace(i,:),fluxLtrace(i,:),fluxNTrace(i),isClosed)
  ! check if the flux surface is closed
  if ((.not. isClosed) .and. i<fluxPsiGridDim) then
    write(*,*)'error: flux surfaces not closed, no correction implemented yet, psi=',fluxPsiGrid(i), ', i=',i
    write(getProtocolUnit(),*)'error: flux surfaces not closed, no correction implemented yet, psi=',fluxPsiGrid(i), ', i=',i
    stop
  ! try correcting the LCFS by 50%
  else if ((.not. isClosed) .and. i==fluxPsiGridDim) then
    do j=1,100
      fluxPsiGrid(i) = fluxPsiGrid(i) - (fluxPsiGrid(i) - fluxPsiGrid(i-1))*0.002d0
      call fluxTrace(fluxPsiGrid(i),fluxTraceDLcoeff,fluxRtrace(i,:),fluxZtrace(i,:),fluxLtrace(i,:),fluxNTrace(i),isClosed)
      if (isClosed) exit
    enddo
    if (isClosed) then
      Psi_LCFS = fluxPsiGrid(i)
      PsiGrid(nF) = Psi_LCFS
      write(*,*)'warning - experimental: Psi_LCFS corrected to be closed, psi_LCFS=',Psi_LCFS
      write(getProtocolUnit(),*)'warning - experimental: Psi_LCFS corrected to be closed psi_LCFS=',Psi_LCFS
    else
      write(*,*)'error: LCFS not closed, correction failed'
      write(getProtocolUnit(),*)'error: LCFS not closed, correction failed'
      stop
    endif
  endif
  if (i==fluxPsiGridDim) then
    RLCFS_LFS = maxval(fluxRtrace(i,1:fluxNTrace(i)))
    RLCFS_HFS = minval(fluxRtrace(i,1:fluxNTrace(i)))
  endif

enddo
fluxPsiGrid(1) = PsiMagAxis
fluxNTrace(1) = 1
fluxRTrace(1,1) = RMagAxis
fluxZTrace(1,1) = ZMagAxis
fluxLTrace(1,1) = 0

! Spline F(Psi) (IMSL or bspline)
write(*,*)'F(psi) spline'
allocate(F_spline_knots(nF+PsiSplineOrder),F_spline_coef(nF))
NF_spline_knots = nF
call DBSNAK (nF,PsiGrid,PsiSplineOrder,F_spline_knots)
call DBSINT (nF,PsiGrid,FPsiGrid,PsiSplineOrder,F_spline_knots,F_spline_coef)
! Find minimum and maxium Z of the LCFS
call ZMinMaxFluxSurface(Psi_LCFS,ZLCFS_MIN,ZLCFS_MAX,RLCFS_MIN,RLCFS_MAX)
! rough guess
! ZLCFS_MIN = minval(fluxZtrace(fluxPsiGridDim,:))
! ZLCFS_MAX = maxval(fluxZtrace(fluxPsiGridDim,:))
! RLCFS_MIN = minval(fluxRtrace(fluxPsiGridDim,:))
! RLCFS_MAX = maxval(fluxRtrace(fluxPsiGridDim,:))

! calculate volume, area
write(*,*)'Flux surface functions and averages calculation'
write(*,*)'CalcLL = ',CalcLL
VolPsiGrid(1) = 0.d0
AreaPsiGrid(1) = 0.d0
do i=2,fluxPsiGridDim
  call calculateVolArea(i,VolPsiGrid(i),AreaPsiGrid(i))
enddo
PlasmaVolume = VolPsiGrid(fluxPsiGridDim)
PlasmaArea = AreaPsiGrid(fluxPsiGridDim)
! flux averages
do i=2,fluxPsiGridDim
  BFluxAvGrid(i)    = fluxAverage(fluxRtrace(i,:),fluxZtrace(i,:),fluxLtrace(i,:),fluxNTrace(i),getBtot_RZ)
  BsqFluxAvGrid(i)  = fluxAverage(fluxRtrace(i,:),fluxZtrace(i,:),fluxLtrace(i,:),fluxNTrace(i),getBsq_RZ)
  BmaxFluxAvGrid(i) = fluxMaximum(fluxRtrace(i,:),fluxZtrace(i,:),fluxLtrace(i,:),fluxNTrace(i),getBtot_RZ)
  ! set parameter for H,fc functions for McGregor CD
  if (CalcLL) then
    LLintegBmax = BmaxFluxAvGrid(i)
    LLdHNTrace = fluxNTrace(i)
    LLdHRtrace(1:LLdHNTrace) = fluxRtrace(i,1:LLdHNTrace)
    LLdHZtrace(1:LLdHNTrace) = fluxZtrace(i,1:LLdHNTrace)
    LLdHLtrace(1:LLdHNTrace) = fluxLtrace(i,1:LLdHNTrace)
    do j=1,LLGridDim
      LLlambdaGrid(j) = dble(j-1)/(LLGridDim-1)
  !    call start_watch(watch)
  !    LLHFunPsiLambdaGrid(i,j) = 0
  !    call qromb(LLdHFun,LLlambdaGrid(j),1.d0,LLHFunPsiLambdaGrid(i,j),LLintegTol,0.d0,0)
  !    call stop_watch(watch)      ! This stops the watch
  !    call print_watch(watch)     ! This prints the measured time

  !    LLHFunPsiLambdaGrid(i,j) = 0.5d0 * LLHFunPsiLambdaGrid(i,j)
       LLHFunPsiLambdaGrid(i,j)  = 0.5d0 * integ2(LLdHFun,LLlambdaGrid(j),1.d0,LLintegTol,ierr)
  !    write(*,*)LLHFunPsiLambdaGrid(i,j)
  !    call start_watch(watch)
  !    write(*,*) 0.5d0 * integ2(LLdHFun,LLlambdaGrid(j),1.d0,LLintegTol,ierr)
  !    call stop_watch(watch)      ! This stops the watch
  !    call print_watch(watch)     ! This prints the measured time
    enddo
  endif
enddo
! constant extrapolation
! BFluxAvGrid(1) = BFluxAvGrid(2)
! BsqFluxAvGrid(1) = BsqFluxAvGrid(2)
! linear extrapolation to magnetic axis
! BFluxAvGrid(1) =   max(0.d0,linInterp(fluxPsiGrid(1),fluxPsiGrid(2:3),BFluxAvGrid(2:3)))
! BsqFluxAvGrid(1) = max(0.d0,linInterp(fluxPsiGrid(1),fluxPsiGrid(2:3),BsqFluxAvGrid(2:3)))
! BmaxFluxAvGrid(1) = max(0.d0,linInterp(fluxPsiGrid(1),fluxPsiGrid(2:3),BmaxFluxAvGrid(2:3)))
! exact values
BFluxAvGrid(1) =   getBtot_RZ(RMagAxis,ZMagAxis)
BsqFluxAvGrid(1) = getBsq_RZ(RMagAxis,ZMagAxis)
BmaxFluxAvGrid(1) = getBtot_RZ(RMagAxis,ZMagAxis)
if (CalcLL) then
  do j=1,LLGridDim
    LLHFunPsiLambdaGrid(1,j)  = linInterp(fluxPsiGrid(1),fluxPsiGrid(2:3),LLHFunPsiLambdaGrid(2:3,j))
  enddo
endif

! spline fluxPsi variables
allocate(flux_spline_knots(fluxPsiGridDim+FluxSplineOrder),Vol_spline_coef(fluxPsiGridDim),Area_spline_coef(fluxPsiGridDim))
allocate(BFluxAv_spline_coef(fluxPsiGridDim),BsqFluxAv_spline_coef(fluxPsiGridDim),BmaxFluxAv_spline_coef(fluxPsiGridDim))
Nflux_spline_knots = fluxPsiGridDim
call DBSNAK (Nflux_spline_knots,fluxPsiGrid,FluxSplineOrder,flux_spline_knots)
call DBSINT (Nflux_spline_knots,fluxPsiGrid,VolPsiGrid,FluxSplineOrder,flux_spline_knots,Vol_spline_coef)
call DBSINT (Nflux_spline_knots,fluxPsiGrid,AreaPsiGrid,FluxSplineOrder,flux_spline_knots,Area_spline_coef)
call DBSINT (Nflux_spline_knots,fluxPsiGrid,BFluxAvGrid,FluxSplineOrder,flux_spline_knots,BFluxAv_spline_coef)
call DBSINT (Nflux_spline_knots,fluxPsiGrid,BsqFluxAvGrid,FluxSplineOrder,flux_spline_knots,BsqFluxAv_spline_coef)
call DBSINT (Nflux_spline_knots,fluxPsiGrid,BmaxFluxAvGrid,FluxSplineOrder,flux_spline_knots,BmaxFluxAv_spline_coef)
if (CalcLL) then
  allocate(LLlambda_spline_knots(LLGridDim+FluxSplineOrder),LLHfun_spline_coef(fluxPsiGridDim*LLGridDim),LLfcFun_spline_coef(fluxPsiGridDim))
  NLLlambda_spline_knots = LLGridDim
  call DBSNAK (NLLlambda_spline_knots,LLlambdaGrid,FluxSplineOrder,LLlambda_spline_knots)
  call DBS2IN(fluxPsiGridDim, fluxPsiGrid, LLGridDim, LLlambdaGrid, LLHFunPsiLambdaGrid, fluxPsiGridDim, FluxSplineOrder, FluxSplineOrder,&
              flux_spline_knots, LLlambda_spline_knots, LLHfun_spline_coef)

  do i=2,fluxPsiGridDim
    LLHFun_lambda_psi = fluxPsiGrid(i)
    LLfcFunPsiGrid(i)    = 1.5d0 * BsqFluxAvGrid(i) / (BmaxFluxAvGrid(i)**2) * integ2(LLHFun_lambda,0.d0,1.d0,LLintegTol,ierr)
  enddo
  LLfcFunPsiGrid(1) = linInterp(fluxPsiGrid(1),fluxPsiGrid(2:3),LLfcFunPsiGrid(2:3))
  !spline LLfcFunPsiGrid here
  call DBSINT (Nflux_spline_knots,fluxPsiGrid,LLfcFunPsiGrid,FluxSplineOrder,flux_spline_knots,LLfcFun_spline_coef)
endif

end subroutine
