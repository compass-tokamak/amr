!> Module containing ray and beam types and functions
module rays

  use vectors
  use equilibrium
  use zeroin
  use generalConstants

  implicit none

  !> Returns the position
  interface getPosition
    module procedure getRayPosition
  end interface

  !> Returns the wave vector
  interface getWaveVector
    module procedure getRayWaveVector
  end interface

  !> Returns the wave vector field (including the position)
  interface getWaveVectorField
    module procedure getRayWaveVectorField
  end interface

  !> Returns the electric vector field
  interface getEVectorField
    module procedure getRayEVectorField
  end interface

  !> Returns the number of rays
  interface getNumberOfRays
    module procedure getNumberOfRays_Beam,getNumberOfRays_GaussianBeam
  end interface

  !> Returns the central ray
  interface getCentralRay
    module procedure getCentralRay_beam,getCentralRay_gaussbeam
  end interface

  !> Instantiate a new Ray 
  interface newRay
    module procedure newRay_vectors,newRay_vField,newRay_vField_complexVField
  end interface

  !> True if the argument is set
  interface isSet
    module procedure isBeamSet,isRaySet
  end interface

  !> Unset the argument
  interface unset
    module procedure unsetRay
  end interface

  !> Returns the coordinate system
  interface getCoordinateSystem
    module procedure getBeamCoordinateSystem,getRayCoordinateSystem
  end interface

  !> Returns the frequency [Hz]
  interface getFrequency
    module procedure getFrequency_ray,getFrequency_gaussbeam
  end interface

   !> Returns the power of a ray or a beam [kW]
  interface getPower
    module procedure getPower_ray,getPower_gaussbeam
  end interface

 !> Returns the radius [m]
  interface getRadius
    module procedure getGaussianBeamRadius
  end interface

  !> Compute the complementary linear polarization
  interface complementaryPolarization
    module procedure complementaryPolarization_gBeam
  end interface

  !> Dumps to stdout
  interface dump
    module procedure dumpRay,dumpBeam,dumpGaussianBeam
  end interface

  !> Dumps to a file
  interface dumpToFile
    module procedure dumpToFileBeam,dumpToFileGaussianBeam
  end interface

  !> Contains single ray parameters.
  !!
  !! Ray is specified by its frequency, wave vector (which determines the position as well),
  !! electric field, and the associated intensity and power.
  type :: Ray
    private
    real(8) :: frequency = 0.d0
    type(VectorField) :: waveVector
    type(ComplexVectorField) :: E
    real(8) :: intensity, power
  end type

  !> Contains a group of rays.
  type :: Beam
    private
    integer(4) :: no_of_rays = 0
    type(ray),allocatable :: beamRays(:)
    real(8) :: radius
  end type

  !> Contains a generalized Gaussian beam.
  !!
  !! The Gaussian beam is spcified by the waist central ray and the waist radius \f$ w_0 \f$.
  !! In addition we add optional fixed divergence,
  !! assymetry \f$ \sigma \f$, radius enlargement \f$ \varepsilon \f$ and width \f$ \lambda \f$.
  !! Intensity distribution in the waist is then 
  !! \f[
  !! I\left( r \right) = \exp \left( - 2\left| \frac{r - \sigma }{\varepsilon w_0 } \right|^{2\lambda } \right)
  !! \f]
  !! Additional properties are used for constructing the beam rays,
  !! which are organized in numberOfCircles circles of raysPerCircle rays plus the central ray.
  !! The circles and rays are distributed uniformly. If circleShift = true then
  !! each circle is rotated by 180/raysPerCircle degrees to more equally fill the waist.
  !! The powerNorm is computed so that \f$ \int {I{\rm d}S}  = 1 \f$.
  type :: GaussianBeam
    private
    integer(4) :: no_of_rays = 0
    type(Ray) :: waistCentralRay
    real(8) :: waistRadius,power
    integer(4) :: noOfCircles,raysPerCircle
    type(Beam) :: waistBeam
    logical(4) :: fixDivergence = .false., circleShift = .false.
    real(8) :: divergence   ! ***divergence in degrees***
    ! intensity = exp(-2*|(r-assymetry)/(waistRadius*radiusEnlargement)|^(2*width))
    real(8) :: assymetry = 0.d0, radiusEnlargement = 1.0d0, width = 1.d0
    ! powerNorm = integral of I*dS over the waist
    ! then central ray intensity = 1/powerNorm to have unit power through the beam waist
    real(8) :: powerNorm
  end type

  ! for zeroLCFS function only
  type(vector),private,save :: zeroLCFS_start,zeroLCFS_vec
  ! for zeroPCO function only
  type(vector),private,save :: zeroPCO_start,zeroPCO_vec
  real(8),private,save :: zeroPCO_plasfreq
  real(8),parameter :: zeroLCFS_ERRABS = 0.d0, zeroLCFS_ERRREL = 1.d-6, zeroPCO_ERRREL = 1.d-6
  integer(4) :: zeroLCFS_MAXFN = 500

  ! for powerNormFunction only
  real(8),private :: powerNormFunction_assymetry,powerNormFunction_waistRadius, &
                     powerNormFunction_rEnlargement,powerNormFunction_width


  private :: signum,isBeamSet,isRaySet,unsetRay,getBeamCoordinateSystem,getRayCoordinateSystem
  private :: getNumberOfRays_Beam,getNumberOfRays_GaussianBeam,getFrequency_ray,getFrequency_gaussbeam
  private :: getPower_ray,getPower_gaussbeam
  private :: complementaryPolarization_gBeam
  private :: dumpBeam,dumpRay,dumpToFileBeam,dumpToFileGaussianBeam

  contains

  real(8) function getGaussianBeamRadius(myBeam,pos,iray)
    implicit none
    type(GaussianBeam),intent(in) :: myBeam
    type(Vector),intent(in) :: pos
    integer(4),intent(in) :: iray
    type(beam) :: beamWaist
    real(8) :: w0,z0,distance
    type(vector) :: N

    beamWaist = getGaussianBeamWaist(myBeam)
    ! distance = getPosition(getCentralRay(beamWaist)) * ( pos - getPosition(getRay(beamWaist,iray)) )
    N = getField(toCartesian(getWaveVectorField(getCentralRay(beamWaist))))
    N = N/l2norm(N)
    distance = abs((toCartesian(pos) - toCartesian(getPosition(getRay(beamWaist,iray)))) * N)
    w0 = beamWaist%radius
    z0 = Pi*w0**2/(vc_m_s/beamWaist%beamRays(1)%frequency)
    if (.not.(myBeam%fixDivergence)) then
      getGaussianBeamRadius = w0*sqrt(1.d0 + (distance/z0)**2)
    else
      getGaussianBeamRadius = w0 + tand(myBeam%divergence/2.0d0)*distance  ! opraveno tand(myBeam%divergence)
    endif

  end function

  real(8) function getWaistRadius(myBeam)
    implicit none
    type(GaussianBeam),intent(in) :: myBeam
    type(beam) :: beamWaist

    beamWaist = getGaussianBeamWaist(myBeam)
    getWaistRadius = beamWaist%radius
  end function

  real(8) function getIntensity(myRay)
    implicit none
    type(Ray),intent(in) :: myRay

    getIntensity = myRay%intensity
  end function

  real(8) function getFrequency_ray(myRay)
    implicit none
    type(Ray),intent(in) :: myRay

    getFrequency_ray = myRay%frequency
  end function

  real(8) function getFrequency_gaussbeam(myBeam)
    implicit none
    type(GaussianBeam),intent(in) :: myBeam

    getFrequency_gaussbeam = myBeam%waistCentralRay%frequency
  end function

  real(8) function getPower_ray(myRay)
    implicit none
    type(Ray),intent(in) :: myRay

    getPower_ray = myRay%power
  end function

  real(8) function getPower_gaussbeam(myBeam)
    implicit none
    type(GaussianBeam),intent(in) :: myBeam

    getPower_gaussbeam = myBeam%power
  end function

  integer(4) function getBeamCoordinateSystem(myBeam)
    implicit none
    type(Beam),intent(in) :: myBeam
     
    getBeamCoordinateSystem = getCoordinateSystem(myBeam%beamRays(1)%waveVector)
  end function

  integer(4) function getRayCoordinateSystem(myRay)
    implicit none
    type(Ray),intent(in) :: myRay
     
    getRayCoordinateSystem = getCoordinateSystem(myRay%waveVector)
  end function
  
  function getCentralRay_beam(myBeam)
    implicit none
    type(ray) :: getCentralRay_beam
    type(beam),intent(in) :: myBeam

    getCentralRay_beam = myBeam%beamRays(1)
  end function

  function getCentralRay_gaussBeam(myBeam)
    implicit none
    type(ray) :: getCentralRay_gaussBeam
    type(gaussianBeam),intent(in) :: myBeam

    getCentralRay_gaussBeam = myBeam%waistCentralRay
  end function

  function getRay(myBeam,index)
    implicit none
    type(ray) :: getRay
    type(beam),intent(in) :: myBeam
    integer(4),intent(in) :: index

    getRay = myBeam%beamRays(index)
  end function

  logical(4) function isBeamSet(myBeam)
    implicit none
    type(Beam),intent(in) :: myBeam

    isBeamSet = (myBeam%no_of_rays > 0)
  end function

  logical(4) function isRaySet(myRay)
    implicit none
    type(ray),intent(in) :: myRay

    isRaySet = (myRay%frequency /= 0)
  end function

  subroutine unsetRay(myRay)
    implicit none
    type(ray),intent(out) :: myRay

    myRay%frequency = 0
  end subroutine

  function newGaussianBeam(centralRay,radius,noOfCircles,raysPerCircle,power,fixDivergence,divergence,circleShift, &
                           assymetry,radiusEnlargement,width)
    implicit none
    type(GaussianBeam) :: newGaussianBeam
    type(Ray),intent(in) :: centralRay
    real(8),intent(in) :: radius,power
    integer(4),intent(in) :: noOfCircles,raysPerCircle
    logical(4),optional :: fixDivergence,circleShift
    real(8),optional :: divergence,assymetry,radiusEnlargement,width

    real(8),parameter :: ERRABS=0.d0,ERRREL=1.d-9
    real(8) :: intResult, ERREST
    integer :: NPTS,IFAIL
#ifdef _IMSL

#else
    real(8),external :: D01AHF
#endif

    newGaussianBeam%no_of_rays = 1+raysPerCircle*noOfCircles
    newGaussianBeam%waistCentralRay = centralRay
    newGaussianBeam%waistRadius = radius
    newGaussianBeam%noOfCircles = noOfCircles
    newGaussianBeam%raysPerCircle = raysPerCircle
    newGaussianBeam%power = power
    if (present(fixDivergence)) then
      newGaussianBeam%fixDivergence = fixDivergence
    else
      newGaussianBeam%fixDivergence = .false.
    endif
    if (present(divergence)) then
      newGaussianBeam%divergence = divergence
    else
      newGaussianBeam%divergence = 0
    endif
    if (present(circleShift)) then
      newGaussianBeam%circleShift = circleShift
    else
      newGaussianBeam%circleShift = .false.
    endif
    if (present(assymetry)) then
      newGaussianBeam%assymetry = assymetry
    endif
    if (present(radiusEnlargement)) then
      newGaussianBeam%radiusEnlargement = radiusEnlargement
    endif
    if (present(width)) then
      newGaussianBeam%width = width
    endif
    ! 1.358212161d0=0.5*Pi*(1-e^-2) ~ 0.5*PI*0.865 -> Wikipedia
    newGaussianBeam%powerNorm = 1.358212161d0 * radius**2  
    if (newGaussianBeam%assymetry/=0.d0 .or. newGaussianBeam%radiusEnlargement/=1.d0 .or. newGaussianBeam%width/=1.d0) then
      ! powerNorm = int(I*dS)
      powerNormFunction_assymetry = newGaussianBeam%assymetry
      powerNormFunction_waistRadius = newGaussianBeam%waistRadius
      powerNormFunction_rEnlargement = newGaussianBeam%radiusEnlargement
      powerNormFunction_width = newGaussianBeam%width
#ifdef _IMSL
      CALL DQDAG (powerNormFunction, 0.d0, radius, ERRABS, ERRREL, 2, intResult, ERREST)
#else
      IFAIL = 0
      intResult = D01AHF(0.d0, radius, ERRREL, NPTS, ERREST, powerNormFunction, 0, IFAIL)
#endif
      newGaussianBeam%powerNorm = intResult*2*Pi
    endif
    newGaussianBeam%waistCentralRay%intensity = power/newGaussianBeam%powerNorm
    if (noOfCircles==0) then
      newGaussianBeam%waistCentralRay%power = power
    else
      newGaussianBeam%waistCentralRay%power = calculateGaussPower(newGaussianBeam,radius/noOfCircles/2)
    endif

  end function

  !> Calculate Gaussian beam power through a circle of a given radius (in the waist plane)
  real(8) function calculateGaussPower(gaussBeam,radius)
    type(GaussianBeam),intent(in) :: gaussbeam
    real(8),intent(in) :: radius
    real(8) :: intResult
    real(8),parameter :: ERRABS=0.d0,ERRREL=1.d-9
    real(8) :: ERREST
    integer :: NPTS,IFAIL
#ifdef _IMSL

#else
    real(8),external :: D01AHF
#endif

    if (gaussBeam%assymetry==0.d0 .and. gaussBeam%radiusEnlargement==1.d0 .and. gaussBeam%width==1.d0) then
      calculateGaussPower = 1.570796327d0*gaussBeam%waistRadius**2*(exp(2*radius**2/gaussBeam%waistRadius**2)-1)* &
                            exp(-2*radius**2/gaussBeam%waistRadius**2)*gaussBeam%waistCentralRay%intensity
    else
      powerNormFunction_assymetry = gaussBeam%assymetry
      powerNormFunction_waistRadius = gaussBeam%waistRadius
      powerNormFunction_rEnlargement = gaussBeam%radiusEnlargement
      powerNormFunction_width = gaussBeam%width
#ifdef _IMSL
      CALL DQDAG (powerNormFunction, 0.d0, radius, ERRABS, ERRREL, 2, intResult, ERREST)
#else
      IFAIL = 0
      intResult = D01AHF(0.d0, radius, ERRREL, NPTS, ERREST, powerNormFunction, 0, IFAIL)
#endif
      calculateGaussPower = intResult*2*Pi*gaussBeam%waistCentralRay%intensity
    endif
  end function

  real(8) function powerNormFunction(x)
    implicit none
    real(8),intent(in) :: x

    powerNormFunction = x*exp(-2*abs((x-powerNormFunction_assymetry)/ &
      (powerNormFunction_waistRadius*powerNormFunction_rEnlargement))**(2*powerNormFunction_width))
  end function

  function getGaussianBeamWaist(gaussBeam)
    implicit none
    type(Beam) :: getGaussianBeamWaist
    type(GaussianBeam),intent(in) :: gaussBeam
    
    if(isSet(gaussBeam%waistBeam)) then
      getGaussianBeamWaist = gaussBeam%waistBeam
    else
      getGaussianBeamWaist = newGaussianBeamWaist(gaussBeam)
    endif
  end function

  subroutine initGaussianBeamWaist(gaussBeam)
    implicit none
    type(GaussianBeam),intent(inout) :: gaussBeam
    
    gaussBeam%waistBeam = newGaussianBeamWaist(gaussBeam)
  end subroutine

  function rayWaistPosition(gaussBeam,iray)
    implicit none
    type(GaussianBeam),intent(in) :: gaussBeam
    integer(4),intent(in) :: iray
    type(vector) :: rayWaistPosition
    integer(4) :: icircle
    real(8) :: rCircle

    if (iray == 1) then
      rayWaistPosition = cartesianVector(0.d0,0.d0,0.d0)
    else if (iray>=2 .and. iray <= gaussBeam%noOfCircles*gaussBeam%raysPerCircle+1) then
      icircle = (iray-2) / gaussBeam%raysPerCircle + 1
      rCircle = icircle*gaussBeam%waistRadius/gaussBeam%noOfCircles
      if (gaussBeam%circleShift) then
        rayWaistPosition = cartesianVector(rCircle*cosd(180.d0/gaussBeam%raysPerCircle*(mod(icircle,2))- &
                           (iray+6)*360.0d0/gaussBeam%raysPerCircle), &
                           rCircle*sind(180.d0/gaussBeam%raysPerCircle*(mod(icircle,2))- &
                           (iray+6)*360.0d0/gaussBeam%raysPerCircle), &
                           0.d0)
      else
        !!! zmeneno z iray + 6 !!!
        rayWaistPosition = cartesianVector(rCircle*cosd(-(iray+10)*360.0d0/gaussBeam%raysPerCircle), &
                               rCircle*sind(-(iray+10)*360.0d0/gaussBeam%raysPerCircle), &
                               0.d0)
      endif
    else
      write(*,*)'Error: iray > nrays in rayWaistPosition'
      stop
    endif
  end function

  function newGaussianBeamWaist(gaussBeam)
    implicit none
    type(beam) :: newGaussianBeamWaist
    type(GaussianBeam),intent(in) :: gaussBeam

!    type(ray),intent(in) :: centralRay
!    real(8),intent(in) :: radius
!    integer(4),intent(in) :: noOfCircles,raysPerCircle
    integer(4) :: nrays,iCircle,iRay,rayIndex

    type(vector) :: rayPos,waveVec,centralPosition,xp,yp,zp,rayVec,rayVec0
    real(8) :: rCircle,circleIntensity,powersum,circlePower
    real(8) :: S(3),N(3),A(3),DCM(3,3)

    nrays = gaussBeam%noOfCircles*gaussBeam%raysPerCircle+1
    newGaussianBeamWaist%no_of_rays = nrays
    newGaussianBeamWaist%radius = gaussBeam%waistRadius
    allocate(newGaussianBeamWaist%beamRays(nrays))
    newGaussianBeamWaist%beamRays(1) = gaussBeam%waistCentralRay
    waveVec = getField(toCartesian(gaussBeam%waistCentralRay%waveVector))
    centralPosition = getPosition(toCartesian(gaussBeam%waistCentralRay%waveVector))
    S = toCartesianArray(centralPosition)
    
    N = toCartesianArray(waveVec)/l2Norm(waveVec)
    powersum = gaussBeam%waistCentralRay%power
    do icircle = 1,gaussBeam%noOfCircles
      rCircle = icircle*gaussBeam%waistRadius/gaussBeam%noOfCircles
      circleIntensity = gaussBeam%waistCentralRay%intensity * &
                        exp(-2*abs((rCircle-gaussBeam%assymetry)/ &
                        (gaussBeam%waistRadius*gaussBeam%radiusEnlargement))**(2*gaussBeam%width))
      if (icircle==gaussBeam%noOfCircles) then
        circlePower = calculateGaussPower(gaussBeam,gaussBeam%waistRadius) - &
                      powersum
        powersum = powersum + circlePower
      else
        circlePower = calculateGaussPower(gaussBeam,gaussBeam%waistRadius/(2*gaussBeam%noOfCircles)*(2*icircle+1)) - &
                      powersum
        powersum = powersum + circlePower
      endif
      do iRay = 1,gaussBeam%raysPerCircle
        rayIndex = (icircle-1)*gaussBeam%raysPerCircle+iRay+1

        ! souradnice xy = rovina waistu, z || centralRay
        rayPos = rayWaistPosition(gaussBeam,rayIndex)

        !!! rotace do puvodnich souradnic !!!
        ! S - stred svazku v kartezkych souradnicich
        ! N - vlnovy vektor, |N| = 1
        ! vektor A-S - kolmy na N, lezi v rovine prochazejici S, rovnobezne s N a osou z
        ! A-S = nova osa x'
        ! nebude fungovat pro N rovnobezne s osou z
        A(1) = (N(1)**4*S(1)+S(1)*N(1)**2*N(3)**2+2*N(1)**2*S(1)*N(2)**2+N(2)**4*S(1)+N(2)**2*S(1)*N(3)**2+ &
               (N(1)**2+N(2)**2)**(1/2)*(N(1)**2+N(3)**2+N(2)**2)**(1/2)*abs(N(1)*N(3)))/(N(1)**2+ &
               N(2)**2)
        A(2) = (N(2)*signum(N(1))*(N(1)**2+N(2)**2)**(1/2)*(N(1)**2+N(3)**2+N(2)**2)**(1/2)* &
               abs(N(3))+2*S(2)*N(1)**2*N(2)**2+S(2)*N(2)**4+S(2)*N(1)**4+S(2)*N(1)**2*N(3)**2+S(2)* &
               N(2)**2*N(3)**2)/(N(1)**2+N(2)**2)
        A(3) = (S(3)*N(1)**2+S(3)*N(2)**2+N(3)**2*S(3)-signum(N(1))*signum(N(3))*(N(1)**2+ &
               N(2)**2)**(1/2)*(N(1)**2+N(3)**2+N(2)**2)**(1/2))

        ! nove osy xp,yp,zp = x',y',z'
        xp = cartesianVector(A-S)
        ! z neznamych duvodu ||xp|| /= 1
        xp = xp/l2norm(xp)
        zp = cartesianVector(N)
        yp = crossProduct(zp,xp)
        ! kontrola kolmosti a normy
!         write(*,*)'xp.yp = ',xp*yp
!         write(*,*)'xp.zp = ',xp*zp
!         write(*,*)'zp.yp = ',zp*yp
!         write(*,*)'xp.xp = ',xp*xp
!         write(*,*)'yp.yp = ',yp*yp
!         write(*,*)'zp.zp = ',zp*zp

        ! DCM - Direction Cosine Matrix
        ! http://en.wikipedia.org/wiki/Rotation_representation_%28mathematics%29#Direction_Cosine_Matrix_.28DCM.29
        DCM(:,1) = toCartesianArray(xp)
        DCM(:,2) = toCartesianArray(yp)
        DCM(:,3) = toCartesianArray(zp)
        ! rotace podle DCM
        rayPos = cartesianVector(matmul((DCM),toCartesianArray(rayPos)))
        ! posun do stredu svazku
        rayPos = rayPos + centralPosition

        ! kontrola kolmosti a vzdalenosti paprsku
!        rayVec = rayPos - centralPosition
!        write(*,*)'rayVec.N = ',rayVec*waveVec
!        write(*,*)'||rayVec||-r = ',l2norm(rayVec)-rCircle

        newGaussianBeamWaist%beamRays(rayIndex)%frequency = gaussBeam%waistCentralRay%frequency
        newGaussianBeamWaist%beamRays(rayIndex)%waveVector = toCoordinateSystem(cartesianVectorField(rayPos,waveVec),getCoordinateSystem(gaussBeam%waistCentralRay%waveVector))
        newGaussianBeamWaist%beamRays(rayIndex)%E = toCoordinateSystem(cartesianVectorField( &
          rayPos,getField(toCartesian(gaussBeam%waistCentralRay%E))),getCoordinateSystem(gaussBeam%waistCentralRay%waveVector))
        newGaussianBeamWaist%beamRays(rayIndex)%intensity = circleIntensity
        newGaussianBeamWaist%beamRays(rayIndex)%power = circlePower/gaussBeam%raysPerCircle
      enddo
    enddo

    ! kontrola uhlu mezi paprsky
!    do icircle = 1,gaussBeam%noOfCircles
!      do iRay = 2,gaussBeam%raysPerCircle
!        rayIndex = (icircle-1)*gaussBeam%raysPerCircle+iRay+1
!        rayPos = getPosition(toCartesian(newGaussianBeamWaist%beamRays(rayIndex)%waveVector))
!        rayVec = rayPos - centralPosition
!        rayPos = getPosition(toCartesian(newGaussianBeamWaist%beamRays(rayIndex-1)%waveVector))
!        rayVec0 = rayPos - centralPosition
!        write(*,*)'alpha = ',acosd(rayVec*rayVec0/l2norm(rayVec)/l2norm(rayVec0))
!      enddo
!    enddo

  end function

  function GaussianBeamSpot(gaussBeam,spotCenter)
    ! Returns 'spot' of a Gaussian beam
    ! i.e. Gaussian beam at spotCenter position
    ! http://en.wikipedia.org/wiki/Gaussian_beam
    implicit none
    type(beam) :: GaussianBeamSpot,gaussBeamWaist
    type(GaussianBeam),intent(in) :: gaussBeam
    type(beam) :: beamWaist
    type(Vector),intent(in) :: spotCenter
    type(Vector) :: d,r,rp,S0,Sp,N0,A0,Ap,k0,k1,n
    type(ComplexVector) :: E0,E1,Ek,Ep1,Ep
    real(8) :: w,w0,z0
    integer(4) :: iRay,targetSystem
    real(8),parameter :: accuracy_zero = 1.d-10

    beamWaist = getGaussianBeamWaist(gaussBeam)
    allocate(GaussianBeamSpot%beamRays(beamWaist%no_of_rays))

    S0 = toCartesian(getPosition(beamWaist%beamRays(1)))
    Sp = toCartesian(spotCenter)
    d =  Sp - S0
    w0 = beamWaist%radius
    z0 = Pi*w0**2/(vc_m_s/beamWaist%beamRays(1)%frequency)
    if (.not.(gaussBeam%fixDivergence)) then
      w = w0*sqrt(1.d0 + (l2norm(d)/z0)**2)
    else
      w = w0 + tand(gaussBeam%divergence/2.0d0)*l2norm(d)   ! opraveno  tand(gaussBeam%divergence)
    endif
    gaussBeamWaist = getGaussianBeamWaist(gaussBeam)
    GaussianBeamSpot%radius = w
    GaussianBeamSpot%no_of_rays = beamWaist%no_of_rays
    targetSystem = getCoordinateSystem(beamWaist)
    do iray=1,beamWaist%no_of_rays
      GaussianBeamSpot%beamRays(iRay)%frequency = beamWaist%beamRays(iRay)%frequency
      ! elektricke pole neni hotovo
      ! konstantni napric Gaussovskym svazkem
      A0 = toCartesian(getPosition(beamWaist%beamRays(iRay)))
      rp = (w/w0) * (A0 - S0)
      Ap = Sp+rp 
      N0 = getField(toCartesian(beamWaist%beamRays(iRay)%waveVector))
      ! sign (N0*(Ap-A0)) pridano kvuli smeru vlnoveho vektoru pro pripad
      ! kdy spot lezi pred waistem
      k1 = sign(1.d0,N0*(Ap-A0))*l2norm(N0)*(Ap-A0)/l2norm(Ap-A0)
      GaussianBeamSpot%beamRays(iRay)%waveVector = toCoordinateSystem(CartesianVectorField(Ap,k1),targetSystem)
      GaussianBeamSpot%beamRays(iRay)%intensity = beamWaist%beamRays(1)%intensity * (w0/w)**2 * &
                                                  exp(-2*abs((l2norm(rp)-gaussBeam%assymetry)/ &
                                                  (w*gaussBeam%radiusEnlargement))**(2*gaussBeam%width))

      GaussianBeamSpot%beamRays(iRay)%power = gaussBeamWaist%beamRays(iRay)%power
      ! elektricke pole E
      ! rozlozime na E = Ek + Ep
      ! Ep lezi v rovine vlnovych vektoru k,k'
      ! potom Ep' = [(Ek x k).Ep]*(Ek x k')
      ! vektorove souciny jsou normovane na 1
      E0 = getField(toCartesian(beamWaist%beamRays(1)%E))
      k0 = getField(toCartesian(beamWaist%beamRays(1)%waveVector))
      k0 = k0/l2norm(k0)
      ! k1 = getField(toCartesian(GaussianBeamSpot%beamRays(iRay)%waveVector))
      k1 = k1/l2norm(k1)
      n = crossProduct(k0,k1)
      if (l2norm(n)>accuracy_zero) then
        n = n/l2norm(n)
        Ek = (E0*n)*n
        Ep = E0 - Ek
        ! Ep1 = (crossProduct(Ek,k0)*Ep)*crossProduct(Ek,k1)
        ! Ep1 = 1.d0/ (Ek*Ek) * Ep1
        Ep1 = (crossProduct(n,k0)*Ep)*crossProduct(n,k1)
        E1 = Ek + Ep1
        ! check whether the new electric field is ok
        if ((l2norm(E1)-l2norm(E0))/l2norm(E0)>accuracy_zero .or. abs((E1*k1)/l2norm(E1))>accuracy_zero) then
          write(*,*)'||E0|| <> ||E1|| or |E1.k1|>0 in GaussianBeamSpot'
        stop
      endif
      else
        E1 = E0
      endif
      GaussianBeamSpot%beamRays(iRay)%E = toCoordinateSystem(CartesianVectorField(Ap,E1),targetSystem)
    enddo
  end function

  function GaussianBeamLCFSSpot(gaussBeam)
    implicit none
    type(Beam) :: GaussianBeamLCFSSpot
    type(GaussianBeam),intent(in) :: gaussBeam
    integer(4) :: iray
    type(Vector) :: myLCFSSpot,centralLCFSSpot

    call findLCFSSpot(gaussBeam%waistCentralRay,centralLCFSSpot)
    if (.not.(isSet(centralLCFSSpot))) then
      write(*,*)'warning: could not find central ray LCFS spot!!!'
      ! stop
    else
      GaussianBeamLCFSSpot = GaussianBeamSpot(gaussBeam,centralLCFSSpot)
      do iray=2,gaussBeam%no_of_rays
        ! write(*,*)iray
        call findLCFSSpot(GaussianBeamLCFSSpot%beamRays(iray),myLCFSSpot)
        if (.not.(isSet(myLCFSSpot))) then
          write(*,*)'Could not find LCFS spot, ray ', iray
          call unset(GaussianBeamLCFSSpot%beamRays(iray))
        else
          call moveRay(GaussianBeamLCFSSpot%beamRays(iray),myLCFSSpot)
        endif
      enddo
    endif
  end function

  function GaussianBeamPCOSpot(gaussBeam)
    implicit none
    type(Beam) :: GaussianBeamPCOSpot
    type(GaussianBeam),intent(in) :: gaussBeam
    integer(4) :: iray
    type(Vector) :: myPCOSpot,centralPCOSpot

    call findPCOSpot(gaussBeam%waistCentralRay,centralPCOSpot)
    if (.not.(isSet(centralPCOSpot))) then
      write(*,*)'warning: could not find central ray PCO spot!!!'
      ! stop
    else
      GaussianBeamPCOSpot = GaussianBeamSpot(gaussBeam,centralPCOSpot)
      do iray=2,gaussBeam%no_of_rays
        ! write(*,*)iray
        call findPCOSpot(GaussianBeamPCOSpot%beamRays(iray),myPCOSpot)
        if (.not.(isSet(myPCOSpot))) then
          write(*,*)'Could not find PCO spot, ray ', iray
          call unset(GaussianBeamPCOSpot%beamRays(iray))
        else
          call moveRay(GaussianBeamPCOSpot%beamRays(iray),myPCOSpot)
        endif
      enddo
    endif
  end function

  subroutine moveRay(myRay,vec)
    implicit none
    type(Ray),intent(inout) :: myRay
    type(Vector),intent(in) :: vec
    type(VectorField) :: vField

    vField = toCartesian(myRay%waveVector)
    call setPosition(vField,vec)
    vField = toCoordinateSystem(vField,getCoordinateSystem(myRay))
    myRay%waveVector = vField
    myRay%E = toCoordinateSystem(cartesianVectorField(toCartesian(vec),getField(toCartesian(myRay%E))), &
              getCoordinateSystem(myRay))
  end subroutine
  
  function getRayPosition(r)
    implicit none
    type(vector) :: getRayPosition
    type(ray),intent(in) :: r

    getRayPosition = getPosition(r%waveVector)
  end function

  function getRayWaveVector(r)
    implicit none
    type(vector) :: getRayWaveVector
    type(ray),intent(in) :: r

    getRayWaveVector = getField(r%waveVector)
  end function

  function getRayWaveVectorField(r)
    implicit none
    type(VectorField) :: getRayWaveVectorField
    type(Ray),intent(in) :: r

    getRayWaveVectorField = r%waveVector
  end function

  function getRayEVectorField(r)
    implicit none
    type(ComplexVectorField) :: getRayEVectorField
    type(Ray),intent(in) :: r

    getRayEVectorField = r%E
  end function

  subroutine setEField(gaussBeam,Enew)
    type(GaussianBeam),intent(inout) :: gaussBeam
    type(ComplexVectorField),intent(in) :: Enew
    integer(4) :: i

    gaussBeam%waistCentralRay%E = Enew
    if (isset(gaussBeam%waistBeam)) then
      do i=1,getNumberOfRays(gaussBeam%waistBeam)
        gaussBeam%waistBeam%beamRays(i)%E = Enew
      enddo
    endif
  end subroutine

  function newRay_vectors (pos,wvec,freq,intensity)
    implicit none
    type(ray) :: newRay_vectors
    type(vector),intent(in) :: pos,wvec
    real(8),intent(in) :: freq,intensity

    newRay_vectors%waveVector = newVectorField(pos,wvec)
    newRay_vectors%frequency = freq
    newRay_vectors%intensity = intensity
  end function

  function newRay_vectors_E (pos,wvec,Efield,freq)
    implicit none
    type(ray) :: newRay_vectors_E
    type(vector),intent(in) :: pos,wvec
    type(ComplexVector),intent(in) :: Efield
    real(8),intent(in) :: freq

    !!! DODELAT !!!

    newRay_vectors_E%waveVector = newVectorField(pos,wvec)
    newRay_vectors_E%E = newComplexVectorField(pos,Efield)
    newRay_vectors_E%frequency = freq
  end function

  function newRay_vField (waveVector,freq,intensity)
    implicit none
    type(ray) :: newRay_vField
    type(vectorField),intent(in) :: waveVector
    real(8),intent(in) :: freq,intensity

    newRay_vField%waveVector = waveVector
    newRay_vField%frequency = freq
    newRay_vField%intensity = intensity
  end function

  function newRay_vField_complexVField (waveVector,E,freq,intensity)
    implicit none
    type(ray) :: newRay_vField_complexVField
    type(vectorField),intent(in) :: waveVector
    type(ComplexVectorField),intent(in) :: E
    real(8),intent(in) :: freq,intensity

    newRay_vField_complexVField%waveVector = waveVector
    newRay_vField_complexVField%E = toCoordinateSystem(E,getCoordinateSystem(waveVector))
    newRay_vField_complexVField%frequency = freq
    newRay_vField_complexVField%intensity = intensity
  end function

  !> Find intersection of a ray with the LCFS
  subroutine findLCFSSpot(myRay,myLCFSSpot)
    implicit none
    type(ray),intent(in) :: myRay
    type(vector),intent(OUT) :: myLCFSSpot
    type(vector) :: start_pos,end_pos,wave_vec,pos
    type(VectorField) :: N_cartesian
    real(8) :: t,norm,dt,searchLength
    integer(4) :: it
    logical :: inside
    ! number of steps for the first and second attempt to find LFFS
    integer(4),parameter :: nt1=20,nt2=500
    ! first attempt maximum length is searchLength=2*minor_radius , second attempt extent_t2*searchLength
    real(8),parameter :: extent_t2 = 2.d0
    real(8) :: zeroLCFS_t0, zeroLCFS_t1

    N_cartesian = toCartesian(myRay%waveVector)
    start_pos = getPosition(N_cartesian)
    wave_vec = getField(N_cartesian)
    norm = l2Norm(wave_vec)
    if (norm==0.d0) then
      write(*,*)'findLCFSSpot failed - zero wave vector'
      return
    endif
    wave_vec = wave_vec/norm
    searchLength = 2*getMinorRadius()  ! 2*
    call unSet(end_pos)
    ! look whether we're in/outside LCFS
    if (isInsideLCFS(start_pos)) then
      ! aim backwards
      dt = -1.d0*searchLength/nt1
      inside = .true.
    else
      dt = searchLength/nt1
      inside = .false.
    endif
    ! the first attempt to delimit the spot
    zeroLCFS_t0 = 0.d0
    do it=1,nt1
      pos = start_pos + (dt*it)*wave_vec
      if (inside.neqv.isInsideLCFS(pos)) then
        end_pos = pos
        zeroLCFS_t1 = dt*it
        exit
      else
        zeroLCFS_t0 = dt*it
      endif
    enddo
    ! if the 1st attempt was not succesful
    ! try within longer interval and with finer step
    if (.not.(isSet(end_pos))) then
      if (inside) then
        dt = -extent_t2*searchLength/nt2
      else
        dt = extent_t2*searchLength/nt2
      endif
      do it=1,nt2
        pos = start_pos + (dt*it)*wave_vec
        if (inside.neqv.isInsideLCFS(pos)) then
          end_pos = pos
          zeroLCFS_t1 = dt*it
          exit
        else
          zeroLCFS_t0 = dt*it
        endif
      enddo
    endif
    ! check whether we were succesful
    if (.not.(isSet(end_pos))) then
      ! write(*,*)'findLCFSSpot failed - coud not find LCFS spot'
      call unset(myLCFSSpot)
      return
    endif

    ! use Brent's method to find the exact spot position
    zeroLCFS_start = start_pos
    zeroLCFS_vec = wave_vec
    ! IMSL routine
    ! call DZBREN (zeroLCFS, zeroLCFS_ERRABS, zeroLCFS_ERRREL, zeroLCFS_t0, zeroLCFS_t1, zeroLCFS_MAXFN)
    ! don't use IMSL
    zeroLCFS_t1 = findRoot(zeroLCFS_t0, zeroLCFS_t1,zeroLCFS,zeroLCFS_ERRREL)
    ! convert to the original coordinate system
    myLCFSSpot = toCoordinateSystem(zeroLCFS_start + zeroLCFS_t1*zeroLCFS_vec,getCoordinateSystem(myRay%waveVector))
  end subroutine

  !> Find intersection of a ray with the plasma cutoff
  subroutine findPCOspot(myRay,myPCOspot)
    use density_temperature
    implicit none
    type(ray),intent(in) :: myRay
    type(vector),intent(OUT) :: myPCOspot
    type(vector) :: start_pos,end_pos,wave_vec,pos
    type(VectorField) :: N_cartesian
    real(8) :: t,norm,dt,searchLength,freq
    integer(4) :: it
    logical :: inside
    ! number of steps for the first and second attempt to find LFFS
    integer(4),parameter :: nt1=20,nt2=500
    ! first attempt maximum length is searchLength=2*minor_radius , second attempt extent_t2*searchLength
    real(8),parameter :: extent_t2 = 2.d0
    real(8) :: zeroPCO_t0, zeroPCO_t1

    N_cartesian = toCartesian(myRay%waveVector)
    start_pos = getPosition(N_cartesian)
    wave_vec = getField(N_cartesian)
    norm = l2Norm(wave_vec)
    freq = getFrequency(myRay)
    if (norm==0.d0) then
      write(*,*)'findPCOspot failed - zero wave vector'
      return
    endif
    wave_vec = wave_vec/norm
    searchLength = 2*getMinorRadius()  ! 2*
    call unSet(end_pos)
    ! look whether we're in/outside PCO
    if (isInsidePCO(start_pos,freq)) then
      ! aim backwards
      dt = -1.d0*searchLength/nt1
      inside = .true.
      ! write(*,*)'starting inside plasma cutoff'
    else
      dt = searchLength/nt1
      inside = .false.
      ! write(*,*)'starting outside plasma cutoff'
    endif
    ! the first attempt to delimit the spot
    zeroPCO_t0 = 0.d0
    do it=1,nt1
      pos = start_pos + (dt*it)*wave_vec
      ! write(*,*)(freq-ne2plasfreq(getne(pos)))/1d9
      if (inside.neqv.isInsidePCO(pos,freq)) then
        end_pos = pos
        zeroPCO_t1 = dt*it
        exit
      else
        zeroPCO_t0 = dt*it
      endif
    enddo
    ! if the 1st attempt was not succesful
    ! try within longer interval and with finer step
    if (.not.(isSet(end_pos))) then
      if (inside) then
        dt = -extent_t2*searchLength/nt2
      else
        dt = extent_t2*searchLength/nt2
      endif
      do it=1,nt2
        pos = start_pos + (dt*it)*wave_vec
        if (inside.neqv.isInsidePCO(pos,freq)) then
          end_pos = pos
          zeroPCO_t1 = dt*it
          exit
        else
          zeroPCO_t0 = dt*it
        endif
      enddo
    endif
    ! check whether we were succesful
    if (.not.(isSet(end_pos))) then
      ! write(*,*)'findPCOSpot failed - coud not find PCO spot'
      call unset(myPCOSpot)
      return
    endif

    ! use Brent's method to find the exact spot position
    zeroPCO_start = start_pos
    zeroPCO_vec = wave_vec
    zeroPCO_plasfreq = freq
    ! IMSL routine
    ! call DZBREN (zeroPCO, zeroPCO_ERRABS, zeroPCO_ERRREL, zeroPCO_t0, zeroPCO_t1, zeroPCO_MAXFN)
    ! don't use IMSL
    zeroPCO_t1 = findRoot(zeroPCO_t0, zeroPCO_t1,zeroPCO,zeroPCO_ERRREL)
    ! convert to the original coordinate system
    myPCOSpot = toCoordinateSystem(zeroPCO_start + zeroPCO_t1*zeroPCO_vec,getCoordinateSystem(myRay%waveVector))
  end subroutine

  real(8) function zeroLCFS(t)
    implicit none
    real(8),intent(in) :: t

    ! psi2 = getPsi(zeroLCFS_start+t*zeroLCFS_vec)
    zeroLCFS = getPsiLCFS() - getPsi(zeroLCFS_start+t*zeroLCFS_vec)
  end function

  real(8) function zeroPCO(t)
    use density_temperature
    real(8),intent(in) :: t

    ! psi2 = getPsi(zeroPCO_start+t*zeroPCO_vec)
    zeroPCO = -zeroPCO_plasfreq + ne2plasfreq(getne(zeroPCO_start+t*zeroPCO_vec))
  end function

  integer(4) function getNumberOfRays_Beam(myBeam)
    implicit none
    type(Beam),intent(in) :: myBeam

    getNumberOfRays_Beam = myBeam%no_of_rays
  end function
  
  integer(4) function getNumberOfRays_GaussianBeam(myBeam)
    implicit none
    type(GaussianBeam),intent(in) :: myBeam

    getNumberOfRays_GaussianBeam = myBeam%no_of_rays
  end function

  function complementaryPolarization_gBeam(gaussBeam)
    implicit none
    type(gaussianBeam) :: complementaryPolarization_gBeam
    type(gaussianBeam),intent(in) :: gaussBeam
    type(complexVector) :: E,E2
    type(VectorField) :: N_vField
    type(ComplexVectorField) :: E2_vField
    type(ray) :: centralRay
    type(vector) :: pos

    E = getField(toCartesian(getEVectorField(getCentralRay(gaussBeam))))
    N_vField = toCartesian(getWaveVectorField(getCentralRay(gaussBeam)))
    pos = getPosition(toCartesian(getWaveVectorField(getCentralRay(gaussBeam))))
    E2 = crossProduct(E,getField(N_vField)/l2norm(getField(N_vField)))
    E2_vField = toCoordinateSystem(newComplexVectorField(pos,E2),getCoordinateSystem(getCentralRay(gaussBeam)))
    centralRay = getCentralRay(gaussBeam)
    centralRay%E = E2_vField
    complementaryPolarization_gBeam = newGaussianBeam( &
      centralRay, &
      gaussBeam%waistRadius,gaussBeam%noOfCircles,gaussBeam%raysPerCircle,gaussBeam%power,gaussBeam%fixDivergence,gaussBeam%divergence,gaussBeam%circleShift, &
      gaussBeam%assymetry,gaussBeam%radiusEnlargement,gaussBeam%width)
  end function

  subroutine dumpRay(r)
    implicit none
    type(ray),intent(in) :: r

    write(*,'(''Ray frequency: '',E10.3)')r%frequency
    write(*,'(''Ray intensity: '',E10.3)')r%intensity
    write(*,'(''Ray power    : '',E10.3)')r%power
    write(*,'(''Position'')')
    call dump(getPosition(r%waveVector))
    write(*,'(''Wave vector'')')
    call dump(getField(r%waveVector))
  end subroutine

  subroutine dumpBeam(myBeam)
    implicit none
    type(Beam),intent(in) :: myBeam
    integer(4) :: iray

    write(*,'(''Number of rays in the beam '',I2)')myBeam%no_of_rays
    do iray=1,myBeam%no_of_rays
      write(*,'(''Ray '',I2,'':'')')iray
      call dump(myBeam%beamRays(iray))
    enddo
  end subroutine

  real(8) function signum(x)
    implicit none
    real(8),intent(in) :: x

    signum = sign(1.d0,x)
  end function

  subroutine dumpGaussianBeam(myBeam)
    implicit none
    type(GaussianBeam),intent(in) :: myBeam

    write(*,'(''Power = '',E10.3)') myBeam%power
    write(*,'(''Number of rays = '',I3)') myBeam%no_of_rays
    write(*,'(''Waist radius = '',E10.3)') myBeam%waistRadius
    write(*,'(''No. of circles = '',I3)') myBeam%noOfCircles
    write(*,'(''Rays per circle = '',I3)') myBeam%raysPerCircle
    write(*,'(''Central ray:'')')
    call dump(myBeam%waistCentralRay)
  end subroutine

  subroutine dumpToFileBeam(myBeam,filename)
    use Utilities
    implicit none
    type(Beam),intent(in) :: myBeam
    character(len=*),intent(in) :: filename
    integer(4) :: iunit,iray
    type(Ray) :: myRay
    type(VectorField) :: k
    type(ComplexVectorField) :: E

    iunit = getFreeUnitNumber()
    open(iunit,file=trim(filename))
    write(iunit,'(''#x,y,z,R,phi,Z,ray,freq,intensity,power,kx,ky,kz,kR,kphi,kZ,Re(Ex),Im(Ex),Re(Ey),Im(Ey),Re(Ez),Im(Ez)'')')
    do iray=1,getNumberOfRays(myBeam)
      myRay = getRay(myBeam,iray)
      k = getWaveVectorField(myRay)
      E = getEVectorField(myRay)
      write(iunit,'((SP, E12.5),5(1X,SP,E12.5),I4,50(1X,SP,E12.5))')toCartesianArray(getPosition(toCartesian(k))),toCylindricalArray(getPosition(toCylindrical(k))), &
        iray,getFrequency(myRay),getIntensity(myRay), getPower(myRay), &
        toCartesianArray(getField(toCartesian(k))),toCylindricalArray(getField(toCylindrical(k))), &
        toCartesianArray(getField(toCartesian(E)))
    enddo
    close(iunit)
  end subroutine
  
  subroutine dumpToFileGaussianBeam(myBeam,filename)
    implicit none
    type(GaussianBeam),intent(in) :: myBeam
    character(len=*),intent(in) :: filename

  end subroutine

end module
