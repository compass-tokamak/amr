!> General physical and mathematical constants
module generalConstants



  !> Ludolph's number \f$ \pi \f$
  real(8),parameter ::     Pi = 3.141592653589793238d0
  !> square root of Ludolph's number \f$ \sqrt{\pi} \f$
  real(8),parameter :: sqrtPi = 1.772453850905516027d0
  !> 2 x Ludolph's number \f$ 2\pi \f$
  real(8),parameter :: TWOPI = 6.283185307179586476925d0
  !> Speed of light in cm/s
  real(8),parameter :: vc_cm_s = 2.99792458d10
  !> Speed of light in m/s
  real(8),parameter :: vc_m_s = 2.99792458d8
  !> Conversion from keV to erg
  real(8),parameter :: ckev_erg = 1.6022d-9
  !> Proton mass in grams
  real(8),parameter :: massp_g = 1.6726485d-24
  !> Electron mass in grams
  real(8),parameter :: masse_g = 9.109534d-28
  !> Electron charge in statcoul
  real(8),parameter :: Ze_statcoul = 4.803242d-10
  !> Electron charge in coulombs
  real(8),parameter :: Ze_SI = 1.6021764624033393558d-19
  !> conversion constant for Hansen current formula 
  !!
  !! (3.e5*1.e4)J[A/cm^2]=e/m*(10*1000)*P_den[kW/m^3]  J[A/cm^2]=P[kw]/(J_const*Vol[m^3])
  real(8),parameter :: J_const = masse_g*3.0d5/Ze_statcoul

  !> Constant Coulomb logarithm, used in Ginzburg collision frequency formula
  real(8),parameter :: C_log = 15.d0
  !> nue_GR_const = (4.0d0*C_log*Ze_statcoul**4*1.0d13/(3.0d0*ckev_erg))*sqrt(2.0d0*pi/(masse_g*ckev_erg)).
  !! nue_ei = nue_GR_const * Zeff * ne[10^19m-3] * Te[keV]^(-3/2)
  real(8),parameter :: nue_GR_const = (4.0d0*C_log*Ze_statcoul**4*1.0d13/(3.0d0*ckev_erg))*2074834639314117309.14516363300665897d0
  !> sqrt(2)
  real(8),parameter :: sqrt2 = 1.4142135623730950488d0
  !> permittivity of free space (SI units)
  real(8),parameter :: epsilon0 = 8.85418781762038985d-12
  !> Constant in McGregor (Lin-Liu) current drive efficiency formula
  !!
  !! LLCDE = \f$ {e^3} / {2\pi\epsilon_0^2} / {1000Z_{\rm e}}\f$.
  !! Contains conversion factors Te: keV -> J, ne: 10^19m-3 -> to m-3, J/Pd: Am-2/Wm-3 -> Acm-2/kWm-3
  real(8),parameter :: LLCDE = 2*Pi*epsilon0**2/(Ze_SI**3) * (1.d3*Ze_SI) / 1.d19 * 1.d-1

  complex(8),parameter :: Wfunc0 = (0.0d0,1.772453850905516027d0)

end module
