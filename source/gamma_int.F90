!> Power deposition at actual ray position.
!!

module gamma_int

  use disp_consts
  use density_temperature
  use besselfun
  use vectors
  use generalConstants
  use LLdrive

  implicit none

  real(8) :: t_old,gamma_old,int_gamma,T_rad,T_rad_intold,Summed_DamFac_int
  logical :: Test_end,Test_end_eff
  complex(8) :: D_coll_GR_sum,D_coll_GR_D1
  real(8) :: gamma_old_coll,int_gamma_coll,D_f_old,Damping_factor ! D_f_old_coll
  real(8) :: dP_total,dP_coll

  ! (2,0:NmaxDc)
  !> \f$ \gamma _{i,n}  =  - 2{\mathop{\rm Im}\nolimits} D_{i,n} \frac{{\partial {\mathop{\rm Re}\nolimits} D}}{{\partial \omega }} \f$.
  !! i - component index, n - harmonic index.
  real(8),allocatable :: gamma_partial_old(:,:)
  !> \f$ \Delta P _{i,n} = \int_{t_{\rm old} }^{t_{\rm new} } { \gamma _{i,n} P_{\rm total}{\rm d}t} \f$ - power deposited to i-th component and n-th harmonic.
  real(8),allocatable :: Damfac_partial(:,:)
  !> \f$ \Delta P _{i,1} \f$ - power deposited to lower harmonics, i.e. \f$ n\omega _{ce}  < \omega \f$.
  !! \f$ \Delta P _{i,2} \f$ - power deposited to higher harmonics, i.e. \f$ n\omega _{ce}  > \omega \f$. 
  real(8),allocatable :: Sumed_Damfac(:,:)
  !> \f$ D_{i,n} \f$ - dispersion function for i-th component and n-th harmonic.
  complex(8),allocatable :: D_parts(:,:)
  !> Current drive efficiency \f$ {j}/{P_d} \f$ for individual components 
  real(8),allocatable :: current_rjpd(:)
  !> Curba rjpd0 output for individual components
  !!
  !! Non-relativistic current-drive efficiency in limit of no
  !! trapped particles, at vparallel=min on relativistic resonance curve
  real(8),allocatable :: curba_rjpd0(:)
  !> Curba denom output for individual components
  !!
  !! denom is normalized absorbed power (denominator in rjpd)
  real(8),allocatable :: curba_denom(:)

  
  private :: RD_cB_oblique_partial,DispersionRelation_D_partial

  contains

  !> Zero all variables. Must be called before each ray tracing (for a single ray).
  subroutine initIntegration()
    t_old=0.0d0
    gamma_old=0.0d0
    gamma_old_coll=0.0d0
    gamma_partial_old=0
    int_gamma=0.0d0
    int_gamma_coll=0.0d0
    T_rad=0.0d0
    T_rad_intold=0.0d0
    Test_end=.false.
    Test_end_eff=.false.
    Summed_DamFac_int=0.0d0
    D_f_old=1
    !D_f_old_coll=1
  end subroutine

  !> Allocate gamma_int variables. Must be called once when the program starts. 
  subroutine allocate_gamma_int(ncomp)

    integer,intent(in) :: ncomp
    
    if (allocated(gamma_partial_old)) then
      call deallocate_gamma_int
    endif
    allocate (gamma_partial_old(ncomp,0:NmaxDc-2),Damfac_partial(ncomp,0:NmaxDc-2),Sumed_Damfac(ncomp,2))
    allocate (D_parts(ncomp,0:NmaxDc-2))
    allocate (current_rjpd(ncomp),curba_rjpd0(ncomp),curba_denom(ncomp))

  end subroutine

  !> Deallocate gamma_int variables. Not necessary as they are allocated only once.
  subroutine deallocate_gamma_int()
    
    deallocate (gamma_partial_old,Damfac_partial,Sumed_Damfac)
    deallocate (D_parts)
    deallocate (current_rjpd,curba_rjpd0,curba_denom)
  end subroutine

  !-----------------------------------------------------------------------
  ! subroutine Absorbed_power_partial(t,RDDisp_domega,D_f_old,Damping_factor,Damfac_partial,Sumed_Damfac)
  !> Calculate partial absorbed power and store into Damfac_partial and Sumed_Damfac.
  !!
  !! \param RDDisp_domega = \f$ \left( {{\rm d} \Re D} / {{\rm d} \omega } \right) ^{-1} \f$
  subroutine Absorbed_power_partial(t,R,phi,Z,Npar,Nperp,RDDisp_domega)
  use disp_consts
  use shot_parameters
  use FavouredCombinations_rt
  use equilibrium
  use density_temperature
  use collisionFrequency
  implicit none
  integer ic,N,n_EC
  real(8),intent(in) :: RDDisp_domega,t,R,phi,Z,Npar,Nperp
  ! real(8),intent(out):: Damfac_partial(2,0:NmaxDc),Sumed_Damfac(2,2)
  real(8) :: t_new,gamma_new
  real(8) :: psi, Tc, rjpd, rjpd0, ratjpd, denom, theta, aspct, Bmin, Bmax

  real(8) :: Te,netot,vt_vc_h,v_res_vc,coll_freq,nue_ei_elastic,nue_ei_nonelastic,nue_en,err_factor
  type(vector) :: pos
  complex(8) :: Ex,Ey

  call DispersionRelation_D_partial(R,phi,Z,Npar,Nperp)
  psi = getPsi(R,phi,Z)

  Sumed_Damfac=0.0d0
  t_new=t
  netot=getne(R,phi,Z)
  select case (current_density_type)
  case(currentDensityCurba)
    call getBminMax(psi,Bmin,Bmax)
    if (ABS (Bmax-Bmin) .lt. 1.0d-08) Bmax = Bmin + SIGN (1.0D-08,Bmax)
    aspct = (Bmax - Bmin) / (Bmax + Bmin)
    theta = atan2(Z,R)
  case(currentDensityLL)
    pos = cylindricalVector(R,phi,Z)
    Ex = dcmplx(0.7071067811865475244d0*nperp/sqrt(nperp*nperp+npar*npar))
    Ey = Ex
  endselect
  Do ic=1,n_components
    Do N=0,NmaxDc-2
      gamma_new=-2.0d0*dimag(D_parts(ic,N))*RDDisp_Domega
      Damfac_partial(ic,N)=-(gamma_new*Damping_factor+gamma_partial_old(ic,N)*D_f_old)*(t_new-t_old)*0.5d0
      !Damfac_partial(ic,N)=max(0.d0,-(gamma_new*Damping_factor+gamma_partial_old(ic,N)*D_f_old)*(t_new-t_old)/2)
      gamma_partial_old(ic,N)=gamma_new
    enddo
  enddo

  do ic=1,n_components
    Sumed_Damfac(ic,1) = 0
    do n_EC = 0,int(1/omcyc_omega)
      Sumed_Damfac(ic,1)=Sumed_Damfac(ic,1)+Damfac_partial(ic,n_EC)
    enddo
    Sumed_Damfac(ic,2) = 0
    do n_EC = int(1/omcyc_omega)+1,NmaxDc-2
      Sumed_Damfac(ic,2)=Sumed_Damfac(ic,2)+Damfac_partial(ic,n_EC)
    enddo
  enddo

  ! correct the (accumulated) error on total damping
  if ((dP_total)>0) then
    err_factor = (sum(Sumed_Damfac)+dP_coll)/dP_total
    if (err_factor<1d-10) then
      write(getProtocolUnit(),*)'err_factor=',err_factor,' dP=',dP_total
      write(getProtocolUnit(),*)'reset to 1.0'
      err_factor=1
    endif
    Damfac_partial = Damfac_partial/err_factor
    Sumed_Damfac = Sumed_Damfac/err_factor
    dP_coll = dP_coll/err_factor
    if (abs(err_factor-1)>1d-3) then
      write(getProtocolUnit(),*)'err_factor=',err_factor,' dP=',dP_total
    endif
  else
    Damfac_partial = 0
    Sumed_Damfac = 0
    dP_coll = 0
  endif


  Do ic=1,n_components
    select case (current_density_type)
    case(currentDensityCurba)
      tc = getTe(psi,ic)
      curba_rjpd0(ic) = 0
      curba_denom(ic) = 0
    case(currentDensityHansen)
      Te=getTe(R,phi,Z,ic)
      vt_vc_h=sqrt(2.0d0*ckev_erg*Te/masse_g)/vc_cm_s
    case(currentDensityLL)
      Te=getTe(psi,ic)
    end select
    current_rjpd(ic) = 0
    Do N=0,NmaxDc-2
      ! THESE CONDITIONS ARE MORE OR LESS EMPIRICAL
      ! if (Damfac_partial(ic,N) > 1d-10 .and. psi<getPsiLCFS()) then
      ! if (gamma_new < -1d-8 .and. psi<getPsiLCFS()) then
      if (Damfac_partial(ic,N)/(t_new-t_old)>1d3 .and. psi<getPsiLCFS()) then
        ! write(*,*)Damfac_partial(ic,N)
        ! write(*,*)Damfac_partial(ic,N)/(t_new-t_old)
        select case (current_density_type)

        !-- current density Hansen --
        case(currentDensityHansen)
          v_res_vc=(1.0d0-dfloat(N)*omcyc_omega)/Npar  ! (omega-nomega_ce)/(kz*c)
          call getCollisionFrequency(netot,Te,Zeff,nue_ei_elastic,nue_ei_nonelastic,nue_en)
          coll_freq = nue_ei_elastic+nue_ei_nonelastic+nue_en
          ! J_partial = J/P_d = current drive efficiency
          current_rjpd(ic) = current_rjpd(ic) - (v_res_vc/vt_vc_h)**2*sign(1.0d0,v_res_vc)*Damfac_partial(ic,N)&
                             /(vt_vc_h*vc_cm_s*coll_freq)

        !-- current density CURBA --
        case(currentDensityCurba)
          call TorGA_curba (rjpd, rjpd0, ratjpd, denom, &
            aspct, Npar, tc, 1, theta, N*omcyc_omega, N, Zeff, 3, 1.d-3, 10, 1)
          current_rjpd(ic) = current_rjpd(ic) + rjpd*Damfac_partial(ic,N)
          curba_rjpd0(ic) = curba_rjpd0(ic) + rjpd0*Damfac_partial(ic,N)
          curba_denom(ic) = curba_denom(ic) + denom*Damfac_partial(ic,N)
        case(currentDensityLL)
          call LLjdrive(rjpd,pos,Zeff,Nperp,Npar,Ex,Ey,N,N,omcyc_omega)
          current_rjpd(ic) = current_rjpd(ic) + rjpd*Damfac_partial(ic,N)*LLCDE*Te/netot
        end select
      endif
    enddo
  enddo

  end subroutine

  !-----------------------------------------------------------------------
  !> Dispersion function for a single plasma component
  !!
  !! This function is used for partial power deposition computations. Ouput is in D_parts(icomponent,n),
  !! n = harmonic number.
  !! \param RCZ \f$ = k_ \bot ^2 \rho _{\rm L}^{\rm 2} \f$
  !! \param icomponent plasma component index
  !! \param RD_cB_oblique_partial Re(D)/Aparam, where Aparam=2*omplas2/(k*vt)**2
  real(8) Function RD_cB_oblique_partial(RCZ,icomponent)

    use FavouredCombinations_rt
    use Constants
    use disp_consts
    use alphaphi_jd
    use EBWDispSaveliev
    use EBWDispDecker

  ! Real part o Dispersion relation for Bernstein waves propagating obliquely to B
  ! ELECTROSTATIC APPROXIMATION  REAL CASE   D_cB_oblique and RCZ are real
  ! With collisions!!!
    implicit none

    real(8) :: Zarg,kzvt

    integer,intent(in) :: icomponent

    integer N
    Real*8 Npar,BSI(NmaxDc),RCZ,AZarg,Npol,Zpol,&
    Aparam,Psiloc
    complex*16 Wn,Wn_p,WARG_C,&
    Wn_m,D_coll_GR,&
    D_coll_GR1,Wargplus_c,Wargminus_c,D_coll_GR_N       
    Real*8 denRZ,TempRZ,Nperp,Btot,denRZ_Z,denRZ_R,TempRZ_Z,&
    TempRZ_R,Btot_z,Btot_R,Ntot2,&
    DenRZ_phi,TempRZ_phi,Btot_phi

    real(8) :: pn,beta_T,ImD_rel_N
    real(8) :: ImD_Saveliev,sumD
    complex(8) :: epolp,epolz
    real(8) :: ImD_FR_N

#ifdef _IMSL
    complex(8),external :: ZERFE
#else
    integer :: IFAIL,NCALC
    complex(8),external :: S15DDF
#endif


  ! input
    common/Npar/Npar,Npol,Zpol,Nperp
    common/plasma_param/DenRz,TempRz,Btot,DenRZ_R,DenRZ_Z,TempRZ_R,TempRz_Z,Btot_R,Btot_Z,Psiloc,DenRZ_phi,TempRZ_phi,Btot_phi

  ! output

  ! unused
  ! Nharm,www,ID_Coll
  ! Npol,Zpol

  ! calls : init_ray, output (Dtest = ), REFINE=1 (2x), RHSEBW

  !	common/D_Domega/D_c_Cparam,D_c_Dparam
  !
  ! The electrostatic dielectric constant D_c for Benstein waves depend 
  ! on four parameters A,B,C,D
  !
  ! D_c=1+A*{1+Suma(-NmaxDc,NmaxDc){i*sqrt(Pi)*C*W[C*(1-n*D)]*exp(-B)*I(n,B)}}
  !
  ! Where W(x)=exp(-x**2)*[1+(2i/sqrt(Pi))*Int(0,x){exp(x**2)dx}]
  ! dW/dx=2i/sqrt(Pi)-2*x*W, 
  ! for large |x| W=(i/(x*sqrt(Pi)))*[1+1/(2*x**2)+..] +exp(-x**2)
  !
  ! for x=C*(1-n*D) |x|>=100 we have 
  ! i*sqrt(Pi)*C*W[C*(1-n*D)]=(-1/(1-n*D))*F(C*(1-n*D)) 
  ! If  U=1/(2*x**2) 
  ! F(x)=1+U*(1+3*U*(1+5*U*(1+7*U)))
  ! dF/dx=-(1/x**3)*(1+6*U*(1+7.5*U*(1+9.33333*U)
  !
  ! Aparam=2*omplas2/(k*vt)**2, k**2=k_par**2+k_perp**2
  ! Bparam=(k_perp*vt)**2/(2*omcyc2) where omcyc2=omcyc**2, omplas2=omplas**2
  ! Cparam=omega/(abs(k_par)*vt)    where vt=sqrt(2*Te/me)
  ! Dparam=omcyc_omega      where omcyc_omega=omcyc/omega
  ! D_c_Aparam=d(D_c)/dAparam etc
  !
  ! For k_par=0 (identity 1-e(-x)I0(x)-2e(-x)I1(x)-2e(-x)I2(x)-...=0!!!)
  !
  ! D_c=1-2*A*D**2*{Suma(1,NmaxDc){exp(-B)*I(n,B)*n**2/[1-(nD)**2]}
  !
    500 FORMAT(34(SP,1PE11.3))
    501 FORMAT(I3,34(SP,1PE11.3))
    502 FORMAT(A3,I3,34(SP,1PE15.7))
    503 FORMAT(A3,34(SP,1PE15.7))
  !
    Zarg=RCZ    !  Zarg=Zarg0_old*Nperp**2
    Ntot2=(Zarg/Zarg0)+Npar**2
    epolp = dcmplx(0.7071067811865475244d0*nperp/sqrt(Ntot2))
    epolz = dcmplx(npar/sqrt(Ntot2))


    kzvt=abs(Npar)*vt_vc ! kz*vt/omega=Nz*vt/v_vac
    if(abs(Zarg).ge.1.0d0) then
      AZarg=abs(Zarg) 
      call BesselIES(AZarg,NmaxDc,BSI)
      if(Zarg.lt.0.0d0) then ! DBSIES do not work for negative arguments 
        do N=1,NmaxDc
          BSI(N)=dfloat((-1)**(N-1))*BSI(N)
        enddo
        BSI=exp(-2.0d0*Zarg)*BSI
      endif	 
    else
#ifdef _IMSL
      CALL DBSINS (Zarg, NmaxDC, BSI)
#else
      call RIBESL(Zarg, 0.d0, NmaxDc, 1, BSI, NCALC)
#endif
      BSI=exp(-Zarg)*BSI
    endif
    Aparam=2.0d0*omplas_omega_2/(Ntot2*vt_vc**2)
    Warg_c=1.0d0/kzvt
#ifdef _IMSL
    Wn=Wfunc0*ZERFE(Warg_c)            ! Wfunc0=i*sqrt(Pi) 
#else
    ifail = 0
    Wn=Wfunc0*S15DDF(Warg_c,ifail)            ! Wfunc0=i*sqrt(Pi) 
#endif
    D_coll_GR=1.0d0+Warg_c*Wn*BSI(1) 
    D_coll_GR1=1
    D_parts(icomponent,0)=Aparam*D_coll_GR
    select case (resonant_damping_model)
    case (resonant_damping_WR,resonant_damping_FR,resonant_damping_Saveliev,resonant_damping_WR_NC)
      ! variables for Joan Decker's weakly-relativistic damping
      beta_T = vt_vc/sqrt2
      ! pn0 = 1.d0/(beta_T*Npar)
      ! non-relativistic
      ! imD_rel = BSI(1)/RCZ*exp(-(1.d0/kzvt)**2)
      ! pn = pn0
      ! wekly-relativistic
      ! pn = pn0 * (1 + abs(beta_T*pn0/(2*Npar)))
      ! imD_rel = BSI(1)/RCZ*exp(-0.5d0*pn*pn)
    end select
    do N=1,NmaxDc-2	
      Wargplus_c=((1.d0,0.d0)+dfloat(n)*omcyc_omega)/kzvt 
      Wargminus_c=((1.d0,0.d0)-dfloat(n)*omcyc_omega)/kzvt
#ifdef _IMSL
      Wn_p=Wfunc0*ZERFE(Wargplus_c)
      Wn_m=Wfunc0*ZERFE(Wargminus_c)
#else
      ifail = 0
      Wn_p=Wfunc0*S15DDF(Wargplus_c,ifail)
      ifail = 0
      Wn_m=Wfunc0*S15DDF(Wargminus_c,ifail)
#endif
    ! if(n.eq.2)www=Warg_c/Wargminus_c
      D_coll_GR_N=Warg_c*(Wn_p+Wn_m)*BSI(N+1)
      D_coll_GR=D_coll_GR+D_coll_GR_N
      ! D_coll_GR1=D_coll_GR1+(omega_c_omega-1.0d0)/kzvt*(Wn_p+Wn_m)*BSI(N+1)

      select case (resonant_damping_model)
      case (resonant_damping_NR)
  	    D_parts(icomponent,N)=Aparam*D_coll_GR_N  ! if(N.le.3)
      case (resonant_damping_WR,resonant_damping_Saveliev,resonant_damping_FR,resonant_damping_WR_NC)
        ! Weakly-relativistic damping
        ! 1 + n*w_ce/w
!        pn0 = (1.d0+n*omcyc_omega)/(beta_T*Npar)
        ! non-relativistic
        ! pn = pn0
        ! wekly-relativistic:
!        pn = pn0 * (1 + beta_T*pn0/(2*Npar))
!        imD_rel_N = exp(-0.5d0*pn*pn)
        ! 1 - n*w_ce/w
!        pn0 = (1.d0-n*omcyc_omega)/(beta_T*Npar)
        ! pn = pn0
!        pn = pn0 * (1 + beta_T*pn0/(2*Npar))
!        imD_rel_N = imD_rel_factor * BSI(n+1)/RCZ * (imD_rel_N + exp(-0.5d0*pn*pn))

        if (resonant_damping_model==resonant_damping_WR_NC) then
          imD_rel_N = ImEBWDispJDWeak(omplas_omega_2,omcyc_omega,beta_T,npar,RCZ,n,BSI(n+1),pn)
        else
          imD_rel_N = ImEBWDispJDWeak_curv(omplas_omega_2,omcyc_omega,beta_T,npar,RCZ,n,BSI(n+1),BSI(n+2),pn) ! + &
        endif
        ! neglecting -n harmonics
        !            ImEBWDispJDWeak_curv(omplas_omega_2,omcyc_omega,beta_T,npar,RCZ,-n,BSI(n+1),BSI(n+2),pn)
        ! imD_rel = imD_rel + imD_rel_N
        ! imD_rel = imD_rel + BSI(n+1)/RCZ * ( exp(-((1.d0-n*omcyc_omega)/kzvt)**2) + exp(-((1.d0+n*omcyc_omega)/kzvt)**2) )   	        
  	    D_parts(icomponent,N) = real(Aparam*D_coll_GR_N) + (0.d0,1.d0)*imD_rel_N
        
        if (resonant_damping_model==resonant_damping_FR) then
          if (abs(pn)<pn_FR_limit) then
            imD_FR_N = alphaphi(epolp,epolp,epolz,omplas_omega_2,omcyc_omega,beta_T,npar,nperp,n)
          else
            imD_FR_N = 0
          endif
  	      D_parts(icomponent,N) = real(Aparam*D_coll_GR_N) + (0.d0,1.d0)*imD_FR_N
        endif
      end select

    enddo

    select case (resonant_damping_model)
    case (resonant_damping_Saveliev)
      sumD = sum(dimag(D_parts(icomponent,:NmaxDc-2)))
      ! D_parts calculated before by Decker's formula, used for distribution between harmonics
      if (sumD>saveliev_limit) then
      !if (sumD>0 .and. pn_min<pn_FR_limit) then
        ! total ImD, this formula cannot separate harmonics
        ImD_Saveliev = ImEBWDispSaveliev(omplas_omega_2,omcyc_omega,beta_T,npar,nperp)
!      endif
!      if (ImD_Saveliev>1d-99) then
        D_parts(icomponent,:NmaxDc-2) = ImD_Saveliev/sumD*D_parts(icomponent,:NmaxDc-2)
      endif
    end select

    ! do N=0,NmaxDc-2
       ! D_parts(ic,N)=D_parts(ic,N)/D_coll_GR1
    !   Sum_D_parts=Sum_D_parts+D_parts(ic,N)
    ! enddo
    ! D_parts(ic,NmaxDc-1)= Sum_D_parts! D_coll_GR1
    ! D_coll_GR=D_coll_GR/D_coll_GR1
    ! D_coll_GR=1.0d0+Aparam*D_coll_GR  !dielectric constant,G.R.75,p.112,(7.23)
    ! D_parts(ic,NmaxDc)=D_coll_GR

    RD_cB_oblique_partial = real(D_coll_GR)

  end function

  !> Call RD_cB_oblique_partial for each plasma component
  subroutine DispersionRelation_D_partial(R,phi,Z,Npar,Nperp)
    use Constants
    use FavouredCombinations_rt
    use Equilibrium
    use shot_parameters
    use local_param
    implicit none
    real(8),intent(in) :: R,phi,Z,Npar,Nperp
    integer(4) :: icomp
    real(8) :: Dtest_total
    real(8) :: RCZ,Dtest
    real(8) :: DenRz,denRZ_total,TempRz,Btot,DenRZ_R,DenRZ_Z,TempRZ_R,&
               TempRz_Z,Btot_R,Btot_Z,Psiloc,DenRZ_phi,TempRZ_phi,Btot_phi
    real(8) :: Npar_com,Npol,Zpol,Nperp_com
    real(8) :: RdDdO,dDdKk_Kk,dDdKp,dDdR,dDdZ,dDdphi
    real(8) :: Br,Br_R,Br_Z,Br_phi,Bz,Bz_R,Bz_Z,Bz_phi,Btor,Btor_R,Btor_Z,Btor_phi
    real(8) :: ID_c,ID_c_total

    real(8),external :: RD_cB_oblique_rt

    ! input for RD_cB_oblique_rt: plazma_param,Npar (computed here, Npol,Zpol not used)
    common/plasma_param/DenRz,TempRz,Btot,DenRZ_R,DenRZ_Z,TempRZ_R,&
                        TempRz_Z,Btot_R,Btot_Z,Psiloc,DenRZ_phi,TempRZ_phi,Btot_phi
    common/Npar/Npar_com,Npol,Zpol,Nperp_com
    ! Disp_output computed in RD_cB_oblique_rt
    ! -> used here
    ! -> used in subroutine output
    common/Disp_output/RdDdO,dDdKk_Kk,dDdKp,dDdR,dDdZ,dDdphi
    ! ID_c computed in RD_cB_oblique_rt, used in OUTPUT for damping
    common/ID_c/ID_c
    common/RCZ/RCZ

    ! put input to common values
    Npar_com = Npar
    Nperp_com= Nperp

    Dtest_total    = 0.d0
    ID_c_total     = 0.d0
    denRZ_total = getne(R,phi,Z)
    call Local_param3(R,Z,phi,psiloc, &
      denRZ,denRZ_Z,denRZ_R,denRZ_phi, &
      TempRZ,TempRZ_Z,TempRZ_R,TempRZ_phi, &
      Br,Br_R,Br_Z,Br_phi, &
      Bz,Bz_R,Bz_Z,Bz_phi, &
      Btor,Btor_R,Btor_Z,Btor_phi, &
      Btot,Btot_R,Btot_Z,Btot_phi,1)
    call initFavouredCombinations_rt(denRZ,denRZ_total,TempRZ,Btot)

    ! Kperp  = dsqrt(Kperp2)
    ! Nperp  = kperp/kvacSI
    ! RCZ    = Zarg0_new * Kperp2
    RCZ    = Zarg0_new * (Nperp*kvacSI)**2
    Dtest  = RD_cB_oblique_partial(RCZ,1)
  
    Dtest_total    = Dtest
    ! pozor tohle je 1/(dD/domega)
    ID_c_total = ID_c

    do icomp=2,n_components
      call Local_param3(R,Z,phi,psiloc, &
        denRZ,denRZ_Z,denRZ_R,denRZ_phi, &
        TempRZ,TempRZ_Z,TempRZ_R,TempRZ_phi, &
        Br,Br_R,Br_Z,Br_phi, &
        Bz,Bz_R,Bz_Z,Bz_phi, &
        Btor,Btor_R,Btor_Z,Btor_phi, &
        Btot,Btot_R,Btot_Z,Btot_phi,icomp)
        call initFavouredCombinations_rt(denRZ,denRZ_total,TempRZ,Btot)
        RCZ    = Zarg0_new * (Nperp*kvacSI)**2
        Dtest  = RD_cB_oblique_partial(RCZ,icomp)
        ! +1 je na zacatku
        Dtest_total    = Dtest_total    + (Dtest - 1.0d0)

    enddo ! icomp
    ! celkove hodnoty do puvodnich promennych
    Dtest    = Dtest_total

  end subroutine

end module
