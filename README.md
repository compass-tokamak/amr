# README #

AMR = Antenna, Mode-conversion, Ray-tracing is a simulation code for electron Bernstein waves (EBW) in tokamaks and stellarators.

See:

- Urban, Jakub, et al. "A survey of electron Bernstein wave heating and current drive potential for spherical tokamaks." Nuclear Fusion 51.8 (2011): 083050.
- Urban, Jakub, and Josef Preinhaelter. "Adaptive finite elements for a set of second-order ODEs." Journal of plasma physics 72.06 (2006): 1041-1044.