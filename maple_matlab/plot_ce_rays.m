% indexes in the output files
freqindex = 7;
toraindex = 4;
polaindex = 5;
ceindex = 11;

% refine mesh meshfactor-times for interpolation
meshfactor = 8;
% number of countours to plot
% ncontours = 8;
% maximum contour value to plot
maxcontour = 0.7;
vcontours = 0:0.1:maxcontour;

% open and read data file
fid = fopen('results_ray.dat', 'r');
filedata = textscan(fid, '%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f', 'headerlines', 1);
fclose(fid);
alldata = zeros(length(filedata{1}),length(filedata));
for i=1:size(alldata,2)
    alldata(:,i) = filedata{i};
end
clear('filedata');

% tora = toroidal angles
% pola = poloidal angles
% freq = frequencies
tora = unique(alldata(:,toraindex));
pola = unique(alldata(:,polaindex));
freq = unique(alldata(:,freqindex));
nfreq = length(freq);
ntora = length(tora);
npola = length(pola);

% construct conversion efficiencies matrix
cedata = zeros(nfreq,npola,ntora);
for fi=1:nfreq
    for i=1:ntora
        for j=1:npola
            cedata(fi,j,i) = alldata(find( alldata(:,toraindex)==tora(i) & alldata(:,polaindex)==pola(j) & ...
                alldata(:,freqindex)==freq(fi) ),ceindex);
        end
    end
end

scrsz = get(0,'ScreenSize');
fig1 = figure('Position',[10 10 scrsz(3)-10 scrsz(4)-100]);
% construct meshes
[X,Y]=meshgrid(tora,pola);
[XI,YI]=meshgrid(tora(1):(tora(length(tora))-tora(1))/(ntora*meshfactor):tora(length(tora)),...
                 pola(1):(pola(length(pola))-pola(1))/(npola*meshfactor):pola(length(pola)));
opt_angles = zeros(nfreq,2);
for fi=1:nfreq
    subplot(2,ceil(nfreq/2),fi)
    % interpolate data
    ZI = interp2(X,Y,squeeze(cedata(fi,:,:)),XI,YI,'cubic');
    % contourf(X,Y,cedata',ncontours);
    [C,h,CF] = contourf(XI,YI,ZI,vcontours);
    % set equal color scaling
    caxis([0 maxcontour]);
    axis equal;
    text_handle = clabel(C,h);
    % set(text_handle,'BackgroundColor',[1 1 .6],'Edgecolor',[.7 .7 .7]);
    title({'Conversion efficiency contours',strcat('frequency = ',num2str(freq(fi)),' GHz')})
    % colorbar
    % find optimum angles
    [maxce,imaxce] = max(reshape(ZI,[],1));
    opt_angles(fi,:) = [XI(imaxce),YI(imaxce)];
    hold on
    plot(XI(imaxce),YI(imaxce),'k+','LineWidth',2,'MarkerEdgeColor','k','MarkerFaceColor','k','MarkerSize',15)
end
disp('Optimum (mean) angles [tor,pol]');
disp(mean(opt_angles));
