##@package amrplot
##Python module for plotting AMR results.

import operator,os.path
from numpy import *
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from matplotlib import rc
from pyAMR import ndigits
try:
    import netCDF4
    haveNetCDF = True
except ImportError:
    haveNetCDF = False
    print('warning: netCDF4 could not be imported, no netCDF functions available')

## Class for handling AMR results
class amrdata:
    """AMR file-format data handling"""
    
    def __init__(self,fname=''):
        self.__str_head = ''
        self.__dict_head = ''
        self.__data = []
        self.__type = ''
        if fname:
            self.readFile(fname)
    
    def hasData(self):
        return self.__data!=[]
    
    def dataFields(self):
        return self.__dict_head.keys()

    def readFile(self,fname):
        self.__data = []
        if not os.path.isfile(fname):
            print "warning: file " + fname + " does not exist"
            return
        self.__type = '1D'
        f_in = open(fname,'r')
        self.__str_head = f_in.readline().strip()
        # remove leading '#'
        if self.__str_head[0]=='#':
            self.__str_head=self.__str_head[1:]
        if ',' in self.__str_head:
            self.__str_head = self.__str_head.split(',')
        else:
            self.__str_head = self.__str_head.split()
        self.__dict_head = dict([(self.__str_head[i].lower().strip(),i) for i in range(len(self.__str_head))])
        # error columns
        errs = []
        data = []
        self.__ndig = 0
        for line in f_in.readlines():
            linea = line.split()
            data.append([])
            for i in range(len(linea)):
                try:
                    val = int(linea[i])
                    self.__ndig = max(ndigits(linea[i]),self.__ndig)
                except ValueError:
                    try:
                        val = float(linea[i])
                        self.__ndig = max(ndigits(linea[i]),self.__ndig)
                    except ValueError:
                        val = linea[i]
                        # convert true->1, false->0
                        if (val.lower() == 't') | (val.lower() == 'true'):
                            val = 1
                        elif (val.lower() == 'f') | (val.lower() == 'false'):
                            val = 0
                        else:
                            # put nan for non-numbers 
                            val = nan
                            if not(i in errs):
                                errs.append(i)
                data[-1].append(val)
        f_in.close()
        if not data:
            print 'warning: empty file ' + fname
            return False
        if errs:
            print 'warning: non-numbers detected in columns %s, file ' % (str(errs),fname)
        try:
            for i in range(len(data[0])):
                self.__data.append(array([dataline[i] for dataline in data]))
        except ValueError:
            print 'error: cannot convert to a numpy array'
            return False
        if len(self.__dict_head)!=len(self.__data):
            print 'warning: header contains a wrong number of columns (%i)' %len(self.__str_head)
            print self.__dict_head

    def readFile2D(self,fname):
        self.__data = []
        if not os.path.isfile(fname):
            print "warning: file " + fname + "does not exist"
            return
        self.__type = '2D'
        f_in = open(fname,'r')
        self.__str_head = f_in.readline().strip()
        # remove leading '#'
        if self.__str_head[0]=='#':
            self.__str_head=self.__str_head[1:]
        if ',' in self.__str_head:
            self.__str_head = self.__str_head.split(',')
        else:
            self.__str_head = self.__str_head.split()
        self.__dict_head = dict([(self.__str_head[i].lower().strip(),i) for i in range(len(self.__str_head))])
        self.__ndig = 0
        # error columns
        errs = []
        data = []
        c1 = []
        c2 = []
        for line in f_in.readlines():
            linea = line.split()
            try:
                c1.append(float(linea[0]))
                c2.append(float(linea[1]))
            except ValueError:
                print 'error: non-number in coordinate column'
                print line
                return False
            data.append([])
            for i in range(2,len(linea)):
                try:
                    val = int(linea[i])
                    self.__ndig = max(ndigits(linea[i]),self.__ndig)
                except ValueError:
                    try:
                        val = float(linea[i])
                        self.__ndig = max(ndigits(linea[i]),self.__ndig)
                    except ValueError:
                        val = linea[i]
                        # convert true->1, false->0
                        if (val.lower() == 't') | (val.lower() == 'true'):
                            val = 1
                        elif (val.lower() == 'f') | (val.lower() == 'false'):
                            val = 0
                        else:
                            # put nan for non-numbers 
                            val = nan
                            if not(i in errs):
                                errs.append(i)
                data[-1].append(val)
        f_in.close()
        if not data:
            print 'warning: empty file ' + fname
            return False
        if errs:
            print 'warning: non-numbers detected in columns ' + str(errs)
        for i in range(len(data)-1):
            if len(data[i])!=len(data[i+1]):
                print 'error: different number of columns near row ' + str(i+1)
                return False
        if len(self.__dict_head)!=len(data[0])+2:
            print 'warning: header contains a wrong number of columns'
        # size1,2 = size in c1,2
        if c1[1]==c1[0]:
            # c1 varies secondary
            size2 = c2[1:].index(c2[0])+1
            size1 = len(c2)//size2
            if size1*size2!=len(c1):
                print 'error: cannot detect dimensions'
                return None
            c2 = c2[:size2]
            c1 = c1[::size2]
            rsorder = 'C'
        else:
            # c2 varies secondary
            size1 = c1[1:].index(c1[0])+1
            size2 = len(c1)//size1
            if size1*size2!=len(c1): # size2!=(len(c1)//size1) or len(c1)%size1!=0:
                print 'error: cannot detect dimensions'
                return None
            c1 = c1[:size1]
            c2 = c2[::size1]
            rsorder = 'F'
        # 1st two datas are coordinates
        self.__data = [array(c1),array(c2)]
        # arrays are ordered as a[i,j] = f(x[i],y[j])
        for i in range(len(data[0])):
            self.__data.append(array([data[j][i] for j in range(len(data))]).reshape((size1,size2),order=rsorder))

    def getData(self,identifier=''):
        if self.hasData():
            try:
                if identifier=='':
                    return [ a.copy() for a in self.__data ]
                elif operator.isNumberType(identifier):
                    return self.__data[identifier].copy()
                else:
                    return self.__data[self.__dict_head[identifier.lower()]].copy()
            except KeyError:
                print 'warning: no data "'+str(identifier)+'"'
                return zeros(self.__data[0].shape)
        else:
            return array([])
        
    def __getitem__(self,key):
        return self.getData(key)

    def __getattr__(self,key):
        return self.getData(key)
    
    def selectData(self,x,y,selectors):
        # selector = [column_name,value]
        # x,y = column names
        if self.hasData():
            xdata = self.getData(x)
            ydata = self.getData(y)
            ndata = len(unique(xdata))
            mask = ones(xdata.shape,dtype=bool)
            for selector in selectors:
                mask = mask & (self.getData(selector[0])==selector[1])
            x_select = xdata[mask]
            y_select = ydata[mask]
            if len(x_select)!=ndata:
                print 'warning: length of the selected data not equal to the number of unique x points'
            return [x_select,y_select]
        else:
            return [array([]),array([])]

    def selectData2D(self,x,y,z,selectors):
        # selector = [column_name,value]
        # x,y = column names
        if self.hasData():
            xdata = self.getData(x)
            ydata = self.getData(y)
            zdata = self.getData(z)
            mask = ones(xdata.shape,dtype=bool)
            for selector in selectors:
                mask = mask & (self.getData(selector[0])==selector[1])
            x_select = xdata[mask]
            y_select = ydata[mask]
            z_select = zdata[mask]
            if len(x_select)>1:
                if x_select[1]==x_select[0]:
                    # x varies secondary
                    # size2 = y_select[1:].index(y_select[0])+1
                    size2 = where(y_select[1:]==y_select[0])[0][0]+1
                    size1 = len(y_select)//size2
                    if size1*size2!=len(y_select):
                        print 'error: cannot detect dimensions'
                        return None
                    y_select = y_select[:size2]
                    x_select = x_select[::size2]
                    rsorder = 'C'
                else:
                    # y_select varies secondary
                    # size1 = x_select[1:].index(x_select[0])+1
                    size1 = where(x_select[1:]==x_select[0])[0][0]+1
                    size2 = len(x_select)//size1
                    if size2*size1!=len(x_select):
                        print 'error: cannot detect dimensions'
                        return None
                    x_select = x_select[:size1]
                    y_select = y_select[::size1]
                    rsorder = 'F'
                z_select = z_select.reshape((size1,size2),order=rsorder).T
            return [x_select,y_select,z_select]
        else:
            return [array([]),array([])]

    def exportNETCDF(self,fname,mode='a',group='',format='NETCDF4',zlib=False):
        if not haveNetCDF:
            print('error: netCDF4 module not imported')
            return False
        if mode=='a' and os.path.exists(fname):
            nc=netCDF4.Dataset(fname,'a',format=format)
        else:
            nc=netCDF4.Dataset(fname,'w',format=format)
        if group:
            if group in nc.groups:
                grp = nc.groups[group]
            else:
                grp = nc.createGroup(group)
        else:
            grp = nc
        if self.__type=='1D':
            dim1 = self.__str_head[0]
            data = self.getData(dim1)
            grp.createDimension(dim1,size=len(data))
            v = grp.createVariable(dim1,data.dtype,dimensions=(dim1,),least_significant_digit=self.__ndig,zlib=zlib)
            v[:] = data
            for vname in self.__str_head[1:]:
                data = self.getData(vname)
                v = grp.createVariable(vname,data.dtype,dimensions=(dim1,),least_significant_digit=self.__ndig,zlib=zlib)
                v[:] =  data
        elif self.__type=='2D':
            dim1 = self.__str_head[0]
            dim2 = self.__str_head[1]
            data = self.getData(dim1)
            grp.createDimension(dim1,size=len(data))
            v = grp.createVariable(dim1,data.dtype,dimensions=(dim1,),least_significant_digit=self.__ndig,zlib=zlib)
            v[:] = data
            data = self.getData(dim2)
            grp.createDimension(dim2,size=len(data))
            v = grp.createVariable(dim2,data.dtype,dimensions=(dim2,),least_significant_digit=self.__ndig,zlib=zlib)
            v[:] = data
            for vname in self.__str_head[2:]:
                data = self.getData(vname)
                v = grp.createVariable(vname,data.dtype,dimensions=(dim1,dim2,),least_significant_digit=self.__ndig,zlib=zlib)
                v[:,:] =  data
        nc.close()
        
def plotRZ(filename,R,Z,title='',psi=None,psilabel='',psilabels=False,psiLCFS=None,RZaxis=None,ncin=5,ncout=0,\
           fpe=None,fpevals=[],fUHR=None,fUHRvals=[],fce=None,nfce=[],fcevals=[],\
           raysRZ=[], raylabels = False, \
           psicolor = '0.2',LCFScolor = '0.2',fpecolor = 'green',fUHRcolor = 'magenta', \
           fcecolor = 'blue',latex=False,\
           beam=None,plotWaist=False,pf=None):
    rc('text', usetex=latex)
    fcecolors = [((0.0,0.0,1.0),),((1.0,0.0,0.0),),((0.0,0.0,0.6),),((0.0,0.0,0.4),),((0.0,0.0,0.2),)]
    fcels = ['solid','dashed','dashdot','dotted']
    resls = fcels
    LCFSls = 'solid'
    psilw = 0.8
    reslw = 1.0
    raylw = 1.0
    # resls = 'solid'
    psils = 'dotted'
    rayls = 'solid'
    if plotWaist and beam:
        phi = arctan2(beam.y_center,beam.x_center)
        N0 = array((beam.Nx*cos(phi)+beam.Ny*sin(phi),beam.Nz))
        Cp = array((beam.x_center*cos(phi)+beam.y_center*sin(phi),beam.z_center))
        Np = array((N0[1],-N0[0]))
        p1 = Cp + beam.waist_radius*Np/(sqrt((Np**2).sum()))
        p2 = Cp - beam.waist_radius*Np/(sqrt((Np**2).sum()))
        waist_line = row_stack((p1,p2))
        p3 = beam.waist_radius*N0/(sqrt((Np**2).sum()))
        waist_vect = (Cp[0],Cp[1],p3[0],p3[1])
        waist_color = 'cyan'
        waist_alpha = 0.4
    else:
        plotWaist = False
    
    pf = plt.figure() if pf is None else plt.figure(pf.number)
    plt.hold(True)
    legs=[[],[]]
    if psi!=None:
        if psiLCFS!=None:
            psi1 = psi.copy()
            psi1[psi1>psiLCFS]=psiLCFS
            psictr = plt.contour(R,Z,psi1.T,ncin,colors=psicolor,linewidths=psilw,linestyles=psils)
            if ncout>0:
                psi2 = psi.copy()
                psi2[psi2<psiLCFS]=psiLCFS
                plt.contour(R,Z,psi2.T,ncout,colors=psicolor,linewidths=psilw,linestyles=psils)
            plt.contour(R,Z,psi.T,[psiLCFS],colors=LCFScolor,linewidths=psilw,linestyles=LCFSls)
        else:
            psictr = plt.contour(R,Z,psi.T,ncin+ncout,colors=psicolor,linewidths=psilw,linestyles=psils)
        # psictr.collections[0].set_label('psi')
        if psilabel:
            legs[0].append( plt.Line2D(range(10), range(10), linestyle=psils, color=psicolor) )
            legs[1].append(psilabel)
        if psilabels:
            plt.clabel(psictr)
    if RZaxis!=None:
        plt.plot([RZaxis[0]],[RZaxis[1]],'+',color=psicolor,markersize =10.0, markeredgewidth =1.0)
    if (fpe!=None) & (fpevals!=[]):
        for n in range(len(fpevals)):
            ctr = plt.contour(R,Z,fpe.T,[fpevals[n]],colors=fpecolor,linewidths=reslw,linestyles=resls[n])
            # ctr.collections[0].set_label('fpe')
            legs[0].append( plt.Line2D(range(10), range(10), linestyle=resls[n], color=fpecolor, linewidth=reslw) )
            legs[1].append('$f_{pe} = %g\\ GHz$' % fpevals[n])
    if (fUHR!=None) & (fUHRvals!=[]):
        for n in range(len(fUHRvals)):
            ctr = plt.contour(R,Z,fUHR.T,[fUHRvals[n]],colors=fUHRcolor,linewidths=reslw,linestyles=resls[n])
            # ctr.collections[0].set_label('fUHR')
            legs[0].append( plt.Line2D(range(10), range(10), linestyle=resls[n], color=fUHRcolor, linewidth=reslw) )
            legs[1].append('$f_{UHR} = %g\\ GHz$' % fUHRvals[n])
    if (fce!=None) & (nfce!=[]) & (fcevals!=[]):
        for n in range(len(nfce)):
            ctr = plt.contour(R,Z,nfce[n]*fce.T,[fcevals[n]],colors=fcecolor,linewidths=reslw,linestyles=fcels[n])
            # ctr = plt.contour(R,Z,nfce[n]*fce,fcevals[n],colors=fcecolor,linewidths=reslw,linestyles=resls)
            # ctr = plt.contour(R,Z,nfce[n]*fce,fcevals[n],colors=fcecolor,linewidths=reslw,linestyles=fcels)
            # ctr.collections[0].set_label('%i fce = %g GHz' % (nfce[n],fcevals[n]))
            legs[0].append( plt.Line2D(range(10), range(10), linestyle=fcels[n], color=fcecolor, linewidth=reslw) )
            legs[1].append('$%i f_{ce} = %g\\ GHz$' % (nfce[n],fcevals[n]))
    if raysRZ!=[]:
        for ray in raysRZ:
            plt.plot(ray['R'],ray['Z'],label=ray['name'],color=ray['color'],linestyle=ray['linestyle'],linewidth=raylw)
            if ray.has_key('spots'):
                for rspot in ray['spots']:
                    plt.plot(ray['R'][rspot],ray['Z'][rspot],c='brown',marker='o',ms=3,ls='',alpha=0.5)
            if ray['name'] != '' and raylabels:
                legs[0].append( plt.Line2D(range(10), range(10), linestyle=ray['linestyle'], color=ray['color'], linewidth=raylw) )
                legs[1].append(ray['name'])
    if plotWaist:
        plt.plot(waist_line[:,0],waist_line[:,1],c=waist_color,lw=2, alpha=waist_alpha)
        plt.arrow(waist_vect[0],waist_vect[1],waist_vect[2],waist_vect[3],color=waist_color, alpha=waist_alpha)
    if latex:
        plt.xlabel('R [m]')
        plt.ylabel('Z [m]')
    else:
        plt.xlabel('R [m]')
        plt.ylabel('Z [m]')
    plt.title(title)
    plt.legend(legs[0],legs[1],loc='best')
    # plt.legend(loc='best')
    plt.axis('image')
    if filename:
        plt.savefig(filename)
        plt.close(pf)

def plotXY(filename,X,Y,title='',psi=None,psilabel='',psilabels=False,psiLCFS=None,ncin=5,ncout=0,\
           fpe=None,fpevals=[],fUHR=None,fUHRvals=[],fce=None,nfce=[],fcevals=[],\
           raysRZ=[], raylabels = False, \
           psicolor = '0.2',LCFScolor = '0.2',fpecolor = 'green',fUHRcolor = 'magenta', \
           fcecolor = 'blue',latex=False,\
           beam=None,plotWaist=False,
           legend_pos = 'best'):
    rc('text', usetex=latex)
    fcels = ['solid','dashed','dashdot','dotted']
    resls = fcels
    LCFSls = 'solid'
    psilw = 0.8
    reslw = 1.0
    raylw = 1.0
    # resls = 'solid'
    psils = 'dotted'
    if plotWaist and beam:
        phi = arctan2(beam.y_center,beam.x_center)
        N0 = array((beam.Nx,beam.Ny))
        Cp = array((beam.x_center,beam.y_center))
        Np = array((N0[1],-N0[0]))
        p1 = Cp + beam.waist_radius*Np/(sqrt((Np**2).sum()))
        p2 = Cp - beam.waist_radius*Np/(sqrt((Np**2).sum()))
        waist_line = row_stack((p1,p2))
        p3 = beam.waist_radius*N0/(sqrt((Np**2).sum()))
        waist_vect = (Cp[0],Cp[1],p3[0],p3[1])
        waist_color = 'cyan'
        waist_alpha = 0.4
    else:
        plotWaist = False
        
    pf = plt.figure()
    plt.hold(True)
    legs=[[],[]]
    if psi!=None:
        if psiLCFS!=None:
            psi1 = psi.copy()
            psi1[psi1>psiLCFS]=psiLCFS
            psictr = plt.contour(X,Y,psi1.T,ncin,colors=psicolor,linewidths=psilw,linestyles=psils)
            if ncout>0:
                psi2 = psi.copy()
                psi2[psi2<psiLCFS]=psiLCFS
                plt.contour(X,Y,psi2.T,ncout,colors=psicolor,linewidths=psilw,linestyles=psils)
            plt.contour(X,Y,psi.T,[psiLCFS],colors=LCFScolor,linewidths=psilw,linestyles=LCFSls)
        else:
            psictr = plt.contour(X,Y,psi.T,ncin+ncout,colors=psicolor,linewidths=psilw,linestyles=psils)
        # psictr.collections[0].set_label('psi')
        if psilabel:
            legs[0].append( plt.Line2D(range(10), range(10), linestyle=psils, color=psicolor) )
            legs[1].append(psilabel)
        if psilabels:
            plt.clabel(psictr)
    if (fpe!=None) & (fpevals!=[]):
        for n in range(len(fpevals)):
            ctr = plt.contour(X,Y,fpe.T,[fpevals[n]],colors=fpecolor,linewidths=reslw,linestyles=resls[n])
            # ctr.collections[0].set_label('fpe')
            legs[0].append( plt.Line2D(range(10), range(10), linestyle=resls[n], color=fpecolor, linewidth=reslw) )
            legs[1].append('$f_{pe} = %g\\ GHz$' % fpevals[n])
    if (fUHR!=None) & (fUHRvals!=[]):
        for n in range(len(fUHRvals)):
            ctr = plt.contour(X,Y,fUHR.T,[fUHRvals[n]],colors=fUHRcolor,linewidths=reslw,linestyles=resls[n])
            # ctr.collections[0].set_label('fUHR')
            legs[0].append( plt.Line2D(range(10), range(10), linestyle=resls[n], color=fUHRcolor, linewidth=reslw) )
            legs[1].append('$f_{UHR} = %g\\ GHz$' % fUHRvals[n])
    if (fce!=None) & (nfce!=[]) & (fcevals!=[]):
        for n in range(len(nfce)):
            ctr = plt.contour(X,Y,nfce[n]*fce.T,[fcevals[n]],colors=fcecolor,linewidths=reslw,linestyles=fcels[n])
            # ctr = plt.contour(R,Z,nfce[n]*fce,fcevals[n],colors=fcecolor,linewidths=reslw,linestyles=resls)
            # ctr = plt.contour(R,Z,nfce[n]*fce,fcevals[n],colors=fcecolor,linewidths=reslw,linestyles=fcels)
            # ctr.collections[0].set_label('%i fce = %g GHz' % (nfce[n],fcevals[n]))
            legs[0].append( plt.Line2D(range(10), range(10), linestyle=fcels[n], color=fcecolor, linewidth=reslw) )
            legs[1].append('$%i f_{ce} = %g\\ GHz$' % (nfce[n],fcevals[n]))
    if raysRZ!=[]:
        for ray in raysRZ:
            plt.plot(ray['x'],ray['y'],label=ray['name'],color=ray['color'],linestyle=ray['linestyle'],linewidth=raylw)
            if ray.has_key('spots'):
                for rspot in ray['spots']:
                    plt.plot(ray['x'][rspot],ray['y'][rspot],c='brown',marker='o',ms=3,ls='',alpha=0.5)
            if ray['name'] != '' and raylabels:
                legs[0].append( plt.Line2D(range(10), range(10), linestyle=ray['linestyle'], color=ray['color'], linewidth=raylw) )
                legs[1].append(ray['name'])
    if plotWaist:
        plt.plot(waist_line[:,0],waist_line[:,1],c=waist_color,lw=2, alpha=waist_alpha)
        plt.arrow(waist_vect[0],waist_vect[1],waist_vect[2],waist_vect[3],color=waist_color, alpha=waist_alpha)
    plt.xlabel('x [m]')
    plt.ylabel('y [m]')
    plt.title(title)
    if legend_pos: plt.legend(legs[0],legs[1],loc=legend_pos)
    # plt.legend(loc='best')
    plt.axis('image')
    if filename:
        plt.savefig(filename)
        plt.close(pf)

def plotContour(filename,x,xlabel,y,ylabel,datas,title='',labels=[],plotlabels=[],fmts=[],contours=[],filled=[],\
                colors=[],cmap=[],cbars=[],splts=[],latex=False):
    for d in datas:
        if d.shape<(2,2):
            print 'error in plotContour: data not 2D (shape < (2,2))'
            return False
    rc('text', usetex=latex)
    defls = 'solid'
    ndata = len(datas)
#    if (ndata!=len(xlabel)) | (ndata!=len(y)) | (ndata!=len(ylabel)) | (ndata!=len(datas)):
#       | ((ndata!=len(ncontours))&(ncontours!=[])) \
#       | ((ndata!=len(contourvals))&(contourvals!=[])) \
#       | ((ndata!=len(filled))&(filled!=[])) :
#        print 'error: input lengths not equal'
#        return False
    pf = plt.figure()
    plt.hold(True)
    legs=[[],[]]
    if (filled==[]) | (filled==False):
        filled = [False for i in range(ndata)]
    elif filled==True:
        filled = [True for i in range(ndata)]
    elif ndata!=len(filled):
        print 'error: ndata!=len(filled)'
        return False
    if (labels==[]):
        labels = ['' for i in range(ndata)]
    elif ndata!=len(labels):
        print 'error: ndata!=len(labels)'
        return False
    if (plotlabels==[]) | (plotlabels==False):
        plotlabels = [False for i in range(ndata)]
    elif plotlabels==True:
        plotlabels = [True for i in range(ndata)]
    elif ndata!=len(plotlabels):
        print 'error: ndata!=len(plotlabels)'
        return False
    if (contours==[]):
        contours = [0 for i in range(ndata)]
    elif ndata!=len(contours):
        print 'error: ndata!=len(contours)'
        return False
    if (cbars==[]) | (cbars==False):
        cbars = [False for i in range(ndata)]
    elif cbars==True:
        cbars = [True for i in range(ndata)]
    elif ndata!=len(cbars):
        print 'error: ndata!=len(cbars)'
        return False
    if fmts==[]:
        fmts=['%1.3f' for i in range(ndata)]
    elif isinstance(fmts, str):
        fmts=[fmts for i in range(ndata)]

    if (colors==[]) | (colors==None):
        colors = [None for i in range(ndata)]
    elif not isinstance(colors,list):
        colors = [colors for i in range(ndata)]
    elif ndata!=len(colors):
        print 'error: ndata!=len(colors)'
        return False
    if (cmap==[]) | (cmap==None):
        cmap = [None for i in range(ndata)]
    elif not isinstance(cmap,list):
        cmap = [cmap for i in range(ndata)]
    elif ndata!=len(cmap):
        print 'error: ndata!=len(cmap)'
        return False

    for n in range(ndata):
        if filled[n]:
            mycontour = plt.contourf
        else:
            mycontour = plt.contour
        if contours[n]!=0:
            ctr = mycontour(x,y,datas[n],contours[n],colors=colors[n],cmap=cmap[n],linestyles=defls)
        else:
            ctr = mycontour(x,y,datas[n],colors=colors[n],cmap=cmap[n])
        if cbars[n]:
            plt.colorbar()
        if plotlabels[n]:
            plt.clabel(ctr,fmt=fmts[n])
        if labels[n]:
            legs[0].append( plt.Line2D(range(10), range(10), linestyle=defls, color='b') )
            legs[1].append(labels[n])
    for splt in splts:
        eval("plt.scatter(splt[0],splt[1],"+splt[2]+")")
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    if any(legs): plt.legend(legs[0],legs[1],loc='best')
    plt.axis('image')
    if filename:
        plt.savefig(filename)
        plt.close(pf)
def plot1(data,x,y,style='k-',xlabel='',ylabel='',filename='',fileext='png',selectors=[],latex=False):
    rc('text', usetex=latex)
    pf = plt.figure()
    if selectors==[]:
        xdata = data.getData(x)
        ydata = data.getData(y)
    else:
        [xdata,ydata] = data.selectData(x,y,selectors)
    pl = plt.plot(xdata,ydata,style)
    if xlabel=='':
        xlabel = x
    if ylabel=='':
        ylabel = y
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    if filename:
        fname = filename + '.' + fileext
    else:
        fname = x+'_'+y+'.' + fileext
    if filename:
        plt.savefig(fname)
        plt.close(pf)

def plotfreqs(filename,data,x,title='',xLCFS=None, \
              fpe=None,fUHR=None,fce=None,nfce=[1],fce_limit=None, \
			  xlimits = None, ylimits = None,\
              xlabel='',ylabel='', \
              fpecolor = 'green',fUHRcolor = 'magenta', \
              fcecolor = 'blue', \
              plt_grid = True, \
              hfreqs = [], \
              selectors=[],latex=False,newfig=True,showlegend=True):
    reslw = 1.0
    lw = 1.0
    LCFScolor = '0.2'
    if selectors!=[]:
        # [xdata,ydata] = data.selectData(x,y,selectors)
        print "plot 2 with selectors not implemented"
        return
    rc('text', usetex=latex)
    if newfig:
        pf = plt.figure()
        ax1 = plt.axes()
    plt.hold(True)
    legs=[[],[]]
    xdata = data.getData(x)
    if fpe!=None:
        plt.plot(xdata,data.getData(fpe),color=fpecolor,linewidth=lw,label=r'$f_{pe}$')
#        legs[0].append(plt.Line2D(range(20),range(20),linestyle='-',color=fpecolor,linewidth=lw) )
#        legs[1].append('fpe')
    if fUHR!=None:
        plt.plot(xdata,data.getData(fUHR),color=fUHRcolor,linewidth=lw,label=r'$f_{UHR}$')
#        legs[0].append(plt.Line2D(range(20),range(20),linestyle='-',color=fUHRcolor,linewidth=lw) )
#        legs[1].append('fUHR')
    if fce!=None:
#        legs[0].append(plt.Line2D(range(20),range(20),linestyle='-',color=fcecolor,linewidth=lw) )
#        legs[1].append('n*fce')
        flaglabel = True
        for n in nfce:
            if fce_limit is not None:
                fce_mask = n*data[fce]<fce_limit
            else:
                fce_mask = ones(data[fce].shape,dtype=bool)
            if any(fce_mask):
                if flaglabel:
                    plt.plot(xdata[fce_mask],n*data[fce][fce_mask],color=fcecolor,linewidth=lw,label=r'$nf_{ce}$')
                    flaglabel=False
                else:
                    plt.plot(xdata[fce_mask],n*data[fce][fce_mask],color=fcecolor,linewidth=lw)
    if xLCFS!=None:
        plt.axvline(x=xLCFS,color=LCFScolor,linestyle='--',linewidth=reslw,label='LCFS')
#        legs[0].append(plt.Line2D(range(20),range(20),linestyle='--',color='darkgrey',linewidth=reslw) )
#        legs[1].append('LCFS')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    if xlimits is not None: plt.xlim(xlimits)
    if ylimits is not None: plt.ylim(ylimits)
    plt.grid(plt_grid)
#    if any(hfreqs):
#        plt.hlines(hfreqs,colors='gray',linestyles='solid')
    for f in hfreqs: plt.axhline(f,c='gray',ls='solid')
    plt.title(title)
#    plt.legend(legs[0],legs[1],loc='best')
    if showlegend: plt.legend(loc='best',handlelength=plt.rcParams['legend.handlelength']*2)
    if filename and newfig:
        plt.savefig(filename)
        plt.close(pf)

def plotdente(filename,data,x,title='',xLCFS=None, \
              den=None,temp=None, \
              xlabel='',ydenlabel='10^19 m-3',ytemplabel='keV',denlabel='density',templabel='temperature', \
              dencolor = 'blue',tempcolor = 'red', \
              expdata=None,expx='R(m)',expden='den(1e19m-3)',exptemp='Te(keV)', \
              denerr = '', temperr='', expxerr = '', \
              selectors=[],latex=False):
    reslw = 1.0
    lw = 1.0
    LCFScolor = '0.2'
    if selectors!=[]:
        # [xdata,ydata] = data.selectData(x,y,selectors)
        print "plotdente with selectors not implemented"
        return
    rc('text', usetex=latex)
    legs=[[],[]]
    pf = plt.figure()
    plt.xlabel(xlabel)
    xdata = data.getData(x)
    if den is not None:
        axden = plt.axes()
        axden.hold(True)
        if temp is not None:
            axtemp = axden.twinx()
            axtemp.hold(True)
    elif temp is not None:
        axtemp = plt.axes()
        axtemp.hold(True)
    if den is not None:
        plt.axes(axden)
        if expdata is not None:
            expden_data = nan_to_num(expdata.getData(expden))
            expden_x = expdata.getData(expx)
            # revome negative values
            expden_data[expden_data<0] = 0
            # remove too large values
            maxval = 5*median(expden_data)
            expmask = expden_data < maxval
            expden_data = expden_data[expmask]
            expden_x = expden_x[expmask]
            axden.plot(expden_x,expden_data,linestyle='',\
                       marker='D',markeredgecolor=dencolor,markerfacecolor='white',markersize=3.5)
            if denerr!='':
                xerr=None
                if expxerr!='':
                    xerr=nan_to_num(expdata.getData(expxerr))
                    xerr = xerr[expmask]
                # check for extensive errors
                denerr_data = nan_to_num(expdata.getData(denerr))
                denerr_data = denerr_data[expmask]
                exterr = 0.3
                errmask = (expden_data + denerr_data) > (1+exterr)*expden_data.max()
                denerr_data[errmask] =  expden_data[errmask]*exterr
                errmask = (expden_data - denerr_data) < (1-exterr)*expden_data.min()
                denerr_data[errmask] =  expden_data[errmask] # *exterr
                plt.errorbar(expden_x,expden_data,\
                             yerr=denerr_data, xerr=xerr,\
                             fmt=None, ecolor=dencolor, elinewidth=reslw, capsize=3,\
                             barsabove=False, lolims=False, uplims=False,\
                             xlolims=False, xuplims=False)
        if type(den)==list:
            dens = den
            dencolors = dencolor
            denlabels = denlabel
        else:
            dens = [den]
            dencolors = [dencolor]
            denlabels = [denlabel]
        for [d,label,color] in zip(dens,denlabels,dencolors):
            axden.plot(xdata,data.getData(d),color=color,linewidth=lw,label=label)
            legs[0].append(plt.Line2D(range(10),range(10),linestyle='-',color=color,linewidth=lw) )
            legs[1].append(label)
        axden.set_ylabel(ydenlabel)
        axden.axis(ymin=0)
        axden.hold(True)
    if temp is not None:
        plt.axes(axtemp)
        if expdata is not None:
            exptemp_data = nan_to_num(expdata.getData(exptemp))
            exptemp_x = expdata.getData(expx)
            exptemp_data[exptemp_data<0] = 0
            # remove too large values
            maxval = 5*median(exptemp_data)
            expmask = exptemp_data < maxval
            exptemp_data = exptemp_data[expmask]
            exptemp_x = exptemp_x[expmask]
            axtemp.plot(exptemp_x,exptemp_data,linestyle='',\
                        marker='D',markeredgecolor=tempcolor,markerfacecolor='white',markersize=3.5)
            if temperr!='':
                xerr=None
                if expxerr!='':
                    xerr=nan_to_num(expdata.getData(expxerr))
                    xerr = xerr[expmask]
                # check for extensive errors
                temperr_data = nan_to_num(expdata.getData(temperr))
                temperr_data = temperr_data[expmask]
                exterr = 0.3
                errmask = (exptemp_data + temperr_data) > (1+exterr)*exptemp_data.max()
                temperr_data[errmask] =  exptemp_data[errmask]*exterr
                errmask = (exptemp_data - temperr_data) < (1-exterr)*exptemp_data.min()
                temperr_data[errmask] =  exptemp_data[errmask] # 100% for extensive errorbars
                plt.errorbar(exptemp_x,exptemp_data,\
                             yerr=temperr_data, xerr=xerr,\
                             fmt=None, ecolor=tempcolor, elinewidth=reslw, capsize=3,\
                             barsabove=False, lolims=False, uplims=False,\
                             xlolims=False, xuplims=False)
        if type(temp)==list:
            temps = temp
            tempcolors = tempcolor
            templabels = templabel
        else:
            temps = [temp]
            tempcolors = [tempcolor]
            templabels = [templabel]
        for [d,label,color] in zip(temps,templabels,tempcolors):
            axtemp.plot(xdata,data.getData(d),color=color,linewidth=lw,label=label)
            legs[0].append(plt.Line2D(range(10),range(10),linestyle='-',color=color,linewidth=lw) )
            legs[1].append(label)
        axtemp.set_ylabel(ytemplabel)
        axtemp.axis(ymin=0)
    if xLCFS is not None:
        plt.axvline(x=xLCFS,color=LCFScolor,linestyle='--',linewidth=reslw,label='LCFS')
        legs[0].append(plt.Line2D(range(10),range(10),linestyle='--',color=LCFScolor,linewidth=reslw) )
        legs[1].append('LCFS')
    plt.title(title)
    plt.legend(legs[0],legs[1],loc='best',handlelength=plt.rcParams['legend.handlelength']*2)
    if filename:
        plt.savefig(filename)
        plt.close(pf)

def plotxy(filename,data,xs,ys,xlabel,ylabels,colors=None,title='',xLCFS=None,yuplimit=None,ylowlimit=None, \
           grid=False,latex=False):
    if not isinstance(ys,(list,tuple)):
        print 'error: ys must be a list or a tuple in plotxy'
        return
    reslw = 1.0
    lw = 1.0
    LCFScolor = '0.2'
    rc('text', usetex=latex)
    pf = plt.figure()
    ax1 = plt.axes()
    plt.hold(True)
    legs=[[],[]]
    if colors==None:
        colors = ['blue' for i in range(len(ys))]
    elif not isinstance(colors,list):
        colors = [colors for i in range(len(ys))]
    if not isinstance(ys,(list,tuple,)): ys = [ys]
    if not isinstance(xs,(list,tuple,)): xs = [xs for i in range(len(ys))]
    for [x,y,color,ylabel] in zip(xs,ys,colors,ylabels):
        if isinstance(x,ndarray):
            xdata = x
        else:
            xdata = data.getData(x)
        datamask = ones(xdata.shape,dtype=bool)
        if isinstance(y,ndarray):
            ydata = y
        else:
            ydata = data.getData(y)
        if yuplimit!=None: datamask &= ydata <= yuplimit
        if ylowlimit!=None: datamask &= ydata >= ylowlimit
        plt.plot(xdata[datamask],ydata[datamask],color=color,linewidth=lw,label=ylabel)
    if xLCFS!=None:
        plt.axvline(x=xLCFS,color=LCFScolor,linestyle='--',linewidth=reslw,label='LCFS')
    plt.xlabel(xlabel)
#    plt.ylabel(ylabel)
    plt.title(title)
    if grid:
        ax1.grid(linestyle='--', linewidth=0.5,color='0.3')
    plt.legend(loc='best',handlelength=plt.rcParams['legend.handlelength']*2)
    if filename:
        plt.savefig(filename)
        plt.close(pf)

## Plot two sets of data using two y axis
def plotx2y(filename,data,x,y1s,y2s,xlabel,y1labels,y2labels,\
            yaxis1_label='',yaxis2_label='',\
            colors1=None,colors2=None,\
            arx = None, ary1s = [], ary2s = [], \
            arcolors1=None, arcolors2=None, ary1labels=[], ary2labels=[], \
            title='',xLCFS=None, \
            grid=False,latex=False):
    reslw = 1.0
    lw = 1.0
    LCFScolor = '0.2'
    rc('text', usetex=latex)
    pf = plt.figure()
    ax1 = plt.axes()
    ax1.hold(True)
    ax2 = ax1.twinx()
    ax2.hold(True)
    xdata = data.getData(x)
    legs = [[],[]]
    for [ax,ys,colors,ylabels,yaxis_label] in zip((ax1,ax2),(y1s,y2s),(colors1,colors2),(y1labels,y2labels),(yaxis1_label,yaxis2_label)):
        plt.axes(ax)
        if colors==None:
            colors = ['blue' for i in range(len(ys))]
        elif not isinstance(colors,list):
            colors = [colors for i in range(len(ys))]
        for [y,color,ylabel] in zip(ys,colors,ylabels):
            ydata = data.getData(y)
            plt.plot(xdata,ydata,color=color,linewidth=lw,label=ylabel)
            legs[0].append(plt.Line2D(range(10),range(10),linestyle='-',color=color,linewidth=lw) )
            legs[1].append(ylabel)
            ax.set_ylabel(yaxis_label)
    # plot additional data in numpy arrays
    if arx!=None:
        for [ax,ys,colors,ylabels] in zip((ax1,ax2),(ary1s,ary2s),(arcolors1,arcolors2),(ary1labels,ary2labels)):
            plt.axes(ax)
            if colors==None:
                colors = ['blue' for i in range(len(ys))]
            elif not(isinstance(colors,list)):
                colors = [colors for i in range(len(ys))]
            for [ydata,color,ylabel] in zip(ys,colors,ylabels):
                plt.plot(arx,ydata,color=color,linewidth=lw,label=ylabel)
                legs[0].append(plt.Line2D(range(10),range(10),linestyle='-',color=color,linewidth=lw) )
                legs[1].append(ylabel)
    if xLCFS!=None:
        plt.axvline(x=xLCFS,color=LCFScolor,linestyle='--',linewidth=reslw,label='LCFS')
        legs[0].append(plt.Line2D(range(10),range(10),linestyle='--',color=LCFScolor,linewidth=reslw) )
        legs[1].append('LCFS')
    plt.xlabel(xlabel)
#    plt.ylabel(ylabel)
    plt.title(title)
    if grid:
        ax1.grid(linestyle='--', linewidth=0.5,color='0.3')
    plt.legend(legs[0],legs[1],loc='best',handlelength=plt.rcParams['legend.handlelength']*2)
    if filename:
        plt.savefig(filename)
        plt.close(pf)


def plotxys(filename,data,x,ys,xlabel,ylabels,scalex=True,scaley=True,colors=None,title='',xLCFS=None, \
           linestyles=None,grid=True,latex=False):
    reslw = 1.0
    lw = 1.0
    LCFScolor = '0.2'
    rc('text', usetex=latex)
    pf = plt.figure()
    ax1 = plt.axes()
    plt.hold(True)
    legs=[[],[]]
    if scaley==True:
        scaley=[True for ii in ys]
    elif scaley==False:
        scaley=[False for ii in ys]
    if linestyles==None:
        linestyles = ['-' for i in range(len(ys))]
    elif not isinstance(linestyles, (list,tuple)):
        linestyles = [linestyles for ii in ys]
    if colors==None:
        colors = ['blue' for i in range(len(ys))]
    elif not(isinstance(colors,list)):
        colors = [colors for i in range(len(ys))]
    if isinstance(x,ndarray):
        xdata = [x for i in ys]
    elif isinstance(x,(str,unicode)):
        xdata = [data.getData(x) for i in ys]
    else:
        xdata = x
        for a in xdata:
            if isinstance(a,(str,unicode)): a = data[x]
    if scalex:
        xscale = max(map(max,map(abs,xdata)))
        xdata = map(lambda a: a/xscale, xdata)
        xlabel = xlabel + ' /%.3g ' % xscale
    for [xx,y,color,ylabel,scaley_i,ls] in zip(xdata,ys,colors,ylabels,scaley,linestyles):
        if isinstance(y,ndarray):
            ydata = y
        else:
            ydata = data.getData(y)
        isscaled = False
        if scaley_i==True:
            yscale = abs(ydata).max()
            isscaled = True
        elif scaley_i=='int':
            yscale = math.ceil(abs(ydata).max())
            isscaled = True
        if isscaled:
            if yscale>1 or yscale<0.2:
                ydata = ydata / yscale
                ylabel = ylabel  + ' /%.3g ' % yscale
        plt.plot(xx,ydata,color=color,linewidth=lw,label=ylabel,ls=ls)
    if xLCFS!=None:
        plt.axvline(x=xLCFS,color=LCFScolor,linestyle='--',linewidth=reslw,label='LCFS')
    plt.xlabel(xlabel)
#    plt.ylabel(ylabel)
    plt.title(title)
    if scaley:
        plt.axis(ymin=-1.03,ymax=1.03)
        plt.yticks(arange(-1,1.01,0.25))
    if grid:
        ax1.grid(linestyle='--', linewidth=0.5,color='0.3')
    plt.legend(loc='best',handlelength=plt.rcParams['legend.handlelength']*2)
    plt.axhline(color='black')
    if filename:
        plt.savefig(filename)
        plt.close(pf)

def implot(filename,x,y,z,data=None,xlabel='',ylabel='',title='',logz=False,extent=None,\
           cbar=False,\
           cmap=None, xLCFS=None,latex=False):
    rc('text', usetex=latex)
    pf = plt.figure()
    ax1 = plt.axes()
    plt.hold(True)
    if data!=None:
        x = data.getData(x)
        y = data.getData(y)
        z = data.getData(z)
    if len(x)>1 and len(y)>1:
        if x.ndim==1 and y.ndim==1:
            sx = (x[-1]-x[0])/(x.size-1)
            xi = arange(x[0],x[-1]+0.5*sx,sx)
            sy = (y[-1]-y[0])/(y.size-1)
            yi = arange(y[0],y[-1]+0.5*sy,sy)
            X, Y = meshgrid(x, y)
            zi = mlab.griddata(X.reshape(-1), Y.reshape(-1), z.T.reshape(-1), xi, yi)
            extent = (xi[0]-sx/2,xi[-1]+sx/2,yi[0]-sy/2,yi[-1]+sy/2)
        else:
            X = x
            Y = y
            zi = z.T
            extent = (x.min(),x.max(),y.min(),y.max())
        plt.imshow(zi,extent=extent,cmap=cmap,origin='lower',interpolation='nearest')
        plt.title(title)
        plt.axis('tight')
        if filename:
            plt.savefig(filename)
            plt.close(pf)

