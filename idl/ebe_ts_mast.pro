pro ebe_ts_mast,shot,time,ts_data

;   input experimental data
; ts_data: 0-ruby, 1-Nd YAG

source = 'MAST::' + strtrim(string(shot),2)

if (ts_data eq 0) then begin
; read ruby laser TS data
  if (shot ge 4675) then data_item_suff='/12' else data_item_suff=''
	trace = getdata('ats_ne'+data_item_suff, source)
	n_e = trace.data
	dn_e = trace.edata
	ts_time = trace.time(0)
	trace = getdata('ats_Te'+data_item_suff, source)
	T_e = trace.data
	dT_e = trace.edata
	trace = getdata('ats_R'+data_item_suff, source)
	R_ts = trace.data
	dR_ts = trace.edata

	; convert from m^(-3) to 10^19 m^(-3)
	n_e = n_e * 1.0e-19
	dn_e = dn_e * 1.0e-19
	; convert from eV to keV
	t_e = t_e * 1.0e-3
	dT_e = dT_e * 1.0e-3
	; keep R in m
endif

if (ts_data eq 1) then begin
; read Nd-YAG laser TS data
	if (shot ge 22916) then signalname = 'AYC_' else signalname = 'ATM_'
	trace = getdata(signalname+'NE', source)
	n_e_t = trace.data
	dn_e_t = trace.edata
	t = trace.time
	trace = getdata(signalname+'TE', source)
	T_e_t = trace.data
	dT_e_t = trace.edata
	trace = getdata(signalname+'R', source)
	;   calculate index of time in TS
	itime = where(abs(t - time) eq min(abs(t - time)))
	if (shot ge 22916) then begin
	  R_ts = trace.data(*,itime)
	  dR_ts = trace.edata(*,itime)
	endif else begin
	  R_ts = trace.data(*,itime)
	  dR_ts = trace.edata(*,itime)
	endelse
	ts_time = t(itime)
	; convert from m^(-3) to 10^19 m^(-3)
	n_e = n_e_t(*,itime) * 1.0e-19
	dn_e = dn_e_t(*,itime) * 1.0e-19
	; convert from eV to keV
	t_e = t_e_t(*,itime)*1.0e-3
	dt_e = dt_e_t(*,itime)*1.0e-3
	; keep R in m
endif

phi = 0.0
Z = 0.0
dphi = 0.0
dZ = 0.0

; write the output file
out_fname = inp_fname(shot,time,'ts')
openw,1,out_fname
printf,1,'#R(m),phi(rad),Z(m),dR(m),dphi(rad),dZ(m),den(1e19m-3),Te(keV),dden(1e19m-3),dTe(keV)'
for i=0,n_elements(R_ts)-1 do begin
	; test for NaNs
	; if NaN appears, it's replaced by -1
	if ( ~(finite(n_e[i]))) then n_e[i]=-1
	if ( ~(finite(dn_e[i]))) then begin
          n_e[i]=-1
          dn_e[i]=-1
        endif
	if ( ~(finite(t_e[i]))) then t_e[i]=-1
	if ( ~(finite(dt_e[i]))) then begin
          t_e[i]=-1
          dt_e[i]=-1
        endif
;	if ( (finite(n_e[i])) and (finite(t_e[i])) ) then begin
		printf,1,R_ts[i],phi,Z,dR_ts[i],dphi,dZ,n_e[i],t_e[i],dn_e[i],dt_e[i],format='(10(E15.7))'
;	endif
endfor
close,1

end      

