//*********************************************************************
// Fortran Interface to C++
/*
!USAGE:
  implicit none
  real * 8 cyl(3),boozer(3),Bfield(3),epsTrunc/1d-6/,epsA/1d-4/,ficyl/0/
  character (len=80) fname
  integer *4 w7x(2)                         ! this array holds the address of C++ object
  integer *4 False/0/,mcload

  EXTERNAL mcload,mcsetB0,mctruncate,mcsetAccuracy,mcsetB0,mccyl2boozer
! Here EXTERNAL operator is used, but for true FORTRAN-90 program you have to
! use FORTRAN-statement INTERFACE for defining prototypes of subroutine called:
!
!   INTERFACE
!    INTEGER(4) FUNCTION MCLOAD(mConf,fname)
!     INTEGER(4) mConf(2)
!     CHARACTER *(*) fname
!    END FUNCTION MCLOAD
!   END INTERFACE

  fname = 'w7x-sc1.bc'
  if  (mcload (w7x,fname)==False ) stop     ! stop if error

  call mctruncate   (w7x,epsTrunc)          ! truncate spectrum
  call mcsetAccuracy(w7x,epsA)              ! set accuracy of coordinate transformation
  call mcsetB0      (w7x,2.5d0,ficyl)       ! scale B, set value of B on magn. axis

  cyl = (/6d0,1d-2,5d-1/)

  call mccyl2boozer (w7x,cyl,boozer,Bfield) ! coordinate transformation

  if(boozer(1) > 1)  print '(/a)', 'Point outside plasma!'
  print '(/a,3f14.6 )','cyl   = (r, fi, z)  =',cyl
  print '( a,3f14.6 )','boozer=(s,theta,phi)=',boozer
  print '( a,3f14.6/)','Bfield=(Br, Bfi, Bz)=',Bfield

  call mcfree(w7x)                          ! destroy C++ object and free memory
  end
*/
//
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "C3dMesh.h"
#include "CPfunction.h"

#include <iostream>
using namespace std;

const static double   pi = 3.14159265358979323846;
const static double   degree = pi/180;

#undef True
#undef False

const static int True(1);
const static int False(0);

#if (defined( __unix__ ) || (defined(linux)) )
  #define __stdcall
  #define PROFILEFUNCTION  profilefunction_
  #define MCLOAD        mcload_
  #define MCISOK        mcisok_
  #define MCISMESHOK    mcismeshok_
  #define MCFREE        mcfree_
  #define MCTRUNCATE    mctruncate_
  #define MCSETACCURACY mcsetaccuracy_
  #define MCCYL2BOOZER  mccyl2boozer_
  #define MCSETMINB0    mcsetminb0_
  #define MCSETB0       mcsetb0_
  #define MCGETB0       mcgetb0_
  #define MCSETAVERAGEB0  mcsetaverageb0_
  #define MCGETAVERAGEB0  mcgetaverageb0_
  #define MCSETAVERAGEBPHI0  mcsetaveragebphi0_
  #define MCGETAVERAGEBPHI0  mcgetaveragebphi0_

  #define MCCYL2BOOZER2 mccyl2boozer2_
  #define MCXYZ2BOOZER2 mcxyz2boozer2_
  #define MCXYZ2S       mcxyz2s_
  #define MCCYL2S       mccyl2s_
  #define MCREFF        mcreff_
  #define MCTORFLUX     mctorflux_
  #define MCVPRIME      mcvprime_
  #define MCIOTA        mciota_
  #define MCVOLUME      mcvolume_
  #define MCFTRAPPED    mcftrapped_
  #define MCBAVRG       mcbavrg_
  #define MCB2AVRG      mcb2avrg_
  #define MCDB12AVRG    mcdb12avrg_
  #define MCDB32AVRG    mcdb32avrg_
  #define MCBDB12AVRG   mcbdb12avrg_
  #define MCBMIN        mcbmin_
  #define MCBMAX        mcbmax_

  #define MCPAVRG       mcpavrg_
  #define MCINTINVPAVRG mcintinvpavrg_
  #define MCTORFLUX2POLFLUX  mctorflux2polflux_

  #define MCJACOBIANB2  mcjacobianb2_

  #define MCRMN          mcrmn_
  #define MCZMN          mczmn_
  #define MCFMN          mcfmn_
  #define MCBMN          mcbmn_

  
  #define MCAVERAGECROSSSECTIONAREA  mcaveragecrosssectionarea_
  #define MCR0          mcr0_
  #define MCRMINOR      mcrminor_
  #define MCNPERIODS    mcnperiods_
  #define MCCOCURRENTSIGN mccocurrentsign_

  #define MCTRACELCMS     mctracelcms_
  #define MCCREATEBORDER  mccreateborder_
  #define MCISINSIDEBORDER mcisinsideborder_
  #define MCWRITE          mcwrite_

// mesh
  #define MCM3DGETRAYENTRYPOINT  mcm3dgetrayentrypoint_
  #define MCCREATEMESH    mccreatemesh_
  #define MCCREATEMESHUSINGSYMMETRY    mccreatemeshusingsymmetry_ 
  #define MCWRITEMESH     mcwritemesh_
  #define MCM3DCYL2S      mcm3dcyl2s_
  #define MCM3DXYZ2S      mcm3dxyz2s_
  #define MCM3DGETDBCYL   mcm3dgetdbcyl_
  #define MCM3DGETDBXYZ   mcm3dgetdbxyz_
  #define MCM3DGETGRADS   mcm3dgetgrads_
  #define MCM3DGETGRADSXYZ mcm3dgetgradsxyz_
  #define MCGETGRADSXYZ    mcgetgradsxyz_

  #define MCXYZISINSIDE   mcxyzisinside_
  #define MCCYLISINSIDE   mccylisinside_

  #define MCM3DGETALLXYZ  mcm3dgetallxyz_
  #define MCM3DGETALLCYL  mcm3dgetallcyl_

  #define MCM3DGETSBXYZ   mcm3dgetsbxyz_
  #define MCM3DGETSBCYL   mcm3dgetsbcyl_
  #define MCGETSBCYL      mcgetsbcyl_
  #define MCGETSBXYZ      mcgetsbxyz_
  #define MCGETBCYL       mcgetbcyl_

  #define MCM3DSETSMAX    mcm3dsetsmax_
  #define MCM3DGETSMAX    mcm3dgetsmax_
  #define MCGETBMINMAX    mcgetbminmax_
  #define MCRAXIS         mcraxis_
  #define MCGETEXTENT       mcgetextent_
  #define MCGETEXTENTLCMS   mcgetextentlcms_
  #define MCSEMIBOOZER2CYL  mcsemiboozer2cyl_
#elif defined( _WIN32 )
    #ifdef _MT
      #pragma message( "Multithread library" ) 
      static char* libDescription = "Multithread mconf library";
    #else
      #pragma message( "Singlethread library") 
     static char* libDescription = "Singlethread mconf library";
    #endif
#endif


template <class T> static inline T mmin (T x, T y) { return (x<y)?x:y; }
template <class T> static inline T mmax (T x, T y) { return (x>y)?x:y; }

static void strtrim(char *f)  // remove initial and trailing blanks
{
  char *p,*p2;
  int i=strlen(f);
  for(p=f; p!=f+i-1; p++) if(*p!=' ') break;
  i=strlen(p);
  for(p2=p+i-1; p2!=p; p2--) if(*p2!=' ') {*(p2+1)=0; break; }
  memmove(f,p,p2-p+2);
};

// remove initial and trailing blanks from FORTRAN string: character(i) fname 
static void strtrim(char *dest, char *fname, int i)  
{
  char fn[1000];
  char *p;
  // remove initial blanks
  for(p=fname; p!=fname+i-1; p++) if(*p!=' ') break;
  i -= p-fname;
  i = mmin(i,int(sizeof(fn))-1);
  strncpy(fn,p,i);
  fn[i]=0;
  // remove trailing blanks
  for(p=fn+i-1; p!=fn; p--) if(*p!=' ') {*(p+1)=0; break; }
  strcpy(dest,fn);
}


//*********************************************************************
extern "C" double __stdcall PROFILEFUNCTION(double *x,double *g)
{
  CPfunction profile;
  return profile.PF(*x,g);
}

//*********************************************************************
// 'destructor'
extern "C" void __stdcall MCFREE(C3dMesh **mConf)
{
  if (*mConf) delete (*mConf); 
  *mConf=0;
}

//*********************************************************************
// create object  C3dMesh
// and load Boozer-coordinate data file: *.bc; *.bc_old; *.bin4; *.bin8
// This function must be called before others.
// The first call must be made with *mConf==0, the following calls 
//  will reload configuration into the same object
// 
extern "C" int __stdcall MCLOAD(C3dMesh **mConf, char * fname, int i)
{
  clock_t tStart = clock();
  char fn[1024];
  strtrim(fn,fname,i);
  if (*mConf==0)
      *mConf = new C3dMesh;   // create object C3dMesh and return to caller the address
  bool ok = (*mConf)->load (fn);
  if(!ok) {
    cout << "MCLOAD: Loading error, file:'"<<fn<<"'\n";
    delete (*mConf);
    *mConf = 0;      // send NULL if errors
  }
//  (*mConf)->M3DpositionTestOff();
//  cout << "MCLOAD: Loading time=" << double(clock() - tStart)/CLOCKS_PER_SEC <<"\n";
  return ok?True:False;
}


//*********************************************************************
extern "C" int __stdcall MCISMESHOK(C3dMesh **mConf)
{
  bool b = (*mConf)->isMeshOK();
  return b?True:False;
}

//*********************************************************************
extern "C" int __stdcall MCISOK(C3dMesh **mConf)
{
  bool b = (*mConf)->isOK();
  return b?True:False;
}

//*********************************************************************
// set truncation level
extern "C" void __stdcall MCTRUNCATE(C3dMesh **mConf, double *epsTrunc)
{
  (*mConf)->truncate(*epsTrunc);
}

//*********************************************************************
// set accuracy of coordinate transformation
extern "C" void __stdcall MCSETACCURACY(C3dMesh **mConf, double *epsA)
{
  (*mConf)->setAccuracy(*epsA);
}

//*********************************************************************
// set value of B on magn. axis at cylindrical angle ficyl
extern "C" void __stdcall MCSETB0(C3dMesh **mConf, double *b, double *ficyl)
{
  (*mConf)->setB0(*b, *ficyl);
}

//*********************************************************************
// set minimum value of B on magn. axis
extern "C" void __stdcall MCSETMINB0(C3dMesh **mConf, double *b)
{
  (*mConf)->setB0(*b);
}

//*********************************************************************
// Get value of B on magn. axis at cylindrical angle ficyl
extern "C" double __stdcall MCGETB0(C3dMesh **mConf,double *ficyl)
{
  return (*mConf)->getB0(*ficyl);
}

//*********************************************************************
// set average B on magn. axis
extern "C" void __stdcall MCSETAVERAGEB0(C3dMesh **mConf, double *b)
{
  (*mConf)->setAverageB0(*b);
}

//*********************************************************************
// Get average B on magn. axis
extern "C" double __stdcall MCGETAVERAGEB0(C3dMesh **mConf)
{
  return (*mConf)->getAverageB0();
}

//*********************************************************************
// set average B on magn. axis
extern "C" void __stdcall MCSETAVERAGEBPHI0(C3dMesh **mConf, double *b)
{
  (*mConf)->setAverageBphi0(*b);
}

//*********************************************************************
// Get average B on magn. axis
extern "C" double __stdcall MCGETAVERAGEBPHI0(C3dMesh **mConf)
{
  return (*mConf)->getAverageBphi0();
}

//*********************************************************************
// Coordinate transformation
// Input :
//  cyl=(r,fi,z) - point in cyl. coordinates
// Output:
//  boozer=(s,theta,phi)
//  Bfield=(Br,Bfi,Bz) in cyl. coordinates
extern "C" void __stdcall MCCYL2BOOZER(C3dMesh **mConf,double *cyl,double *boozer,double *Bfield)
{
  Vector3d c,b;
  c.setFrom(cyl);                   // copy cyl to vector c
  if((*mConf)->cylIsInside(c) ) {   // function cylIsInside returns 'TRUE' if the point lies inside LCMS
    b = (*mConf)->cyl2boozer(c);    // b is a Vector3d(s,theta,phi)
    b.copyTo(boozer);               // copy to array
    b = (*mConf)->getBcyl();
    b.copyTo(Bfield);               // Bfield=(Br,Bfi,Bz) in cyl. coordinates
  }
  else {
    b = 0;
    b.copyTo(Bfield);
    b[0]=1e4;              // point lies outside plasma, set huge value of 's' as signal
    b.copyTo(boozer);      // copy to array
  }
}

//*********************************************************************
// Faster version of MCCYL2BOOZER, without testing whether point lies inside LCMS
// Coordinate transformation
// Input :
//  cyl=(r,fi,z) - point in cyl. coordinates
// Output:
//  boozer=(s,theta,phi)
//  Bfield=(Br,Bfi,Bz) in cyl. coordinates
extern "C" void __stdcall MCCYL2BOOZER2(C3dMesh **mConf,double *cyl,double *boozer,double *Bfield)
{
  Vector3d c(cyl),b;
  b = (*mConf)->cyl2boozer(c);    // b is a Vector3d(s,theta,phi)
  b.copyTo(boozer);               // copy to array
  b = (*mConf)->getBcyl();
  b.copyTo(Bfield);               // Bfield=(Br,Bfi,Bz) in cyl. coordinates
}

//*********************************************************************
// Coordinate transformation
// Input :
//  xyz -- point in cartesian coordinates
// Output:
//  boozer=(s,theta,phi)
//  Bfield  in cartesian coordinates
extern "C" void __stdcall MCXYZ2BOOZER2(C3dMesh **mConf,double *xyz,double *boozer,double *Bfield)
{
  Vector3d c(xyz),b;
  b = (*mConf)->xyz2boozer(c);    // b is a Vector3d(s,theta,phi)
  b.copyTo(boozer);               // copy to array
  b = (*mConf)->getBxyz();
  b.copyTo(Bfield);               // Bfield=(Bx,By,Bz) in cart. coordinates
}


//*********************************************************************
// Coordinate transformation,
// Find label of flux surface
// Input :
//  xyz -- point in cartesian coordinates
// Output:
// function returns label of flux surface -- normalized flux 's'
//
extern "C" double __stdcall MCXYZ2S(C3dMesh **mConf,double *xyz)
{
  Vector3d c(xyz);
  return (*mConf)->xyz2s(c);
}

//*********************************************************************
// Coordinate transformation,
// Find label of flux surface
// Input :
//  cyl -- point in cartesian coordinates
// Output:
// function returns label of flux surface -- normalized flux 's'
//
extern "C" double __stdcall MCCYL2S(C3dMesh **mConf,double *cyl)
{
  Vector3d c(cyl);
  return (*mConf)->cyl2s(c);
}

//*********************************************************************
extern "C" void __stdcall MCGETSBXYZ(C3dMesh **mConf,double *xyz,double *S,double *B)
{
  Vector3d c(xyz),b;
  *S=(*mConf)->xyz2s(c);
  b = (*mConf)->getBxyz();
  b.copyTo(B);               // Bfield=(Bx,By,Bz) in cart. coordinates
}

//*********************************************************************
extern "C" void __stdcall MCGETSBCYL(C3dMesh **mConf,double *cyl,double *S,double *B)
{
  Vector3d c(cyl),b;
  *S=(*mConf)->cyl2s(c);
  b = (*mConf)->getBcyl();
  b.copyTo(B);               // Bfield=(Br,Bfi,Bz) in cyl. coordinates
}


//*********************************************************************
// Input :
//  boozer=(s,theta,phi) -- point in Boozer coordinates
// Output:
//  B  -- magnetic field  in cylindrical coordinates
extern "C" void __stdcall MCGETBCYL(C3dMesh **mConf,double *boozer,double *B)
{
  Vector3d b,booz(boozer);
  (*mConf)->NJacobian (booz);
  b = (*mConf)->getBcyl();
  b.copyTo(B);               // Bfield=(Br,Bfi,Bz) in cyl. coordinates
}

//*********************************************************************
// s to toroidal Flux
extern "C" double __stdcall MCTORFLUX(C3dMesh **mConf,double *s)
{
  return (*mConf)->Flux(*s);
}

//*********************************************************************
// The method returns the direction of the toroidal current, which increases the absolute value of the iota.
// This current is called co-current. 
// The direction returned by this method is the direction with respect to the right-handed cylindrical coordinates system.
// @return 1 or -1 depending on the sign of the co-current in the right-handed cylindrical coordinates system.
extern "C" int __stdcall MCCOCURRENTSIGN(C3dMesh **mConf,double *s)
{
  return (*mConf)->getCoCurrentSign();
}

//*********************************************************************
// s to r_eff
extern "C" double __stdcall MCREFF(C3dMesh **mConf,double *s)
{
  return (*mConf)->r(*s);
}

//*********************************************************************
// Vprime(s)
extern "C" double __stdcall MCVPRIME(C3dMesh **mConf,double *s)
{
  return (*mConf)->Vprime(*s);
}

//*********************************************************************
// Volume(s)
extern "C" double __stdcall MCVOLUME(C3dMesh **mConf,double *s)
{
  return (*mConf)->Volume(*s);
}

//*********************************************************************
// Fraction of trapped particles on surface s
extern "C" double __stdcall MCFTRAPPED(C3dMesh **mConf,double *s)
{
  return (*mConf)->ftrapped(*s);
}

//*********************************************************************
// <B> on surface s
extern "C" double __stdcall MCBAVRG(C3dMesh **mConf,double *s)
{
  return (*mConf)->Bavrg(*s);
}

//*********************************************************************
// <B^2> on surface s
extern "C" double __stdcall MCB2AVRG(C3dMesh **mConf,double *s)
{
  return (*mConf)->B2avrg(*s);
}

//*********************************************************************
// <(1-b)^0.5>; b = B(s,th,phi)/Bmax(s)
extern "C" double __stdcall MCDB12AVRG(C3dMesh **mConf,double *s)
{
  return (*mConf)->db12Avrg(*s);
}

//*********************************************************************
// <(1-b)^1.5>
extern "C" double __stdcall MCDB32AVRG(C3dMesh **mConf,double *s)
{
  return (*mConf)->db32Avrg(*s);
}

//*********************************************************************
// <b(1-b)^0.5>
extern "C" double __stdcall MCBDB12AVRG(C3dMesh **mConf,double *s)
{
  return (*mConf)->db12Avrg(*s)-(*mConf)->db32Avrg(*s);;
}

//*********************************************************************
// <p> -- flux average of pitch angle on the surface
//   p_avrg(s,x) = <sqrt(1-x*B(s,th,ph)/Bmax(s))>, where 0<=x<=1, 0<=s<=1,
// and definition of x is the following x=(1-p^2)/b, b=B/Bmax
extern "C" double __stdcall MCPAVRG(C3dMesh **mConf,double *s,double *x)
{
  return (*mConf)->pavrg(*s,*x);
}

//****************************************************************************
// @return Integral(from x to 1) dx'/p_avrg(s,x') , 
//         where p_avrg(s,x) = <sqrt(1-x*B(s,th,ph)/Bmax(s))> 0<=x<=1, 0<=s<=1
extern "C" double __stdcall MCINTINVPAVRG(C3dMesh **mConf,double *s,double *x)
{
  return (*mConf)->integralInvPavrg(*s,*x);
}

//*********************************************************************
// fabs(Jacobian*B^2) on surface s for (s,theta,phi)-coordinates
extern "C" double __stdcall MCJACOBIANB2(C3dMesh **mConf,double *s)
{
  return (*mConf)->JacobianB2(*s);
}

//*********************************************************************
// iota(s)
extern "C" double __stdcall MCIOTA(C3dMesh **mConf,double *s)
{
  return (*mConf)->iota(*s);
}

//*********************************************************************
// @return normalized poloidal flux
extern "C" double __stdcall MCTORFLUX2POLFLUX(C3dMesh **mConf,double *s)
{
  return (*mConf)->sPol(*s);
}

//*********************************************************************
// R0(s)
extern "C" double __stdcall MCR0(C3dMesh **mConf)
{
  return (*mConf)->R0();
}

//*********************************************************************
// Rmn(s)
extern "C" double __stdcall MCRMN(C3dMesh **mConf, int *m, int *n, double *s)
{
  return (*mConf)->Rmn_sp(*s,*m,*n);
}

//*********************************************************************
// Zmn(s)
extern "C" double __stdcall MCZMN(C3dMesh **mConf, int *m, int *n, double *s)
{
  return (*mConf)->Zmn_sp(*s,*m,*n);
}

//*********************************************************************
// Phimn(s)
extern "C" double __stdcall MCFMN(C3dMesh **mConf, int *m, int *n, double *s)
{
  return (*mConf)->Phimn_sp(*s,*m,*n);
}

//*********************************************************************
// Bmn(s)
extern "C" double __stdcall MCBMN(C3dMesh **mConf, int *m, int *n, double *s)
{
  return (*mConf)->Bmn_sp(*s,*m,*n);
}

//*********************************************************************
// The method returns the average area of the cross section of the surface \e s
// @param s is the normalized toroidal flux.
extern "C" double __stdcall MCAVERAGECROSSSECTIONAREA(C3dMesh **mConf,double *s)
{
  return (*mConf)->averageCrossSectionArea(*s);
}

//*********************************************************************
// rminor(s)
extern "C" double __stdcall MCRMINOR(C3dMesh **mConf)
{
  return (*mConf)->rminor();
}

//*********************************************************************
// rminor(s)
extern "C" int __stdcall MCNPERIODS(C3dMesh **mConf)
{
  return (*mConf)->nPeriods ();
}

//*********************************************************************
// Find intersection of the Ray with the last surface.
// The ray is R(t)=r0+rd*t  with t>0
// The origin of ray must be outside of the last surface
//
// INPUT:
//   Real *8 R1(3)  -- the first(origin) and
//   Real *8 R2(3)  --   the second point of the ray (cartesian coord.)
// OUTPUT:
//   Real *8 Entry(3) -- entry point of the ray into plasma (cartesian coord.)
//   Real *8 Exit(3)  -- exit  point of the ray from plasma (cartesian coord.)
//
/// The method traces the Last Closed Magnetic Surface (LCMS).
/// The method finds intersections of the ray with the LCMS, where the
/// ray is R(t)=r1+(r2-r1)/|r2-r1|*t,  and t>=0.
/// @param R1 is the cartesian coordinates of the first point of the ray, must be outside with respect to the LCMS.
/// @param R2 is the cartesian coordinates of the second point of the ray
/// @param Entry this vector at return contains the coordinates of the entry point of the ray into plasma
/// @param Exit  this vector at return contains the coordinates of the exit point
/// @return
/// \li -2 -- the origin r1 of the ray lies inside LCMS;
/// \li -1 -- the ray does not enters into plasma;
/// \li  0 -- segment r1--r2 is outside plasma;
/// \li  1 -- only entry point lies on the segment r1--r2, i.e. r2 is inside plasma;
/// \li  2 -- entry and exit points lie on the segment r1--r2, i.e. r1 and r2 are outside plasma;
///
///  if returned value >= 0 then entry and exit points of the ray are in \e entrypoint and \e exitpoint
extern "C" int __stdcall MCTRACELCMS(C3dMesh **mConf,double *R1,double *R2,double *Entry,double *Exit)
{
  Vector3d r1(R1),r2(R2); // 1st and 2nd points of the ray
  Vector3d r0(R1),rd(R2); // origin and direction of the ray
  rd  = r2-r0;            // direction of the ray
  rd /= rd.abs();         // normalize; the ray is r(t)=r0+rd*t  with t>0
  Vector3d ent(0), ex(0); // zero output
  ent.copyTo(Entry);
  ex.copyTo(Exit);

  if(!(*mConf)->isOK()) return -10;
  if((*mConf)->xyzIsInside(r0)) return -2; // if TRUE then point r0 lies inside LCMS

  double dphi = 1*degree; // change this if you need more accurate calculations

  (*mConf)->traceLast(r0,rd,dphi,dphi);

  int size     = (*mConf)->getTraceSize();
  const Vector3d *R  = (*mConf)->getTraceRxyz();
  if(size==0) return -1; // ray does not enter in plasma

  ent = R[0];  // point where ray enters into plasma
  ex  = R[1];  // point where ray exits  plasma
  ent.copyTo(Entry);
  ex.copyTo(Exit);

  double r21 = (r2-r1).abs2();
  double e1 = (ent-r1).abs2();
  double x1 = (ex -r1).abs2();
//  CRayTrace::freeTrace(); // ?
//1  std::cout<< double(clock() - tStart)/CLOCKS_PER_SEC<<" traceLCMS"<<std::endl;
  if(e1> r21) return 0;  // segment r1--r2 is outside plasma
  if(x1> r21) return 1;  // only entry point lies on the segment r1--r2, i.e. r2 is inside plasma
  if(x1<=r21) return 2;  // entry and exit points lie on the segment r1--r2
  return 0;
}

//*********************************************************************
// Find entry point of the Ray into the plasma(last closed surface)
// The ray is R(t)=r0+rd*t  with t>0
// It is assumed, that the origin of ray lies outside of the last surface
//
// INPUT:
//   r0, rd  -- origin and direction of the ray in cartesian coordinates
// OUTPUT:
//  entryPoint 
//  @return  if the method succeeds, the return value is true; false otherwise.
extern "C" int __stdcall MCM3DGETRAYENTRYPOINT(C3dMesh **mConf, double *r0,double *rd, double *entryPoint)
{
  Vector3d R0(r0),Rd(rd); // origin and direction of the ray
  Vector3d entry;
  bool b = (*mConf)->M3DgetRayEntryPoint(R0,Rd,entry); 
  entry.copyTo(entryPoint);
  return b?True:False;
}

//****************************************************************************
// create border 'slast' of last contour at cylindrical angle 'ficyl'
// borderSize -- # of point in contour
// ficyl -- cylindrical angle
//  function returns 0 if no memory
extern "C" int __stdcall MCCREATEBORDER(C3dMesh **mConf,double *slast,double *ficyl,int *BrdSize)
{
  bool b = (*mConf)->createBorder(*slast,*ficyl,*BrdSize);
  return b?True:False;
}

//****************************************************************************
// MCCREATEBORDER must be callled first!
// INPUT:
//  REAL*8 XYZ(3) -- point in cartesian coordinates
// OUTPUT:
//  function returns 'TRUE' if the point lies inside Border
//
extern "C" int __stdcall MCISINSIDEBORDER(C3dMesh **mConf,double *XYZ)
{
  Vector3d xyz(XYZ);
  double rc = sqrt(xyz.x()*xyz.x()+xyz.y()*xyz.y());
  double fi = (*mConf)->mod2pi(atan2(xyz.y(),xyz.x()));
  double zc = xyz.z();
  Vector3d cyl(rc,fi,zc);
  bool b = (*mConf)->cylIsInsideBorder(cyl);
  return b?True:False;
}

//****************************************************************************
//****************************************************************************
//****************************************************************************
//The function creates 3d-mesh.
// The function tabulates the magnetic field, the flux surface label, and grad(s) in cylindrical coordinates.
extern "C" int __stdcall MCCREATEMESH(C3dMesh **mConf,double *dr,double *dz,double *dfi)
{
  bool b = (*mConf)->createMesh(*dr,*dz,*dfi);
  return b?True:False;
}

//The function creates 3d-mesh.
// The function tabulates the magnetic field, the flux surface label, and grad(s) in cylindrical coordinates.
// Stellarator symmetry is used
extern "C" int __stdcall MCCREATEMESHUSINGSYMMETRY(C3dMesh **mConf,double *dr,double *dz,double *dfi)
{
  bool b = (*mConf)->createMeshUsingSymmetry(*dr,*dz,*dfi);
  return b?True:False;
}


//*********************************************************************
// get B and derivatives dB/dx,dB/dy,dB/dz in point xyz
//  Input:
//    Real *8 xyz(3) -- cartesian coordinates
//  Output:
//    Real *8 B(3) -- cartesian coordinates of B
//    Real *8 dBdx(3) -- dB/dx, where dBdx(1)=dBx/dx, dBdx(2)=dBy/dx, dBdx(3)=dBx/dx
//    Real *8 dBdy(3) -- dB/dy
//    Real *8 dBdz(3) -- dB/dz
//
extern "C" void __stdcall MCM3DGETDBXYZ(C3dMesh **mConf,double *xyz,double *B,double *dBdx,double *dBdy,double *dBdz)
{
  Vector3d R(xyz);
  Vector3d dbdx, dbdy, dbdz;

  Vector3d b=(*mConf)->M3DgetdBxyz(R,dbdx,dbdy,dbdz);
  b.copyTo(B);
  dbdx.copyTo(dBdx);
  dbdy.copyTo(dBdy);
  dbdz.copyTo(dBdz);
}

//*********************************************************************
// get B and derivatives dB/dr, dB/dfi/r, dB/dz in point cyl
//  Input:
//    Real *8 cyl(3) -- cylindrical coordinates
//  Output:
//    Real *8 B(3) -- cylindrical coordinates of B
//    Real *8 dBdr(3)   -- dB/dr, where dBdr(1)=dBr/dr, dBdr(2)=dBfi/dr, dBdr(3)=dBz/dr
//    Real *8 dBdfir(3) -- dB/dfi/r
//    Real *8 dBdz(3)   -- dB/dz
//
extern "C" void __stdcall MCM3DGETDBCYL(C3dMesh **mConf,double *cyl,double *B,double *dBdr,double *dBdfir,double *dBdz)
{
  Vector3d R(cyl);
  Vector3d dbdr, dbdfir, dbdz;

  Vector3d b=(*mConf)->M3DgetdBcyl(R,dbdr,dbdfir,dbdz);
  b.copyTo(B);
  dbdr.copyTo(dBdr);
  dbdfir.copyTo(dBdfir);
  dbdz.copyTo(dBdz);
}


//*********************************************************************
// Coordinate transformation using 3d mesh interpolation
// Find label of flux surface
// Input :
//  xyz -- point in cartesian coordinates
// Output:
// function returns label of flux surface -- normalized flux 's'
//
extern "C" double __stdcall MCM3DXYZ2S(C3dMesh **mConf,double *xyz)
{
  Vector3d c(xyz);
  return (*mConf)->M3Dxyz2s(c);
}

//*********************************************************************
// Coordinate transformation using 3d mesh interpolation
// Find label of flux surface
// Input :
//  cyl -- point in cartesian coordinates
// Output:
// function returns label of flux surface -- normalized flux 's'
extern "C" double __stdcall MCM3DCYL2S(C3dMesh **mConf,double *cyl)
{
  Vector3d c(cyl);
  return (*mConf)->M3Dcyl2s(c);
}

//*********************************************************************
//   function retuns grad(s) = Vector3d(ds/dr, ds/dfi/r, ds/dz)
//  Input:
//    Real *8 cyl(3) -- cylindrical coordinates
//  Output:
//    Real *8 grads(3) -- Vector3d(ds/dr, ds/dfi/r, ds/dz)
extern "C" void __stdcall MCM3DGETGRADS(C3dMesh **mConf,double *cyl,double *grads)
{
  Vector3d R(cyl);
  Vector3d g=(*mConf)->M3DgetGrads(R);
  g.copyTo(grads);
}

//*********************************************************************
//   function retuns grad(s) in cartesian coordinates
//  Input:
//    Real *8 xyz(3) -- cartesian coordinates
//  Output:
//    Real *8 grads(3) --  grads in cartesian coordinates
extern "C" void __stdcall MCM3DGETGRADSXYZ(C3dMesh **mConf,double *xyz,double *grads)
{
  Vector3d R(xyz);
  Vector3d g=(*mConf)->M3DgetGradsxyz(R);
  g.copyTo(grads);
}

//*********************************************************************
//   function retuns grad(s) in cartesian coordinates
//  Input:
//    Real *8 xyz(3) -- cartesian coordinates
//  Output:
//    Real *8 grads(3) --  grads in cartesian coordinates
extern "C" void __stdcall MCGETGRADSXYZ(C3dMesh **mConf,double *xyz,double *grads)
{
  Vector3d R(xyz);
  double s=(*mConf)->xyz2s(R);
  Vector3d g = (*mConf)->getGradsxyz();
  g.copyTo(grads);
}
//****************************************************************************
// INPUT:
//  REAL*8 XYZ(3) -- point in cartesian coordinates
// OUTPUT:
//  function returns 'TRUE' if the point lies inside LCMS
extern "C" int __stdcall MCXYZISINSIDE(C3dMesh **mConf,double *XYZ)
{
  Vector3d xyz(XYZ);
  bool b = (*mConf)->xyzIsInside(xyz);
  return b?True:False;
}

//****************************************************************************
// INPUT:
//  REAL*8 cyl(3) -- point in cartesian coordinates
// OUTPUT:
//  function returns 'TRUE' if the point lies inside LCMS
extern "C" int __stdcall MCCYLISINSIDE(C3dMesh **mConf,double *cyl)
{
  Vector3d c(cyl);
  bool b = (*mConf)->cylIsInside(c);
  return b?True:False;
}

//*********************************************************************
// save mesh file
// *.bin4; *.bin8 are supported
extern "C" int __stdcall MCWRITEMESH(C3dMesh **mConf,char * fname,int i)
{
  char fn[1024];
  strtrim(fn,fname,i);
  bool ok= (*mConf)->writeMesh(fn);
  if(!ok)
    cout << "mcWriteMesh: writing error\n";
  return ok?True:False;
}


//*********************************************************************
// save mesh file
// *.bin4; *.bin8 are supported
extern "C" int __stdcall MCWRITE(C3dMesh **mConf,char * fname,int i)
{
  char fn[1024];
  strtrim(fn,fname,i);
  bool ok= (*mConf)->write(fn);
  if(!ok)
    cout << "mcWrite: writing error\n";
  return ok?True:False;
}

//*********************************************************************
// get B and derivatives dB/dx,dB/dy,dB/dz in point xyz
//  Input:
//    Real *8 xyz(3) -- cartesian coordinates
//  Output:
//    Real *8 B(3) -- cartesian coordinates of B
//    Real *8 dBdx(3) -- dB/dx, where dBdx(1)=dBx/dx, dBdx(2)=dBy/dx, dBdx(3)=dBx/dx
//    Real *8 dBdy(3) -- dB/dy
//    Real *8 dBdz(3) -- dB/dz
//
/// @return \b s (normalized toroidal flux), which normally satisfies \b 0<=s<=1; \n
/// <b>1<s<1.5</b> means that point lies outside LCMS in the extrapolated region; \n
/// \b s>1000 means that point lies far outside from LCMS; \n
///
extern "C" void __stdcall MCM3DGETALLXYZ(C3dMesh **mConf,double *xyz,double *S,double *B,double *gradS,double *gradB)
{
  Vector3d R(xyz);
  Vector3d b,dbdx,dbdy,dbdz,gs;

  *S = (*mConf)->M3DgetdBGradsxyz(R,b,dbdx,dbdy,dbdz,gs);
  b.copyTo(B);
  gs.copyTo(gradS);
//#define DB(row,col)  dB[(col)*3 + (row)] // Fortran77 style addressing
  dbdx.copyTo(gradB);  // dB(i,j) = dB_i/dx_j
  dbdy.copyTo(gradB+3);
  dbdz.copyTo(gradB+6);
}

//*********************************************************************
// Coordinate transformation using 3d mesh interpolation
// Find label of flux surface and B
// Input :
//  xyz -- point in cartesian coordinates
// Output:
// function returns label of flux surface -- normalized flux 's'
//
extern "C" void __stdcall MCM3DGETSBXYZ(C3dMesh **mConf,double *xyz,double *S,double *B)
{
  Vector3d R(xyz);

  *S = (*mConf)->M3Dxyz2s(R);
  if(*S<0) *S = (*mConf)->xyz2s(R);
  ((*mConf)->M3DgetBxyz(R)).copyTo(B);
}

//*********************************************************************
/// @return \b s (normalized toroidal flux), which normally satisfies \b 0<=s<=1; \n
/// <b>1<s<1.5</b> means that point lies outside LCMS in the extrapolated region; \n
/// \b s>1000 means that point lies far outside from LCMS; \n
///
extern "C" void __stdcall MCM3DGETALLCYL(C3dMesh **mConf,double *cyl,double *S,double *B,double *gradS,double *gradB)
{
  Vector3d R(cyl);
  Vector3d b,dbdr,dbdfir,dbdz,gs;

  *S = (*mConf)->M3DgetdBGradscyl(R,b,dbdr,dbdfir,dbdz,gs);
  b.copyTo(B);
  gs.copyTo(gradS);
  dbdr.copyTo(gradB);  // dB(i,j) = dB_i/dx_j
  dbdfir.copyTo(gradB+3);
  dbdz.copyTo(gradB+6);
}

//*********************************************************************
// Coordinate transformation using 3d mesh interpolation
// Find label of flux surface and B
// Input :
//  xyz -- point in cartesian coordinates
// Output:
// function returns label of flux surface -- normalized flux 's'
//
extern "C" void __stdcall MCM3DGETSBCYL(C3dMesh **mConf,double *cyl,double *S,double *B)
{
  Vector3d R(cyl);
  *S = (*mConf)->M3Dcyl2s(R);
  if(*S<0) *S = (*mConf)->cyl2s(R);
  ((*mConf)->M3DgetBcyl(R)).copyTo(B);
}


//*********************************************************************
//
extern "C" void __stdcall MCGETBMINMAX(C3dMesh **mConf,double *s,double *Bmin,double *Bmax )
{
  *Bmin=(*mConf)->Bmin(*s);
  *Bmax=(*mConf)->Bmax(*s);
}

//*********************************************************************
//
extern "C" double __stdcall MCBMIN(C3dMesh **mConf,double *s)
{
  return (*mConf)->Bmin(*s);
}

//*********************************************************************
//
extern "C" double __stdcall MCBMAX(C3dMesh **mConf,double *s)
{
  return (*mConf)->Bmax(*s);
}


//*********************************************************************
//
extern "C" double __stdcall MCRAXIS(C3dMesh **mConf,double *ficyl)
{
  return ((*mConf)->semiboozer2cyl(0,0,*ficyl))[0];
}

//*********************************************************************
//
extern "C" void __stdcall MCSEMIBOOZER2CYL(C3dMesh **mConf,double *semiboozer,double *cyl)
{
  Vector3d sb(semiboozer);
  Vector3d c = (*mConf)->semiboozer2cyl(sb);
  c.copyTo(cyl);               // copy to array cyl
}

//*********************************************************************
//  Return the sizes of a rectangle circumscribed about the cross section of the surface s at cylindrical angle fi.
extern "C" void __stdcall MCGETEXTENT(C3dMesh **mConf,double *s,double *fi,double *Rmin,double *Rmax,double *Zmin,double *Zmax)
{
  (*mConf)->getExtent(*Rmin,*Rmax,*Zmin,*Zmax,*s,*fi);
}

//*********************************************************************
//  Return the sizes of a rectangle circumscribed about the cross section of the LCMS.
extern "C" void __stdcall MCGETEXTENTLCMS(C3dMesh **mConf,double *Rmin,double *Rmax,double *Zmin,double *Zmax)
{
  (*mConf)->getExtentLCMS(*Rmin,*Rmax,*Zmin,*Zmax);
}

//*********************************************************************
//
extern "C" void __stdcall MCM3DSETSMAX(C3dMesh **mConf,double *sMax)
{
  (*mConf)->setsMax(*sMax);
}

//*********************************************************************
//
extern "C" double __stdcall MCM3DGETSMAX(C3dMesh **mConf)
{
  return (*mConf)->getsMax();
}
