SUBDIRS = ebe_run ebe_integration
SUBDIRS_OLD = conv_eff ray_tracing ebw_wkb test_profiles integrace waist_position waist_data

# 'phony' targets are not filenames ==> will be executed always
.PHONY : $(SUBDIRS) $(SUBDIRS_OLD)
.PHONY : all clean clean_ebe doc clean_outputs luke

all: $(SUBDIRS) pymodules luke

$(SUBDIRS):
ifneq ($(SITE),)
	$(MAKE) -C $@
else
	@echo "Error: SITE must be defined"
	@false
endif

clean:
	for dir in $(SUBDIRS); do \
	$(MAKE) clean -C $$dir; \
	done
	rm -f pyAMR/*.so

realclean: clean
	for dir in $(SUBDIRS); do \
	$(MAKE) realclean -C $$dir; \
	done
	rm -f pyAMR/antenna_mast.so

clean_ebe:
	@./utils/clean_ebe
ebeclean: clean_ebe
ebe_clean: clean_ebe

clean_outputs:
	@./utils/clean_outputs

doc:
	svn log > doc/svn.log
	doc/makedoc '$(PREP)' './source' './doc/pre'
	doxygen doc/Doxyfile
	rm -f doc/svn.log doc/pre/*

depend:
	for dir in $(SUBDIRS); do \
	$(MAKE) depend -C $$dir; \
	done

show:
	for dir in $(SUBDIRS); do \
	$(MAKE) show -C $$dir; \
	done

pymodules: pyAMR/antenna_mast.so pyAMR/rwefit.so
pyAMR/antenna_mast.so: source/antenna_mast.F90
	f2py -c -m antenna_mast $(F2PYOPTS) source/antenna_mast.F90
	mv antenna_mast.so pyAMR
pyAMR/rwefit.so: source/rwefit.c source/rwgfile.f source/rwafile.f
	$(PYF) $(PYFOPTS) -c source/rwgfile.f -o obj/rwgfile.o
	$(PYF) $(PYFOPTS) -c source/rwafile.f -o obj/rwafile.o
	f2py -c -m rwefit source/rwefit.c obj/rwafile.o obj/rwgfile.o $(PYFLIBS)
	mv rwefit.so pyAMR

luke: output/luke/ebe6_defaults.txt
#	ln -f -s $(LUKEPATH)/Project_DKE/Modules/AMR/rundke_AMR.m ./

output/luke/ebe6_defaults.txt: ebe6_defaults.cfg
	python pyAMR/pyLUKE.py extractConfig ebe6_defaults.cfg output/luke/ebe6_defaults.txt

include make.flags
