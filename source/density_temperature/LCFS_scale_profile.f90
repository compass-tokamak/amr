subroutine LCFS_scale_profile(n,psi_grid,prof_data,scale_factor,scale_break)
  implicit none
  integer(4),intent(in) :: n
  real(8),intent(in) :: psi_grid(:)
  real(8),intent(inout) :: prof_data(:)
  real(8),intent(in) :: scale_factor,scale_break
  real(8) :: psi_break,x
  integer(4) :: i

  if (scale_factor==1) return
  psi_break = getPsiAxis() + (getPsiLCFS() - getPsiAxis()) * scale_break
  do i=1,n
    if (psi_grid(i)>getPsiLCFS()) then
      prof_data(i) = prof_data(i) * scale_factor
    else if (psi_grid(i)>psi_break) then
      x = (psi_grid(i)-psi_break)/(getPsiLCFS()-psi_break)
      prof_data(i) = prof_data(i) * scale_poly(x,scale_factor)
    endif 
  enddo

end subroutine

real(8) function scale_poly(x,fac)
  real(8),intent(in) :: x,fac

  scale_poly = 1+(-3+3*fac)*x**2+(2-2*fac)*x**3
end function
