function inp_fname,shot,time,prefix

; returs filename of input file for EBE simulation
; to be placed in the input directory
; format is prefix_shot_time.out

if ((shot ge 100000) and (shot le 999999)) then begin
  file_name= string(prefix,shot,time,format='(A,''_'',I6,''_'',F5.3,''.out'')')
endif
if ((shot ge 10000) and (shot le 99999)) then begin
  file_name= string(prefix,shot,time,format='(A,''_0'',I5,''_'',F5.3,''.out'')')
endif
if ((shot ge 1000) and (shot le 9999)) then begin
  file_name= string(prefix,shot,time,format='(A,''_00'',I4,''_'',F5.3,''.out'')')
endif
if ((shot ge 100) and (shot le 999)) then begin
  file_name= string(prefix,shot,time,format='(A,''_000'',I3,''_'',F5.3,''.out'')')
endif
if ((shot ge 10) and (shot le 99)) then begin
  file_name= string(prefix,shot,time,format='(A,''_0000'',I2,''_'',F5.3,''.out'')')
endif
if ((shot ge 0) and (shot le 9)) then begin
  file_name= string(prefix,shot,time,format='(A,''_00000'',I1,''_'',F5.3,''.out'')')
endif

file_path = '../input/'
return,(file_path+file_name)

end      

