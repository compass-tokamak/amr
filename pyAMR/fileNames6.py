## @package fileNames6
#  Python module with file naming function.

import sys

## Return array of integrated profiles file names.
#
# Particularly [int_profiles_reff, int_profiles_reff_ce, int_profiles_R, int_profiles_R_ce]
# with corresponding suffixes and relative path (ebe_run/output). 
def intProfiles(iAnt,iConf,time):
    res = []
    res.append('ebe_integration/output/int_profiles_reff'+ \
               getAntConfSuffix(iAnt,iConf) + \
               getTimeSuffix(time)+'.dat')
    res.append('ebe_integration/output/int_profiles_reff_ce'+ \
               getAntConfSuffix(iAnt,iConf) + \
               getTimeSuffix(time)+'.dat')
    res.append('ebe_integration/output/int_profiles_R'+ \
               getAntConfSuffix(iAnt,iConf) + \
               getTimeSuffix(time)+'.dat')
    res.append('ebe_integration/output/int_profiles_R_ce'+ \
               getAntConfSuffix(iAnt,iConf) + \
               getTimeSuffix(time)+'.dat')
    return res

## Return array of output (summed) integrated profiles file names.
#
# Particularly [int_profiles_reff, int_profiles_reff_ce, int_profiles_R, int_profiles_R_ce]
# with corresponding suffixes. 
def summedIntProfiles(iConf,time):
    res = []
    res.append('sum_int_profiles_reff'+ \
               getConfSuffix(iConf) + \
               getTimeSuffix(time)+'.dat')
    res.append('sum_int_profiles_reff_ce'+ \
               getConfSuffix(iConf) + \
               getTimeSuffix(time)+'.dat')
    res.append('sum_int_profiles_R'+ \
               getConfSuffix(iConf) + \
               getTimeSuffix(time)+'.dat')
    res.append('sum_int_profiles_R_ce'+ \
               getConfSuffix(iConf) + \
               getTimeSuffix(time)+'.dat')
    return res

def getAntSuffix(iAnt): 
    if iAnt>=100:
        print 'error: wrong iAnt in getAntConfSuffix'
        sys.exit(1)
    return ('_%02i' % iAnt)

def getAntConfSuffix(iAnt,iConf):
    return getAntSuffix(iAnt) + getConfSuffix(iConf)

def getConfSuffix(iConf):
    if (iConf>=10000):
        print 'error: wrong iConf in getAntConfSuffix'
        sys.exit(1)
    return '_%04i' % iConf

def getTimeSuffix(time):
    timesuf = '%5.3f' % time
    return '_'+timesuf

def getShotTimeSuffix(shotn,time):
    timesuf = '%5.3f' % time
    shotsuf = '%06u' % shotn
    return '_' + shotsuf + '_' + timesuf

def getShotSuffix(shotn):
    shotsuf = '%06u' % shotn
    return '_' + shotsuf

def getIRaySuffix(iRay):
    return '_%02u' % iRay

def getRTFileName(shotn,time,iAnt,iConf,iRay):
    return 'RT' + getRTFileSuffix(shotn,time,iAnt,iConf,iRay) + '.dat'

def getDPFileName(shotn,time,iAnt,iConf,iRay):
    return 'DP' + getRTFileSuffix(shotn,time,iAnt,iConf,iRay) + '.dat'

def getRTFileSuffix(shotn,time,iAnt,iConf,iRay):
    return getAntConfSuffix(iAnt,iConf) + getShotTimeSuffix(shotn,time) + getIRaySuffix(iRay)

def getIntProfFileName(iAnt,iConf,time,proftype):
    return 'int_profiles_'+proftype+getAntConfSuffix(iAnt,iConf)+getTimeSuffix(time)+'.dat'

def getRayPDFileName(iAnt,iConf,shotn,time,iRay,proftype):
    return 'pd_'+proftype+getAntConfSuffix(iAnt,iConf)+getShotTimeSuffix(shotn,time)+getIRaySuffix(iRay)+'.dat'

def getLUKEFileName(experiment,shotn,time,iAnt,iConf):
    return 'LUKE_RESULTS_'+experiment+getShotSuffix(shotn)+getTimeSuffix(time)+'_AMR'+getAntConfSuffix(iAnt,iConf)+'.mat'

def getWKBcurveFileName(iAnt,iConf,shotn,time,iRay):
    return 'wkbcurve'+getAntConfSuffix(iAnt,iConf)+getShotTimeSuffix(shotn,time)+getIRaySuffix(iRay)+'.dat'

def getEqdskFileName(shotn,time):
    return 'eqdsk'+getShotTimeSuffix(shotn,time)+'.dat'
