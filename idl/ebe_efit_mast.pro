pro ebe_efit_mast,shot,time,equil_ver

; psi is OK on MAST
SGNPSIORIG = +1

source = 'MAST::' + strtrim(string(shot),2)

; get Psi(R,Z,t)
trace = getdata('EFM_psi(R,Z)', source)
t_efit = trace.time
rpsi = trace.x
zpsi = trace.y

;   calculate index of ts time in EFIT
itime = where(abs(t_efit - time) eq min(abs(t_efit -time)))

; extract the value at this time
psi = trace.data(*,*,itime)
; change the sign of psi
psi = SGNPSIORIG*psi

trace = getdata('EFM_BVAC_VAL', source)
Bvac_val = trace.data(itime)
trace = getdata('EFM_BVAC_R', source)
BvacR = Bvac_val*trace.data(itime)

trace = getdata('EFM_psi_axis', source)
psi_a = SGNPSIORIG*trace.data(itime)

trace = getdata('EFM_psi_boundary', source)
psi_b = SGNPSIORIG*trace.data(itime)

trace = getdata('EFM_p(psi)_(C)', source)
p_psi = trace.data(*,itime)

trace = getdata('EFM_f(psi)_(C)', source)
f_psi = trace.data(*,itime)

trace = getdata('EFM_q(psi)_(C)', source)
q_psi = trace.data(*,itime)

; read EFIT R of separatrix on HFS and LFS 
trace = getdata('EFM_R(Psi100)_IN', source)
rmidin = trace.data(itime)
trace = getdata('EFM_R(Psi100)_OUT', source)
rmidout = trace.data(itime)

; read EFIT R,Z of magnetic axis
trace = getdata('EFM_magnetic_axis_R', source)
rmaxis = trace.data(itime)
trace = getdata('EFM_magnetic_axis_Z', source)
zmaxis = trace.data(itime)

;;       end of reading of experimental data
;:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
;

; Write data to an ASCII file
out_fname = inp_fname(shot,time,'psi')
openw,1,out_fname
printf,1,FORMAT='(I8,'' '',F7.4,'' '',I3)',shot,t_efit(itime),n_elements(rpsi)
printf,1,'Bvac*R'
printf,1,BvacR
printf,1,'psi'
printf,1,psi
printf,1,'rpsi'
printf,1,rpsi
printf,1,'zpsi'
printf,1,zpsi
printf,1,'f_psi'
printf,1,f_psi
printf,1,'psi_a,b'
printf,1,psi_a
printf,1,psi_b
printf,1,'p_psi'
printf,1,p_psi
printf,1,'q_psi'
printf,1,q_psi
printf,1,'Rmaxis,Zmaxis - magnetic axis'
printf,1,rmaxis
printf,1,zmaxis
printf,1,'Rmidin,Rmidout - HFS and LFS separatrix'
printf,1,rmidin
printf,1,rmidout
close,1

; end of procedure
end      
