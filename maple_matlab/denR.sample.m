fname = 'ts.dat';
% den_R = '.88*exp(-1/15.625e6*(R-0.7)^6)+.12*exp(-1/40*(R-0.7)^2)';
den_R = '.88*exp(-1/100.0e9*(1000*R-700.0)^6)+.12*exp(-1/40000*(1000*R-700.0)^2)';
den0 = 0.4e14;
temp_R = 'exp(-156.2500000*(R-.7)^2)+0.1e-1';
temp0 = 0.05;
R0 = 0.5;
R1 = 0.85;
N = 150;
phi0 = 0;
Z0 = 0;

Rgrid = R0:(R1-R0)/(N-1):R1;
phi = phi0*ones(size(Rgrid));
Z = Z0*ones(size(Rgrid));
den = ones(size(Rgrid));
temp = ones(size(Rgrid));

for i=1:N
    R = Rgrid(i);
    den(i) = den0*eval(den_R);
    temp(i) = temp0*eval(temp_R);
end

fig1 = figure();
subplot(2,2,1);
plot(Rgrid,den);
f_pe = 8.98e3*sqrt(den)/1e9;
subplot(2,2,2);
plot(Rgrid,f_pe);
subplot(2,2,3);
plot(Rgrid,temp);

fid = fopen(fname,'w');
fprintf(fid,'#R,phi,Z,den,Temp\n');
for i=1:N
    fprintf(fid,'%.5e %.5e %.5e %.5e %.5e\n',[Rgrid(i) phi(i) Z(i) den(i) temp(i)]);
end
