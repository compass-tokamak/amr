pro ebe_data_nstx

openr,2,'ebe_data.in'
readf,2,ebe_shotn
ebe_shotn=long(ebe_shotn)
; print,ebe_shotn,format='(''Shot number: '',I6)'
readf,2,ts_data
ts_data=long(ts_data)
equil_ver=''
readf,2,equil_ver
; print,equil_ver
; ebe_time=make_array(1000,/float)
; i=0
while ~ eof(2) do begin
  readf,2,ebe_time
;  i=i+1
;  print,ebe_time,format='(''Time: '',F5.3)'
  ebe_ts_nstx,ebe_shotn,ebe_time,ts_data
  ebe_efit_nstx,ebe_shotn,ebe_time,equil_ver
endwhile
close,2

end
