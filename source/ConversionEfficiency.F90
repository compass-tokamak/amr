subroutine ConversionEfficiency(myRay1,myRay2,polarization, &
                                 conv_eff1,nNodes1,nodesExceed1,UHRCount1, &
                                 conv_eff2,nNodes2,nodesExceed2,UHRCount2)

  use shot_parameters
  use rays
  use vectors
  use equilibrium
  use density_temperature
  use PlasmaSlab
  use generalConstants
  use Constants
  use fileNames
  use utilities
  use analytical_profiles

  implicit none

  integer(4),parameter :: polarizationBoth = 0, polarizationLinear = 1, polarizationComplementary = 2

  type(ray),intent(in) :: myRay1,myRay2
  integer(4),intent(in) :: polarization
  real(8),intent(out) :: conv_eff1,conv_eff2
  integer(4),intent(out) :: nNodes1,UHRCount1,nNodes2,UHRCount2
  logical,intent(out) :: nodesExceed1,nodesExceed2

  real(8) :: freq
  integer :: n2
  logical :: nexceed
  common/nodes/n2,nexceed
  ! common/UHR_count/uhr_count,pr_count
  real(8) :: xe1,x_uhr,x_pr
  integer(4) :: UHRcount

  freq = getFrequency(myRay1)

  select case (conv_eff_method)
  case (conv_eff_adaptive)
    select case (polarization)
    case(polarizationBoth)
      call initSlab(myRay1,LSOL)
      call Adaptive(freq,conv_eff1,nNodes1,UHRCount1)
      nodesExceed1 = nexceed
      call initSlab(myRay2,LSOL)
      call Adaptive(freq,conv_eff2,nNodes2,UHRCount2)
      nodesExceed2 = nexceed
      if (conv_eff1+conv_eff2 > 1) then
        write(*,'(''warning: conversion efficiency exceeds 100%: '',F5.3)')conv_eff1+conv_eff2
        write(*,'(''forcing 100% conversion efficiency'')')
        write(getProtocolUnit(),'(''warning: conversion efficiency exceeds 100%: '',F5.3)')conv_eff1+conv_eff2
        write(getProtocolUnit(),'(''forcing 100% conversion efficiency'')')
        conv_eff1 = conv_eff1/(conv_eff1+conv_eff2)
        conv_eff2 = 1.d0 - conv_eff1
      endif
    case(polarizationLinear)
      call initSlab(myRay1,LSOL)
      call Adaptive(freq,conv_eff1,nNodes1,UHRCount1)
      nodesExceed1 = nexceed
	    conv_eff2 = 0; nNodes2 = 0; UHRCount2 = 0; nodesExceed2 = .false.
    case(polarizationComplementary)
      call initSlab(myRay2,LSOL)
      call Adaptive(freq,conv_eff2,nNodes2,UHRCount2)
      nodesExceed2 = nexceed
	    conv_eff1 = 0; nNodes1 = 0; UHRCount1 = 0; nodesExceed1 = .false.
    end select
  case (conv_eff_preinh)
    call initSlab(myRay1,LSOL)
    call find_xwkb(xe1,x_uhr,x_pr,UHRcount,conv_eff1)
    select case (polarization)
    case(polarizationBoth)
	    conv_eff2 = 0
    case(polarizationLinear)
	    conv_eff2 = 0
    case(polarizationComplementary)
	    conv_eff2 = conv_eff1
	    conv_eff1 = 0
    end select
    nNodes1 = 0; UHRCount1 = 0; nodesExceed1 = .false.
    nNodes2 = 0; UHRCount2 = 0; nodesExceed2 = .false.
  end select
  if (conv_eff1<1d-100) conv_eff1 = 0
  if (conv_eff2<1d-100) conv_eff2 = 0

end subroutine

