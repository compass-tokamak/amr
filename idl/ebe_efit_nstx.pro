pro ebe_efit_nstx,shot,time,equil_ver

; psi is reversed on NSTX
; (the current sign in EFIT is reversed)
SGNPSIORIG = -1

; equil_ver = equilibrium to use, i.e. 'efit01', 'efit02' etc.

        mdsconnect,'europa.pppl.gov:8501'
        mdsopen,equil_ver,shot
        psirz = mdsvalue('\psirz')
        bcentr= mdsvalue('\bcentr')
        rbcent    = mdsvalue('\rbcent')
        r     = mdsvalue('\r')
        z     = mdsvalue('\z')
        fpol  = mdsvalue('\fpol')
        ssibry= mdsvalue('\ssibry')
        ssimag= mdsvalue('\ssimag')
        qpsi  = mdsvalue('\qpsi')
        ; volume= mdsvalue('\volume')
        pres = mdsvalue('\pres')
        t_efit= mdsvalue('dim_of(\ssimag)')
        itime = where(abs(t_efit - time) eq min(abs(t_efit -time)))
        psi   = SGNPSIORIG*psirz(*,*,itime)
        BvacR = bcentr(itime)*rbcent(itime)
        rpsi = r(*,itime)
        zpsi = z(*,itime)
        f_psi= fpol(*,itime)
        psi_a= SGNPSIORIG*ssimag(itime)
        psi_b= SGNPSIORIG*ssibry(itime)
        ; v_psi = volume(itime)
        q_psi = qpsi(*,itime)
        p_psi= pres(*, itime)
        
        rmaxis = mdsvalue('\rmaxis')
        zmaxis = mdsvalue('\zmaxis')
        rmidin = mdsvalue('\rmidin')
        rmidout = mdsvalue('\rmidout')

;stop
;
;;       end of reading of experimental data
;:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
;

	 out_fname = inp_fname(shot,time,'psi')
   openw,1,out_fname
   printf,1,FORMAT='(I8,'' '',F7.4,'' '',I3)',shot,time,n_elements(rpsi)
   printf,1,'Bvac*R'
   printf,1,BvacR
   printf,1,'psi'
   printf,1,psi
   printf,1,'rpsi'
   printf,1,rpsi
   printf,1,'zpsi'
   printf,1,zpsi
   printf,1,'f_psi'
   printf,1,f_psi
   printf,1,'psi_a,b'
   printf,1,psi_a
   printf,1,psi_b
   printf,1,'p_psi'
   printf,1,p_psi
   printf,1,'q_psi'
   printf,1,q_psi
   printf,1,'Rmaxis,Zmaxis - magnetic axis'
   printf,1,rmaxis(itime)
   printf,1,zmaxis(itime)
   printf,1,'Rmidin,Rmidout - HFS and LFS separatrix'
   printf,1,rmidin(itime)
   printf,1,rmidout(itime)
   close,1


;
end      
