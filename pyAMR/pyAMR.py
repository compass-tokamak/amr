'''AMR Python functions.

This was originally in ebe6.py.
'''

import os,sys,ConfigParser,string,math,os.path
import antenna6 as antenna6
from numpy import *
import fileNames6 as fn

# constants
runFlagsConst = {'TestProfiles':1,'ConvEff':2,'RayTracing':4,'WaistData':8,'ProfilesExport':16}

possiblePolarizations = {'both':0,'circular':0,'linear':1,'linear_complementary':2,'e_cartesian':1,'e_rotating':1}

possibleAverageValues = {"none":0,"mean":1,"median":2,"constant":3}
possibleExtendValues  = {"none":0,"exp":1,"constant":2}

possibleDevices=['tokamak','stellarator']
deviceTokamak=possibleDevices.index('tokamak')
deviceStellarator=possibleDevices.index('stellarator')

possibleSplineMethods = {'b-spline':1,'akima':2,'cscon':3,'cubic':4,'dpchim':5}

possibleCurrentDensities={'none':0,'simple':1,'hansen':2,'curba':3,'mcgregor':4,'travis':5}
bannedCurrentDensities=('curba',)

resonantDampingModels = {'non-relativistic':0,'weakly-relativistic':1,'fully-relativistic':2,'saveliev':3,'weakly-relativistic-nocurv':4}
bannedResonantDampingModels = ()

CollisionalDampingModels = {'ginzburg-ruchadze':0,'cold':1,'none':2}

beam_spot_types = {'lcfs':0,'pco':1}

possibleConvEffMethods = {'adaptive':0,'preinh':1}

# Possible flux coordinate types with their labels used in integration output
possible_flux_coord = {'r_eff':'r_eff[m]','psi':'psi','rho':'rho'}
luke_flux_coord = {'rho':'xrhoP'}
# Flux coordinate labels in output figures
flux_coord_plt_labels = {'r_eff':'reff [m]','psi':'psi','rho':'rho'}

possible_m_interpreters = ('matlab','octave')

def getPossible(possibilities,value,convert=True,exit=True):
    value = value.lower()
    res = None
    if value in possibilities:
        if not convert:
            res = value
        elif isinstance(possibilities,dict):
            res = possibilities[value]
        elif isinstance(possibilities, list) or isinstance(possibilities, tuple):
            res = possibilities.index(value)
    if exit and res is None:
        print('Error: ' + value + 'not in ' + str(possibilities))
        sys.exit(2)
    return res

ebeFileHeader='#time          ' + \
              'freq           ' + \
              'waist_x        ' + \
              'waist_y        ' + \
              'waist_z        ' + \
              'Nx             ' +\
              'Ny             ' +\
              'Nz             ' +\
              'ReEx           ' +\
              'ImEx           ' +\
              'ReEy           ' +\
              'ImEy           ' +\
              'ReEz           ' +\
              'ImEz           ' +\
              'power[kW]      ' +\
              'iPol ' +\
              'waist_radius   ' +\
              'fix divergence   ' +\
              'assymetry      ' +\
              'enlargement    ' +\
              'width          ' +\
              'nCrc ' +\
              'nRpC ' +\
              'turn ' +\
              'rayS ' +\
              'rayE ' +\
              'iAnt ' +\
              'iCfg ' +\
              'runWinProj ' +\
              '\n'

def readConfig(cfgfilename,defaults_cfgfilename):
    '''
    Read and parse AMR cfg files
    ''' 
    my_conf = ConfigParser.SafeConfigParser()
    my_conf.read([defaults_cfgfilename,cfgfilename])
    my_conf_man = ConfigParser.SafeConfigParser()
    my_conf_man.read([cfgfilename])
    amr_cfg = {}
    for sec in my_conf.sections(): exec('amr_cfg[\''+sec+'\']={}')
    # Import data into variables
    # Mandatory fields
    try:
        amr_cfg['input']['device']=string.lower(my_conf_man.get('input','device'))
        amr_cfg['input']['device_no'] = getPossible(possibleDevices,amr_cfg['input']['device'])
        amr_cfg['input']['experiment']=my_conf_man.get('input','experiment').lower()
        amr_cfg['input']['shot_number']=my_conf_man.getint('input','shot_number')
        if (amr_cfg['input']['shot_number']<0) | (amr_cfg['input']['shot_number']>999999):
            print 'error: shot_number must be between 0 and 999999'
            sys.exit(1)
        amr_cfg['input']['start_time']=my_conf_man.getfloat('input','start_time')
        if (amr_cfg['input']['start_time']<0) | (amr_cfg['input']['start_time']>=10):
            print 'error: start_time must be >=0 and <10'
            sys.exit(1)       
        amr_cfg['input']['end_time']=my_conf_man.getfloat('input','end_time')
        if (amr_cfg['input']['end_time']<0) | (amr_cfg['input']['end_time']>=10):
            print 'error: end_time must be >=0 and <10'
            sys.exit(1)
        amr_cfg['input']['end_time'] = max(amr_cfg['input']['end_time'],amr_cfg['input']['start_time'])       
        amr_cfg['input']['use_ts_times'] = my_conf_man.getboolean('input','ts_times')
        amr_cfg['profiles']['equil_ver'] = my_conf_man.get('profiles','equil_ver').lower()
        # Read antenna configuration
        amr_cfg['antenna']['antenna_ids'] = my_conf_man.get('antenna','ids').split()
        amr_cfg['antenna']['cfg_file'] = my_conf_man.get('antenna','cfg_file')
    except (ConfigParser.NoOptionError,ValueError), inst:
        print 'error in the cfg file:'
        print inst
        sys.exit(1)    
    
    # Optional fields with defaults specified in ebe6_defaults.cfg
    try:
        amr_cfg['input']['time_samples']=max(int(my_conf.get('input','time_samples')),1)
        amr_cfg['input']['run_IDL'] = my_conf.getboolean('input','run_IDL')
        amr_cfg['output']['central_ray_only'] = my_conf.getboolean('output','central_ray_only')
        amr_cfg['output']['ray_details'] = my_conf.getboolean('output','ray_details')
        amr_cfg['output']['power_deposition'] = my_conf.getboolean('output','power_deposition')
        if not(my_conf.get('output','current_density').lower() in possibleCurrentDensities):
            print 'error: unknown current density model'
            sys.exit(1)
        else:   
            amr_cfg['output']['current_density'] = possibleCurrentDensities[my_conf.get('output','current_density').lower()]
        if amr_cfg['output']['current_density'] in bannedCurrentDensities:
            print 'error: unfinished current density model: '+amr_cfg['output']['current_density']
            sys.exit(1)
        amr_cfg['output']['conv_eff'] = my_conf.getboolean('output','conv_eff')
        amr_cfg['output']['ray_tracing'] = my_conf.getboolean('output','temperature')
        amr_cfg['output']['test_profiles'] = my_conf.getboolean('output','test_profiles')
        amr_cfg['output']['run_code'] = my_conf.getboolean('output','run_code')
        amr_cfg['output']['postprocess'] = my_conf.getboolean('output','postprocess')
        amr_cfg['output']['sum_antennas'] = my_conf.getboolean('output','sum_antennas')
        amr_cfg['output']['run_luke'] = my_conf.getboolean('output','run_luke')
        amr_cfg['cluster']['ncpu']=my_conf.getint('cluster','ncpu')
        if string.lower(my_conf.get('cluster','job')) == 'optimum' :
            amr_cfg['cluster']['jobDistribution'] = 'optimum'
        else:
            amr_cfg['cluster']['jobDistribution'] = 'beam'
        amr_cfg['cluster']['que']=my_conf.get('cluster','que')
        amr_cfg['cluster']['priority']=my_conf.getint('cluster','priority')
        amr_cfg['cluster']['master_priority']=my_conf.getint('cluster','master_priority')
        amr_cfg['cluster']['check_int']=my_conf.getfloat('cluster','check_int')
        amr_cfg['cluster']['use_cluster'] = my_conf.getboolean('cluster','use_cluster')
        amr_cfg['cluster']['use_pp'] = my_conf.getboolean('cluster','use_pp')
        amr_cfg['cluster']['pp_servers'] = tuple(my_conf.get('cluster','pp_servers').split())
#        if not(amr_cfg['cluster']['use_cluster']):
#            amr_cfg['cluster']['ncpu'] = 1
        # Resolves whether some processes will be run on the cluster
        if not(my_conf.get('adaptive','method').lower() in possibleConvEffMethods):
            print 'error: unknown conversion efficiency calculation method'
            sys.exit(1)
        else:
            amr_cfg['adaptive']['conv_eff_method'] = possibleConvEffMethods[my_conf.get('adaptive','method').lower()]
        amr_cfg['adaptive']['coll_freq']=my_conf.getfloat('adaptive','coll_freq')
        amr_cfg['adaptive']['n_init']=my_conf.getint('adaptive','n_init')
        amr_cfg['adaptive']['accuracy']=my_conf.getfloat('adaptive','accuracy')
        amr_cfg['adaptive']['accuracy_ap']=0.01 # not used
        amr_cfg['adaptive']['extension_xend_search']=my_conf.getfloat('adaptive','extension_xend_search')
        amr_cfg['ray_tracing']['Zeff']=my_conf.getfloat('ray_tracing','Zeff')
        amr_cfg['ray_tracing']['Nperp']=my_conf.getint('ray_tracing','Nperp')
        amr_cfg['ray_tracing']['accuracy']=my_conf.getfloat('ray_tracing','accuracy')
        amr_cfg['ray_tracing']['power_limit'] = my_conf.getfloat('ray_tracing','power_limit')
        amr_cfg['ray_tracing']['time_step'] = my_conf.getfloat('ray_tracing','time_step')
        amr_cfg['ray_tracing']['maximum_time'] = my_conf.getfloat('ray_tracing','maximum_time')
        amr_cfg['ray_tracing']['check_harmonic'] = my_conf.getboolean('ray_tracing','rt_check_harmonic')
        amr_cfg['ray_tracing']['check_harmonic_n'] = my_conf.getint('ray_tracing','rt_check_harmonic_n')
        if my_conf.get('ray_tracing','resonant_damping_model').lower() in resonantDampingModels:
            amr_cfg['ray_tracing']['resonant_damping_model'] = resonantDampingModels[my_conf.get('ray_tracing','resonant_damping_model').lower()]
        else:
            print 'error: '+my_conf.get('ray_tracing','resonant_damping_model')+' damping model not supported'
            sys.exit(2)
        if amr_cfg['ray_tracing']['resonant_damping_model'] in bannedResonantDampingModels:
            print 'error: '+amr_cfg['ray_tracing']['resonant_damping_model']+' damping model not working'
            sys.exit(2)
        if my_conf.get('ray_tracing','collisional_damping_model').lower() in CollisionalDampingModels:
            amr_cfg['ray_tracing']['collisional_damping_model'] = CollisionalDampingModels[my_conf.get('ray_tracing','collisional_damping_model').lower()]
        else:
            print 'error: '+my_conf.get('ray_tracing','collisional_damping_model')+' damping model not supported'
            sys.exit(2)
        amr_cfg['ray_tracing']['saveliev_accuracy']=my_conf.getfloat('ray_tracing','saveliev_accuracy')
        amr_cfg['ray_tracing']['saveliev_mmax']=my_conf.getint('ray_tracing','saveliev_mmax')
        amr_cfg['ray_tracing']['saveliev_limit']=my_conf.getfloat('ray_tracing','saveliev_limit')
        amr_cfg['ray_tracing']['pn_FR_limit']=my_conf.getfloat('ray_tracing','pn_FR_limit')
        amr_cfg['ray_tracing']['debug']=my_conf.getboolean('ray_tracing','debug')
        amr_cfg['ray_tracing']['W_asympt']=my_conf.getfloat('ray_tracing','W_asympt')

        amr_cfg['collisions']['n_sigma_ei_nonelastic']=my_conf.getint('collisions','n_sigma_ei_nonelastic')
        amr_cfg['collisions']['n_sigma_en']=my_conf.getint('collisions','n_sigma_en')
        amr_cfg['collisions']['neutral_concentration']=my_conf.getfloat('collisions','neutral_concentration')
        amr_cfg['collisions']['datafile']=my_conf.get('collisions','datafile')
        if amr_cfg['collisions']['datafile'].strip() == '': amr_cfg['collisions']['datafile'] = 'none'

        amr_cfg['test_profiles']['R_min'] = my_conf.getfloat('test_profiles','R_min')
        amr_cfg['test_profiles']['R_max'] = my_conf.getfloat('test_profiles','R_max')
        amr_cfg['test_profiles']['Z_min'] = my_conf.getfloat('test_profiles','Z_min')
        amr_cfg['test_profiles']['Z_max'] = my_conf.getfloat('test_profiles','Z_max')
        amr_cfg['test_profiles']['phi_test'] = my_conf.getfloat('test_profiles','phi_test')
        amr_cfg['test_profiles']['phi_min'] =my_conf.getfloat('test_profiles','phi_min')
        amr_cfg['test_profiles']['phi_max'] = my_conf.getfloat('test_profiles','phi_max')
        amr_cfg['test_profiles']['R_dim'] = my_conf.getint('test_profiles','R_dim')
        amr_cfg['test_profiles']['Z_dim'] = my_conf.getint('test_profiles','Z_dim')
        amr_cfg['test_profiles']['phi_dim'] = my_conf.getint('test_profiles','phi_dim')
        amr_cfg['test_profiles']['wkb_curve_ray'] = my_conf.getint('test_profiles','wkb_curve_ray')
        amr_cfg['test_profiles']['shot_time_test'] = my_conf.getfloat('test_profiles','shot_time_test')
        amr_cfg['test_profiles']['Nparal_above'] = my_conf.getfloat('test_profiles','Nparal_above')
        amr_cfg['test_profiles']['Nparal_below'] = my_conf.getfloat('test_profiles','Nparal_below')
        
        amr_cfg['profiles']['eq_input_type']  = my_conf.get('profiles','input_type').lower()
        amr_cfg['profiles']['eq_input_path']  = my_conf.get('profiles','eq_input_path')
        if amr_cfg['profiles']['eq_input_path'].lower()=='default': amr_cfg['profiles']['eq_input_path']='input'
        amr_cfg['profiles']['eq_input_path'] = os.path.realpath(amr_cfg['profiles']['eq_input_path']) + os.path.sep
        if not os.path.isdir(amr_cfg['profiles']['eq_input_path']):
            print 'error: cannot find directory ' + amr_cfg['profiles']['eq_input_path']
            sys.exit(1)
        amr_cfg['profiles']['psiSurfDim']  = my_conf.getint('profiles','psiSurfDim')
        amr_cfg['profiles']['fluxTraceDLcoeff']  = my_conf.getfloat('profiles','fluxTraceDLcoeff')
        amr_cfg['profiles']['equilSplineOrder']  = my_conf.getfloat('profiles','equilSplineOrder')
        amr_cfg['profiles']['FluxFnSplineOrder']  = my_conf.getfloat('profiles','FluxFnSplineOrder')
        amr_cfg['profiles']['export'] = my_conf.get('profiles','export').lower()
        amr_cfg['profiles']['export_nR'] = my_conf.getfloat('profiles','export_nR')
        amr_cfg['profiles']['export_nZ'] = my_conf.getfloat('profiles','export_nZ')
        amr_cfg['profiles']['export_npsi'] = my_conf.getfloat('profiles','export_npsi')
        # LUKE section
        amr_cfg['luke']['outputs'] = my_conf.getboolean('luke','outputs')
        if amr_cfg['output']['run_luke']: amr_cfg['luke']['outputs'] = True
        if amr_cfg['luke']['outputs'] and not amr_cfg['output']['postprocess']:
            print 'warning: postprocess switched on because of LUKE output'
            amr_cfg['output']['postprocess'] = True
        amr_cfg['luke']['dsmin'] = my_conf.getfloat('luke', 'dsmin')
        amr_cfg['luke']['kmode'] = my_conf.getint('luke', 'kmode')
        amr_cfg['luke']['np'] = my_conf.getint('luke', 'np')
        amr_cfg['luke']['nt'] = my_conf.getint('luke', 'nt')
        amr_cfg['luke']['equilibrium'] = my_conf.get('luke', 'equilibrium').lower()
        amr_cfg['luke']['id_dkeparam'] = my_conf.get('luke', 'id_dkeparam')
        if amr_cfg['luke']['outputs']:
            if amr_cfg['luke']['equilibrium']=='psi-theta':
                amr_cfg['profiles']['export'] = 'psi-theta'
                amr_cfg['profiles']['export_nR'] = amr_cfg['luke']['np']
                amr_cfg['profiles']['export_nZ'] = amr_cfg['luke']['nt']
                amr_cfg['profiles']['export_npsi'] = amr_cfg['luke']['np']
            elif amr_cfg['luke']['equilibrium']=='r-z':
                amr_cfg['profiles']['export'] = 'matlab'
            else:
                print 'error: "' + amr_cfg['luke']['equilibrium'] + '" LUKE equilibrium not recognized'
                sys.exit(1)
        
        if my_conf.get('general', 'beam_spot_type').lower() in beam_spot_types:
            amr_cfg['general']['beam_spot_type'] =  beam_spot_types[my_conf.get('general', 'beam_spot_type').lower()]
        else:
            print my_conf.get('general', 'beam_spot_type') + ' - unknown beam spot type'
            sys.exit(2)
        if my_conf.get('general','m_interpreter').lower() in possible_m_interpreters:
            amr_cfg['general']['m_interpreter'] =  my_conf.get('general','m_interpreter').lower()
        else:
            print my_conf.get('general','m_interpreter') + ' - unknown interpreter'
            sys.exit(2)
        if (amr_cfg['profiles']['eq_input_type']=='mclib') | ((amr_cfg['profiles']['eq_input_type']=='default') & (amr_cfg['input']['device_no']==deviceStellarator)):
            if my_conf.get('profiles','B_axis').strip()=='':
                print 'error: B_axis must be specified'
                sys.exit(1)
            amr_cfg['profiles']['B_axis'] = float(eval(my_conf.get('profiles','B_axis')))
            amr_cfg['profiles']['phi0'] = my_conf.getfloat('profiles','phi0')
            amr_cfg['profiles']['s_max'] = my_conf.getfloat('profiles','s_max')
            amr_cfg['profiles']['s_LCFS'] = my_conf.getfloat('profiles','s_LCFS')
            amr_cfg['profiles']['epsTrunc'] = my_conf.getfloat('profiles','epsTrunc')
            amr_cfg['profiles']['epsA'] = my_conf.getfloat('profiles','epsA')
            amr_cfg['profiles']['compute_mesh'] = my_conf.getboolean('profiles','compute_mesh')
        else:
            # must put some values, which won't be used anyway
            amr_cfg['profiles']['B_axis'] = 0.0
            amr_cfg['profiles']['phi0'] = 0.0
            amr_cfg['profiles']['s_max'] = 0.0
            amr_cfg['profiles']['s_LCFS'] = 0.0
            amr_cfg['profiles']['epsTrunc'] = 0.0
            amr_cfg['profiles']['epsA'] = 0.0
            amr_cfg['profiles']['compute_mesh'] = False
        # default ts_data_no = 0
        amr_cfg['profiles']['ts_data_no'] = 0
        if amr_cfg['input']['run_IDL']:
            amr_cfg['profiles']['ts_data'] = my_conf.get('profiles','ts_data').lower()
            if amr_cfg['input']['experiment'] == 'nstx':
                if amr_cfg['profiles']['ts_data'] == 'raw':
                    amr_cfg['profiles']['ts_data_no'] = 0
                elif amr_cfg['profiles']['ts_data'] == 'spline':
                    amr_cfg['profiles']['ts_data_no'] = 1
                else:
                    print("WRONG TS_DATA!")
                    sys.exit(1)
            elif amr_cfg['input']['experiment'] == 'mast':
                if amr_cfg['profiles']['ts_data'] == 'ruby':
                    amr_cfg['profiles']['ts_data_no'] = 0
                elif amr_cfg['profiles']['ts_data'] == 'ndyag':
                    amr_cfg['profiles']['ts_data_no'] = 1
                else:
                    print("WRONG TS_DATA!")
                    sys.exit(1)
        amr_cfg['profiles']['n_components'] = my_conf.getint('profiles','n_components')
        amr_cfg['profiles']['LSOL']=my_conf.getfloat('profiles','LSOL')
        amr_cfg['profiles']['analytical_profiles'] = my_conf.getboolean('profiles','analytical_profiles')
        try:
            amr_cfg['profiles']['ims'] = possibleSplineMethods[my_conf.get('profiles','spline_type').lower()]
        except KeyError:
            print 'error: unknown profiles spline type'
            sys.exit(1)
        except ConfigParser.NoOptionError:
            try:
                amr_cfg['profiles']['ims'] = my_conf.getint('profiles','ims')
            except (ValueError,ConfigParser.NoOptionError):
                print 'error: unknown profiles spline type'
                sys.exit(1)
        amr_cfg['profiles']['monotonize'] = my_conf.getboolean('profiles','monotonize')
        amr_cfg['profiles']['monotonize_freq'] = my_conf.getfloat('profiles','monotonize_freq')
        amr_cfg['profiles']['errorToleranceDen'] = my_conf.getfloat('profiles','errorToleranceDen')
        amr_cfg['profiles']['errorToleranceTe'] = my_conf.getfloat('profiles','errorToleranceTe')
        amr_cfg['profiles']['averageProfilesDen'] =  getPossible(possibleAverageValues,my_conf.get('profiles','averageProfilesDen'))
        amr_cfg['profiles']['averageProfilesTe'] = getPossible(possibleAverageValues,my_conf.get('profiles','averageProfilesTe'))
        amr_cfg['profiles']['averageDistanceDen'] = my_conf.getfloat('profiles','averageDistanceDen')
        amr_cfg['profiles']['averageDistanceTe'] = my_conf.getfloat('profiles','averageDistanceTe')
        amr_cfg['profiles']['extendProfilesDen'] = getPossible(possibleExtendValues,my_conf.get('profiles','extendProfilesDen'))
        amr_cfg['profiles']['extendProfilesTe'] = getPossible(possibleExtendValues,my_conf.get('profiles','extendProfilesTe'))
        amr_cfg['profiles']['extendDistanceDen'] = my_conf.getfloat('profiles','extendDistanceDen')
        amr_cfg['profiles']['extendDistanceTe'] = my_conf.getfloat('profiles','extendDistanceTe')
        amr_cfg['profiles']['extDataPoints'] = my_conf.getfloat('profiles','extDataPoints')
        amr_cfg['profiles']['LSOL_den']=my_conf.getfloat('profiles','LSOL_den')
        amr_cfg['profiles']['LSOL_Te']=my_conf.getfloat('profiles','LSOL_Te')
        amr_cfg['profiles']['LCFS_scale_break']=my_conf.getfloat('profiles','LCFS_scale_break')
        amr_cfg['profiles']['LCFS_scale_den_factor']=my_conf.getfloat('profiles','LCFS_scale_den_factor')
        amr_cfg['profiles']['LCFS_scale_Te_factor']=my_conf.getfloat('profiles','LCFS_scale_Te_factor')
        amr_cfg['profiles']['Bpol_scale']=my_conf.getfloat('profiles','Bpol_scale')
        amr_cfg['profiles']['Btor_scale']=my_conf.getfloat('profiles','Btor_scale')
        amr_cfg['profiles']['psi_R_shift']=my_conf.getfloat('profiles','psi_R_shift')
        
        amr_cfg['integration']['integrate_flux'] = my_conf.getboolean('integration','integrate_flux')
        amr_cfg['integration']['integrate_R'] = my_conf.getboolean('integration','integrate_R')
        if amr_cfg['integration']['integrate_R']:
            amr_cfg['integration']['R_min'] = my_conf.getfloat('integration','R_min')
            amr_cfg['integration']['R_max'] = my_conf.getfloat('integration','R_max')
        amr_cfg['integration']['integrate_flux'] = my_conf.getboolean('integration','integrate_flux')
        if amr_cfg['integration']['integrate_flux']:
            amr_cfg['integration']['flux_coordinate'] = my_conf.get('integration','flux_coordinate').lower()
            if not (possible_flux_coord.has_key(amr_cfg['integration']['flux_coordinate'])):
                print 'error: unknow flux coordinate ' + my_conf.get('integration','flux_coordinate')
                sys.exit(1)
            amr_cfg['integration']['flux_min'] = my_conf.getfloat('integration','flux_min')
            amr_cfg['integration']['flux_max'] = my_conf.getfloat('integration','flux_max')
        amr_cfg['integration']['tabdim'] = my_conf.getfloat('integration','tabdim')
        amr_cfg['integration']['type'] = my_conf.get('integration','type').lower()
        
        amr_cfg['profiles']['analytic'] = 'F'
        if amr_cfg['profiles']['analytical_profiles']:
            amr_cfg['profiles']['analytic'] = 'T'
            # convert from deg to rad
            amr_cfg['analytical_profiles']['phi'] = math.radians(my_conf.getfloat('analytical_profiles','phi'))
            amr_cfg['analytical_profiles']['nGrid'] = my_conf.getint('analytical_profiles','nGrid')
            amr_cfg['analytical_profiles']['denParams'] = eval(my_conf.get('analytical_profiles','den_parameters'))
            amr_cfg['analytical_profiles']['TeParams'] = eval(my_conf.get('analytical_profiles','Te_parameters'))
            amr_cfg['analytical_profiles']['type'] = my_conf.get('analytical_profiles','type').lower()
          
            if (len(amr_cfg['analytical_profiles']['denParams']) >= amr_cfg['profiles']['n_components']) and (len(amr_cfg['analytical_profiles']['TeParams']) >= amr_cfg['profiles']['n_components']):
                amr_cfg['analytical_profiles']['denParams'] = amr_cfg['analytical_profiles']['denParams'][0:amr_cfg['profiles']['n_components']]
                amr_cfg['analytical_profiles']['TeParams'] = amr_cfg['analytical_profiles']['TeParams'][0:amr_cfg['profiles']['n_components']]
            else:
                print 'Error: not enough analytical parameters'
                sys.exit(1)
          
            f_analytic = open('input/analytical_profiles.dat','w')
            f_analytic.write(amr_cfg['analytical_profiles']['type'] + '\n')
            f_analytic.write('%.6e phi_analytic [rad]\n' % amr_cfg['analytical_profiles']['phi'])
            f_analytic.write('%i nGrid\n' % amr_cfg['analytical_profiles']['nGrid'])
            f_analytic.write('%i nDenParameters\n' % len(amr_cfg['analytical_profiles']['denParams'][0]))
            for params in amr_cfg['analytical_profiles']['denParams']:
                for param in params:
                    f_analytic.write('%.8e ' % param)
                f_analytic.write('\n')
            f_analytic.write('%i nTenParameters\n' % len(amr_cfg['analytical_profiles']['TeParams'][0]))
            for params in amr_cfg['analytical_profiles']['TeParams']:
                for param in params:
                    f_analytic.write('%.8e ' % param)
                f_analytic.write('\n')
            f_analytic.close()
        
        amr_cfg['figures']['create'] = my_conf.getboolean('figures','create')
        if amr_cfg['figures']['create']:
            try:
                import matplotlib
            except ImportError:
                print 'error: numpy and matplotlib must be installed for plotting'
                amr_cfg['figures']['create'] = False
        # if amr_cfg['figures']['create']:
        # read always because of iAMR
        amr_cfg['figures']['errbars'] = my_conf.getboolean('figures','error_bars')
        amr_cfg['figures']['uselatex'] = my_conf.getboolean('figures','use_latex')
        amr_cfg['figures']['ext'] = my_conf.get('figures','ext')
        amr_cfg['figures']['experimental_data'] = my_conf.getboolean('figures', 'experimental_data')
        amr_cfg['figures']['rho_LCFS'] = my_conf.getfloat('figures', 'rho_LCFS')
        amr_cfg['figures']['plot_bw'] = my_conf.getboolean('figures', 'plot_bw')
        amr_cfg['figures']['flux_coordinate'] = my_conf.get('integration','flux_coordinate').lower()
        amr_cfg['figures']['test_profiles'] = my_conf.getboolean('figures','test_profiles')
        amr_cfg['figures']['luke_results'] = my_conf.getboolean('figures','luke_results')
        amr_cfg['figures']['conv_eff_angular_ray'] = my_conf.getboolean('figures','conv_eff_angular_ray')
        amr_cfg['figures']['ebe_Trad_time'] = my_conf.getboolean('figures','ebe_Trad_time')
        amr_cfg['figures']['power_deposition'] = my_conf.getboolean('figures','power_deposition')
        amr_cfg['figures']['power_deposition_comp'] = my_conf.getboolean('figures','power_deposition_comp')
        amr_cfg['figures']['power_deposition_individual'] = my_conf.getboolean('figures','power_deposition_individual')
        amr_cfg['figures']['power_deposition_spectra'] = my_conf.getboolean('figures','power_deposition_spectra')
        amr_cfg['figures']['ray_tracing_details'] = my_conf.getboolean('figures','ray_tracing_details')
        amr_cfg['figures']['ray_tracing_details_individual'] = my_conf.getboolean('figures','ray_tracing_details_individual')
        amr_cfg['figures']['wkb_curves'] = my_conf.get('figures','wkb_curves').lower()
        amr_cfg['figures']['plot_waists'] = my_conf.getboolean('figures','plot_waists')
        amr_cfg['figures']['rt_power_dep_markers'] = array(map(float,my_conf.get('figures','rt_power_dep_markers').split()))
        amr_cfg['figures']['rt_power_dep_markers'] = amr_cfg['figures']['rt_power_dep_markers'] \
                                                     [(amr_cfg['figures']['rt_power_dep_markers']>=0) & (amr_cfg['figures']['rt_power_dep_markers']<=1)]

    except (ConfigParser.NoOptionError,ValueError), inst:
        print 'error in the cfg file:'
        print inst
        raise    
    return amr_cfg

def initAntennas(amr_cfg):
    '''
    Initialize antennas and return an array of antenna6.antenna objects
    '''
    antennas = []
    for id in amr_cfg['antenna']['antenna_ids']:
        antennas.append(antenna6.antenna(id,amr_cfg))
    return antennas

def initShotTimes(amr_cfg):
    '''
    Initialize (calculate or read) shot times for the simulation
    '''
    ebe_times = []
    if amr_cfg['input']['use_ts_times']:
        if amr_cfg['input']['run_IDL']:
            f_shot_no=open('idl/shot_no.in','w')
            f_shot_no.write('%i\n' % amr_cfg['input']['shot_number'])
            f_shot_no.write('%i\n' % amr_cfg['profiles']['ts_data_no'])
            f_shot_no.close()
            # os.spawnlp(os.P_WAIT,'idl','idl','ts_times_run.pro')
            # os.system("idl ts_times_run_"+experiment+".pro")
            run_idl("ts_times_run_"+amr_cfg['input']['experiment']+".pro")
            os.rename('idl/ts_times_'+shotstr(amr_cfg['input']['shot_number'])+'.out','input/ts_times_'+shotstr(amr_cfg['input']['shot_number'])+'.out')
        try:
            f_ts_times=open('input/ebe_times_'+shotstr(amr_cfg['input']['shot_number'])+'.dat','r')
        except IOError:
            print 'Warning: input/ebe_times_'+shotstr(amr_cfg['input']['shot_number'])+'.dat not found, using idl/ts_times_'+shotstr(amr_cfg['input']['shot_number'])+'.out'
            f_ts_times=open('input/ts_times_'+shotstr(amr_cfg['input']['shot_number'])+'.out','r')
        for fa_line in f_ts_times.readlines():
            if (float(fa_line)>=amr_cfg['input']['start_time'] and float(fa_line)<=amr_cfg['input']['end_time']):
                ebe_times.append(float(fa_line))
        f_ts_times.close()
    # contruct ebe_times equidistantly
    else:
        ebe_times = linspace(amr_cfg['input']['start_time'],amr_cfg['input']['end_time'],amr_cfg['input']['time_samples']).tolist()
    return ebe_times

def writeShotParamsFile(amr_cfg,fname):
    f_shot_params=open(fname,'w')
    f_shot_params.write('%i shot_number\n' % amr_cfg['input']['shot_number'])
    f_shot_params.write('%f LSOL\n' % amr_cfg['profiles']['LSOL'])
    f_shot_params.write(amr_cfg['profiles']['analytic']+' analytic\n')
    f_shot_params.write('%f Zeff\n' % amr_cfg['ray_tracing']['Zeff'])
    if amr_cfg['output']['ray_details']:
        f_shot_params.write('F all_rays\n')
    else:
        f_shot_params.write('T all_rays\n')
    if amr_cfg['luke']['outputs']:
        f_shot_params.write('T LUKE output\n')
    else:
        f_shot_params.write('F LUKE output\n')
    if amr_cfg['output']['power_deposition']:
        f_shot_params.write('T power_deposition\n')
    else:
        f_shot_params.write('F power_deposition\n')
    f_shot_params.write('%i current density calculation type\n' % amr_cfg['output']['current_density'])
    f_shot_params.write('%f extension_xend_search\n' % amr_cfg['adaptive']['extension_xend_search'])
    f_shot_params.write('%i Nperp\n' % amr_cfg['ray_tracing']['Nperp'])
    f_shot_params.write('%e rt_accuracy\n' % amr_cfg['ray_tracing']['accuracy'])
    f_shot_params.write('%e rt_power_limit\n' % amr_cfg['ray_tracing']['power_limit'])
    f_shot_params.write('%e rt_time_step\n' % amr_cfg['ray_tracing']['time_step'])
    f_shot_params.write('%e rt_maximum_time\n' % amr_cfg['ray_tracing']['maximum_time'])
    f_shot_params.write('%i N_device\n' % amr_cfg['input']['device_no'])
    f_shot_params.write('%f LSOL_den\n' % amr_cfg['profiles']['LSOL_den'])
    f_shot_params.write('%f LSOL_Te\n' % amr_cfg['profiles']['LSOL_Te'])
    f_shot_params.write(amr_cfg['profiles']['equil_ver'] + ' \n')
    f_shot_params.write(amr_cfg['profiles']['eq_input_type'] + ' \n')
    f_shot_params.write(amr_cfg['profiles']['eq_input_path'] + ' \n')
    f_shot_params.write(amr_cfg['profiles']['export'] + ' \n')
    f_shot_params.write('%i eq_export_nR\n' % amr_cfg['profiles']['export_nR'])
    f_shot_params.write('%i eq_export_nZ\n' % amr_cfg['profiles']['export_nZ'])
    f_shot_params.write('%i eq_export_npsi\n' % amr_cfg['profiles']['export_npsi'])
    f_shot_params.write('%f B on axis at phi=phi0\n' % amr_cfg['profiles']['B_axis'])
    f_shot_params.write('%f phi0\n' % amr_cfg['profiles']['phi0'])
    f_shot_params.write('%f s_max\n' % amr_cfg['profiles']['s_max'])
    f_shot_params.write('%f s_LCFS\n' % amr_cfg['profiles']['s_LCFS'])
    f_shot_params.write('%e epsTrunc\n' % amr_cfg['profiles']['epsTrunc'])
    f_shot_params.write('%e epsA\n' % amr_cfg['profiles']['epsA'])
    if amr_cfg['profiles']['compute_mesh']:
        f_shot_params.write('T compute_mesh\n' )
    else:
        f_shot_params.write('F compute_mesh\n' )
    f_shot_params.write('%i n_components\n' % amr_cfg['profiles']['n_components'])
    f_shot_params.write('%i method of Den,Temp splines\n' % amr_cfg['profiles']['ims'])
    if amr_cfg['profiles']['monotonize']:
        f_shot_params.write('T monotonize density profile\n')
    else:
        f_shot_params.write('F monotonize density profile\n')
    f_shot_params.write('%e monotonize_freq\n' % (amr_cfg['profiles']['monotonize_freq']*1.0e9))
    f_shot_params.write('%e errorToleranceDen\n' % amr_cfg['profiles']['errorToleranceDen'])
    f_shot_params.write('%e errorToleranceTe\n' % amr_cfg['profiles']['errorToleranceTe'])
    f_shot_params.write('%i averageProfilesDen\n' % amr_cfg['profiles']['averageProfilesDen'])
    f_shot_params.write('%i averageProfilesTe\n' % amr_cfg['profiles']['averageProfilesTe'])
    f_shot_params.write('%e averageDistanceDen\n' % amr_cfg['profiles']['averageDistanceDen'])
    f_shot_params.write('%e averageDistanceTe\n' % amr_cfg['profiles']['averageDistanceTe'])
    f_shot_params.write('%i extendProfilesDen\n' % amr_cfg['profiles']['extendProfilesDen'])
    f_shot_params.write('%i extendProfilesTe\n' % amr_cfg['profiles']['extendProfilesTe'])
    f_shot_params.write('%e extendDistanceDen\n' % amr_cfg['profiles']['extendDistanceDen'])
    f_shot_params.write('%e extendDistanceTe\n' % amr_cfg['profiles']['extendDistanceTe'])
    # f_shot_params.write('%i extDataPoints\n' % extDataPoints)
    f_shot_params.write('%g extDataPoints\n' % amr_cfg['profiles']['extDataPoints'])
    f_shot_params.write('%i n_sigma_ei_nonelastic\n' % amr_cfg['collisions']['n_sigma_ei_nonelastic'])
    f_shot_params.write('%i n_sigma_en\n' % amr_cfg['collisions']['n_sigma_en'])
    f_shot_params.write('%g neutral_concentration\n' % amr_cfg['collisions']['neutral_concentration'])
    f_shot_params.write(amr_cfg['collisions']['datafile']+'\n')
    if amr_cfg['ray_tracing']['check_harmonic']:
        f_shot_params.write('T rt_check_harmonic\n')
    else:
        f_shot_params.write('F rt_check_harmonic\n')
    f_shot_params.write('%i rt_check_harmonic_n\n' % amr_cfg['ray_tracing']['check_harmonic_n'])
    f_shot_params.write('%i eq_psiSurfDim\n' % amr_cfg['profiles']['psiSurfDim'])
    f_shot_params.write('%g fluxTraceDLcoeff\n' % amr_cfg['profiles']['fluxTraceDLcoeff'])
    f_shot_params.write('%i equilSplineOrder\n' % amr_cfg['profiles']['equilSplineOrder'])
    f_shot_params.write('%i equilSplineOrder\n' % amr_cfg['profiles']['FluxFnSplineOrder'])
    f_shot_params.write('%i conv_eff_method\n' % amr_cfg['adaptive']['conv_eff_method'])
    f_shot_params.write('%i resonant_damping_model\n' % amr_cfg['ray_tracing']['resonant_damping_model'])
    f_shot_params.write('%i collisional_damping_model\n' % amr_cfg['ray_tracing']['collisional_damping_model'])
    f_shot_params.write('%f saveliev_accuracy\n' % amr_cfg['ray_tracing']['saveliev_accuracy'])
    f_shot_params.write('%i saveliev_mmax\n' % amr_cfg['ray_tracing']['saveliev_mmax'])
    f_shot_params.write('%f saveliev_limit\n' % amr_cfg['ray_tracing']['saveliev_limit'])
    f_shot_params.write('%i beamSpotType\n' % amr_cfg['general']['beam_spot_type'])
    f_shot_params.write('%g LCFS_scale_break\n' % amr_cfg['profiles']['LCFS_scale_break'])
    f_shot_params.write('%g LCFS_scale_den_factor\n' % amr_cfg['profiles']['LCFS_scale_den_factor'])
    f_shot_params.write('%g LCFS_scale_Te_factor\n' % amr_cfg['profiles']['LCFS_scale_Te_factor'])
    f_shot_params.write('%g Bpol scale\n' % amr_cfg['profiles']['Bpol_scale'])
    f_shot_params.write('%g Btor scale\n' % amr_cfg['profiles']['Btor_scale'])
    f_shot_params.write('%g psi R-shift\n' % amr_cfg['profiles']['psi_R_shift'])
    if amr_cfg['ray_tracing']['debug']:
        f_shot_params.write('T ray_tracing_debug\n')
    else:
        f_shot_params.write('F ray_tracing_debug\n')
    f_shot_params.write('%g ray_tracing_W_asympt\n' % amr_cfg['ray_tracing']['W_asympt'])
    f_shot_params.write('%g pn_FR_limit\n' % amr_cfg['ray_tracing']['pn_FR_limit'])
    f_shot_params.close()

def writeWindowFile(iAnt,ant):
    '''
    Write antenna window file, which is read in windowProjection Fortran subroutine
    '''
    myWindow = ant.getWindow()
    filename = 'input/window_%02i' % iAnt + '.dat'
    wf = open(filename,'w')
    wf.write(myWindow.type + '\n')
    wf.write('%i nWindow' % ant.n_window_points + '\n')
    wf.write('%g a' % myWindow.a + '\n')
    wf.write('%g a/b' % myWindow.a2b + '\n')
    wf.write('%g %g %g (x,y,z) center' % (myWindow.center[0],myWindow.center[1],myWindow.center[2]) + '\n')
    wf.write('%g %g %g (x,y,z) normal vector' % (myWindow.normal[0],myWindow.normal[1],myWindow.normal[2]) + '\n')
    wf.close()

def ebeInputLine(time,iAnt,ant,iConf,gaussBeam,iRayStart,iRayEnd,runWindowProjection=False):
    '''
    Construct single ebe_NNNN.dat line
    '''
    line  = "%+.6e %+.6e " % (time,gaussBeam.freq)
    line += "%+.6e %+.6e %+.6e " % (gaussBeam.x_center,gaussBeam.y_center,gaussBeam.z_center)
    line += "%+.6e %+.6e %+.6e " % (gaussBeam.Nx,gaussBeam.Ny,gaussBeam.Nz)
    line += "%+.6e %+.6e %+.6e %+.6e %+.6e %+.6e " % (gaussBeam.Ex.real,gaussBeam.Ex.imag,gaussBeam.Ey.real,gaussBeam.Ey.imag,gaussBeam.Ez.real,gaussBeam.Ez.imag)
    line += "%+.6e " % gaussBeam.power
    line += "%4i " % getPossible(possiblePolarizations, ant.polarization)
    line += "%+.6e " % gaussBeam.waist_radius
    if gaussBeam.fix_divergence:
        line += "T %+.6e " % gaussBeam.fixed_divergence
    else:
        line += "F %+.6e " % 0.0
    line += "%+.6e %+.6e %+.6e " % (gaussBeam.gauss_assymetry,gaussBeam.gauss_radius_enlargement,gaussBeam.gauss_width)
    if gaussBeam.turnCircles:
        line += "%4i %4i    T " % (gaussBeam.noOfCircles,gaussBeam.raysPerCircle)
    else:
        line += "%4i %4i    F " % (gaussBeam.noOfCircles,gaussBeam.raysPerCircle)
    line += "%4i %4i " % (iRayStart,iRayEnd)
    line += "%4i %4i " % (iAnt,iConf)
    if runWindowProjection:
        line += "         T "
    else:
        line += "         F "
    
    line += "\n"
    return line

def preAMR(amr_cfg,ebe_times):
    '''
    Preprocessing run after config is read and input data (IDL routines etc.) prepared
    '''
    # Convert eqdsk files to matlab-style
    if amr_cfg['profiles']['eq_input_type']=='eqdsk':
        # import pyAMR.rwefit as rwefit #,efit_eqdsk
        import rwefit
        for time in ebe_times:
            eq = rwefit.readg([os.path.join(amr_cfg['profiles']['eq_input_path'],fn.getEqdskFileName(amr_cfg['input']['shot_number'],time))])
            psi_lcfs=eq['ssibry'][0]
            psi_axis=eq['ssimag'][0]
            xpsi=linspace(psi_axis,psi_lcfs,len(eq['qpsi'][0,:]))
            prefix = os.path.join(amr_cfg['profiles']['eq_input_path'],'m_equil'+fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],time))
            fvals = open(prefix+'_values.dat','w')
            fvals.write('%.8g\n' %  eq['rmaxis'])
            fvals.write('%.8g\n' %  eq['zmaxis'])
            fvals.write('%.8g\n' %  (psi_lcfs-psi_axis))
            fvals.write('%.8g\n' %  eq['rbdry'][0,:].min()) # RLCFS_HFS
            fvals.write('%.8g\n' %  eq['rbdry'][0,:].max()) # RLCFS_LFS
            fvals.close()
            fR = prefix+'_R.dat'
            fZ = prefix+'_Z.dat'
            fPsiRZ = prefix+'_PsiRZ.dat'
            fPsi = prefix+'_PsiF.dat'
            fFpsi = prefix+'_F.dat'
            savetxt(fR,eq['rgrid'][0,:],fmt='%+15.8e')
            savetxt(fZ,eq['zgrid'][0,:],fmt='%+15.8e')
            savetxt(fPsiRZ,eq['psirz'][0,:,:]-psi_axis,fmt='%+15.8e')
            savetxt(fPsi,xpsi-psi_axis,fmt='%+15.8e')
            savetxt(fFpsi,eq['fpol'][0,:],fmt='%+15.8e')
        amr_cfg['profiles']['eq_input_type']='matlab'

def findRayOutput(amr_cfg,rays_per_cpu,time,ebe_times,antennas,iAnt,iConf,iRay):
    raysPerTime = 0
    beamsPerTime = 0
    for a in antennas:
        if amr_cfg['output']['central_ray_only']:
            raysPerTime += a.getNumberOfConfigurations()
        else:      
            raysPerTime += a.getNumberOfRays()
        beamsPerTime+= a.getNumberOfConfigurations()
    try:
        it = ebe_times.index(time)
    except ValueError:
        print 'error: findRayOutput cannot find time %g in ebe_times' % time
        sys.exit(4)
    if amr_cfg['cluster']['jobDistribution'] == 'optimum':
        nrays = it*raysPerTime
        # count preceding antennas
        for ia in range(iAnt):
            if amr_cfg['output']['central_ray_only']:
                nrays += antennas[ia].getNumberOfConfigurations()
            else:
                nrays += antennas[ia].getNumberOfRays()
        # count preceding configurations in our antenna
        for ic in range(iConf):
            if amr_cfg['output']['central_ray_only']:
                nrays += 1
            else:
                nrays += antennas[iAnt].getBeamWaist(ic).getNumberOfRays()
        nrays += iRay - 1
        rpid = nrays // rays_per_cpu # pids start from 0
        rline = nrays % rays_per_cpu +1 # lines start from 1
    elif amr_cfg['cluster']['jobDistribution'] == 'beam':
        nbeams = it*beamsPerTime
        for ia in range(iAnt):
            nbeams += antennas[ia].getNumberOfConfigurations()
        rpid = nbeams + iConf
        rline = iRay
    return [rpid,rline]

def run_idl(prog,amr_path):
    os.system('cd idl; idl '+os.path.join(amr_path,'idl',prog)+'; cd ..')
  
def protocol(p_file,text):
    ptext = str(text)
    if ptext[-1]=='\n':
        ptext = ptext[:-1]
    p_file.write(ptext+'\n')
    print(ptext)

def ebe_files_init(amr_cfg,pid,ray_tracing,conv_eff,cluster_run):
    runFlags = 0
    # reconsider ProfilesExport flag (now LUKE cannot run from here as it needs postprocessing)
    if ray_tracing:
        runFlags |= runFlagsConst['RayTracing']
        runFlags |= runFlagsConst['WaistData']
    if conv_eff:
        runFlags |= runFlagsConst['ConvEff']
        runFlags |= runFlagsConst['WaistData']
    if os.path.exists(ebe_bname(pid)): os.remove(ebe_bname(pid))
    if cluster_run:
        fbatch=open('./'+ebe_bname(pid),'w')
        fbatch.write('#!/bin/bash \n')
        # process name
        fbatch.write('#PBS -N '+ebe_bname(pid)+'\n')
        # output file
        fbatch.write('#PBS -o '+ebe_bname(pid)+'_out\n')
        # error output file
        fbatch.write('#PBS -e '+ebe_bname(pid)+'_err\n')
        fbatch.write('#PBS -j oe\n')
        # 1 CPU per node, 1 node per process
        fbatch.write('#PBS -l nodes=1:ppn=1\n')
        # choose que
        fbatch.write('#PBS -q '+amr_cfg['cluster']['que']+'\n')
        fbatch.write('#PBS -V\n')
        # priority of the processes
        fbatch.write('#PBS -p %i\n' % amr_cfg['cluster']['priority'])
        fbatch.write('cd "$PBS_O_WORKDIR/ebe_run" \n')
        fbatch.write('./ebe_run.exe %i %i \n' % (runFlags,pid))
    else:
        fbatch = None
    finit=open('input/'+ebe_fname(pid),'w')
    finit.write(ebeFileHeader)
    return [fbatch,finit]

# Id string = xxxx, xxxx = 4 digit process id
def pidstr(id):
    if id <10:
        result='000'+str(id)
    elif id < 100:
        result='00'+str(id)
    elif id < 1000:
        result='0'+str(id)
    else:
        result=str(id)
    return result


# Config files for each process (= list of tasks)
# ebe_fname = ebe_xxxx.dat, xxxx=process id (i.e. 0002)
# each processor = 1 process
def ebe_fname(id):
    result='ebe_'+pidstr(id)+'.dat'
    return result

# Batch filename for each process
def ebe_bname(id):
    result='ebe_'+pidstr(id)
    return result

def shotstr(n):
    return '%06i' % n

def setpriority(priority=1,pid=None):
    """ Set The Priority of a Windows or Unix Process.  Priority is a value between 0-5 where
      2 is normal priority.  Default sets the priority of the current
      python process but can take any valid process handle/ID
      (for Windows pid is a handle, for Unix pid is a process ID).
      
      Uses pywin32, see http://www.python.net/crew/mhammond/win32/
      or http://sourceforge.net/projects/pywin32/ """
  
    import platform
    if platform.system() == 'Windows':
        import win32api,win32process,win32con
        priorityclasses = [win32process.IDLE_PRIORITY_CLASS, \
                          win32process.BELOW_NORMAL_PRIORITY_CLASS, \
                          win32process.NORMAL_PRIORITY_CLASS, \
                          win32process.ABOVE_NORMAL_PRIORITY_CLASS, \
                          win32process.HIGH_PRIORITY_CLASS, \
                          win32process.REALTIME_PRIORITY_CLASS]
        if pid == None:
            pid = win32api.OpenProcess(win32con.PROCESS_ALL_ACCESS, True, win32api.GetCurrentProcessId())
        win32process.SetPriorityClass(pid, priorityclasses[priority])

    elif platform.system() == 'Linux':
        priorityclasses = [19,10,0,-5,-10,-19]
        if pid == None:
            os.nice(priorityclasses[priority])
        else:
            niceCommand = "renice -n %d -p %d" % (priorityclasses[priority],pid)
            os.system(niceCommand)

def round2(x):
    return round(x,2)

def round3(x):
    return round(x,3)

def readProfiles(amr_cfg,ebe_times):
    '''
    Read test_profiles outputs
    
    Return [prof0d,prof1d,prof1d_comp,prof2d,profpsi,profpsi_comp,profexp]
    '''
    import amrplot as amrplot
    prof0d = []
    prof1d = []
    prof1d_comp = []
    prof2d = []
    profpsi= []
    profpsi_comp = []
    profexp= []
    prof3d = []
    profequat = []
    indir = 'ebe_run/output/'
    indirt = amr_cfg['profiles']['eq_input_path']
    for t in ebe_times:
        fname = indir + 'profiles_0D' + fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],t) + '.dat'
        prof0d.append(amrplot.amrdata()) 
        prof0d[-1].readFile(fname)
        fname = indir + 'profiles_1D' + fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],t) + '.dat'
        prof1d.append(amrplot.amrdata()) 
        prof1d[-1].readFile(fname)
        fname = indir + 'profiles_2D' + fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],t) + '.dat'
        prof2d.append(amrplot.amrdata()) 
        prof2d[-1].readFile2D(fname)
        fname = indir + 'profiles_psi' + fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],t) + '.dat'
        profpsi.append(amrplot.amrdata()) 
        profpsi[-1].readFile(fname)
        if (not (amr_cfg['profiles']['analytical_profiles'] or amr_cfg['profiles']['eq_input_type']=='matlab')) and amr_cfg['figures']['experimental_data']:
            fname = os.path.join(indirt, 'ts' + fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],t) + '.out')
            profexp.append(amrplot.amrdata()) 
            profexp[-1].readFile(fname)
        if amr_cfg['profiles']['n_components']>1:
            fname = indir + 'profiles_1D_comp' + fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],t) + '.dat'
            prof1d_comp.append(amrplot.amrdata()) 
            prof1d_comp[-1].readFile(fname)
            fname = indir + 'profiles_psi_comp' + fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],t) + '.dat'
            profpsi_comp.append(amrplot.amrdata()) 
            profpsi_comp[-1].readFile(fname)
        if amr_cfg['test_profiles']['phi_dim']>1:
            fname = indir + 'profiles_3D' + fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],t) + '.dat'
            prof3d.append(amrplot.amrdata()) 
            prof3d[-1].readFile(fname)
            fname = indir + 'profiles_equat' + fn.getShotTimeSuffix(amr_cfg['input']['shot_number'],t) + '.dat'
            profequat.append(amrplot.amrdata()) 
            profequat[-1].readFile2D(fname)
    return prof0d,prof1d,prof1d_comp,prof2d,profpsi,profpsi_comp,profexp,prof3d,profequat

def detectCPUs():
    """
     Detects the number of CPUs on a system. Cribbed from pp.
     """
    try:
        from multiprocessing import cpu_count
        return cpu_count()
    except ImportError:  
        # Linux, Unix and MacOS:
        if hasattr(os, "sysconf"):
            if os.sysconf_names.has_key("SC_NPROCESSORS_ONLN"):
                # Linux & Unix:
                ncpus = os.sysconf("SC_NPROCESSORS_ONLN")
                if isinstance(ncpus, int) and ncpus > 0:
                    return ncpus
            else: # OSX:
                return int(os.popen2("sysctl -n hw.ncpu")[1].read())
            # Windows:
            if os.environ.has_key("NUMBER_OF_PROCESSORS"):
                ncpus = int(os.environ["NUMBER_OF_PROCESSORS"]);
                if ncpus > 0:
                    return ncpus
            return 1 # Default

def ndigits(s):
    ss = s.strip().lower().split('e')[0]
    ss = reduce(lambda a,b: a+b, ss.split('.'))
    if not ss[0].isdigit(): ss = ss[1:]
    if ss.isdigit():
        return len(ss)
    else:
        raise ValueError(s + ' seems not to be a number')
