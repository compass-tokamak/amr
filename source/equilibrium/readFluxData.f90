!! Read equilibrium given by flux surfaces, e.g. MAST-U TRASP modelling
integer(4) function readFluxData(equilibriumFileName,psiSurfDim,fluxTraceDLcoeff,splineOrderPsi,splineOrderFluxFn,psi_scale,Btor_scale,psi_R_shift)
  use zeroin
#ifdef _IMSL

#else
  use bspline
#endif
  implicit none
  character*(*),intent(in) :: equilibriumFileName
  integer(4),intent(in) :: psiSurfDim
  real(8),intent(in) :: fluxTraceDLcoeff,psi_scale,Btor_scale,psi_R_shift
  integer,optional :: splineOrderPsi,splineOrderFluxFn

  integer(4) :: fileUnit,n_surfs,npt,n,i,k
  real(8) :: rtest,ztest,bpoltest
  real(8),allocatable :: psin(:),psiabs(:),q(:),p(:),f(:),dens(:),temp(:),R(:,:),Z(:,:)
  real(8),allocatable :: xydata(:,:),fdata(:)
  real(8) :: Rmin,Zmin,dR,dZ
#ifdef _IMSL
  real(8) :: DBSVAL
#else
  integer :: ier
  integer,allocatable :: iwk(:,:)
  real(8),allocatable :: wk(:,:)
#endif

  readFluxData = 0
  equilibriumType = Equilibrium2D

  if (allocated(PsiRZGrid)) then
    deallocate (PsiRZGrid,RGrid,ZGrid)
    deallocate (FPsiGrid)
  endif
  if (allocated(pPsiGrid)) deallocate (pPsiGrid,qPsiGrid)
  if (allocated(PsiGrid)) then
    deallocate(PsiGrid)
  endif

  if (present(splineOrderPsi)) then
    PsiSplineOrder = splineOrderPsi
  else
    PsiSplineOrder = DefaultPsiSplineOrder
  endif
  if (present(splineOrderFluxFn)) then
    fluxSplineOrder = splineOrderFluxFn
  else
    fluxSplineOrder = DefaultPsiSplineOrder
  endif

  ! get dimensions, assume all flux surfaces have the same number of points
  fileUnit = openFile(equilibriumFileName,action='read')
  read(fileUnit,*)
  read(fileUnit,*)
  read(fileUnit,*)n_surfs
  read(fileUnit,*)
  read(fileUnit,*)npt
  close(fileUnit)
  allocate(R(n_surfs,npt),Z(n_surfs,npt))
  allocate(psin(n_surfs),psiabs(0:n_surfs),q(n_surfs),p(n_surfs),f(0:n_surfs),dens(n_surfs),temp(n_surfs))

  fileUnit = openFile(equilibriumFileName,action='read')
  read(fileUnit,*) Psi_LCFS,PsiMagAxis,RMagAxis,ZMagAxis,BvacR
  !psi_sep, psi_axis=poloidal magnetic flux at LCMS and magnetic axis
  !X0,Z0=magnetic axis position (meters), Bvac in T
  BvacR=-1.d0*BvacR*RMagAxis  !in MAST, Gauss
!  Psi_LCFS   = -Psi_LCFS !var
!  PsiMagAxis = -PsiMagAxis !var
  read(fileUnit,*) rtest,ztest,bpoltest !R,Z,Bpol(R,Z) for test purpose
  read(fileUnit,*) n !number of magnetic surfaces
  if (n_surfs /= n) then
    write(*,*)'error: n/=n_surfs'
    stop
  endif

  do i=1,n_surfs
    read(fileUnit,*) psin(i),psiabs(i),q(i),p(i),f(i),dens(i),temp(i)
    !psin=normalised poloidal flux for a given magnetic surface 'i'
    !psiabs=poloidal flux for a given magnetic surface 'i'
    !q=safety factor for a given magnetic surface 'i'
    !p=pressure=2*n*Te for a given magnetic surface 'i'
    !f(n_surfs)=Btor_plasma/Btor_vacuum (plasma influence on vacuum toroidal magnetic field)
    !dens(n_surfs)=plasma density/10^19 m^-3
    !temp(n_surfs)=plasma temperature in KeV
    read(fileUnit,*) n ! number of flux surface points
    if (npt /= n) then
      write(*,*)'error: n/=npt'
      stop
    endif
    do k=1,npt
      read(fileUnit,*) R(i,k),Z(i,k) !in meters
    !rcoord,zcoord=spatial points along a given magnetic surface 'i'
    end do
  end do
  close(fileUnit)

  npt = npt-1 ! last data points = first
  PsiRDim = 65
  PsiZDim = PsiRDim
  allocate(xydata(2,n_surfs*npt),fdata(n_surfs*npt),RGrid(PsiRDim),ZGrid(PsiZDim),PsiRZGrid(PsiRDim,PsiZDim))
  do i=1,n_surfs
    do k=1,npt
      xydata(1,(i-1)*npt+k) = R(i,k)
      xydata(2,(i-1)*npt+k) = Z(i,k)
      fdata((i-1)*npt+k) = psiabs(i)
    enddo
  enddo
  psiabs(0) = PsiMagAxis
  f(0) = f(1)
  f = f*BvacR

  Rmin = minval(R(n_surfs,:))
  Zmin = minval(Z(n_surfs,:))
  dR = (maxval(R(n_surfs,:)) - Rmin) / (PsiRDim-1)
  dZ = (maxval(Z(n_surfs,:)) - Zmin) / (PsiZDim-1)
  do i=1,PsiRDim
    RGrid(i) = Rmin + dR*(i-1)
  enddo
  do i=1,PsiZDim
    ZGrid(i) = Zmin + dZ*(i-1)
  enddo

  ! interpolate scattered data
#ifdef _IMSL
    call DSURF(n_surfs*npt, xydata, fdata, PsiRDim, PsiZDim, RGrid, ZGrid, PsiRZGrid, PsiRDim)
#else
    allocate(iwk(n_surfs*npt*39,n_surfs*npt*39))
    allocate(wk(n_surfs*npt*36,n_surfs*npt*36))
    call SDSF3P(1,n_surfs*npt,xydata(1,:),xydata(2,:),fdata,PsiRDim,RGrid,PsiZDim,ZGrid,PsiRZGrid,IER,WK,IWK)
    deallocate(iwk)
    deallocate(wk)
#endif

  ! RLCFS_HFS = Rmin
  ! RLCFS_LFS = RGrid(PsiRDim)
  RLCFS_HFS = (RGrid(1)+RGrid(2))/2
  RLCFS_LFS = (RGrid(PsiRDim-1)+RGrid(PsiRDim))/2

  allocate(PsiGrid(n_surfs+1),FPsiGrid(n_surfs+1))
  PsiGrid = psiabs
  FPsiGrid = f

  call Psi2DSpline(PsiRDim,PsiZDim,n_surfs+1,.false.,psiSurfDim,fluxTraceDLcoeff,psi_scale,Btor_scale,psi_R_shift)

  ! correct BvacR (n case Psi2DSpline changes Psi_LCFS)
  write(*,*)'BvacR correction: ',BvacR-DBSVAL(Psi_LCFS,PsiSplineOrder,F_spline_knots,NF_spline_knots,F_spline_coef)
  BvacR = DBSVAL(Psi_LCFS,PsiSplineOrder,F_spline_knots,NF_spline_knots,F_spline_coef) ! FPsiGrid(PsiRDim)



  deallocate(R,Z)
  deallocate(psin,psiabs,q,p,f,dens,temp)
  deallocate(xydata,fdata)

end function
