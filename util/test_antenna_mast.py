#import sys,os
#sys.path.append('D:\\Workspace\\AMR-pythonize')
#sys.path.append('D:\\Workspace\\AMR-pythonize\\util')
#print sys.path
from pyAMR import antenna_mast
#import antenna_mast

freq=35920000000.00
beam_inc_ry=   12.0
beam_inc_zr=   12.0
anglee_eq = 45.0
xr_mirror=2.25
ytor_mirror=-0.03
z_mirror=-0.22

# call Fortran subroutine, output parameters in a list
print 'write from Fortran'
print  antenna_mast.waist_position(freq,beam_inc_ry,beam_inc_zr,anglee_eq,xr_mirror,ytor_mirror,z_mirror)
[x_waist,y_waist,z_waist,nx_waist,ny_waist,nz_waist,ex_transm,ey_transm,ez_transm,w02m,transm_coeff] = \
antenna_mast.waist_position(freq,beam_inc_ry,beam_inc_zr,anglee_eq,xr_mirror,ytor_mirror,z_mirror)
print nx_waist*ex_transm + ny_waist*ey_transm + nz_waist*ez_transm

#[xR_waist,Z_waist,Y_waist,w02M] = antenna_mast.waist_position(10e9,poloidal_angle,toroidal_angle,xR_mirror,Z_mirror,Ytor_mirror)


# print output results
#print 'results in Python'
#print 'xR_waist = %g' % xR_waist
#print 'Z_waist = %g' % Z_waist
#print 'Y_waist = %g' % Y_waist
#print 'w02M = %g' % w02M
# alternative way to call
#print '--- alternative way to call Fortran---'
#print 'write from Fortran'
fresult = antenna_mast.waist_position(freq,beam_inc_ry,beam_inc_zr,anglee_eq,xr_mirror,ytor_mirror,z_mirror)
# print output results
print 'results in Python'
print fresult
