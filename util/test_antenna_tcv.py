import subprocess as sp
import math

def iPrompt(text,defval=''):
    '''Interactive user input with a default value.'''
    ans = raw_input(text + ' [' + str(defval) + '] : ')
    ans = ans.strip()
    if ans=='': ans = defval
    return ans

def E_transmitted_plain(N_xR0_in,N_tor0,N_Z0,angleE_eq,freq):
    N_xR0 = -N_xR0_in
    transm_coeff = 1.0
    Ez_transm=sind(angleE_eq)
    aa=N_tor0**2+N_xR0**2
    bb=-N_Z0*N_xR0*Ez_transm
    cc=Ez_transm**2*N_Z0**2-cosd(angleE_eq)**2*N_tor0**2
    dd=bb*bb-aa*cc
    if abs(dd) < 1e-15:
        dd = 0.0
    Ex_transm=(-bb+math.sqrt(dd))/aa
    if N_tor0==0:
        Ey_transm = math.sqrt(1.0-Ez_transm**2-Ex_transm**2)
    else:
        Ey_transm=(-Ez_transm*N_Z0+Ex_transm*N_xR0)/N_tor0
    return [complex(Ex_transm),complex(Ey_transm),complex(Ez_transm),transm_coeff]

# sinus, cosinus, phi in degrees
def sind(phi):
    return math.sin(math.radians(phi))
def cosd(phi):
    return math.cos(math.radians(phi))

# --- E_transmitted auxiliary functions ---
def fE1_tor(cg, cg0, cg1, cg2, cg3, cg4):
    return math.sqrt(0.1e1 / (cg ** 2 * cg0 ** 2 + cg1 ** 2 * cg2 ** 2 - \
    2 * cg3 * cg1 * cg2 * cg0 + cg3 ** 2 * cg0 ** 2 - 2 * cg * cg1 * \
    cg4 * cg0 + cg1 ** 2 * cg4 ** 2 + cg ** 2 * cg2 ** 2 - 2 * cg * cg2 \
    * cg4 * cg3 + cg4 ** 2 * cg3 ** 2)) *(-cg * cg2 + cg4 * cg3)
def fE1_Z(cg, cg0, cg1, cg2, cg3, cg4):
    return -sqrt(0.1e1 / (cg ** 2 * cg0 ** 2 + cg1 ** 2 * cg2 ** 2 \
    - 2 * cg3 * cg1 * cg2 * cg0 + cg3 ** 2 * cg0 ** 2 - 2 * cg * cg1 * \
    cg4 * cg0 + cg1 ** 2 * cg4 ** 2 + cg ** 2 * cg2 ** 2 - 2 * cg * cg2 \
    * cg4 * cg3 + cg4 ** 2 * cg3 ** 2)) *(-cg1 * cg2 + cg0 * cg3) 
def fE1_R(cg, cg0, cg1, cg2, cg3, cg4):
    return sqrt(0.1e1 / (cg ** 2 * cg0 ** 2 + cg1 ** 2 * cg2 ** 2 - \
    2 * cg3 * cg1 * cg2 * cg0 + cg3 ** 2 * cg0 ** 2 - 2 * cg * cg1 * \
    cg4 * cg0 + cg1 ** 2 * cg4 ** 2 + cg ** 2 * cg2 ** 2 - 2 * cg * cg2 \
    * cg4 * cg3 + cg4 ** 2 * cg3 ** 2)) * (-cg1 * cg4 + cg * cg0) 
# --- end E_transmitted auxiliary functions ---

launcher_no = int(iPrompt('Launcher number',1))
theta_l = float(iPrompt('Poloidal angle (theta_l)',31))
phi_l = float(iPrompt('Toroidal angle (phi_l)',-109))
E_eq_angle = float(iPrompt('E_eq_angle',0))
freq = float(iPrompt('Frequency [GHz]',82.7))
m_interpreter = iPrompt('M-interpreter (matlab | octave)','matlab')
if m_interpreter.lower()=='octave':
    cmd = 'octave --eval "tcv_beam(%g,%g,%i)"' % (theta_l,phi_l,launcher_no)
elif m_interpreter.lower()=='matlab':
    cmd = 'matlab -nodisplay -r "tcv_beam(%g,%g,%i)"' % (theta_l,phi_l,launcher_no)
else:
    print 'error: unknow m-interpreter'
    sys.exit(2)
pipe = sp.Popen(cmd, shell=True, stdout=sp.PIPE).stdout
for res in pipe: 
    if 'TCV_Beam' in res: break
pipe.close()
res2 = res.strip().split()
for i,s in enumerate(res2):
    if 'TCV_Beam:' in s:
        ires = i+1
        exit
res2f = map(float,res2[ires:])
x_center = res2f[0]
y_center = res2f[1]
z_center = res2f[2]
waist_radius = res2f[3]
Nx = res2f[4]
Ny = res2f[5]
Nz = res2f[6]
# calculate the polarization (arbitrary, although perpendicular to the wave vector)
freq = 60.0
[Ex,Ey,Ez,transm_coeff] = \
    E_transmitted_plain(Nx,Ny,Nz,E_eq_angle,freq)
fix_divergence = False

print 'Waist center (x,y,z) [m]: (%g,%g,%g)' %(x_center,y_center,z_center)
print 'Waist radius: %g m' %(waist_radius)
print 'Central beam (Nx,Ny,Nz): (%g,%g,%g)' %(Nx,Ny,Nz)
print 'Central beam (Ex,Ey,Ez): (%g+%gi,%g+%gi,%g+%gi)' %(Ex.real,Ex.imag,Ey.real,Ey.imag,Ez.real,Ez.imag)
