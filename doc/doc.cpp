/*! \mainpage

\section intro Introduction
AMR (Antenna---Mode-conversion---Ray-tracing, also Adaptive Mesh Refinement) is a simulation code for electron Bernstein waves in toroidal fusion devices.

\section seccontents Contents
  - \ref pageinst
  - \ref pagelog
  - \ref pageexec
  - \ref pageluke
  - \ref pagelog

\page pagelog Change log (subversion log)

\include svn.log

\page pageinst Installation
\section secdown Download
AMR is distributed throuhg a Subversion repository. The location is (insert your username) <A HREF="svn+ssh:///home/ebw/svnrepos/AMR/trunk">svn+ssh://USERNAME\@sun1.ipp.cas.cz/home/ebw/svnrepos/AMR/trunk</A>. On IPP Sun computers file:///home/ebw/svnrepos/AMR/trunk can be used.

\section secprereq Prerequisites
- Fortran 90/95 compiler, e.g. Compaq Visual Fortran, Intel Fortran, gfortran etc.
- <A HREF="http://www.vni.com/products/imsl/fortran/overview.php">IMSL</A> or <A HREF="http://www.nag.co.uk/numeric/fl/FLdescription.asp">NAG</A> Fortran numerical library (commercial). IMSL is preferred and switched on by defining _IMSL preprocessor symbol.
- <A HREF="http://www.netlib.org/lapack/">LAPACK</A> (or equivalently <A HREF="http://math-atlas.sourceforge.net/">ATLAS</A>, <A HREF="http://www.intel.com/cd/software/products/asmo-na/eng/307757.htm">Intel</A>/<A HREF="http://www.amd.com/acml">AMD</A> math kernel/core libraries etc) if IMSL not used.
- (optional) <A HREF="http://cernlib.web.cern.ch">CERNLIB</A> for Saveliev dispersion.
- (optional) MCLIB library for stellarator %equilibrium. Switched on by _MCLIB  preprocessor symbol. Included pre-built for Linux.
- <A HREF="http://www.python.org">Python</A>, 2.5 or 2.6 recommended.
  - (for MS Windows only) <A HREF="http://sourceforge.net/projects/pywin32/">Python for Windows extension (PyWin32)</A>
  - (required) <A HREF="http://www.scipy.org">NumPy</A> enhanced math functionality, necessary for figure generation and Fortran-Python interfaced functions (antenna_mast).
  - (optional) <A HREF="http://matplotlib.sourceforge.net/">Matplotlib</A> for figure generation.
  - (optional) <A HREF="http://code.google.com/p/formlayout/">Formlayout</A> for AMR GUI (amrgui.py) (formlayout requires PyQt4).
  - (optional) <A HREF="http://www.parallelpython.com/">Parallel Python </A> enables simple parallel run on multi-CPU (multi-core) computers or clusters.
  - (recommended) For MS Windows and Ubuntu, <A HREF="http://www.pythonxy.com">Python(x,y)</A> contains all (and even more) of the above Python modules. <A HREF="http://www.enthought.com/products/getepd.php"> Enthought Python Distribution</A> is another scientific Python distribution available for many platforms.
- (optional) <A HREF="http://www.doxygen.org">Doxygen</A> for documentation generation. Use preferably the latest svn version.

\section seccompilation Compilation
\subsection subseclinux Linux
\b Makefile is provided to build the whole AMR code. The make process is dependent on the \b SITE environment variable. Either set SITE, e.g. in a shell initialization script (.bashrc, .cshrc etc.), or use 'make SITE=<your_site_specifier>'. For each platform (SITE), the \b make.flags file must be adjusted. Simply run \verbatim make or make SITE=<your_site_specifier> \endverbatim to compile AMR.
\subsection subsecwin Windows
Project files for Compaq Visual Fortran are available in %ebe_run and %ebe_integration directories. Compile these projects to create the executable files. Compilation of the Fortran-Python interfaced functions with f2py is more cumbersome. A solution that should work is (see also http://www.scipy.org/F2PY_Windows):
  -# Download and install gfortran from http://gcc.gnu.org/wiki/GFortranBinaries
  -# Set "C_INCLUDE_PATH" environment variable to "C:\Path\to\gfortran\include" (The gfortran installer automatically adds its "bin" directory to your path)
  -# Modify the cygwinccompiler.py file in your Python intallation directory to use LooseVersion instead of StrictVersion for ld_version and dllwrap_version (also add to the import line)
  -# For compilation use \verbatim python path\to\f2py.py -c --fcompiler=gnu95 --compiler=mingw32 --f90exec=path\to\gfortran.exe --opt="-O" -lmsvcr71 \endverbatim

Use the above command to compile antenna_mast if needed by running it with '-m antenna_mast source\antenna_mast.f90' appended.

\page pageexec Executing AMR
\section seccfg Configuration files
There are two main configuration files: \b ebe6.cfg, \b ebe6_defaults.cfg (see \ref pagecfgdef), placed in the AMR root directory. In ebe6_defaults.cfg, which is a part of the subversion tree, are all configuration options with a short description and mostly with default values. If no default value is present, this entry must be included and specified in ebe6.cfg, which contains user supplied configuration. Suggested way to create a configuration is
  -# copy ebe6_defaults.cfg to ebe6.cfg
  -# edit ebe6.cfg---modify individual parameters, delete those which are not important for the particular setup

AMR first reads ebe6_defaults.cfg and then ebe6.cfg; in this way, the ebe6.cfg has a higher priority than ebe6_defaults.cfg for duplicate entries.

The \b antenna configuration file name is specified in ebe6.cfg. You may create this file by copying \b antenna_template.cfg and modyfying the new file.

\section secinput Input files
Input files must be placed in the input directory. The requirements depend on a particular configuration. SHOTNO is a 6-digit shot number with leading zeros (e.g. 001124) and T.TTT is the time in a fixed format (e.g. 0.050).

\subsection subseceqinput Equilibrium
  - \b psi_SHOTNO_T.TTT.out for IDL-style %equilibrium. These files can be created autamatically by the IDL programs (in the idl directory) if idl = yes in the cfg file.
  - \b m_equil_SHOTNO_T.TTT_VARNAME.dat for Matlab-style %equilibrium. VARNAME is the variable name the file contains: F, PsiF, PsiRZ, R, Z, values. Matlab-style %equilibrium files are output, e.g., by LUKE.

\subsection subsecprofinput Plasma profiles
  - \b ts_SHOTNO_T.TTT.out for experimental profiles. These files can be created autamatically by the IDL programs (in the idl directory) if idl = yes in the cfg file.
  - \b m_profs_SHOTNO_T.TTT_VARNAME.dat for Matlab-style profiles, e.g. from LUKE. VARNAME = R, ne, Te.

\subsection subsecotherinp Other
  - \b ts_times_SHOTNO.out contains a list of the experimental profiles data, typically Thomson scattering times.
  - \b ebe_times_SHOTNO.dat can override the ts_times file to select only particular simulations times. These file are used only for ts_times=yes in the cfg file.

\section secrun Running AMR
AMR is run (after it has been compiled) by \verbatim python ebe6.py \endverbatim
from the root AMR directory. Alternatively, you can specify a different configuration file by \verbatim python ebe6.py config_file_name.cfg \endverbatim

Various outputs are written, depending on the configuration, into ebe_run/output, ebe_integration/output, output directories. There are also protocol directories containing protocol (log) files, and protocol.txt file in the root directory.

\page pageluke AMR - LUKE interface

\section seclukeouts AMR outputs for LUKE
All files are located in output/luke directory.
\subsection subseclukerays luke_rays.dat
Contains configuration for each ray
\verbatim
time [s] | antenna no. |  config. no. | frequency [GHz] | power [kW] | conv_eff | window_transmission | xw | yw | zw | rw
\endverbatim
[xw, yw, zw] = waist position [m], rw = waist radius [m]
\subsection subseclukeeqprof Equilibrium and profiles
Stored in m_equil_SHOTNO_T.TTT_VAR.dat, m_profs_SHOTNO_T.TTT_VAR.dat files. SHOTNO = shot number (6 digits),
T.TTT = time, VAR is the output variable, i.e. R, Z, psi, BR, Bphi, BZ for the equilibrium and psi, ne, Te, for the profiles.
VAR=Zeff files contain single value of Zeff (there's no Zeff profile in AMR).
m_equil files with VAR=values contain
\verbatim
Raxis
Zaxis
Psi_LCFS
\endverbatim

\section seclukerayouts Ray tracing output for LUKE
\subsection subseclukerayfiles Output files
LUKERT_IA_ICNF_SHOTNO_T.TTT_IR.dat, IA = antenna number (starts with 00), ICNF = configuration number (starts with 0000),
SHOTNO = shot number (6 digits), T.TTT = time, IR = ray number (starts with 01). Columns in RT files are
\verbatim
1   2   3   4   5
R   Z  psi N|| Nperp
\endverbatim

\section seclukecfg ebe6.cfg
\code
[output]
# run LUKE from AMR
run_luke = yes | no

[luke]
# create LUKE outputs (turned on automatically if run_luke = yes)
outputs = yes | no
# minimum ray fragment length [m]
dsmin = 
# Wave damping model
# 0: cold
# 1: kinetic
# 2: R2D2 (requires IMSL)
# 4: electrostatic (fast)
kmode = 
# LUKE np, nt parameter (dimension of psi/theta grids)
np = no. of psi grid-points
nt = no. of theta grid-points 
# equilibrium export type (R-Z or psi-theta)
# (psi-theta overrides settings in [profiles] section)
# (R-Z turns 'matlab' export on with nR,nZ specified in [profiles] section)
equilibrium = R-Z

[profiles]
export = matlab
export_nR = dimension in R
export_nZ = dimension in Z
export_npsi = dimension in psi (for Te, ne profiles)
\endcode

\section secrunningluke Runnig LUKE from AMR
Set run_luke = yes in the cfg file. AMR will set the AMR environment variable
and execute LUKE by \verbatim matlab -nodisplay -r rundke_AMR(experiment,id_simul,shot_number,shot_time,iAntenna,iConfiguration,np,nt) \endverbatim

\page pagecfgdef ebe6_defaults.cfg

\include ebe6_defaults.cfg

 */
