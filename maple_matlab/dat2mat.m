function output = dat2mat(datfile),
%
% This function reads .dat AMR output files and returns matlab structures
% corresponding to the data fields.
%
% by J. Decker (joan.decker@cea.fr) and J. Urban (jakub.urban@ipp.cas.cz)
%
% 20 April 2009
%
if ~exist(datfile,'file'),
    error('Data file not found. Include full path or odify matlab path.')
end
%
fid = fopen(datfile);
%
fields = textscan(fid,'%s',1,'Whitespace','\n');
%
istart = 1;
if (fields{1}{1}(1)=='#'),
    istart = 2;
end
fields = fields{1}{1}(istart:end);
%
if(any(fields == ',')),
    sep=',';
else
    sep=' ';
end
fields = strsplit(sep,fields);
fields_mask = true(size(fields));
for ifield = 1:length(fields)
    if isempty(fields{ifield})
        fields_mask(ifield)=false;
    end
end
fields = fields(fields_mask);
nfields = length(fields);
%
data = textscan(fid,repmat('%f',[1,nfields]));
%
for ifield = 1:nfields,
    %
    % fieldname = fields(issep(ifield) + 1:issep(ifield + 1) - 1);
    fieldname = fields{ifield};
    %
    mfieldname = fieldname;
    mfieldname(fieldname == '+') = 'p';
    mfieldname(fieldname == '-') = 'm';
    mfieldname(fieldname == '/') = 'o';
    mfieldname(fieldname == '*') = 't';
    mfieldname(fieldname == '[') = '_';
    mfieldname(fieldname == ']') = '_';
    mfieldname(fieldname == '(') = '_';
    mfieldname(fieldname == ')') = '_';
    if ~isnan(str2double(mfieldname(1))),
        mfieldname = ['n',mfieldname];
    end
    %
    output.fieldnames.(mfieldname) = fieldname;
    output.(mfieldname) = data{ifield}.';
    %
end
%
fclose(fid);
%




