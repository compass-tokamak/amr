!> Trace flux surface
!!
!! Solves dx/dl = df/dy / ||grad f||,
!!        dy/dl =-df/dy / ||grad f||
!! using a Runge-Kutta solver.
!! (Kuznetsov, E.B. Optimal parametrization in numerical construction of curve. Journal of the Franklin Institute, 2007. 344(5): p. 658-671.)
!!
!! \param psi psi value of the flux surface
!! \param dlcoef coefficient for Runge-Kutta tracing solver step dl = dlcoeff*2*Pi*(x0-RMagAxis)
!! \param Rtrace array of R-coordinates of the flux surface trace
!! \param Ztrace array of Z-coordinates of the flux surface trace
!! \param ltrace array of the curve length along the flux surface trace
!! \param nsteps number of trace points (R,Z,l-trace length)
!! \param isClosed true if the flux surface is closed
!! \param eps (optional) Runge-Kutta accuracy, default 1e-6
!! \param n_B_extrem (optional) number of |B| minima

subroutine fluxTrace(psi,dlcoeff,Rtrace,Ztrace,ltrace,nsteps,isClosed,eps,n_B_extrem)
! subroutine fluxTrace(R,ntheta,theta,isClosed)
#ifdef _IMSL

#else
  use rkf45
#endif

!  use equilibrium
!  use utilities
!  use generalConstants

  implicit none

  real(8),intent(in) :: psi,dlcoeff
  logical,intent(out) :: isClosed
  real(8),intent(out) :: Rtrace(:),Ztrace(:),ltrace(:)
  integer(4),intent(out) :: nsteps
  real(8),optional :: eps
  integer(4),optional :: n_B_extrem

  real(8) :: x0,y0,l,l2,dl,theta0,theta2,dtheta,theta1,maxl,tol,B0,B1,sgnextr
  integer,parameter :: Neq=2
  real(8),parameter :: EPS_DEFAULT = 1.d-6
  real(8) :: PARAM(50),Y(Neq),Yp(Neq)
  integer IDO,iunit,ncross,maxsteps,rkstep
  character(len=50) :: myformat,fname
  integer(4) :: n_extrem
  logical :: calcB
  type(vector) :: gpsi,zeroPsi_start
  real(8) :: t1,cyl(3)

  if(present(eps)) then
    tol = eps
  else
    tol = EPS_DEFAULT
  endif

  calcB = present(n_B_extrem)

  maxsteps = min(size(Rtrace),size(Ztrace),size(ltrace))

!  myformat = '(20(E13.5))'
!  write(fname,'(''testLCFS'',F9.6,''.dat'')')getrho(psi)
!  iunit=openFile(fname)
!  write(iunit,'(''x,y,l'')')

  ! starting point
  ! x0 = getRLCFS_LFS()  ! + 5.1d-4
  y0 = getZMagAxis()
  diffPsiUserValueR_PsiValue = psi
  diffPsiUserValueR_ZValue = y0
  x0 = findRoot(getRMagAxis(),getRMax(),diffPsiUserValueR,tol)
  l  = 0
  Rtrace(1) = x0
  Ztrace(1) = y0
  ltrace(1) = l
  ! trace contour step (in trace length)
  dl = dlcoeff*2*Pi*(x0-RMagAxis)
  ! maximum curve length
  maxl = 2*Pi*(x0-getRMagAxis())*10
  theta0 = 0 !mytheta(y0,x0)
  theta1 = 0
  theta2 = theta0
  dtheta = -10
  nsteps = 1
  isClosed = .true.
  if (calcB) B0 = getBtot(Rtrace(1),0d0,Ztrace(1))

  ! now solve dx/dl = df/dy / ||grad f||
  !           dy/dl =-df/dy / ||grad f||

  ncross = 0
  y = (/x0,y0/)
  do while (ncross<2)
    PARAM = 0
    l2 = l+dl
!    write(iunit,myformat)y(1),y(2),l,getBtot(y(1),0d0,y(2))
#ifdef _IMSL
    IDO = 1
    CALL DIVPRK (IDO, Neq, fluxTrace_FCN, l, l2, TOL, PARAM, Y)
#else
    IDO = 1
    call r8_rkf45 ( fluxTrace_FCN, Neq, y, yp, l, l2, tol, 0.d0, IDO )
    ! IDO = -2
#endif
    nsteps = nsteps + 1
    if (nsteps>maxsteps) then
      write(*,*)'nsteps>maxsteps in fluxTrace'
      stop
    endif
!    if (abs(getPsi(y(1),0d0,y(2))-psi)>tol) then
!      zeroPsi_start = cylindricalVector(y(1),0d0,y(2))
!      call getGradPsi(zeroPsi_start,gpsi)
!      gpsi = sign(1.d0,psi-getPsi(y(1),0d0,y(2)))*gpsi
!      t1=tol*1d1
!      do while (zeroPsi(t1)*zeroPsi(0.d0)>0)
!        t1 = t1 + tol*1d1
!      enddo
!      t1 = findRoot(0.d0,t1,zeroPsi,tol)
!      cyl = toCylindricalArray(zeroPsi_start + t1*gpsi)
!      y(1) = cyl(1)
!      y(2) = cyl(3)
!    endif
    Rtrace(nsteps) = y(1)
    Ztrace(nsteps) = y(2)
    ltrace(nsteps) = l2
    ! pozor na skok +/-
    theta2 = mytheta(y(2),y(1))
    if (calcB) B1 = getBtot(Rtrace(nsteps),0d0,Ztrace(nsteps))
    if (nsteps==2) then
      ! dtheta = abs(theta2-theta0)
      dtheta = theta2
      ncross = 0
      sgnextr = B1-B0
      n_extrem = 0
    ! else if (abs(theta2-theta0)<dtheta) then
    else
      if (theta2*dtheta<0) then
        ncross = ncross+1
        dtheta = theta2
!        if (ncross==1) then
!          iHalfTrace = nsteps-1
!        endif
      else if (y(2)>=PsiRZDomain(4) .or. y(2)<=PsiRZDomain(3) .or. y(1)<=PsiRZDomain(1) .or. y(1)>=PsiRZDomain(2)) then
        ! not closed
        isClosed = .false.
        ! write(*,*)'Flux surface not closed'
        ncross = 2
      else if (dtheta>theta2) then
        isClosed = .false.
        ! write(*,*)'Flux surface not closed'
        ncross = 2
        ! write(*,*)'problem'
      endif
      if (calcB .and. (B1-B0)*sgnextr<0) then
        n_extrem  = n_extrem+1
        sgnextr = -sgnextr
      endif
      B0 = B1
    endif
!    write(iunit,myformat)y(1),y(2),l
    ! IDO = 2
    do rkstep=1,400 ! re-initialize Runge-Gutta after 400 steps
      l2 = l+dl
#ifdef _IMSL
      CALL DIVPRK (IDO, Neq, fluxTrace_FCN, l, l2, TOL, PARAM, Y)
#else
      call r8_rkf45 ( fluxTrace_FCN, Neq, y, yp, l, l2, tol, 0.d0, IDO )
      if (ido==7) then
        ido = 2
      else if (ido/=2 .and. ido/=-2) then
        write(*,*)'warning: ido = ',ido,' after r8_rkf45 if fluxTrace, nsteps = ',nsteps
      endif
#endif
      nsteps = nsteps + 1
      if (nsteps>maxsteps) then
        write(*,*)'nsteps>maxsteps in fluxTrace'
        stop
      endif
      ! correction
!      if (abs(getPsi(y(1),0d0,y(2))-psi)>tol) then
!        zeroPsi_start = cylindricalVector(y(1),0d0,y(2))
!        call getGradPsi(zeroPsi_start,gpsi)
!        gpsi = sign(1.d0,psi-getPsi(y(1),0d0,y(2)))*gpsi
!        t1=tol*1d1
!        do while (zeroPsi(t1)*zeroPsi(0.d0)>0)
!          t1 = t1 + tol*1d1
!        enddo
!        t1 = findRoot(0.d0,t1,zeroPsi,tol)
!        cyl = toCylindricalArray(zeroPsi_start + t1*gpsi)
!        y(1) = cyl(1)
!        y(2) = cyl(3)
!      endif

      theta2 = mytheta(y(2),y(1))
      Rtrace(nsteps) = y(1)
      Ztrace(nsteps) = y(2)
      ltrace(nsteps) = l2
      if (calcB) B1 = getBtot(Rtrace(nsteps),0d0,Ztrace(nsteps))
      ! if (abs(theta2-theta0)<dtheta) then
        ! we arrived at the starting point
      if (theta2*dtheta<0) then
        ncross = ncross +1
        dtheta = theta2
!        if (ncross==1) then
!          iHalfTrace = nsteps-1
!        endif
      else if (y(2)>=PsiRZDomain(4) .or. y(2)<=PsiRZDomain(3) .or. y(1)<=PsiRZDomain(1) .or. y(1)>=PsiRZDomain(2)) then
        ! not closed
        isClosed = .false.
        ! write(*,*)'Flux surface not closed'
        ncross = 2
      else if (dtheta>theta2) then
        isClosed = .false.
        ! write(*,*)'Flux surface not closed'
        ncross = 2
        ! write(*,*)'problem'
      endif
      if (calcB .and. (B1-B0)*sgnextr<0) then
        n_extrem  = n_extrem+1
        sgnextr = -sgnextr
      endif
      B0 = B1
!      write(iunit,myformat)y(1),y(2),l
      if (ncross==2) exit
    enddo
#ifdef _IMSL
    IDO = 3
    CALL DIVPRK (IDO, Neq, fluxTrace_FCN, l, l2, TOL, PARAM, Y)
#else
    ! call r8_rkf45 ( fluxTrace_FCN, Neq, y, yp, l, l2, tol, 0.d0, IDO )
#endif
    if (l>maxl) then
      write(*,*)'l > maxl in traceLCFS! maxl = ',maxl
      stop
    endif
  enddo

  if (sqrt((Rtrace(nsteps)-x0)**2+(Ztrace(nsteps)-y0)**2)>2*dl) then
    write(*,*)'error: last flux trace point misaligned - surface not closed?'
    isClosed = .false.
  else
    Rtrace(nsteps) = x0
    Ztrace(nsteps) = y0
    ltrace(nsteps) = ltrace(nsteps-1) + sqrt((Rtrace(nsteps-1)-Rtrace(nsteps))**2 + (Ztrace(nsteps-1)-Ztrace(nsteps))**2)
  endif

  if (calcB) n_B_extrem = n_extrem

!  write(*,*)'curve length = ',ltrace(nsteps) !,l+sqrt((x0-y(1))**2+(y0-y(2))**2)
!  write(*,*)'nsteps = ',nsteps
!  close(iunit)

  contains

  real(8) function mytheta(y,x)
    real(8),intent(in) :: y,x
    
!    if (y>0) then
      mytheta = atan2(y-getZMagAxis(),x-getRMagAxis())
!    else
!      mytheta = TWOPI+atan2(y-getZMagAxis(),x-getRMagAxis())
!    endif
  end function

  real(8) function zeroPsi(t)
    real(8),intent(in) :: t

    zeroPsi = psi - getPsi(zeroPsi_start + t*gpsi)
  end function

end subroutine

!> Input function for the R-K sover used in fluxTrace
#ifdef _IMSL
subroutine fluxTrace_FCN (N, T, Y, YPRIME)
  integer,intent(in) :: N
#else
subroutine fluxTrace_FCN (T, Y, YPRIME)
#endif
  real(8),intent(in) :: T
  real(8),dimension(*),intent(in) :: Y
  real(8),dimension(*),intent(out) :: YPRIME
  ! save for the out-of-domain case
  real(8),save :: fx,fy,norm

  if (y(2)>PsiRZDomain(4) .or. y(2)<PsiRZDomain(3) .or. y(1)<PsiRZDomain(1) .or. y(1)>PsiRZDomain(2)) then
    ! write(*,*)'warning: out of bounds during flux surface tracing'
  else
    fx = DPsi_RZ(1,0,y(1),y(2))
    fy = DPsi_RZ(0,1,y(1),y(2))
    norm = sqrt(fx*fx+fy*fy)
  endif
  ! -/+  to move in +theta (counter-clockwise)
  YPRIME(1) =-fy/norm
  YPRIME(2) = fx/norm

end subroutine

!> Calculate the flux average of a given function (R,Z)
!!
!! \param Rtrace R-coordinates of the flux surface trace
!! \param Ztrace Z-coordinates of the flux surface trace
!! \param ltrace l-parameter (length) of the flux surface trace
!! \param Rtrace R-coordinates of the flux surface trace
!! \param ntrace number of trace points
!! \param fnRZ the function to average, real(8) fnRZ(R,Z)
real(8) function fluxAverage_trace(Rtrace,Ztrace,ltrace,ntrace,fnRZ)

  real(8),intent(in) :: Rtrace(:),Ztrace(:),ltrace(:)
  integer(4),intent(in) :: ntrace
  real(8),external :: fnRZ
  real(8) :: Bpol,norm
  integer(4) :: i

  fluxAverage_trace = 0
  norm = 0
  do i=2,ntrace
    Bpol = getBpol(Rtrace(i),Ztrace(i))
    ! evaluate in the center of the line element
    ! Bpol = getBpol((Rtrace(i)+Rtrace(i-1))/2,(Ztrace(i)+Ztrace(i-1))/2)
    fluxAverage_trace = fluxAverage_trace + fnRZ(Rtrace(i),Ztrace(i)) * (ltrace(i)-ltrace(i-1)) / Bpol
    norm = norm + (ltrace(i)-ltrace(i-1)) / Bpol
  enddo
  fluxAverage_trace = fluxAverage_trace / norm

end function

!> Calculate the flux average of a given function (R,Z)
!!
!! This function searchs fluxPsiGrid to find index i for which fluxPsiGrid(i) <= psi <= fluxPsiGrid(i+1).
!! For these indeces the flux averages are evaluated and linerly interpolated to the input psi.
!!
!! \param psi psi-coordinate
!! \param fnRZ the function to average, real(8) fnRZ(R,Z)
real(8) function fluxAverage_psi(psi,fnRZ)

  real(8),intent(in) :: psi
  real(8),external :: fnRZ
  real(8) :: aver(2)
  integer(4) :: ipsi
  
  if (psi>psi_LCFS) then
    fluxAverage_psi = 0
  else
    ipsi = binSearch(psi,fluxPsiGrid)
    if (ipsi==fluxPsiGridDim) then
      aver(1) = fluxAverage(fluxRtrace(ipsi-1,:),fluxZtrace(ipsi-1,:),fluxltrace(ipsi-1,:),fluxNTrace(ipsi+1),fnRZ)
      aver(2) = fluxAverage(fluxRtrace(ipsi,:),fluxZtrace(ipsi,:),fluxltrace(ipsi,:),fluxNTrace(ipsi),fnRZ)
      fluxAverage_psi = linInterp(psi,fluxPsiGrid(ipsi-1:ipsi),aver)
    else
      aver(1) = fluxAverage(fluxRtrace(ipsi,:),fluxZtrace(ipsi,:),fluxltrace(ipsi,:),fluxNTrace(ipsi),fnRZ)
      aver(2) = fluxAverage(fluxRtrace(ipsi+1,:),fluxZtrace(ipsi+1,:),fluxltrace(ipsi+1,:),fluxNTrace(ipsi+1),fnRZ)
      fluxAverage_psi = linInterp(psi,fluxPsiGrid(ipsi:ipsi+1),aver)
    endif
  endif

end function

!> Calculate flux-surface maximum of a given function.
real(8) function fluxMaximum_trace(Rtrace,Ztrace,ltrace,ntrace,fnRZ)

  real(8),intent(in) :: Rtrace(:),Ztrace(:),ltrace(:)
  integer(4),intent(in) :: ntrace
  real(8),external :: fnRZ
  integer(4) :: i
  real(8) :: fluxmax

  fluxmax = fnRZ(Rtrace(1),Ztrace(1))
  do i=2,ntrace-1
    fluxmax = max(fluxmax,fnRZ(Rtrace(i),Ztrace(i)))
  enddo
  fluxMaximum_trace = fluxmax
end function


