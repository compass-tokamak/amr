from numpy import *
import matplotlib.pylab as plt
import scipy.interpolate
import scipy.optimize
import scipy as S
from matplotlib.mlab import *

# specify input file name and read
equilibriumFileName = 'eql_var_Dave_5.dat'
feq = open(equilibriumFileName,'r')
#for i,fl in enumerate(feq):
#    if i==2: n_surfs = int(fl)
#    if i==4: 
#        npt = int(fl)
#        break
        
fl = feq.readline().strip().split()
Psi_LCFS = float(fl[0])
PsiMagAxis = float(fl[1])
RMagAxis  = float(fl[2])
ZMagAxis = float(fl[3])
BvacR = float(fl[4])
BvacR=-1*BvacR*RMagAxis

fl = feq.readline().strip().split()
rtest = float(fl[0])
ztest = float(fl[1])
bpoltest = float(fl[2])

fl = feq.readline()
n_surfs = int(fl)

psin=[]; psiabs=[]; q=[]; p=[]; f=[]; dens=[]; temp=[];
R=[[] for dummy in range(n_surfs)]; Z=[[] for dummy in range(n_surfs)]

i=0
fl = feq.readline()
while fl.strip():
    # print fl
    flspl = fl.strip().split()
    psin.append(float(flspl[0]))
    psiabs.append(float(flspl[1]))
    q.append(float(flspl[2]))
    p.append(float(flspl[3]))
    f.append(float(flspl[4]))
    dens.append(float(flspl[5]))
    temp.append(float(flspl[6]))
    npt = int(feq.readline())
    print('i=%i, npt=%i'%(i,npt))
    for k in range(npt):
        flspl = feq.readline().strip().split()
        R[i].append(float(flspl[0]))
        Z[i].append(float(flspl[1]))
    i+=1
    fl = feq.readline()

feq.close()

# convert to numpy arrays
R=array(R)
Z=array(Z)
# plot flux surfaces
plt.figure(1)
for i in range(n_surfs):
    plt.plot(R[i,:],Z[i,:],'b-')
plt.axis('image')
plt.show()

# prepare for interpolation
PsiRDim = 65
PsiZDim = 135
extfRL = 1.1
extfRH = 1.1
extfZ  = 1.05
# 0.87 OK with quintic and 205 pts, but not in LUKE
# 0.8 quintic 205 still no good in LUKE
LCFS_coeff = 1
#interp_function = 'quintic'
interp_function = 'cubic'
# interp_function = 'linear'
# reduce the number of datapoints by ndiv (avoid memory problems)
ndiv = 2

Ri = linspace(R.min()/(extfRH),R.max()*extfRL,PsiRDim)
Zi = linspace(Z.min()*extfZ,Z.max()*extfZ,PsiZDim)
# exclude last datapoint
inrange = range(0,npt-1,ndiv) #+ [npt-1]
ni = len(inrange)
Ra = zeros(n_surfs*ni)
Za = zeros(n_surfs*ni)
psia = zeros(n_surfs*ni)
for i in range(n_surfs):
    for k,j in enumerate(inrange):
        Ra[i*(ni)+k] = R[i,j]
        Za[i*(ni)+k] = Z[i,j]
        #psia[i*(ni)+k] = psin[i]
        psia[i*(ni)+k] = psiabs[i]
# interpolate, linear best for extrapolation
rbfi     = S.interpolate.Rbf(Ra, Za, psia, function=interp_function,smooth=0)  # radial basis function interpolator instance
rbfi_ext = S.interpolate.Rbf(Ra, Za, psia, function=interp_function,smooth=0)  # radial basis function interpolator instance
#psii = rbfi(Ri, Zi)
psii = zeros((PsiRDim,PsiZDim))
for i in range(PsiRDim):
    for j in range(PsiZDim):
        psii[i,j] = rbfi(Ri[i], Zi[j])
        if psii[i,j]>psiabs[-1]: psii[i,j] = rbfi_ext(Ri[i], Zi[j])
# plot interpolated flux surfaces
plt.figure(2)
plt.contour(Ri,Zi,psii.T,linspace(psiabs[0],psiabs[-1],5))
plt.contour(Ri,Zi,psii.T,linspace(psiabs[-1],psii.max(),5,endpoint=False))
plt.plot(Ra,Za,'b+')
plt.axis('image')
plt.show()

# dig out equilibrium parameters
# construct R(psi) (or psi(R)) in midplane
R_psi = zeros((len(psin)+1))
for i,psi in enumerate(psin):
    R_psi[i+1] = R[i,:].max()
def rbfi_a(x):
    return rbfi(x[0],x[1])
x0 = array([R_psi[1:].min(),0])
xmin = S.optimize.fmin(rbfi_a,x0,xtol=1e-9, ftol=1e-9)
fmin = rbfi_a(xmin)
print xmin
R_axis = xmin[0]
Z_axis = xmin[1]
R_psi[0] = xmin[0]
if LCFS_coeff<1: print 'Reducing LCFS psi value by %g ' % LCFS_coeff
psi_LCFS = psiabs[-1]*LCFS_coeff
plt.contour(Ri,Zi,psii.T,array([psi_LCFS]),colors='r',linewidths=2,linestyles='dashed')
def psi_midpl(x):
    return array([rbfi(xi,Z_axis)-psi_LCFS for xi in x])
R_LCFS_HFS = R[-1,:].min()
R_LCFS_LFS = R_psi[-1]
R_LCFS_HFS = S.optimize.fsolve(psi_midpl,R_LCFS_HFS)
R_LCFS_LFS = S.optimize.fsolve(psi_midpl,R_LCFS_LFS)
print 'Original R LCFS: %.6f, %.6f' % (R[-1,:].min(),R_psi[-1])
print 'New R LCFS     : %.6f, %.6f' % (R_LCFS_HFS,R_LCFS_LFS)

i = 2
# psi_axis = (psin[i]*psi_LCFS - psiabs[i])/(psin[i] - 1)
psi_axis = fmin

# save to files
st_suff = '_000100_1.000'
fn_pref = 'm_equil' + st_suff
savetxt(fn_pref + '_R.dat',Ri,fmt='%.10g')
savetxt(fn_pref + '_Z.dat',Zi,fmt='%.10g')
savetxt(fn_pref + '_PsiRZ.dat',psii.T,fmt='%.10g')
F_psi = array(f[0:1] + f) * BvacR
savetxt(fn_pref + '_F.dat',F_psi,fmt='%.10g')
psi_F = array([psi_axis] + psiabs)
savetxt(fn_pref + '_PsiF.dat',psi_F,fmt='%.10g')
f_v = open(fn_pref + '_values.dat','w')
f_v.write('%.10g\n'%R_axis)
f_v.write('%.10g\n'%Z_axis)
f_v.write('%.10g\n'%psi_LCFS)
f_v.write('%.10g\n'%R_LCFS_HFS)
f_v.write('%.10g\n'%R_LCFS_LFS)
f_v.close()

# ne,Te - no inter/extrapolation
fn_pref = 'm_profs' + st_suff
savetxt(fn_pref + '_R.dat',R_psi,fmt='%.10g')
ne_psi = array(dens[0:1] + dens)
savetxt(fn_pref + '_ne.dat',ne_psi*1e19,fmt='%.10g')
Te_psi = array(temp[0:1] + temp)
savetxt(fn_pref + '_Te.dat',Te_psi,fmt='%.10g')
