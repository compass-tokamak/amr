module utilities
  implicit none

  interface median
    module procedure median_real
  end interface

  interface mean
    module procedure mean_real
  end interface

  interface str
    module procedure int2str
  end interface

  integer(4),private :: protocolUnit = -1

  contains

  ! find a free (unopened) unit number
  integer(4) function getFreeUnitNumber()
    implicit none
    integer(4) :: ifile
    logical :: op

    do ifile = 101,999
      inquire(unit=ifile,opened=op)
      if (.not.op) exit
    enddo
    getFreeUnitNumber = ifile
  end function

  ! opens a file and returns its unit number
  integer(4) function openFile(file,action,status)
    implicit none 
    character*(*),intent(in) :: file
    character*(*),optional :: action,status
    integer(4) :: funit
    logical :: fexist

    fexist = .true.
    if (present(status)) then
      if (trim(status)=='old' .or. status=='OLD') then
        inquire(file=file,exist=fexist)
      endif
    endif
    if (fexist) then
      funit = getFreeUnitNumber()
      if (present(action) .and. present(status)) then
        open(funit,file=file,action=action,status=status)
      else if (present(action)) then
        open(funit,file=file,action=action)
      else if (present(status)) then
        open(funit,file=file,status=status)
      else
        open(funit,file=file)
      endif
    else
      funit = -1
    endif
    openFile = funit
  end function

  ! opens a protocol file and returns its unit number
  subroutine openProtocolFile(fname)
    implicit none 
    character*(*),intent(in) :: fname

    protocolUnit = openFile(fname)
  end subroutine

  integer(4) function getProtocolUnit()
    implicit none 

    getProtocolUnit = protocolUnit
  end function

  subroutine closeProtocolFile()
    implicit none 

    close(protocolUnit)
  end subroutine

  ! returns number of lines in a file
  integer(4) function file_length(fname)
    implicit none 
    character*(*),intent(in) :: fname
    integer :: eof
    integer(4) :: n,ifile

    n = 0
    ifile = openFile(trim(fname),action='read')
    do while (.true.)
      read(ifile,*,iostat=eof)
      if (eof/=0) exit
      n = n+1
    enddo
    close(ifile)
    file_length = n
  end function

  real(8) function median_real(len,sample)
    use m_mrgref
    implicit none
    integer(4),intent(in) :: len
    real(8),intent(in) :: sample(len)
    integer(4) :: rank(len)

    call mrgref(sample(1:len),rank)
    if (modulo(len,2) == 0) then
      median_real = (sample(rank(len/2)) + sample(rank(len/2+1))) / 2
    else
      median_real = sample(rank(len/2+1))
    endif
  end function

  real(8) function mean_real(len,sample)
    use m_mrgref
    implicit none
    integer(4),intent(in) :: len
    real(8),intent(in) :: sample(len)

    mean_real = sum(sample(1:len))/len
  end function

  function int2str(i)
    implicit none
    character(len=10):: int2str
    integer(4),intent(in) :: i

    write(int2str,'(I0)')i
  end function

  !> Binary search in a sorted array
  integer(4) function binSearch(val,arr,nar)
    integer(4),intent(in),optional :: nar
    real(8),intent(in) :: val,arr(:)
    integer(4) :: a,b,m
    logical :: inv

    if (present(nar)) then
      b = nar
    else
      b = size(arr)
    endif
    a = 1

    inv = (arr(a) > arr(b))

    do while (a<=b)
      m = (a+b)/2
      if (arr(m)<=val .and. val<=arr(m+1)) exit
      if ((arr(m) < val) .neqv. inv) then
        a = m + 1
      else
        b = m - 1
      endif
    enddo
    binSearch = m
  end function

  !> Linear inter/extrapolation 
  real(8) function linInterp(x,xarr,yarr)
    real(8),intent(in) :: x,xarr(2),yarr(2)

    linInterp = yarr(1) + (yarr(1)-yarr(2))/(xarr(1)-xarr(2))*(x-xarr(1))
  end function

  !> Numerical integration
  !!
  !! 10-21 Gauss-Kronrod adaptive quadrature. Calls IMSL DQDAG or NAG D01AJF.
  !! \param f real(8) function of x
  !! \param a lower intergration limit
  !! \param b upper intergration limit
  !! \param tol relative error tolerance
  !! \param error indicater, 0 if success
  real(8) function integ2(f,a,b,tol,ierr)
    real(8),external :: f
    real(8),intent(in) :: a,b,tol
    integer(4),optional :: ierr
    integer(4) :: err_loc
    real(8) :: res,abserr
#ifdef _IMSL

#else                                                                        
    integer            lwk,liw 
    parameter          (lwk=2000,liw=lwk/4) 
    double precision   wk(lwk), wk2(lwk) 
    integer            iw(liw), iw2(liw) 
#endif                                                                        

    err_loc = 0

#ifdef _IMSL
    ! IRULE = 2 for 10-21 points rule
    CALL DQDAG (f,a,b,0.0d0,tol,2,res,abserr)
#else
    err_loc = 1
    CALL D01AJF(f,a,b,0.0d0,tol,res,abserr,wk,lwk,iw,liw,err_loc)
#endif

    if (present(ierr)) ierr = err_loc
    integ2 = res
  end function

! GFORTAN ADDITIONS
#ifdef _GFORTRAN
  real(8) function tand(a)
    use generalConstants
    real(8),intent(in) :: a

    tand = tan(a/180*Pi)
  end function

  real(8) function cosd(a)
    use generalConstants
    real(8),intent(in) :: a

    cosd = cos(a/180*Pi)
  end function

  real(8) function sind(a)
    use generalConstants
    real(8),intent(in) :: a

    sind = sin(a/180*Pi)
  end function

  real(8) function acosd(a)
    use generalConstants
    real(8),intent(in) :: a

    acosd = acos(a)*180/Pi
  end function

  real(8) function asind(a)
    use generalConstants
    real(8),intent(in) :: a

    asind = asin(a)*180/Pi
  end function

#endif
! GFORTRAN ADDITIONS

end module
