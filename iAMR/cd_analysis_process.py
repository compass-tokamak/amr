'''
Current-drive analysis---process results read by cd_analysis_read and stored in
cd_results_all[sim_id][ifreq].
Create cd_rho_max, cd_I_tot, cd_eff 2D arrays [ifreq,iSimlu_id].
'''

from numpy import *
import matplotlib
from matplotlib.pylab import *
from matplotlib.pyplot import *
import amrplot
import os.path

Ptot_min = float(iPrompt('Minimum Ptot:',defval='0.6'))
Ptot_max = float(iPrompt('Maximum Ptot:',defval='1.1'))
cd_mech_crit = 0.9
n_harm_luke = [1,2,3]

if iPrompt('Walk through subdirs (requires only subdirs in current directory) (amrpath MUST be defined) (y/n)',defval='n').lower() == 'y':
    for d in os.listdir('.'):
        if os.path.isdir(d):
            os.chdir(d)
            if any(map(lambda sx: os.path.splitext(sx)[1]=='.cfg', os.listdir('.'))):
                execfile(os.path.join(amrpath,'iAMR.py'))
                execfile(os.path.join(iamrpath,'cd_analysis_read.py'))
            os.chdir('..')

# assuming results are in cd_results_all

# read plasma data
try:
    ne19
    TekeV
    Rmaj
except NameError:
    try:
        ne19 = profpsi[0].getData('denrz')[0]
        TekeV = profpsi[0].getData('temprz')[0]
        Rmaj = prof0d[0].getData('r_axis')[0]
    except (NameError,IndexError):
        print 'Plasma parameters unknown (test_profiles outputs not found). Please enter manually.'
        ne19 = float(iPrompt('Electron density [10^19 m-3]'))
        TekeV = float(iPrompt('Electron temperature [keV]'))
        Rmaj = float(iPrompt('Major radius [m]'))
    
nsim = len(cd_results_all)
sim_ids = cd_results_all.keys()
sim_ids.sort()

cd_freqs = []
# simulation (calculated) frequencies for particular sim_id
cd_calc_freqs = {}
for iz,sim_id in enumerate(sim_ids):
    cd_calc_freqs[sim_id] = [res['freq'] for res in cd_results_all[sim_id]]
    for f in cd_calc_freqs[sim_id]:
        if not f in cd_freqs: cd_freqs.append(f)
cd_freqs.sort()
cd_freqs=array(cd_freqs)
nfreq = len(cd_freqs)
cd_rhomax = zeros((nfreq,nsim))
cd_rhojmax = zeros((nfreq,nsim))
cd_rhomid = zeros((nfreq,nsim))
cd_rhojmid= zeros((nfreq,nsim))
cd_rhofwhm = zeros((nfreq,nsim))
cd_rhojfwhm = zeros((nfreq,nsim))
cd_Itot = zeros((nfreq,nsim))
cd_Ptot = zeros((nfreq,nsim))
cd_PtotP0 = zeros((nfreq,nsim))
cd_cdeff = zeros((nfreq,nsim))
cd_zeta = zeros((nfreq,nsim))
freq_min = zeros(nfreq)
freq_max = zeros(nfreq)
cd_mechanism = zeros((nfreq,nsim),dtype='|S24')
cd_harm_approach = zeros((nfreq,nsim),dtype='|S24')
cd_harm_n = zeros((nfreq,nsim))
cd_Npar_av = zeros((nfreq,nsim))
cd_Npar_av2 = zeros((nfreq,nsim))
cd_Npar_sigma = zeros((nfreq,nsim))
# masks 1) if calculated 2) if absorbed P within limits
cd_calc_mask = zeros((nfreq,nsim),dtype=bool)
cd_Plim_mask = zeros((nfreq,nsim),dtype=bool)

cd_all_vars = [ \
    'cd_freqs',\
    'cd_rhomax',\
    'cd_rhojmax',\
    'cd_rhomid',\
    'cd_rhojmid',\
    'cd_rhofwhm',\
    'cd_rhojfwhm',\
    'cd_Itot',\
    'cd_Ptot',\
    'cd_PtotP0',\
    'cd_cdeff',\
    'cd_zeta',\
    'freq_min',\
    'freq_max',\
    'cd_calc_mask',\
    'cd_Plim_mask',\
    'cd_calc_freqs',\
    'sim_ids',\
    'cd_results_all',\
    'cd_mechanism',\
    'cd_harm_n',\
    'cd_Npar_av',\
    'cd_Npar_sigma',\
    'cd_harm_approach',\
    ]

fn_ne = S.interpolate.InterpolatedUnivariateSpline(profpsi[0]['rho'],profpsi[0]['denrz'],k=3)
fn_Te = S.interpolate.InterpolatedUnivariateSpline(profpsi[0]['rho'],profpsi[0]['temprz'],k=3)

for ifreq,freq in enumerate(cd_freqs):
    for iz,sim_id in enumerate(sim_ids):
#        mPd = cd_results_all[sim_id][ifreq]['LPd'][mi]
#        luke_results_1p{ifreq,iz}.mPd = mPd;
#        luke_results_1p{ifreq,iz}.mi = mi;
        cd_calc_mask[ifreq,iz] = freq in cd_calc_freqs[sim_id]
        if cd_calc_mask[ifreq,iz]:
            ifreq_calc = cd_calc_freqs[sim_id].index(freq)
            mi = abs(cd_results_all[sim_id][ifreq_calc]['Lj']).argmax()
            cd_rhojmax[ifreq,iz] = cd_results_all[sim_id][ifreq_calc]['Lrho'][mi]
            mi = abs(cd_results_all[sim_id][ifreq_calc]['LPd']).argmax()
            cd_rhomax[ifreq,iz] = cd_results_all[sim_id][ifreq_calc]['Lrho'][mi]
            cd_rhomid[ifreq,iz] = mean(cd_results_all[sim_id][ifreq_calc]['fwhm_LPd'][1])
            if cd_rhomid[ifreq,iz]<1e-3:
                print('cd_rhomid[%i,%i]<1e-3' % (ifreq,iz))
                print('sim_id: %s, freq:%g' % (sim_id,freq))
                print('fwhm_LPd=%s' % str(cd_results_all[sim_id][ifreq_calc]['fwhm_LPd']))
            cd_rhojmid[ifreq,iz] = mean(cd_results_all[sim_id][ifreq_calc]['fwhm_Lj'][1])
            cd_rhojmax[ifreq,iz] = cd_results_all[sim_id][ifreq_calc]['Lrho'][abs(cd_results_all[sim_id][ifreq_calc]['Lj']).argmax()]
            cd_rhofwhm[ifreq,iz] = cd_results_all[sim_id][ifreq_calc]['fwhm_LPd'][0]
    #        if cd_rhofwhm[ifreq,iz]<1e-3:
    #            print [ifreq,iz]
    #            print [sim_id][ifreq]
    #            print cd_results_all[sim_id][ifreq_calc]['fwhm_LPd']
            cd_rhojfwhm[ifreq,iz] = cd_results_all[sim_id][ifreq_calc]['fwhm_Lj'][0]
            # scale to 1 MW
            cd_Ptot[ifreq,iz] = cd_results_all[sim_id][ifreq_calc]['LPtot']
            cd_PtotP0[ifreq,iz] = cd_results_all[sim_id][ifreq_calc]['LPtot']/cd_results_all[sim_id][ifreq_calc]['LP0']
            cd_Plim_mask[ifreq,iz] = Ptot_min<=cd_PtotP0[ifreq,iz]<=Ptot_max
    #        print sim_id+(',%g,%i' % (ifreq,iz))
    #        print cd_PtotP0[ifreq,iz]
            cd_Itot[ifreq,iz] = 1e-3*cd_results_all[sim_id][ifreq_calc]['LItot']/(cd_results_all[sim_id][ifreq_calc]['LPtot']) #/cd_results_all[sim_id][ifreq_calc]['P0'])
            if not (Ptot_min < cd_PtotP0[ifreq,iz] < Ptot_max):
                print('Ptot/P0 = %g, freq = %g, sim_id = ' % ((cd_results_all[sim_id][ifreq_calc]['LPtot']/cd_results_all[sim_id][ifreq_calc]['P0']),freq) + sim_id)
            else:
                if freq_min[ifreq]>cd_Itot[ifreq,iz]: freq_min[ifreq] = cd_Itot[ifreq,iz]
                if freq_max[ifreq]<cd_Itot[ifreq,iz]: freq_max[ifreq] = cd_Itot[ifreq,iz]
            cd_cdeff[ifreq,iz] = abs(cd_Itot[ifreq,iz]) * 3.27 * Rmaj * ne19 / TekeV;
            cd_zeta[ifreq,iz] = cd_results_all[sim_id][ifreq_calc]['zeta']
            
            # === CD mechanism analysis
            dPrayharm = array([r['dPharm'] for r in cd_results_all[sim_id][ifreq_calc]['rays']])
            dPray = sum(dPrayharm,axis=2)
            dPharm = sum(dPrayharm,axis=0)
            # dPharm = reduce(add,[r['dPharm'] for r in cd_results_all[sim_id][ifreq_calc]['rays']])
            dPrho = sum(dPharm,axis=1)
            rhosort = argsort(dPrho)
            # indeces of absorption location, whose sum dP > cd_mech_crit
            rho_ind = rhosort[cumsum(dPrho[rhosort])>(1-cd_mech_crit)*sum(dPrho)]
            # find harmonic numbers
            harm_rho = [argwhere(a>(1-cd_mech_crit)*sum(a)).reshape(-1) for a in dPharm[rho_ind]]
            # harm_rho = map(lambda h: h[0] if len(h)==1 else nan,harm_rho)
            Npar_rayrho = array([r['Npar'] for r in cd_results_all[sim_id][ifreq_calc]['rays']])
            nanmask = isfinite(Npar_rayrho[:,rho_ind])
            weights=dPray[:,rho_ind]
            weights[~nanmask]=0
            if sum(weights)<=0:
                print('cannot calculate Npar averages, sum(dP_ray)=0; freq = %g, sim_id = %s' % (freq, sim_id))
                Npar_av2 = nan
                Npar_av = nan
                Npar_sigma = nan
                w_wce_av = nan
                nce  = nan
                absp_approach = 'Undetermined'
                cd_mechanism[ifreq,iz] = 'Undetermined'
            else:
                Npar_av2 = average(nan_to_num(Npar_rayrho[:,rho_ind]),weights=weights)
                errmask = sum(weights,axis=0)==0
                weights[:,errmask]=1
                Npar_av_rho = average(nan_to_num(Npar_rayrho[:,rho_ind]),weights=weights,axis=0)
                Npar_av_rho[errmask]=nan
                # weighted standard deviation
                # Npar_sigma_rho = sqrt(average(nan_to_num((Npar_rayrho[:,rho_ind]-tile(Npar_av_rho,(Npar_rayrho.shape[0],1)))**2),weights=weights,axis=0))
                Npar_sigma_rho = sqrt(average(nan_to_num((Npar_rayrho[:,rho_ind]-Npar_av_rho)**2),weights=weights,axis=0))
                Npar_sigma_rho[errmask]=nan
                Npar_av = average(Npar_av_rho[~errmask],weights=dPrho[rho_ind][~errmask])
                Npar_sigma = sqrt(average((Npar_av_rho[~errmask]-Npar_av)**2,weights=dPrho[rho_ind][~errmask]))
                
                w_wce_rayrho = array([r['w_wce'] for r in cd_results_all[sim_id][ifreq_calc]['rays']])
                #nanmask = isfinite(w_wce_rayrho[:,rho_ind])
                #weights=dPray[:,rho_ind]
                #weights[~nanmask]=0
                #errmask_wce = sum(weights,axis=0)==0
                #weights[:,errmask_wce]=1
                w_wce_av_rho = average(nan_to_num(w_wce_rayrho[:,rho_ind]),weights=weights,axis=0)
                w_wce_av_rho[errmask]=nan
                # weighted standard deviation
                # Npar_sigma_rho = sqrt(average(nan_to_num((w_wce_rayrho[:,rho_ind]-tile(Npar_av_rho,(w_wce_rayrho.shape[0],1)))**2),weights=weights,axis=0))
                w_wce_sigma_rho = sqrt(average(nan_to_num((w_wce_rayrho[:,rho_ind]-w_wce_av_rho)**2),weights=weights,axis=0))
                w_wce_sigma_rho[errmask]=nan
                w_wce_av = average(w_wce_av_rho[~errmask],weights=dPrho[rho_ind][~errmask])
                w_wce_sigma = sqrt(average((w_wce_av_rho[~errmask]-w_wce_av)**2,weights=dPrho[rho_ind][~errmask]))
                
                harm_rho_n = (array(map(lambda h: n_harm_luke[h[0]] if len(h)==1 else -1, harm_rho)))[~errmask]
                harm_rho_n50 = []
                for h,e,ir in zip(harm_rho,~errmask,rho_ind):
                    if e and len(h)>1:
                        if max(dPharm[ir,:])>0.5*sum(dPharm[ir,:]): 
                            harm_rho_n50.append(n_harm_luke[argmax(dPharm[ir,:])])
                        else:
                            harm_rho_n50.append(-1)
                    elif e:
                        harm_rho_n50.append(n_harm_luke[h[0]])
                harm_rho_n50 = array(harm_rho_n50)
                harm_rho_n = harm_rho_n50
                if len(unique(harm_rho_n))==1:
                    nce = harm_rho_n[0]
                    # Fisch-Boozer sign, -1 for LFS approach
                    sgnFB = ones(harm_rho_n.shape,dtype=int_)
                    sgnFB[w_wce_av_rho[~errmask]>=harm_rho_n] = -1
                    if w_wce_av>nce:
                        #sgnFB=-1
                        absp_approach = 'LFS'
                    else: 
                        #sgnFB=1
                        absp_approach = 'HFS'
                elif len(unique(harm_rho_n[len(harm_rho_n)//3:]))==1:
                    nce = harm_rho_n[-1]
                    # Fisch-Boozer sign, -1 for LFS approach
                    sgnFB = ones(harm_rho_n.shape,dtype=int_)
                    sgnFB[w_wce_av_rho[~errmask]>=harm_rho_n] = -1
                    if w_wce_av>nce:
                        #sgnFB=-1
                        absp_approach = 'Mostly LFS'
                    else: 
                        #sgnFB=1
                        absp_approach = 'Mostly HFS'
                elif -1 in harm_rho_n:
                    nce = nan
                    #sgnFB = zeros(harm_rho_n.shape,dtype=int_)
                    #if sum(harm_rho_n==-1)==1: 
                    sgnFB = ones(harm_rho_n.shape,dtype=int_)
                    sgnFB[w_wce_av_rho[~errmask]>=harm_rho_n] = -1
                    if sum(harm_rho_n==-1)<=2:
                        dPharm
                    sgnFB[harm_rho_n==-1] = 0
                    absp_approach = 'Undetermined'
                else:
                    nce = nan
                    sgnFB = ones(harm_rho_n.shape,dtype=int_)
                    sgnFB[w_wce_av_rho[~errmask]>=harm_rho_n] = -1
                    absp_approach = 'Undetermined'
                jNparsign=sign(Npar_av_rho[~errmask]*cd_results_all[sim_id][ifreq_calc]['Lj'][rho_ind][~errmask])
                if all(sgnFB==0):
                    cd_mechanism[ifreq,iz] = 'Undetermined'
                elif len(unique(jNparsign*sgnFB))==1:
                    cd_mechanism[ifreq,iz] = 'Fisch-Boozer' if jNparsign[0]*sgnFB[0]>0 else 'Ohkawa'
                elif len(unique((jNparsign*sgnFB)[len(jNparsign)//3:]))==1:
                    cd_mechanism[ifreq,iz] = 'Mostly Fisch-Boozer' if jNparsign[-1]*sgnFB[-1]>0 else 'Mostly Ohkawa'
                else:
                    cd_mechanism[ifreq,iz] = 'Undetermined'
                
#                if isfinite(sgnFB) and len(unique(jNparsign))==1:
#                    cd_mechanism[ifreq,iz] = 'Fisch-Boozer' if jNparsign[0]*sgnFB>0 else 'Ohkawa'
#                elif isfinite(sgnFB) and len(unique(jNparsign[len(jNparsign)//3:]))==1:
#                    cd_mechanism[ifreq,iz] = 'Mostly Fisch-Boozer' if jNparsign[-1]*sgnFB>0 else 'Mostly Ohkawa'
#                else:
#                    cd_mechanism[ifreq,iz] = 'Undetermined'

            cd_harm_n[ifreq,iz] = nce
            cd_harm_approach[ifreq,iz] = absp_approach
            cd_Npar_av[ifreq,iz] = Npar_av
            cd_Npar_av2[ifreq,iz] = Npar_av2
            cd_Npar_sigma[ifreq,iz] = Npar_sigma
                    
if 'mathtext.default' in matplotlib.rcParams: matplotlib.rc('mathtext',default='regular')
# set default text properties
#font = {'family' : 'monospace',
font = {'weight' : 'bold',
        'size'   : 20}
rc('font', **font)
nticks = 4

def plot_by_simid(y,sel='',fsel=None,yerr=None):
    pf = figure()
    if not (isinstance(sel,list) or isinstance(sel,tuple)): sel = [sel]
    if fsel==None: fsel = array(ones(cd_freqs.shape),dtype=bool)
    for i,si in enumerate(sim_ids):
        if any(map(lambda s: s.lower() in si.lower(),sel)):
            pl = plot(cd_freqs[fsel & cd_Plim_mask[:,i]],y[fsel & cd_Plim_mask[:,i],i],label=si,marker='o')[0]
            if not yerr==None:
                errorbar(cd_freqs[fsel & cd_Plim_mask[:,i]],y[fsel & cd_Plim_mask[:,i],i],yerr[fsel & cd_Plim_mask[:,i],i],ls='None',color=pl.get_color())
    legend(loc='best')
    xlim(cd_freqs[fsel][0]-0.5,cd_freqs[fsel][-1]+0.5)
    pf.show()

if  amr_cfg['input']['experiment']=='nhtx':
    pl_colsel_rho = {'midpl':'b','40':'g','80':'r','120':'c','20':'m','60':'c'}
    pl_marksel_all = {'midpl+':'>','midpl-':'<','40+':'o','40-':'d','80+':'s','80-':'p','120+':'+','120-':'x','60+':'3','60-':'4'}
else:
    pl_colsel_rho = {'midpl':'b','20':'g','40':'r','10':'c','30':'m',\
    '50':'brown','60':'c','80':'m'}
    pl_marksel_all = {'midpl+':'>','midpl-':'<','20+':'o','20-':'d','40+':'s','40-':'p',\
                      '10+':'+','10-':'x','30+':'3','30-':'4','60+':'d','60-':'D',\
                      '50+':'h','50-':'H'}
fcolmap = cm.gnuplot
pl_colsel_all = {'midpl':'b','+':'g','-':'r'}
pl_colsel_ifreq = fcolmap(((array(cd_freqs)-min(cd_freqs))/(max(cd_freqs)-min(cd_freqs))*fcolmap.N).astype(int_))
pl_marksel_rho = {'+':'>','-':'<'}
    
def plt_sel(sid):
    import re
    sid = sid.lower()
    if 'tor-' in sid:
        torsgn = '-'
    else:
        torsgn = '+'
    if 'midpl' in sid:
        zsel = 'midpl'
        vsgn = ''
    else:
        res = re.search('z[+-]\d+',sid)
        zsel = sid[res.start()+2:res.end()]
        vsgn = sid[res.start()+1:res.start()+2]
    return (vsgn, zsel, torsgn)

def cd_all_plot(zetam=0,ln_sel=[],show_legend=False,nticks=2):
    pf = figure(figsize=[12,6])
    ax1 = axes()
    ax1.hold(True)
    xlabel('frequency [GHz]')
    ylabel('I total [A/W]')
    if zetam>=0:
        ax2 = ax1.twinx()
        ax2.hold(True)
    axes(ax1)
    alpha = 0.6
    if ln_sel!=[]: alpha = 0.3
    for i,sid in enumerate(sim_ids):
        # fsel = (cd_PtotP0[:,i]<Ptot_max) & (cd_PtotP0[:,i]>Ptot_min)
        fsel = cd_Plim_mask[:,i]
        sid_sel = plt_sel(sid)
        if sid_sel[0]:
            c = pl_colsel_all[sid_sel[0]]
        else:
            c = pl_colsel_all[sid_sel[1]]
        m = pl_marksel_all[sid_sel[1]+sid_sel[2]]
        if sid_sel[1]!='midpl':
            labl = 'Z'+ sid_sel[0] + sid_sel[1] + ', $N_\phi$' + sid_sel[2]
        else:
            labl = 'midpl' + ', $N_\phi$' + sid_sel[2]
        if sid in ln_sel:
            ls = '-'
            plot(cd_freqs[fsel],cd_Itot[fsel,i],ls=ls,lw=1.5,c=c,marker=m,ms=10,mew=1.5, mec=c, alpha=0.6)
        else:
            ls = 'None'
            pl = plot(cd_freqs[fsel],cd_Itot[fsel,i],label=labl,ls=ls,c=c,marker=m,ms=10,mew=1.5, mec=c, alpha=alpha)[0]
        # pl = plot(cd_freqs[fsel],cd_Itot[fsel,i],label=sid)
    if ln_sel==[]:
        bar(cd_freqs-diff(cd_freqs)[0]/4, 1.03*(freq_max-freq_min), width = 2*diff(cd_freqs)[0]/4, bottom = 1.03*freq_min, color='grey', \
            linewidth=2, edgecolor = 'b', alpha=0.5 )
        fill_between(cd_freqs,freq_max,freq_min,color='y',alpha=0.7)
    else:
        fill_between(cd_freqs,freq_max,freq_min,color='y',alpha=0.1)
    axhline(0,c='k',ls='--')
    if show_legend: legend(loc='best',prop={'size':16})
    lim1 = ylim()
    # title('Total EBW driven current, NSTX H-mode, 2nd harmonic')
    if zetam==0:
        ym = max(map(abs,lim1))
    else:
        ym = zetam / (3.27 * Rmaj * ne19 / TekeV)
    ylim(-ym,ym)
    yticks(linspace(-ym,ym,2*nticks+1))
    locs, labels = yticks()
    yticks(locs,map(str,map(lambda xx: round(xx,2),locs)))
    # lim1 = ylim()
    if show_legend:
        xlim(cd_freqs[0]-2*(cd_freqs[1]-cd_freqs[0]),cd_freqs[-1]+0.5)
    else:
        xlim(cd_freqs[0]-0.5,cd_freqs[-1]+0.5)
    if zetam>=0:
        axes(ax2)
        ylabel('current drive efficiency $\\zeta$')
        ym = ym* 3.27 * Rmaj * ne19 / TekeV
        ylim(-ym,ym)
        yticks(linspace(-ym,ym,2*nticks+1))
        locs, labels = yticks()
        yticks(locs,map(str,map(lambda xx: round(xx,2),locs)))

    pf.show()

def cd_rhomid_plot(sgn='+',shift=False,ln_sel=''):
    if sgn=='+':
        sel = ['midpl','z+']
    else:
        sel = ['midpl','z-']
    pf = figure(figsize=[12,6])
    xlabel('frequency [GHz]')
    ylabel('poloidal $\\rho$')
    isel = -1
    for i,sid in enumerate(sim_ids):
        if any(map(lambda s: s.lower() in sid.lower(),sel)):
            isel +=1
            fsel = cd_Plim_mask[:,i] #(cd_PtotP0[:,i]<Ptot_max) & (cd_PtotP0[:,i]>Ptot_min)
            y = cd_rhomid[fsel,i]
            yep = cd_rhofwhm[fsel,i].copy()
            yep[(y+yep)>1]=1-y[(y+yep)>1]
            yem = cd_rhofwhm[fsel,i].copy()
            yem[(y-yem)<0]=y[(y-yem)<0]
            yerr = column_stack((yem,yep))
            yp = cd_rhomax[fsel,i]
            sid_sel = plt_sel(sid)
            c = pl_colsel_rho[sid_sel[1]]
            m = pl_marksel_rho[sid_sel[2]]
            if shift and sid_sel[1]!='midpl':
                f = cd_freqs[fsel]+(float(sid_sel[1])/300)*float(sid_sel[2]+'1')
            else:
                f = cd_freqs[fsel]+0.05*float(sid_sel[2]+'1')
            if ln_sel==sid_sel[2]:
                ls = ':'
            else:
                ls = 'None'
            if sid_sel[1]!='midpl':
                labl = 'Z'+ sid_sel[0] + sid_sel[1] + ', $N_\phi$' + sid_sel[2]
            else:
                labl = 'midpl' + ', $N_\phi$' + sid_sel[2]
            pl = plot(f,yp,label=labl,ls=ls,c=c,marker=m,ms=10)[0]
            errorbar(f,y,yerr.T,ls='None',color=pl.get_color())
        # pl = plot(cd_freqs[fsel],cd_Itot[fsel,i],label=sid)
    legend(loc='best',prop={'size':14})
    xlim(cd_freqs[0]-0.5,cd_freqs[-1]+0.5)
    ylim(0,1)
    pf.show()

def cd_mech_plot(shift=True,show_leg=True,zetamax=1):
    pf = figure(figsize=[12,6])
    axes((0.1,0.15,0.85,0.8))
    xlabel('frequency [GHz]')
    ylabel('poloidal $\\rho$')
    pf2 = figure(figsize=[12,6])
    axes((0.1,0.15,0.85,0.8))
    xlabel('poloidal $\\rho$')
    ylabel(r'$\|\zeta\|$')
#    pf3 = figure(figsize=[12,6])
#    xlabel('frequency [GHz]')
#    ylabel('poloidal $\\rho$')
    for ifreq,freq in enumerate(cd_freqs):
        for isid,sid in enumerate(sim_ids):
            if cd_calc_mask[ifreq,isid] and cd_Plim_mask[ifreq,isid]:
                sid_sel = plt_sel(sid)                
                if 'Fisch' in cd_mechanism[ifreq,isid]:
                    m = 'd'
                elif 'Ohkawa' in cd_mechanism[ifreq,isid]:
                    m = 'o'
                else:
                    m = 'x'
                    if isnan(cd_harm_n[ifreq,isid]): m='+'
                if 'Mostly' in cd_mechanism[ifreq,isid]:
                    mec = pl_colsel_rho[sid_sel[1]]
                    #mfc = 'w'
                    mfc = mec
                    #mfcf = 'w'
                    mecf = pl_colsel_ifreq[ifreq]
                    mfcf = mecf
                else:
                    mfc = pl_colsel_rho[sid_sel[1]]
                    mec = mfc
                    mfcf = pl_colsel_ifreq[ifreq]
                    mecf = mfcf
#                if abs(cd_Npar_sigma[ifreq,isid]/cd_Npar_av[ifreq,isid])>0.3:
#                    m = '+'
                
                figure(pf.number)
                plot(freq,cd_rhojmid[ifreq,isid],ls='none',marker=m,mec=mec,mfc=mfc,mew=1.5,ms=13)
                figure(pf2.number)
                plot(cd_rhojmid[ifreq,isid],abs(cd_zeta[ifreq,isid]),ls='none',marker=m,mec=mecf,mfc=mfcf,mew=1.5,ms=13)
                if m in ('d','o'):
                    mappr = '$L$' if 'LFS' in cd_harm_approach[ifreq,isid] else '$H$'
                    plot(cd_rhojmid[ifreq,isid],abs(cd_zeta[ifreq,isid]),ls='none',marker=mappr,mec='w',mfc='w',mew=0.5,ms=9)
#                figure(pf3.number)
#                if isnan(cd_harm_n[ifreq,isid]):
#                    m = '+'
#                else:
#                    m = '$%i$' % int(cd_harm_n[ifreq,isid])
#                plot(freq,cd_rhojmid[ifreq,isid],ls='none',marker=m,mec=mec,mfc=mfc,mew=1.5,ms=10)
    figure(pf.number)
    if show_leg: legend(loc='best',prop={'size':14})
    xlim(cd_freqs[0]-0.5,cd_freqs[-1]+0.5)
    ylim(0,1)
    pf.show();
    figure(pf2.number)
    ylim((0,zetamax))
    pf2.show()
    # pf3.show()
    pf_leg = figure(figsize=[16,12])
    subplot(121)
    legs1 = ( plt.Line2D(range(10), range(10), linestyle='-', color=c, linewidth=5.0) for c in pl_colsel_ifreq)
    legs2 = ('%.1f GHz' % f for f in cd_freqs)
    legend(legs1,legs2,loc=1)
    subplot(122)
    legs1 = [ plt.Line2D(range(10), range(10), marker=m, ms=13, mew=1.5, linestyle='None', mfc='k', mec='k') for m in ('d','o','x','+')]
    legs1 += [plt.Line2D(range(10), range(10), marker=m, ms=13, mew=1.5, linestyle='None', mfc='w', mec='k') for m in ('$L$','$H$')]
    legs2 = ('Fisch-Boozer','Ohkawa','Udetermined','Harmonic overlap',r'$n\omega_{ce}<\omega$ absorption',r'$n\omega_{ce}>\omega$ absorption')
    legend(legs1,legs2,numpoints=1,scatterpoints=1,loc=2)
    pf_leg.show()


def cd_rhojmid_plot(sgn='+',shift=False,ln_sel=[],show_leg=True):
    if sgn=='+':
        # sel = ['midpl','+10','+20','+30','+40']
        sel = ['midpl','z+']
    else:
        sel = ['midpl','z-']
    pf = figure(figsize=[12,6])
    xlabel('frequency [GHz]')
    ylabel('poloidal $\\rho$')
    isel = -1
    for i,sid in enumerate(sim_ids):
        if any(map(lambda s: s.lower() in sid.lower(),sel)):
            print sid
            isel +=1
            fsel = cd_Plim_mask[:,i] #(cd_PtotP0[:,i]<Ptot_max) & (cd_PtotP0[:,i]>Ptot_min)
            y = cd_rhojmid[fsel,i]
            yep = cd_rhojfwhm[fsel,i].copy()
            yep[(y+yep)>1]=1-y[(y+yep)>1]
            yem = cd_rhojfwhm[fsel,i].copy()
            yem[(y-yem)<0]=y[(y-yem)<0]
            yerr = row_stack((yem,yep))
            yp = cd_rhojmax[fsel,i]
            sid_sel = plt_sel(sid)
            c = pl_colsel_rho[sid_sel[1]]
            m = pl_marksel_rho[sid_sel[2]]
            if shift and sid_sel[1]!='midpl':
                f = cd_freqs[fsel]+(float(sid_sel[1])/200)*float(sid_sel[2]+'1')
            else:
                f = cd_freqs[fsel]+0.05*float(sid_sel[2]+'1')
            if sid_sel[2] in ln_sel:
                ls = '-'
            else:
                ls = 'None'
            if sid_sel[1]!='midpl':
                labl = 'Z'+ sid_sel[0] + sid_sel[1] + ', $N_\phi$' + sid_sel[2]
            else:
                labl = 'midpl' + ', $N_\phi$' + sid_sel[2]
            pl = plot(f,yp,label=labl,ls=ls,c=c,marker=m,ms=10)[0]
            if any(yerr):
                errorbar(f,y,yerr,ls='None',color=pl.get_color())
            if sid_sel[2] in ln_sel:
                print yerr[0,:] 
                fill_between(f,y-yerr[0,:],y+yerr[1,:],color=pl.get_color(),alpha=0.3)
        # pl = plot(cd_freqs[fsel],cd_Itot[fsel,i],label=sid)
    if show_leg: legend(loc='best',prop={'size':14})
    xlim(cd_freqs[0]-0.5,cd_freqs[-1]+0.5)
    ylim(0,1)
    pf.show()

   
def cd_rho_cover(implot=False):
    i=0
    cov_rho = zeros(len(cd_freqs)*len(sim_ids))
    cov_Itot = zeros(len(cd_freqs)*len(sim_ids))
    cov_jmax = zeros(len(cd_freqs)*len(sim_ids))
    cov_zeta = zeros(len(cd_freqs)*len(sim_ids))
    cov_mask = ones(len(cd_freqs)*len(sim_ids),dtype=bool)
    for ifreq,freq in enumerate(cd_freqs):
        for isid,sid in enumerate(sim_ids):
            if cd_calc_mask[ifreq,isid]:
                cov_rho[i] = cd_rhojmax[ifreq,isid]
                cov_Itot[i] = cd_Itot[ifreq,isid]
                cov_jmax[i] = max(abs(cd_results_all[sid][cd_calc_freqs[sid].index(freq)]['Lj']))*1e1
                cov_zeta[i] = cd_Itot[ifreq,isid]*3.27*Rmaj*fn_ne(cov_rho[i])/fn_Te(cov_rho[i])
                cov_mask[i] = cd_Plim_mask[ifreq,isid]
                if implot and cov_mask[i]:
                    figure(isid)
                    title(sid)
                    plot(cov_rho[i],cov_zeta[i],'kx')
                    annotate(('%g ' % freq),(cov_rho[i],cov_zeta[i]))
                i += 1
    if implot:
        figure()
        semilogy(cov_rho[cov_mask],cov_jmax[cov_mask],ls='None',marker='o',c='b')
    return cov_rho,cov_Itot,cov_mask,cov_jmax,cov_zeta

def plt_freq_range(freq_ranges=(cd_freqs,),title=''):
    #figure()
    it = 0
    amrplot.plotfreqs(None,prof1d[it],'R',title=title,xLCFS=prof0d[it]['RLCFS_LFS'], \
                              fpe='fpe',fUHR='fUHR',fce='fce',nfce=[1,2,3,4],fce_limit=prof1d[it]['fUHR'].max(), \
                              xlabel='R [m]',ylabel='GHz', \
                              fpecolor = 'green',fUHRcolor = 'magenta', \
                              fcecolor = 'blue',
                              hfreqs = [], \
                              selectors=[],latex=False)
    for fr in freq_ranges: fill_between(prof1d[it]['R'],fr.min(),fr.max(),color='cyan',alpha=0.7)
    legend(loc='upper left')
    

def cd_save(ident):
    params = ''
    for v in cd_all_vars:
        params += ','+v+'_'+ident+'='+v
    params = "'"+ident+ "'"+ params
    estr = 'savez('+params+')'
    print estr
    eval(estr)
    
    #cd_calc_freqs = {}
    #sim_ids

