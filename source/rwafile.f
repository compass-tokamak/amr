cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine dima(eqdsk,ierr
     &               ,mco2v,mco2r
     &               ,nsilop0,magpri0,nfcoil0,nesum0
     & )

      parameter (nfcoil=18,nsilop=44,nrogow=1)
      parameter (magpri67=29,magpri322=31,magprirdp=8,magudom=5)
      parameter (magpri=73 )
      parameter (nesum=6)
      parameter (nco2v=3,nco2r=1)
      character uday*10,mfvers*5
      character eqdsk*256,vernum*8,rline*23
      character qmflag*3,limloc*4
      dimension mfvers(2)
      data neqdsk/55/

      do i=1,256
         if (ichar(eqdsk(i:i)) .eq. 0 ) then
            leqdsk=i-1
            goto 5433
         endif
      enddo
 5433 open(unit=neqdsk,file=eqdsk(1:leqdsk),form='FORMATTED',
     &       status='OLD',err=9999)
c
        read (neqdsk,1055,err=3000,end=3000) uday,(mfvers(j),j=1,2)
c        if ( uday(3:3) .ne. "-" .or. uday(7:7) .ne. "-" ) goto 3000
        read (neqdsk,5432,err=3000,end=3000) rline
 5432   format(a23)
        read (neqdsk,1040,err=3000,end=3000) xdum
        read (neqdsk,1060,err=3000,end=3000) xdum,idum,idum,limloc,
     &                              mco2v,mco2r,qmflag,
     &                              nlold,nlnew
        do i = 1,6
           read (neqdsk,1040,err=3000,end=3000) xdum,xdum,xdum,xdum
        enddo
        read (neqdsk,1040,err=3000,end=3000) (xdum,k=1,mco2v)
        read (neqdsk,1040,err=3000,end=3000) (xdum,k=1,mco2v)
        read (neqdsk,1040,err=3000,end=3000) (xdum,k=1,mco2r)
        read (neqdsk,1040,err=3000,end=3000) (xdum,k=1,mco2r)
        do i = 1,11
           read (neqdsk,1040,err=3000,end=3000) xdum,xdum,xdum,xdum
        enddo
        read (neqdsk,1041,err=3000,end=3000) nsilop0,
     &          magpri0,nfcoil0,nesum0
        goto 916
c
c-------------------------------------------------------------------
c-- binary format                                                 --
c-------------------------------------------------------------------
 3000   continue
        close(unit=neqdsk)
        open(unit=neqdsk,file=eqdsk(1:leqdsk),status='OLD'
     &      ,form='UNFORMATTED',err=9999)
        read (neqdsk,err=9999) uday,(mfvers(j),j=1,2)
c        if ( uday(3:3) .ne. "-" .or. uday(7:7) .ne. "-" ) then
c           print *,'b',uday(3:3),uday(7:7),uday,eqdsk(1:leqdsk)
c     & ,' IS NOT AN AEQDSK'
c           goto 9999
c        endif
        read (neqdsk,err=9999) ishot,ktime
        read (neqdsk,err=9999) xdum
        read (neqdsk,err=9999) xdum,idum,idum,limloc,
     &                              mco2v,mco2r,qmflag,
     &                              nlold,nlnew
        do i = 1,6
           read (neqdsk,err=9999) xdum,xdum,xdum,xdum
        enddo
        read (neqdsk,err=9999) (xdum,k=1,mco2v)
        read (neqdsk,err=9999) (xdum,k=1,mco2v)
        read (neqdsk,err=9999) (xdum,k=1,mco2r)
        read (neqdsk,err=9999) (xdum,k=1,mco2r)
        do i = 1,11
           read (neqdsk,err=9999) xdum,xdum,xdum,xdum
        enddo
        read (neqdsk,err=9999) nsilop0,magpri0,nfcoil0,nesum0
c
  916   continue
        ierr=0
        close(unit=neqdsk)
        return

 9999   ierr=1
      close(unit=neqdsk)
      mco2v = 3
      mco2r = 1
      nlold = 33
      nlnew = 39
      nsilop0 = nsilop
      magpri0 = magpri
      nfcoil0 = nfcoil
      nesum0 = nesum
      return
c
 1040 format (1x,4e16.9)
 1041 format (1x,4i5)
 1042 format (1x,a42)
 1050 format (1x,i5,11x,i5)
 1053 format (1x,i6,11x,i5)
 1055 format (1x,a10,2a5)
 1060 format (1x,f8.3,9x,i5,11x,i5,1x,a3,1x,i3,1x,i3,1x,a3,1x,2i5)

      end subroutine dima
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine reada(eqdsk,ierr
     &         ,csilop, crogow, cmpr2, ccbrsp, eccurt
     &         ,rco2r, rco2v, chordv ,dco2r, dco2v
     &         ,rseps,zseps
     &         ,rdat,idat
     &         ,nsilop,magpri,nfcoil,nesum,nco2r,nco2v
     &         ,iqmflag,ilimloc
     & )
      parameter (nrogow=1)
      parameter (magpri67=29,magpri322=31,magprirdp=8,magudom=5)
c
      dimension csilop(nsilop),crogow(nrogow)
     &         ,cmpr2(magpri),ccbrsp(nfcoil)
     &         ,eccurt(nesum)
     &         ,rco2r(nco2r),rco2v(nco2v),chordv(nco2v)
     &         ,dco2r(nco2r),dco2v(nco2v)
     &         ,mfvers(2),rseps(2),zseps(2)
      character uday*10,mfvers*5
      character eqdsk*256,vernum*8,rline*23
      data neqdsk/55/
      character qmflag*3,limloc*4
      integer*1 iqmflag(4),ilimloc(5)

      parameter (MIDAT=12,MRDAT=121)
      dimension idatl(MIDAT),rdatl(MRDAT),rdat(1),idat(1)

        equivalence (idatl( 1),ishot),   (idatl( 2),jflag)   
     &            , (idatl( 3),lflag),   (idatl( 4),mco2v)
     &            , (idatl( 5),mco2r)   
     &            , (idatl( 6),nlold),   (idatl( 7),nlnew)
     &            , (idatl( 8),nsilop0), (idatl( 9),magpri0)
     &            , (idatl(10),nfcoil0), (idatl(11),nesum0)
     &            , (idatl(12),nvernum)

        equivalence                     (rdatl( 1),time)
     &            , (rdatl( 2),tsaisq), (rdatl( 3),rcencm)
     &            , (rdatl( 4),bcentr), (rdatl( 5),pasmat)
     &            , (rdatl( 6),cpasma), (rdatl( 7),rout) 
     &            , (rdatl( 8),zout),   (rdatl( 9),aout)
        equivalence (rdatl(10),eout),   (rdatl(11),doutu) 
     &            , (rdatl(12),doutl),  (rdatl(13),vout)
     &            , (rdatl(14),rcurrt), (rdatl(15),zcurrt) 
     &            , (rdatl(16),qsta),   (rdatl(17),betat)
     &            , (rdatl(18),betap),  (rdatl(19),ali) 
        equivalence (rdatl(20),oleft),  (rdatl(21),oright)
     &            , (rdatl(22),otop),   (rdatl(23),obott) 
     &            , (rdatl(24),qpsib),  (rdatl(25),vertn)
     &            , (rdatl(26),shearb), (rdatl(27),bpolav) 
     &            , (rdatl(28),s1),     (rdatl(29),s2)
        equivalence (rdatl(30),s3),     (rdatl(31),qout) 
     &            , (rdatl(32),olefs),  (rdatl(33),orighs)
     &            , (rdatl(34),otops),  (rdatl(35),sibdry) 
     &            , (rdatl(36),areao),  (rdatl(37),wplasm)
     &            , (rdatl(38),terror), (rdatl(39),elongm) 
        equivalence (rdatl(40),qqmagx), (rdatl(41),cdflux)
     &            , (rdatl(42),alpha),  (rdatl(43),rttt) 
     &            , (rdatl(44),psiref), (rdatl(45),xndnt)
     &            , (rdatl(46),sepexp), (rdatl(47),obots) 
     &            , (rdatl(48),btaxp),  (rdatl(49),btaxv)
        equivalence (rdatl(50),aaq1),   (rdatl(51),aaq2) 
     &            , (rdatl(52),aaq3),   (rdatl(53),seplim)
     &            , (rdatl(54),rmagx),  (rdatl(55),zmagx) 
     &            , (rdatl(56),simagx), (rdatl(57),taumhd)
     &            , (rdatl(58),betapd), (rdatl(59),betatd) 
        equivalence (rdatl(60),wplasmd),(rdatl(61),fluxx)
     &            , (rdatl(62),vloopt), (rdatl(63),taudia) 
     &            , (rdatl(64),qmerci), (rdatl(65),tavem)
     &            , (rdatl(66),pbinj),  (rdatl(67),rvsin)
     &            , (rdatl(68),zvsin),  (rdatl(69),rvsout)
        equivalence (rdatl(70),zvsout), (rdatl(71),vsurfa)
     &            , (rdatl(72),wpdot),  (rdatl(73),wbdot)
     &            , (rdatl(74),slantu), (rdatl(75),slantl)
     &            , (rdatl(76),zuperts),(rdatl(77),chipre)
     &            , (rdatl(78),cjor95), (rdatl(79),pp95)
        equivalence (rdatl(80),ssep),   (rdatl(81),yyy2)
     &            , (rdatl(82),xnnc),   (rdatl(83),cprof)
     &            , (rdatl(84),oring),  (rdatl(85),cjor0)
     &            , (rdatl(86),fexpan), (rdatl(87),qqmin)
     &            , (rdatl(88),chigamt),(rdatl(89),ssi01)
        equivalence (rdatl(90),fexpvs), (rdatl(91),sepnose)
     &            , (rdatl(92),ssi95),  (rdatl(93),rqqmin)
     &            , (rdatl(94),cjor99), (rdatl(95),cj1ave)
     &            , (rdatl(96),rmidin), (rdatl(97),rmidout)
     &            , (rdatl(98),psurfa), (rdatl(99),peak)
        equivalence (rdatl(100),dminux), (rdatl(101),dminlx)
     &            , (rdatl(102),dolubaf),(rdatl(103),dolubafm)
     &            , (rdatl(104),diludom),(rdatl(105),diludomm)
     &            , (rdatl(106),ratsol), (rdatl(107),rvsiu)
     &            , (rdatl(108),zvsiu),  (rdatl(109),rvsid)
        equivalence (rdatl(110),zvsid),  (rdatl(111),rvsou)
     &            , (rdatl(112),zvsou),  (rdatl(113),rvsod)
     &            , (rdatl(114),zvsod),  (rdatl(115),condno)
     &            , (rdatl(116),psin32), (rdatl(117),psin21)
     &            , (rdatl(118),rq32in), (rdatl(119),rq21top)
     &            , (rdatl(120),chilibt),(rdatl(121),ali3)

        nlold=35
        nlnew=39
        do i=1,256
           if (ichar(eqdsk(i:i)) .eq. 0 ) then
              leqdsk=i-1
              goto 5433
           endif
        enddo
 5433   open(unit=neqdsk,file=eqdsk(1:leqdsk),form='FORMATTED',
     &       status='OLD',err=9999)
c
        read (neqdsk,1055,err=3000,end=3000) uday,(mfvers(j),j=1,2)
c        if ( uday(3:3) .ne. "-" .or. uday(7:7) .ne. "-" ) goto 3000
        read (neqdsk,5432,err=3000,end=3000) rline
 5432   format(a23)
        read(rline,1050,err=3000,end=3000) ishot,ktime
        read(rline,1053,err=3000,end=3000) ishot2,ktime2
        if (ishot2.gt.ishot) then
           ishot=ishot2
           ktime=ktime2
        endif
        vernum=mfvers(2)(2:5)//mfvers(1)(1:2)//mfvers(1)(4:5)
        read(vernum,6000,err=3000,end=3000) nvernum
 6000   format (i8)
        read (neqdsk,1040,err=3000,end=3000) time
        read (neqdsk,1060,err=3000,end=3000) time,jflag,lflag,limloc,
     &                              mco2v,mco2r,qmflag,
     &                              nlold,nlnew
c        read (neqdsk,1060,err=3000,end=3000) time,jflag,lflag,limloc,
c     .                     mco2v,mco2r,qmflag
        if (mco2v.ne.nco2v ) then
           print *,'MCO2V IN FILE ',mco2v,
     &          '!= MAXIMUM DIMENSION ',nco2v
           goto 9999
        endif
        if (mco2r.ne.nco2r ) then
           print *,'MCO2R IN FILE ',mco2r,
     &          '!=MAXIMUM DIMENSION ',nco2r
           goto 9999
        endif
        read (neqdsk,1040,err=3000,end=3000) tsaisq,rcencm,bcentr,pasmat
        read (neqdsk,1040,err=3000,end=3000) cpasma,rout,zout,aout
        read (neqdsk,1040,err=3000,end=3000) eout,doutu,doutl,vout
        read (neqdsk,1040,err=3000,end=3000) rcurrt,zcurrt,qsta,betat
        read (neqdsk,1040,err=3000,end=3000) betap,ali,oleft,oright
        read (neqdsk,1040,err=3000,end=3000) otop,obott,qpsib,vertn
        read (neqdsk,1040,err=3000) (rco2v(k),k=1,mco2v)
        read (neqdsk,1040,err=3000) (dco2v(k),k=1,mco2v)
        read (neqdsk,1040,err=3000) (rco2r(k),k=1,mco2r)
        read (neqdsk,1040,err=3000) (dco2r(k),k=1,mco2r)
        read (neqdsk,1040,err=3000) shearb,bpolav,s1,s2
        read (neqdsk,1040,err=3000) s3,qout,olefs,orighs
        read (neqdsk,1040,err=3000) otops,sibdry,areao,wplasm
        read (neqdsk,1040,err=3000) terror,elongm,qqmagx,cdflux
        read (neqdsk,1040,err=3000) alpha,rttt,psiref,xndnt
        read (neqdsk,1040,err=3000) rseps(1),zseps(1),rseps(2),zseps(2)
        read (neqdsk,1040,err=3000) sepexp,obots,btaxp,btaxv
        read (neqdsk,1040,err=3000) aaq1,aaq2,aaq3,seplim
        read (neqdsk,1040,err=3000) rmagx,zmagx,simagx,taumhd
        read (neqdsk,1040,err=3000) betapd,betatd,wplasmd,fluxx
        read (neqdsk,1040,err=3000) vloopt,taudia,qmerci,tavem
c
        read (neqdsk,1041,err=3000) nsilop0,magpri0,nfcoil0,nesum0
        if (nsilop0.ne.nsilop ) then
           print *,'NSILOP0 IN FILE ',nsilop0,
     &          '!= MAXIMUM DIMENSION ',nsilop
           goto 9999
        endif
        if (magpri0.ne.magpri ) then
           print *,'MAGPRI0 IN FILE ',magpri0,
     &          '!=MAXIMUM DIMENSION ',magpri
           goto 9999
        endif
        if (nesum0.ne.nesum ) then
           print *,'NESUM0 IN FILE ',nesum0,
     &          '!=MAXIMUM DIMENSION ',nesum
           goto 9999
        endif
        read (neqdsk,1040,err=3000) (csilop(k),k=1,nsilop0)
     &                    ,(cmpr2(k), k=1,magpri0)
        read (neqdsk,1040,err=3000) (ccbrsp(k),k=1,nfcoil0)
        read (neqdsk,1040,err=3000) (eccurt(k),k=1,nesum0)
c
        read (neqdsk,1040,err=3000) pbinj,rvsin,zvsin,rvsout
        read (neqdsk,1040,err=3000) zvsout,vsurfa,wpdot,wbdot
        read (neqdsk,1040,err=3000) slantu,slantl,zuperts,chipre
        read (neqdsk,1040,err=3000) cjor95,pp95,ssep,yyy2
        read (neqdsk,1040,err=3000) xnnc,cprof,oring,cjor0
        read (neqdsk,1040,err=3000) fexpan,qqmin,chigamt,ssi01
        read (neqdsk,1040,err=3000) fexpvs,sepnose,ssi95,rqqmin
        read (neqdsk,1040,err=3000) cjor99,cj1ave,rmidin,rmidout
        read (neqdsk,1040,end=916,err=916) psurfa,peak,dminux,dminlx
        read (neqdsk,1040,end=916,err=916) dolubaf,dolubafm,diludom,
     .                      diludomm
        read (neqdsk,1040,end=916,err=916) ratsol,rvsiu,zvsiu,rvsid
        read (neqdsk,1040,end=916,err=916) zvsid,rvsou,zvsou,rvsod
        read (neqdsk,1040,end=916,err=916) zvsod,condno,psin32,psin21
        read (neqdsk,1040,end=916,err=916) rq32in,rq21top,chilibt,ali3
        goto 916
c
c-------------------------------------------------------------------
c-- binary format                                                 --
c-------------------------------------------------------------------
 3000   continue
        close(unit=neqdsk)
        open(unit=neqdsk,file=eqdsk(1:leqdsk),status='OLD'
     &       ,form='UNFORMATTED',err=9999)
        read (neqdsk,err=9999) uday,(mfvers(j),j=1,2)
c        if ( uday(3:3) .ne. "-" .or. uday(7:7) .ne. "-" ) then
c           print *,eqdsk(1:leqdsk),' IS NOT AN AEQDSK'
c           goto 9999
c        endif
        vernum=mfvers(2)(2:5)//mfvers(1)(1:2)//mfvers(1)(4:5)
        read(vernum,6000,err=9999) nvernum
        read (neqdsk,err=9999) ishot,ktime
        read (neqdsk,err=9999) time
        read (neqdsk,err=9999) time,jflag,lflag,limloc,
     .                mco2v,mco2r,qmflag,nlold,nlnew
c        read (neqdsk,err=9999) time,jflag,lflag,limloc,
c     .                mco2v,mco2r,qmflag
        if (mco2v.ne.nco2v ) then
           print *,'MCO2V IN FILE ',mco2v,
     &          '!= MAXIMUM DIMENSION ',nco2v
           goto 9999
        endif
        if (mco2r.ne.nco2r ) then
           print *,'MCO2R IN FILE ',mco2r,
     &          '!=MAXIMUM DIMENSION ',nco2r
           goto 9999
        endif
        read (neqdsk,err=9999) tsaisq,rcencm,bcentr,pasmat
        read (neqdsk,err=9999) cpasma,rout,zout,aout
        read (neqdsk,err=9999) eout,doutu,doutl,vout
        read (neqdsk,err=9999) rcurrt,zcurrt,qsta,betat
        read (neqdsk,err=9999) betap,ali,oleft,oright
        read (neqdsk,err=9999) otop,obott,qpsib,vertn
        read (neqdsk,err=9999) (rco2v(k),k=1,mco2v)
        read (neqdsk,err=9999) (dco2v(k),k=1,mco2v)
        read (neqdsk,err=9999) (rco2r(k),k=1,mco2r)
        read (neqdsk,err=9999) (dco2r(k),k=1,mco2r)
        read (neqdsk,err=9999) shearb,bpolav,s1,s2
        read (neqdsk,err=9999) s3,qout,olefs,orighs
        read (neqdsk,err=9999) otops,sibdry,areao,wplasm
        read (neqdsk,err=9999) terror,elongm,qqmagx,cdflux
        read (neqdsk,err=9999) alpha,rttt,psiref,xndnt
        read (neqdsk,err=9999) rseps(1),zseps(1),rseps(2)
     .                      ,zseps(2)
        read (neqdsk,err=9999) sepexp,obots,btaxp,btaxv
        read (neqdsk,err=9999) aaq1,aaq2,aaq3,seplim
        read (neqdsk,err=9999) rmagx,zmagx,simagx,taumhd
        read (neqdsk,err=9999) betapd,betatd,wplasmd,fluxx
        read (neqdsk,err=9999) vloopt,taudia,qmerci,tavem
c
        read (neqdsk,err=9999) nsilop0,magpri0,nfcoil0,nesum0
        if (nsilop0.ne.nsilop ) then
           print *,'NSILOP0 IN FILE ',nsilop0,
     &          '!= MAXIMUM DIMENSION ',nsilop
           goto 9999
        endif
        if (magpri0.ne.magpri ) then
           print *,'MAGPRI0 IN FILE ',magpri0,
     &          '!=MAXIMUM DIMENSION ',magpri
           goto 9999
        endif
        if (nesum0.ne.nesum ) then
           print *,'NESUM0 IN FILE ',nesum0,
     &          '!=MAXIMUM DIMENSION ',nesum
           goto 9999
        endif
        read (neqdsk,err=9999) (csilop(k),k=1,nsilop0)
     &               ,(cmpr2(k), k=1,magpri0)
        read (neqdsk,err=9999) (ccbrsp(k),k=1,nfcoil0)
        read (neqdsk,err=9999) (eccurt(k),k=1,nesum0)
c
        read (neqdsk,err=9999) pbinj,rvsin,zvsin,rvsout
        read (neqdsk,err=9999) zvsout,vsurfa,wpdot,wbdot
        read (neqdsk,err=9999) slantu,slantl,zuperts,chipre
        read (neqdsk,err=9999) cjor95,pp95,ssep,yyy2
        read (neqdsk,err=9999) xnnc,cprof,oring,cjor0
        read (neqdsk,err=9999) fexpan,qqmin,chigamt,ssi01
        read (neqdsk,err=9999) fexpvs,sepnose,ssi95,rqqmin
        read (neqdsk,err=9999) cjor99,cj1ave,rmidin,rmidout
        read (neqdsk,end=916,err=916) psurfa,peak,dminux,dminlx
        read (neqdsk,end=916,err=916) dolubaf,dolubafm,diludom,
     .                      diludomm
        read (neqdsk,end=916,err=916) ratsol,rvsiu,zvsiu,rvsid
        read (neqdsk,end=916,err=916) zvsid,rvsou,zvsou,rvsod
        read (neqdsk,end=916,err=916) zvsod,condno,psin32,psin21
        read (neqdsk,end=916,err=916) rq32in,rq21top,chilibt,ali3
c
  916   continue
        ierr=0
        close(unit=neqdsk)
        do i=1,3
           iqmflag(i) = ichar(qmflag(i:i))
        enddo
        do i=1,4
           ilimloc(i) = ichar(limloc(i:i))
        enddo
        nidat=MIDAT
        do i=1,nidat
           idat(i)=idatl(i)
        enddo
        nrdat=MRDAT
        do i=1,nrdat
           rdat(i)=rdatl(i)
        enddo
        return

 9999   ierr=1
      close(unit=neqdsk)
      nrdat=1
      nidat=1
      rdat(1)=0.0
      idat(1)=0
      return
c
 1040 format (1x,4e16.9)
 1041 format (1x,4i5)
 1050 format (1x,i5,11x,i5)
 1053 format (1x,i6,11x,i5)
 1055 format (1x,a10,2a5)
 1060 format (1x,f8.3,9x,i5,11x,i5,1x,a3,1x,i3,1x,i3,1x,a3,1x,2i5)

      end subroutine reada
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine writea(eqdsk,ierr,iform
     &         ,csilop, crogow, cmpr2, ccbrsp, eccurt
     &         ,rco2r, rco2v, chordv ,dco2r, dco2v
     &         ,rseps,zseps
     &         ,rdat,idat
     &         ,iqmflag,ilimloc
     & )
      parameter (nfcoil=18,nsilop=44,nrogow=1)
      parameter (magpri67=29,magpri322=31,magprirdp=8,magudom=5)
      parameter (magpri=73 )
      parameter (nesum=6)
      parameter (nco2v=3,nco2r=1)
c
      dimension csilop(nsilop),crogow(nrogow)
     &         ,cmpr2(magpri),ccbrsp(nfcoil)
     &         ,eccurt(nesum)
     &         ,rco2r(nco2r),rco2v(nco2v),chordv(nco2v)
     &         ,dco2r(nco2r),dco2v(nco2v)
     &         ,mfvers(2),rseps(2),zseps(2)
      character uday*10,mfvers*5
      character qmflag*3,limloc*4
      integer*1 iqmflag(4),ilimloc(5)
      character eqdsk*256
      data neqdsk/55/,xdum/0.0/

      parameter (MIDAT=12,MRDAT=121)
      dimension idatl(MIDAT),rdatl(MRDAT),rdat(1),idat(1)

        equivalence (idatl( 1),ishot),   (idatl( 2),jflag)   
     &            , (idatl( 3),lflag),   (idatl( 4),mco2v)
     &            , (idatl( 5),mco2r)   
     &            , (idatl( 6),nlold),   (idatl( 7),nlnew)
     &            , (idatl( 8),nsilop0), (idatl( 9),magpri0)
     &            , (idatl(10),nfcoil0), (idatl(11),nesum0)
     &            , (idatl(12),nvernum)

        equivalence                     (rdatl( 1),time)
     &            , (rdatl( 2),tsaisq), (rdatl( 3),rcencm)
     &            , (rdatl( 4),bcentr), (rdatl( 5),pasmat)
     &            , (rdatl( 6),cpasma), (rdatl( 7),rout) 
     &            , (rdatl( 8),zout),   (rdatl( 9),aout)
        equivalence (rdatl(10),eout),   (rdatl(11),doutu) 
     &            , (rdatl(12),doutl),  (rdatl(13),vout)
     &            , (rdatl(14),rcurrt), (rdatl(15),zcurrt) 
     &            , (rdatl(16),qsta),   (rdatl(17),betat)
     &            , (rdatl(18),betap),  (rdatl(19),ali) 
        equivalence (rdatl(20),oleft),  (rdatl(21),oright)
     &            , (rdatl(22),otop),   (rdatl(23),obott) 
     &            , (rdatl(24),qpsib),  (rdatl(25),vertn)
     &            , (rdatl(26),shearb), (rdatl(27),bpolav) 
     &            , (rdatl(28),s1),     (rdatl(29),s2)
        equivalence (rdatl(30),s3),     (rdatl(31),qout) 
     &            , (rdatl(32),olefs),  (rdatl(33),orighs)
     &            , (rdatl(34),otops),  (rdatl(35),sibdry) 
     &            , (rdatl(36),areao),  (rdatl(37),wplasm)
     &            , (rdatl(38),terror), (rdatl(39),elongm) 
        equivalence (rdatl(40),qqmagx), (rdatl(41),cdflux)
     &            , (rdatl(42),alpha),  (rdatl(43),rttt) 
     &            , (rdatl(44),psiref), (rdatl(45),xndnt)
     &            , (rdatl(46),sepexp), (rdatl(47),obots) 
     &            , (rdatl(48),btaxp),  (rdatl(49),btaxv)
        equivalence (rdatl(50),aaq1),   (rdatl(51),aaq2) 
     &            , (rdatl(52),aaq3),   (rdatl(53),seplim)
     &            , (rdatl(54),rmagx),  (rdatl(55),zmagx) 
     &            , (rdatl(56),simagx), (rdatl(57),taumhd)
     &            , (rdatl(58),betapd), (rdatl(59),betatd) 
        equivalence (rdatl(60),wplasmd),(rdatl(61),fluxx)
     &            , (rdatl(62),vloopt), (rdatl(63),taudia) 
     &            , (rdatl(64),qmerci), (rdatl(65),tavem)
     &            , (rdatl(66),pbinj),  (rdatl(67),rvsin)
     &            , (rdatl(68),zvsin),  (rdatl(69),rvsout)
        equivalence (rdatl(70),zvsout), (rdatl(71),vsurfa)
     &            , (rdatl(72),wpdot),  (rdatl(73),wbdot)
     &            , (rdatl(74),slantu), (rdatl(75),slantl)
     &            , (rdatl(76),zuperts),(rdatl(77),chipre)
     &            , (rdatl(78),cjor95), (rdatl(79),pp95)
        equivalence (rdatl(80),ssep),   (rdatl(81),yyy2)
     &            , (rdatl(82),xnnc),   (rdatl(83),cprof)
     &            , (rdatl(84),oring),  (rdatl(85),cjor0)
     &            , (rdatl(86),fexpan), (rdatl(87),qqmin)
     &            , (rdatl(88),chigamt),(rdatl(89),ssi01)
        equivalence (rdatl(90),fexpvs), (rdatl(91),sepnose)
     &            , (rdatl(92),ssi95),  (rdatl(93),rqqmin)
     &            , (rdatl(94),cjor99), (rdatl(95),cj1ave)
     &            , (rdatl(96),rmidin), (rdatl(97),rmidout)
     &            , (rdatl(98),psurfa), (rdatl(99),peak)
        equivalence (rdatl(100),dminux), (rdatl(101),dminlx)
     &            , (rdatl(102),dolubaf),(rdatl(103),dolubafm)
     &            , (rdatl(104),diludom),(rdatl(105),diludomm)
     &            , (rdatl(106),ratsol), (rdatl(107),rvsiu)
     &            , (rdatl(108),zvsiu),  (rdatl(109),rvsid)
        equivalence (rdatl(110),zvsid),  (rdatl(111),rvsou)
     &            , (rdatl(112),zvsou),  (rdatl(113),rvsod)
     &            , (rdatl(114),zvsod),  (rdatl(115),condno)
     &            , (rdatl(116),psin32), (rdatl(117),psin21)
     &            , (rdatl(118),rq32in), (rdatl(119),rq21top)
     &            , (rdatl(120),chilibt),(rdatl(121),ali3)
        
        nlold=35
        nlnew=39
        ktime=1
        uday = '24-Oct-10 '
        mfvers(1) = '01/21'
        mfvers(2) = '/2000'
        idum = 7
        xdum = 0.0

        do i=1,256
           if (ichar(eqdsk(i:i)) .eq. 0 ) then
              leqdsk=i-1
              goto 5433
           endif
        enddo

 5433   do i=1,3
           qmflag(i:i) = achar(iqmflag(i))
        enddo
        do i=1,4
           limloc(i:i) = achar(ilimloc(i))
        enddo
        nidat=MIDAT
        do i=1,nidat
           idatl(i)=idat(i)
        enddo
        nrdat=MRDAT
        do i=1,nrdat
           rdatl(i)=rdat(i)
        enddo

        if ( mco2v.eq.0  ) mco2v   = nco2v
        if ( mco2r.eq.0  ) mco2r   = nco2r
        if ( nsilop0.eq.0) nsilop0 = nsilop
        if ( nfcoil0.eq.0) nfcoil0 = nfcoil
        if ( nesum0.eq.0 ) nesum0  = nesum

        if (iform.eq.1) then
        open(unit=neqdsk,file=eqdsk(1:leqdsk)
     &        ,status='REPLACE',form='FORMATTED')
c
        write (neqdsk,1055) uday,(mfvers(j),j=1,2)
        if (ishot.le.99999) then
        write (neqdsk,1050) ishot,ktime
        else
        write (neqdsk,1053) ishot,ktime
        endif
        write (neqdsk,1040) time
        write (neqdsk,1060) time,jflag,lflag,limloc,
     .                     mco2v,mco2r,qmflag,nlold,nlnew
c        write (neqdsk,1060) time,jflag,lflag,limloc,
c     .                     mco2v,mco2r,qmflag
        write (neqdsk,1040) tsaisq,rcencm,bcentr,pasmat
        write (neqdsk,1040) cpasma,rout,zout,aout
        write (neqdsk,1040) eout,doutu,doutl,vout
        write (neqdsk,1040) rcurrt,zcurrt,qsta,betat
        write (neqdsk,1040) betap,ali,oleft,oright
        write (neqdsk,1040) otop,obott,qpsib,vertn
        write (neqdsk,1040) (rco2v(k),k=1,mco2v)
        write (neqdsk,1040) (dco2v(k),k=1,mco2v)
        write (neqdsk,1040) (rco2r(k),k=1,mco2r)
        write (neqdsk,1040) (dco2r(k),k=1,mco2r)
        write (neqdsk,1040) shearb,bpolav,s1,s2
        write (neqdsk,1040) s3,qout,olefs,orighs
        write (neqdsk,1040) otops,sibdry,areao,wplasm
        write (neqdsk,1040) terror,elongm,qqmagx,cdflux
        write (neqdsk,1040) alpha,rttt,psiref,xndnt
        write (neqdsk,1040) rseps(1),zseps(1),rseps(2),zseps(2)
        write (neqdsk,1040) sepexp,obots,btaxp,btaxv
        write (neqdsk,1040) aaq1,aaq2,aaq3,seplim
        write (neqdsk,1040) rmagx,zmagx,simagx,taumhd
        write (neqdsk,1040) betapd,betatd,wplasmd,fluxx
        write (neqdsk,1040) vloopt,taudia,qmerci,tavem
c
        if ( magpri0.eq.0) then
           if (ishot.lt.91000) then 
              magpri0 = magpri67+magpri322
           else
              magpri0 = magpri
           endif
        endif
        write (neqdsk,1041) nsilop0,magpri0,nfcoil0,nesum0
        write (neqdsk,1040) (csilop(k),k=1,nsilop0)
     &                     ,(cmpr2(k), k=1,magpri0)
        write (neqdsk,1040) (ccbrsp(k),k=1,nfcoil0)
        write (neqdsk,1040) (eccurt(k),k=1,nesum0)
c
        write (neqdsk,1040) pbinj,rvsin,zvsin,rvsout
        write (neqdsk,1040) zvsout,vsurfa,wpdot,wbdot
        write (neqdsk,1040) slantu,slantl,zuperts,chipre
        write (neqdsk,1040) cjor95,pp95,ssep,yyy2
        write (neqdsk,1040) xnnc,cprof,oring,cjor0
        write (neqdsk,1040) fexpan,qqmin,chigamt,ssi01
        write (neqdsk,1040) fexpvs,sepnose,ssi95,rqqmin
        write (neqdsk,1040) cjor99,cj1ave,rmidin,rmidout
        write (neqdsk,1040) psurfa,peak,dminux,dminlx
        write (neqdsk,1040) dolubaf,dolubafm,diludom,
     .                      diludomm
        write (neqdsk,1040) ratsol,rvsiu,zvsiu,rvsid
        write (neqdsk,1040) zvsid,rvsou,zvsou,rvsod
        write (neqdsk,1040) zvsod,condno,psin32,psin21
        write (neqdsk,1040) rq32in,rq21top,chilibt,ali3
c
c-------------------------------------------------------------------
c-- binary format                                                 --
c-------------------------------------------------------------------
        else
        open(unit=neqdsk,file=eqdsk(1:leqdsk)
     &       ,status='REPLACE',form='UNFORMATTED')
        write (neqdsk) uday,(mfvers(j),j=1,2)
        write (neqdsk) ishot,ktime
        write (neqdsk) time
        write (neqdsk) time,jflag,lflag,limloc,
     .                mco2v,mco2r,qmflag,nlold,nlnew
c        write (neqdsk) time,jflag,lflag,limloc,
c     .                mco2v,mco2r,qmflag
        write (neqdsk) tsaisq,rcencm,bcentr,pasmat
        write (neqdsk) cpasma,rout,zout,aout
        write (neqdsk) eout,doutu,doutl,vout
        write (neqdsk) rcurrt,zcurrt,qsta,betat
        write (neqdsk) betap,ali,oleft,oright
        write (neqdsk) otop,obott,qpsib,vertn
        write (neqdsk) (rco2v(k),k=1,mco2v)
        write (neqdsk) (dco2v(k),k=1,mco2v)
        write (neqdsk) (rco2r(k),k=1,mco2r)
        write (neqdsk) (dco2r(k),k=1,mco2r)
        write (neqdsk) shearb,bpolav,s1,s2
        write (neqdsk) s3,qout,olefs,orighs
        write (neqdsk) otops,sibdry,areao,wplasm
        write (neqdsk) terror,elongm,qqmagx,cdflux
        write (neqdsk) alpha,rttt,psiref,xndnt
        write (neqdsk) rseps(1),zseps(1),rseps(2)
     .                      ,zseps(2)
        write (neqdsk) sepexp,obots,btaxp,btaxv
        write (neqdsk) aaq1,aaq2,aaq3,seplim
        write (neqdsk) rmagx,zmagx,simagx,taumhd
        write (neqdsk) betapd,betatd,wplasmd,fluxx
        write (neqdsk) vloopt,taudia,qmerci,tavem
c
        if ( magpri0.eq.0) then
           if (ishot.lt.91000) then 
              magpri0 = magpri67+magpri322
           else
              magpri0 = magpri
           endif
        endif
        write (neqdsk) nsilop0,magpri0,nfcoil0,nesum0
        write (neqdsk) (csilop(k),k=1,nsilop0)
     &                ,(cmpr2(k), k=1,magpri0)
        write (neqdsk) (ccbrsp(k),k=1,nfcoil0)
        write (neqdsk) (eccurt(k),k=1,nesum0)
c
        write (neqdsk) pbinj,rvsin,zvsin,rvsout
        write (neqdsk) zvsout,vsurfa,wpdot,wbdot
        write (neqdsk) slantu,slantl,zuperts,chipre
        write (neqdsk) cjor95,pp95,ssep,yyy2
        write (neqdsk) xnnc,cprof,oring,cjor0
        write (neqdsk) fexpan,qqmin,chigamt,ssi01
        write (neqdsk) fexpvs,sepnose,ssi95,rqqmin
        write (neqdsk) cjor99,cj1ave,rmidin,rmidout
        write (neqdsk) psurfa,peak,dminux,dminlx
        write (neqdsk) dolubaf,dolubafm,diludom,
     .                      diludomm
        write (neqdsk) ratsol,rvsiu,zvsiu,rvsid
        write (neqdsk) zvsid,rvsou,zvsou,rvsod
        write (neqdsk) zvsod,condno,psin32,psin21
        write (neqdsk) rq32in,rq21top,chilibt,ali3
c
        endif

 916    continue
        ierr=0
        close(unit=neqdsk)
        return

 9999   ierr=1
      close(unit=neqdsk)
      return
c
 1040 format (1x,4e16.9)
 1041 format (1x,4i5)
 1050 format (1x,i5,11x,i5)
 1053 format (1x,i6,11x,i5)
 1055 format (1x,a10,2a5)
 1060 format (1h*,f8.3,9x,i5,11x,i5,1x,a3,1x,i3,1x,i3,1x,a3,1x,2i5)

      end subroutine writea
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
