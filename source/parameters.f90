! ====================================
! global parameters definitions
!
! maxn - maximum number of nodes
!
! l2dev_zero - when ||f1||^2 < l2dev_zero then
!		l2dev = ||f1 - f2||
!
! maxdev_zero - similar to l2dev_zero
!
! absp_zero - when |absp1| < absp_zero then
!		the absorved power accuracy requiremet is
!		|absp1 - absp2| < absp_zero_acc_coeff * acc_absp
!
! _increase, _decrease - coefficients used when changing n or xe
!
! min_step_change, max_step_change - min (max) coefficients for adaptive mesh building
! dev_neighbours - no of neighbour nodes for deviation calculation
!									 (local dev. is maximum dev. among all these nodes)
!
! if exp_init is true then the initial mesh density is 
!   n_init*( 1 + ... equidistant term
!   uhr_prmt_a/coll_freq*exp(-uhr_prmt_b**2*(x-x_uhr)**2) + ... gaussian in UHR position
!   pr_prmt_a*exp(-pr_prmt_b**2*(x-x_pr)**2)) ... gaussian in PR position
! ====================================

integer maxn
real(8) l2dev_zero, maxdev_zero, absp_zero, absp_zero_acc_coeff
real(8) coll_freq_decrease, n_increase, xe_increase, xe_decrease
real(8) min_step_change, max_step_change
integer dev_neighbours
parameter (maxn = 30000, l2dev_zero = 1.d-6, maxdev_zero = 1.d-6, absp_zero = 1.d-3, absp_zero_acc_coeff = 2.d0)
parameter (coll_freq_decrease = 0.5d0, n_increase = 1.5d0, xe_increase = 1.05d0, xe_decrease = 0.95d0)
parameter (min_step_change = 1.05d0, max_step_change = 1.8d0, dev_neighbours = 5)

logical exp_init
real(8) uhr_prmt_a,uhr_prmt_b,pr_prmt_a,pr_prmt_b
parameter (exp_init=.true.,uhr_prmt_a=1.5d-2,uhr_prmt_b=7.5d0,pr_prmt_a=4.d0,pr_prmt_b=3.d0)
