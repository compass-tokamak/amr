## @package antenna6
## Python module for antenna description.


import ConfigParser,math,sys,os,os.path,re
from numpy import *

## Construct an array of [toroidal_angle,poloidal_angle,E_eq_angle].
##
## \param toroidal_angles input from ebe6.cfg (string), structure dependent on angle_scan parameter
## \param poloidal_angles input from ebe6.cfg (string), structure dependent on angle_scan parameter
## \param angles_E_equatorial input from ebe6.cfg (string), structure dependent on angle_scan parameter
## \param angle_scan angle scan type, currently none, all, coupled
## \param n_tor_angles number of toroidal angles (for angle scans)
## \param n_pol_angles number of poloidal angles (for angle scans)
## \param n_E_eq_angles number of E_eq angles (for angle scans)
## \return array of [toroidal_angle,poloidal_angle,E_eq_angle] triplets (in degrees)
def constructAngles(toroidal_angles, \
                    poloidal_angles, \
                    angles_E_equatorial, \
                    angle_scan, \
                    n_tor_angles, \
                    n_pol_angles, \
                    n_E_eq_angles):

    tor_angles = array(toroidal_angles.split(),dtype=float_)
    pol_angles = array(poloidal_angles.split(),dtype=float_)
    E_eq_angles = array(angles_E_equatorial.split(),dtype=float_)
    n_tor_angles = int(n_tor_angles)
    n_pol_angles = int(n_pol_angles)
    n_E_eq_angles= int(n_E_eq_angles)

    if angle_scan.lower() == 'none':
        angles = [[tor_angles[0],pol_angles[0],E_eq_angles[0]]]
    elif angle_scan.lower() == 'all':
        if ((len(tor_angles)!=2) & (not((len(tor_angles)==1) & (n_tor_angles==1)))) | \
           ((len(pol_angles)!=2) & (not((len(pol_angles)==1) & (n_pol_angles==1)))) | \
           ((len(E_eq_angles)!=2) & (not((len(E_eq_angles)==1) & (n_E_eq_angles==1)))):
            print "Error: wrong antenna angles specification"
            sys.exit(-2)
        if (n_tor_angles<1) | (n_pol_angles<1) | (n_E_eq_angles<1):
            print "Error: wrong antenna angles specification"
            sys.exit(-2)
        angles = []
        for i_tor in range(n_tor_angles):
            if n_tor_angles>1:
                phi_tor = tor_angles[0] + i_tor * (tor_angles[1]-tor_angles[0])/(n_tor_angles-1)
            else:
                phi_tor = tor_angles[0]
            for i_pol in range(n_pol_angles):
                if n_pol_angles>1:
                    phi_pol = pol_angles[0] + i_pol * (pol_angles[1]-pol_angles[0])/(n_pol_angles-1)
                else:
                    phi_pol = pol_angles[0]
                for i_E_eq in range(n_E_eq_angles):
                    if n_E_eq_angles>1:
                        phi_E_eq = E_eq_angles[0] + i_E_eq * (E_eq_angles[1]-E_eq_angles[0])/(n_E_eq_angles-1)
                    else:
                        phi_E_eq = E_eq_angles[0]
                    angles.append([phi_tor,phi_pol,phi_E_eq])
    elif angle_scan.lower() == 'coupled':
        if (len(tor_angles)!=len(pol_angles)) or (len(tor_angles)!=len(E_eq_angles)):
            print "Error: number of antenna angles must be identical for coupled scan"
            sys.exit(-2)
        angles = [[tor,pol,E_eq] for [tor,pol,E_eq] in zip(tor_angles,pol_angles,E_eq_angles)]
    else:
        print 'Error: angle_scan = ' + angle_scan + ' not implemented'
        sys.exit(-2)
    return array(angles)

## Window description
class window:
    def __init__(self):
            ## Window type, presently only elliptic. Warning, no orientation of the axis implemented yet!
            ## Therefore, only circular windows are interpreted correctly (a2b must be 1)
            self.type = ''
            ## Major axis length
            self.a = 0.0
            ## Major to minor axis ratio
            self.a2b = 0.0
            ## Window center (x,y,z)
            self.center = [0.0,0.0,0.0]
            ## Window normal vector (nx,ny,nz)
            self.normal = [0.0,0.0,0.0]

## Contains antenna parameters.
class antenna:
    
    def __init__(self,id,amr_cfg):
        # id - id anteny
        # cfg_file - jmeno konfig. souboru
        # experiment - nazev experimentu (MAST, NSTX, WEGA ...)
        self.amr_cfg = amr_cfg
        cfg_file = amr_cfg['antenna']['cfg_file']
        experiment = amr_cfg['input']['experiment']
        self.id = id
        self.experiment = str.strip(str(experiment))
        self.cfg_file = str.strip(str(cfg_file))
        self.read_cfg_file(self.cfg_file)

    ## Read configuration file, initialize members
    ## \param cfg_file config file name
    def read_cfg_file(self,cfg_file):
        # first read and interpret common antenna elements
        my_conf = ConfigParser.SafeConfigParser()
        my_conf.read([cfg_file])
        sid = self.id
        self.my_conf = my_conf
        self.model = my_conf.get(sid,'model')
        pow_ar = my_conf.get(sid,'power').lower().split()
        # kW default
        if len(pow_ar)==1:
            pow_ar.append('kw')
        if pow_ar[1]=='w':
            self.power = 1e-3*float(pow_ar[0])
        elif pow_ar[1]=='kw':
            self.power = float(pow_ar[0])
        elif pow_ar[1]=='mw':
            self.power = 1e3*float(pow_ar[0])
        else:
            print 'error: antenna ' + sid + ' power (" ' + my_conf.get(sid,'power') + '") wrong'
            sys.exit(-2)
        self.angles = constructAngles(my_conf.get(sid,'toroidal_angles'), \
                                      my_conf.get(sid,'poloidal_angles'), \
                                      my_conf.get(sid,'angles_E_equatorial'), \
                                      my_conf.get(sid,'angle_scan'), \
                                      my_conf.get(sid,'n_tor_angles'), \
                                      my_conf.get(sid,'n_pol_angles'), \
                                      my_conf.get(sid,'n_E_eq_angles'))
        self.polarization = my_conf.get(sid,'polarization').lower()
        self.frequencies = array(my_conf.get(sid,'frequencies').split(),dtype=float_)
        if self.polarization == 'e_cartesian':
            self.E_x = complex(re.sub(r'\s','',my_conf.get(sid,'E_x')))
            self.E_y = complex(re.sub(r'\s','',my_conf.get(sid,'E_y')))
            self.E_z = complex(re.sub(r'\s','',my_conf.get(sid,'E_z')))
        elif self.polarization == 'e_rotating':
            self.E_p = complex(re.sub(r'\s','',my_conf.get(sid,'E_p')))
            self.E_m = complex(re.sub(r'\s','',my_conf.get(sid,'E_m')))
        self.beamWaists = constructBeamWaists(self,self.frequencies,self.angles[:,0],self.angles[:,1],self.angles[:,2])
        self.hasWindow = my_conf.getboolean(sid,'vacuum_window')
        self.n_window_points = my_conf.getint('general','n_window_points')
        if self.hasWindow:
            self.initWindow()

    ## Initialize window connected with this antenna. 
    def initWindow(self):
        # ...
        # input for waist data window computation (or separate routine?)
        print 'warning: incomplete initWindow()'
        self.window = window()
        if self.my_conf.get(self.id,'window_type').lower()=="circular":
            self.window.type = 'elliptic'
            self.window.a = self.my_conf.getfloat(self.id,'window_radius')
            # major to minor axis ratio
            self.window.a2b = 1.0
            self.window.center = [self.my_conf.getfloat(self.id,'window_x_center'), \
                                  self.my_conf.getfloat(self.id,'window_y_center'), \
                                  self.my_conf.getfloat(self.id,'window_z_center')]
            self.window.normal = [self.my_conf.getfloat(self.id,'window_nx'), \
                                  self.my_conf.getfloat(self.id,'window_ny'), \
                                  self.my_conf.getfloat(self.id,'window_nz')]
        else:
            print 'error: window type unknown (' + self.my_conf.get(self.id,'window_type') + ')'
            
    def getWindow(self):
        if self.hasWindow:
            return self.window
        else:
            return False

    def getWindowParameters(self):
        # ...
        # returns n_window, (x,y,z) of window points (single array)
        # or (better?) n_window, type, parameters
        print 'warning: incomplete getWindowParameters()'

    ## Calculate total number of antenna configurations (concerning angles, frequencies).
    def getNumberOfConfigurations(self):
        return len(self.angles)*len(self.frequencies)

    ## Return angles for particular configuration.
    ## \param iconf configuration identification number
    ## \return [toroidal_angle,poloidal_angle,E_eq_angle] triplet (in degrees)
    def getAngles(self,iconf):
        return self.angles[self.iAngle(iconf)]
    
    def iAngle(self,iconf):
        return iconf % len(self.angles)
    
    def iFreq(self,iconf):
        return iconf // len(self.angles)

    def iConf(self,iFreq,iAngle):
        return iFreq*len(self.angles)+iAngle

    def getFrequencies(self):
        return self.frequencies

    ## Return beam waist particular configuration.
    ## \param iconf configuration identification number
    ## \return GaussBeam instance
    def getBeamWaist(self,iconf):
        # iconf = 0 .. getNumberOfConfigurations
        i_freq = self.iFreq(iconf)
        i_angle= self.iAngle(iconf)
        return self.beamWaists[i_freq][i_angle]
        # return constructBeamWaist(self,self.frequencies[i_freq],self.angles[i_angle][0],self.angles[i_angle][1],self.angles[i_angle][2])

    ## Total number of rays (summed for all configurations).
    ## \return total number of rays
    def getNumberOfRays(self):
        nRays = 0
        for iconf in range(self.getNumberOfConfigurations()):
            nRays = nRays + self.getBeamWaist(iconf).getNumberOfRays()
        return nRays

    def dump(self,screen=True):
        text  = "id: " + str(self.id) + "\n"
        text += "experiment: " + self.experiment + "\n"
        text += "model: " + self.model + "\n"
        text += "angles [toroidal,poloida,E_eq]: " + str(self.angles) + "\n"
        text += "polarization: " + self.polarization + "\n"
        text += "frequencies [GHz]: " + str(self.frequencies) + "\n"
        text += "number of configurations: " + str(self.getNumberOfConfigurations()) + "\n"
        if screen:
            print text
        return text

## Contains modified Gaussian beam parameters
class GaussBeam:
    "Modified Gaussian beam parameters"

    def __init__(self, \
                 freq = 0.0, \
                 power = 1.0, \
                 x_center = 0.0, \
                 y_center = 0.0, \
                 z_center = 0.0, \
                 Nx = 0.0, \
                 Ny = 0.0, \
                 Nz = 0.0, \
                 Ex = 0.0, \
                 Ey = 0.0, \
                 Ez = 0.0, \
                 transm_coeff = 1.0, \
                 waist_radius = 0.0, \
                 fix_divergence = False, \
                 fixed_divergence = 0.0, \
                 gauss_assymetry = 0.0, \
                 gauss_radius_enlargement = 1.0, \
                 gauss_width = 1.0,\
                 noOfCircles = 5,\
                 raysPerCircle = 8, \
                 turnCircles = False):
        self.x_center = x_center
        self.y_center = y_center
        self.z_center = z_center
        self.Nx = Nx
        self.Ny = Ny
        self.Nz = Nz
        self.Ex = Ex
        self.Ey = Ey
        self.Ez = Ez
        self.transm_coeff = transm_coeff
        self.waist_radius = waist_radius
        self.fix_divergence = fix_divergence
        self.fixed_divergence = fixed_divergence
        self.gauss_assymetry = gauss_assymetry
        self.gauss_radius_enlargement = gauss_radius_enlargement
        self.gauss_width = gauss_width
        self.noOfCircles = noOfCircles
        self.raysPerCircle = raysPerCircle
        self.turnCircles = turnCircles

    def dump(self):
        print "Modified Gaussian beam parameters:"
        print "frequency: " + str(self.freq) + " GHz"
        print "x_center = " + str(self.x_center) + " m"
        print "y_center = " + str(self.y_center) + " m"
        print "z_center = " + str(self.z_center) + " m"
        print "Nx = " + str(self.Nx)
        print "Ny = " + str(self.Ny)
        print "Nz = " + str(self.Nz)
        print "Ex = " + str(self.Ex)
        print "Ey = " + str(self.Ey)
        print "Ez = " + str(self.Ez)
        print "transm_coeff = " + str(self.transm_coeff)
        print "waist_radius = " + str(self.waist_radius) + " m"
        print "fix_divergence = " + str(self.fix_divergence)
        print "fixed_divergence = " + str(self.fixed_divergence) + "deg"
        print "gauss_assymetry = " + str(self.gauss_assymetry)
        print "gauss_radius_enlargement = " + str(self.gauss_radius_enlargement)
        print "gauss_width = " + str(self.gauss_width)

    def getNumberOfRays(self):
        return self.noOfCircles*self.raysPerCircle+1

## Calculate waist beam parameters dependent on the antenna model and configuration
## \param ant Antenna description in antenna instance
## \param freq frequency in GHz
## \param toroidal_angle toroidal angle
## \param poloidal_angle poloidal angle
## \param E_eq_angle E_eq angle
## \return Gaussian beam waist parameters contained in GaussBeam instance
def constructBeamWaists(ant,freqs,toroidal_angles,poloidal_angles,E_eq_angles):
    if not iterable(freqs): freqs = (freqs,)
    if not iterable(toroidal_angles): freqs = (toroidal_angles,)
    if not iterable(poloidal_angles): freqs = (poloidal_angles,)
    if not iterable(E_eq_angles): freqs = (E_eq_angles,)
    if not len(toroidal_angles)==len(poloidal_angles)==len(E_eq_angles):
        print('error: angles must have equal lengths in constructBeamWaist')
        return None
    sid = ant.id
    my_conf = ant.my_conf
    waistBeams = []

    if (ant.model.lower() == 'tcv'):
        import subprocess as sp
        launcher_no = my_conf.getint(sid,'launcher_no')
        theta_l = array_str(array(poloidal_angles),max_line_width=1e20,precision=12)
        phi_l = array_str(array(toroidal_angles),max_line_width=1e20,precision=12)
        if ant.amr_cfg['general']['m_interpreter'].lower()=='octave':
            cmd = 'octave --path ./maple_matlab --eval "tcv_beam(%s,%s,%i)"' % (theta_l,phi_l,launcher_no)
        elif ant.amr_cfg['general']['m_interpreter'].lower()=='matlab':
            # cmd = 'matlab -nodisplay -r "tcv_beam(%s,%s,%i)"' % (theta_l,phi_l,launcher_no)
            if not os.getenv('MATLABPATH'):
                os.environ['MATLABPATH']=''
            else:
                os.environ['MATLABPATH']=os.pathsep+os.environ['MATLABPATH']
            os.environ['MATLABPATH']=os.path.join(os.getcwd(),'maple_matlab')+os.environ['MATLABPATH']
            cmd = '/usr/local/bin/matlab76 -nodisplay -r "tcv_beam(%s,%s,%i)"' % (theta_l,phi_l,launcher_no)
        else:
            print 'error: unknown m-interpreter'
            sys.exit(2)
        print('TCV antenna function call: '+cmd)
        pipe = sp.Popen(cmd, shell=True, stdout=sp.PIPE).stdout
        tcv_beam_params = []
        for res in pipe: 
            if 'TCV_Beam:' in res:
                res2 = res.split()
                ires = res2.index('TCV_Beam:')+1
                tcv_beam_params.append(res2[ires:])
        pipe.close()
        tcv_beam_params = array(tcv_beam_params,dtype=float_)
    
    for iFreq,freq in enumerate(freqs):
        waistBeams.append([])
        for iAngle,(toroidal_angle,poloidal_angle,E_eq_angle) in enumerate(zip(toroidal_angles,poloidal_angles,E_eq_angles)):
            waistBeam = GaussBeam()
            waistBeam.freq = freq
            waistBeam.power = ant.power
            waistBeam.noOfCircles = my_conf.getint('general','noOfCircles')
            waistBeam.raysPerCircle = my_conf.getint('general','raysPerCircle')
            waistBeam.turnCircles = my_conf.getboolean('general','turnCircles')
            # standard Gaussian antenna
            if (ant.model.lower() == 'gaussian'):
                # check for cartesian/cylindrical coordinates
                if my_conf.has_option(sid,'x_waist'): 
                    waistBeam.x_center = my_conf.getfloat(sid,'x_waist')
                    waistBeam.y_center = my_conf.getfloat(sid,'y_waist')
                    waistBeam.z_center = my_conf.getfloat(sid,'z_waist')
                else:
                    R =   my_conf.getfloat(sid,'R_waist')
                    phi = my_conf.getfloat(sid,'phi_waist')
                    Z =   my_conf.getfloat(sid,'Z_waist')
                    waistBeam.x_center = R*cosd(phi)
                    waistBeam.y_center = R*sind(phi)
                    waistBeam.z_center = Z
                # angle to wave vector conversion
                phi_tor = math.degrees(math.atan2(waistBeam.y_center,waistBeam.x_center))
                N_R = -cosd(toroidal_angle)
                N_phi = sind(toroidal_angle)
                waistBeam.Nx = cosd(poloidal_angle) * ( N_R*cosd(phi_tor) - N_phi*sind(phi_tor) )
                waistBeam.Ny = cosd(poloidal_angle) * ( N_R*sind(phi_tor) + N_phi*cosd(phi_tor) )
                waistBeam.Nz = sind(poloidal_angle)
                if ant.polarization == 'e_cartesian':
                    waistBeam.Ex = ant.E_x
                    waistBeam.Ey = ant.E_y
                    waistBeam.Ez = ant.E_z
                    waistBeam.transm_coeff = 1
                elif ant.polarization == 'e_rotating':
                    # (x1,y1,z1)-coordinates: z1 || N, x1 perp. to Z-axis
                    Ex1 = ant.E_p + ant.E_m
                    Ey1 = complex(0,1)*(ant.E_p - ant.E_m)
                    x1 = cross(array((waistBeam.Nx,waistBeam.Ny,waistBeam.Nz)),array((0,0,1)))
                    x1 = x1/dot(x1,x1)
                    y1 = cross(x1,array((waistBeam.Nx,waistBeam.Ny,waistBeam.Nz)))
                    waistBeam.Ex = x1[0]*Ex1 + y1[0]*Ey1
                    waistBeam.Ey = x1[1]*Ex1 + y1[1]*Ey1
                    waistBeam.Ez = x1[2]*Ex1 + y1[2]*Ey1
                    waistBeam.transm_coeff = 1
                else:
                    [waistBeam.Ex,waistBeam.Ey,waistBeam.Ez,waistBeam.transm_coeff] = \
                        E_transmitted_plain(waistBeam.Nx,waistBeam.Ny,waistBeam.Nz,E_eq_angle,freq)
                # check for rayleigh_rangeparamater
                if my_conf.has_option(sid,'rayleigh_range'):
                    waistBeam.waist_radius = 9768.669469*math.sqrt(my_conf.getfloat(sid,'rayleigh_range') / (freq*1e9)) 
                    waistBeam.fix_divergence = False
                # enter the waist radius
                else:
                    waistBeam.waist_radius = my_conf.getfloat(sid,'waist_radius')
                    if my_conf.has_option(sid,'fix_divergence'):
                        waistBeam.fix_divergence = my_conf.getboolean(sid,'fix_divergence')
                    else:
                        waistBeam.fix_divergence = False
                    if waistBeam.fix_divergence:
                        waistBeam.fixed_divergence = my_conf.getfloat(sid,'fixed_divergence')
                if my_conf.has_option(sid,'gauss_assymetry'):
                    waistBeam.gauss_assymetry = my_conf.getfloat(sid,'gauss_assymetry')
                else:
                    waistBeam.gauss_assymetry = 0.0
                if my_conf.has_option(sid,'gauss_radius_enlargement'): 
                    waistBeam.gauss_radius_enlargement = my_conf.getfloat(sid,'gauss_radius_enlargement')
                else:
                    waistBeam.gauss_radius_enlargement = 1.0
                if my_conf.has_option(sid,'gauss_width'): 
                    waistBeam.gauss_width = my_conf.getfloat(sid,'gauss_width')
                else:
                    waistBeam.gauss_width = 1.0
        
            elif ((ant.model.lower() == 'nstx_2006_hf') | (ant.model.lower() == 'nstx_2006_lf')):
                vc = 2.9979e8
                if (ant.model.lower() == 'nstx_2006_hf'):
                    a_freq_exp = 1.0e9*28
                    a_xR_W_center = 1.884
                    a_Z_W_center = -0.190000000
                    a_Ytor_W_center = 0.
                    a_w_exp =0.035
                    a_L_lens_W_center=0.037
                    a_L_exp_W_center_waist =0.14
                    a_f_lens =0.125
                elif (ant.model.lower() == 'nstx_2006_lf'):
                    a_freq_exp = 1.0e9*16.5
                    a_xR_W_center = 1.884
                    a_Z_W_center = 0.190000000
                    a_Ytor_W_center = 0.
                    a_w_exp =0.022
                    a_L_lens_W_center=0.0787
                    a_L_exp_W_center_waist =0.1313
                    a_f_lens =0.1
                
                phi_tor = math.degrees(math.atan2(a_Ytor_W_center,a_xR_W_center))
                N_R = -cosd(toroidal_angle)
                N_phi = sind(toroidal_angle)
                waistBeam.Nx = cosd(poloidal_angle) * ( N_R*cosd(phi_tor) - N_phi*sind(phi_tor) )
                waistBeam.Ny = cosd(poloidal_angle) * ( N_R*sind(phi_tor) + N_phi*cosd(phi_tor) )
                waistBeam.Nz = sind(poloidal_angle)
                
                N_tor=   waistBeam.Ny 
                N_Z  =   waistBeam.Nz
                N_xR = - waistBeam.Nx
        
                a_L_exp=a_L_exp_W_center_waist+a_L_lens_W_center            
                lambda_exp=vc/a_freq_exp            
                ah=a_L_exp/a_f_lens-1.0
                bh=(math.pi*a_w_exp**2/(lambda_exp*a_f_lens))**2
                # print '2006_antenna,freq,lense_waist_dist,a_w_exp',a_freq_exp/1.0e9,a_L_exp,a_w_exp
                den=ah*ah+bh
                xh=ah/den
                yh=1.0/den
                # print 'ah,bh,xh,yh',ah,bh,xh,yh
                L10=(1.0+xh)*a_f_lens
                w10=a_w_exp*math.sqrt(yh)
                # print 'control GB',ah-xh/(xh*xh+bh/(ah*ah+bh)**2)
                # print ' w1,L1 -28GHz',w10,L10
                lambd = vc/(freq*1e9)
                b1 = (math.pi*w10**2/(lambd*a_f_lens))**2
                a1 = L10/a_f_lens-1.0
                den = a1*a1+b1
                L2 = a_f_lens*(1.0+a1/den)
                w2 = w10*math.sqrt(1.0/den)
                # print'freq,lens-waist distance L2, waist radius w2',freq/1.0e9,L2,w2
                t = L2-a_L_lens_W_center
                xR = a_xR_W_center-abs(N_xR)*t
                Y = a_Ytor_W_center+N_tor*t
                Z = a_Z_W_center+N_Z*t
                Ang_div=0
                # print freq,IRealTest,xR,Z,Y,w2,L2,w10,L10 !!!nezmeneno poradi Z,Y na Y,Z a w2 je primo v [m]!!!
                # return [a_IRealTest,xR,Z,Y,w2,Ang_div,a_xR_W_center,a_Z_W_center,L2,w10,L10]
                waistBeam.x_center = xR
                waistBeam.y_center = Y
                waistBeam.z_center = Z
                [waistBeam.Ex,waistBeam.Ey,waistBeam.Ez,waistBeam.transm_coeff] = \
                    E_transmitted_plain(waistBeam.Nx,waistBeam.Ny,waistBeam.Nz,E_eq_angle,freq)
                waistBeam.waist_radius = w2
                waistBeam.fix_divergence = False
                waistBeam.fixed_divergence = 0.0
                waistBeam.gauss_assymetry = 0.0
                waistBeam.gauss_radius_enlargement = 1.0
                waistBeam.gauss_width = 1.0
            
            elif ((ant.model.lower() == 'nstx_2007_hf') | (ant.model.lower() == 'nstx_2007_lf')):
                vc = 2.9979e8
                if (ant.model.lower() == 'nstx_2007_hf'):
                    # 28GHz spot 50cm from window diameter 10cm => w_exp=0.05 at L_exp_W_center_waist=0.57
                    # we artifiatialy shift waist out of plasma so L_exp_W_center_waist=0.10
                    phi_tor = math.degrees(math.atan2(a_Ytor_W_center,a_xR_W_center))
                    N_R = -cosd(toroidal_angle)
                    N_phi = sind(toroidal_angle)
                    waistBeam.Nx = cosd(poloidal_angle) * ( N_R*cosd(phi_tor) - N_phi*sind(phi_tor) )
                    waistBeam.Ny = cosd(poloidal_angle) * ( N_R*sind(phi_tor) + N_phi*cosd(phi_tor) )
                    waistBeam.Nz = sind(poloidal_angle)
                    N_tor=   waistBeam.Ny 
                    N_Z  =   waistBeam.Nz
                    N_xR = - waistBeam.Nx
                    a_xR_W_center = 1.884
                    a_Z_W_center = -0.190000000
                    a_Ytor_W_center = 0.
                    L_exp_W_center_waist=0.10
                    t=L_exp_W_center_waist
                    xR = a_xR_W_center-abs(N_xR)*t
                    Y = a_Ytor_W_center+N_tor*t
                    Z = a_Z_W_center+N_Z*t
                    w2=0.043
                    Ang_div=math.pi/180
                    L2=0
                    w10=0
                    L10=0
                    # print 'antenna_2007-HFband,freq,window_waist_dist,a_w_exp',freq/1.0e9,L_exp_W_center_waist,w2
                    # return [a_IRealTest,xR,Z,Y,w2,Ang_div,a_xR_W_center,a_Z_W_center,L2,w10,L10]
                elif (ant.model.lower() == 'nstx_2007_lf'):
                    # 15GHz spot 50cm from window diameter 14cm => w_exp=0.06 at L_exp_W_center_waist=0.17
                    a_xR_W_center = 1.884
                    a_Z_W_center = 0.190000000
                    a_Ytor_W_center = 0.
                    L_exp_W_center_waist=0.17
                    t=L_exp_W_center_waist
                    xR = a_xR_W_center-abs(N_xR)*t
                    Y = a_Ytor_W_center+N_tor*t
                    Z = a_Z_W_center+N_Z*t
                    w2=0.06
                    Ang_div=math.atan(1./(50-17))
                    L2=0
                    w10=0
                    L10=0
                    # print 'antenna_2007_LFband,freq,window_waist_dist,a_w_exp',freq/1.0e9,L_exp_W_center_waist,w2
                    # print ' ang_div=',Ang_div
                    # return [a_IRealTest,xR,Z,Y,w2,Ang_div,a_xR_W_center,a_Z_W_center,L2,w10,L10]
                waistBeam.x_center = xR
                waistBeam.y_center = Y
                waistBeam.z_center = Z
                [waistBeam.Ex,waistBeam.Ey,waistBeam.Ez,waistBeam.transm_coeff] = \
                    E_transmitted_plain(waistBeam.Nx,waistBeam.Ny,waistBeam.Nz,E_eq_angle,freq)
                waistBeam.waist_radius = w2
                waistBeam.fix_divergence = True
                waistBeam.fixed_divergence = Ang_div
                waistBeam.gauss_assymetry = 0.0
                waistBeam.gauss_radius_enlargement = 1.0
                waistBeam.gauss_width = 1.0
        
            elif (ant.model.lower() == 'double_mirror'):
                import pyAMR.antenna_mast as antenna_mast
                x_mirror = my_conf.getfloat(sid,'x_mirror')
                y_mirror = my_conf.getfloat(sid,'y_mirror')
                z_mirror = my_conf.getfloat(sid,'z_mirror')
                [x_waist,Y_waist,Z_waist,Nx_waist,Ny_waist,Nz_waist, \
                 EX_transm,EY_transm,EZ_transm,w02M,Transm_coeff] = \
                 antenna_mast.waist_position(freq*1e9,poloidal_angle,toroidal_angle,E_eq_angle,x_mirror,y_mirror,z_mirror)
                waistBeam.x_center = x_waist
                waistBeam.y_center = Y_waist
                waistBeam.z_center = Z_waist
                waistBeam.Nx = Nx_waist
                waistBeam.Ny = Ny_waist
                waistBeam.Nz = Nz_waist
                waistBeam.Ex = EX_transm
                waistBeam.Ey = EY_transm
                waistBeam.Ez = EZ_transm
                waistBeam.transm_coeff = Transm_coeff
                waistBeam.waist_radius = w02M
                waistBeam.fix_divergence = False
                # waistBeam.dump()
        
            elif (ant.model.lower() == 'tcv'):
                res2f = tcv_beam_params[ant.iConf(iFreq,iAngle)]
                waistBeam.x_center = res2f[0]
                waistBeam.y_center = res2f[1]
                waistBeam.z_center = res2f[2]
                waistBeam.waist_radius = res2f[3]
                waistBeam.Nx = res2f[4]
                waistBeam.Ny = res2f[5]
                waistBeam.Nz = res2f[6]
                # calculate the polarization (arbitrary, although perpendicular to the wave vector)
                [waistBeam.Ex,waistBeam.Ey,waistBeam.Ez,waistBeam.transm_coeff] = \
                    E_transmitted_plain(waistBeam.Nx,waistBeam.Ny,waistBeam.Nz,E_eq_angle,freq)
                waistBeam.fix_divergence = False
        
            # model not implemented
            else:
                print "Error in constructBeamWaist: experimetnt and/or model not implemented"
                sys.exit(-2)
            waistBeams[-1].append(waistBeam)
    return waistBeams

def E_transmitted_plain(N_xR0_in,N_tor0,N_Z0,angleE_eq,freq):
    N_xR0 = -N_xR0_in
    transm_coeff = 1.0
    Ez_transm=sind(angleE_eq)
    aa=N_tor0**2+N_xR0**2
    bb=-N_Z0*N_xR0*Ez_transm
    cc=Ez_transm**2*N_Z0**2-cosd(angleE_eq)**2*N_tor0**2
    dd=bb*bb-aa*cc
    if abs(dd) < 1e-15:
        dd = 0.0
    Ex_transm=(-bb+math.sqrt(dd))/aa
    if N_tor0==0:
        Ey_transm = math.sqrt(1.0-Ez_transm**2-Ex_transm**2)
    else:
        Ey_transm=(-Ez_transm*N_Z0+Ex_transm*N_xR0)/N_tor0
    return [complex(Ex_transm),complex(Ey_transm),complex(Ez_transm),transm_coeff]

# sinus, cosinus, phi in degrees
def sind(phi):
    return math.sin(math.radians(phi))
def cosd(phi):
    return math.cos(math.radians(phi))

# --- E_transmitted auxiliary functions ---
def fE1_tor(cg, cg0, cg1, cg2, cg3, cg4):
    return math.sqrt(0.1e1 / (cg ** 2 * cg0 ** 2 + cg1 ** 2 * cg2 ** 2 - \
    2 * cg3 * cg1 * cg2 * cg0 + cg3 ** 2 * cg0 ** 2 - 2 * cg * cg1 * \
    cg4 * cg0 + cg1 ** 2 * cg4 ** 2 + cg ** 2 * cg2 ** 2 - 2 * cg * cg2 \
    * cg4 * cg3 + cg4 ** 2 * cg3 ** 2)) *(-cg * cg2 + cg4 * cg3)
def fE1_Z(cg, cg0, cg1, cg2, cg3, cg4):
    return -math.sqrt(0.1e1 / (cg ** 2 * cg0 ** 2 + cg1 ** 2 * cg2 ** 2 \
    - 2 * cg3 * cg1 * cg2 * cg0 + cg3 ** 2 * cg0 ** 2 - 2 * cg * cg1 * \
    cg4 * cg0 + cg1 ** 2 * cg4 ** 2 + cg ** 2 * cg2 ** 2 - 2 * cg * cg2 \
    * cg4 * cg3 + cg4 ** 2 * cg3 ** 2)) *(-cg1 * cg2 + cg0 * cg3) 
def fE1_R(cg, cg0, cg1, cg2, cg3, cg4):
    return math.sqrt(0.1e1 / (cg ** 2 * cg0 ** 2 + cg1 ** 2 * cg2 ** 2 - \
    2 * cg3 * cg1 * cg2 * cg0 + cg3 ** 2 * cg0 ** 2 - 2 * cg * cg1 * \
    cg4 * cg0 + cg1 ** 2 * cg4 ** 2 + cg ** 2 * cg2 ** 2 - 2 * cg * cg2 \
    * cg4 * cg3 + cg4 ** 2 * cg3 ** 2)) * (-cg1 * cg4 + cg * cg0) 
# --- end E_transmitted auxiliary functions ---


##ants = []
##ants.append(antenna("1","antenna_WEGA.cfg","WEGA"))
### ants[0].dump()
### print "--- Beam waist 0 ---"
### ants[0].getBeamWaist(0).dump()
##print "Total rays " + str(ants[0].getNumberOfRays())
##for ic in range(ants[0].getNumberOfConfigurations()):
##    print "Configuration " + str(ic)
##    print "Rays " + str(ants[0].getBeamWaist(ic).getNumberOfRays())
##print "--------------------"
##print "--------------------"

